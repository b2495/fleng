#!/bin/sh
# configuration script for FLENG bootstrap compiler

prolog=
target=

usage () {
    cat >&2 << EOF
usage: $0 [OPTION ...]

    --help                  show this message.
    --with-prolog=PROLOG    select what Prolog implementation should
                            be used, use either "swi", "yap" or "gnu".
    --target=TARGET         specifies target architecture
EOF
    exit $1
}

while [ -n "$1" ]; do
    case "$1" in
        --with-prolog)
            shift
            prolog="$1";;
        --with-prolog=*)
            prolog=$(echo "$1" | awk -F= '{print $2}' -);;
        --target)
            shift
            arch="$1";;
        --target=*)
            arch=$(echo "$1" | awk -F= '{print $2}' -);;
        -h|-help|--help)
            usage 0
            exit;;
        *) usage 1;;
    esac
    shift
done

arch0=$(echo "$arch" | sed 's/[bm]$//')

printf "checking operating system ... "
os="$(uname)"
flavor=
case "$os" in
    OpenBSD)
        if [ "$(uname -r)" '>' 7.3 ]; then
            flavor=b
        fi;;
    Darwin)
        flavor=m;;
esac
echo $os

if [ -z "$arch" ]; then
    printf "checking architecture ... "
    arch="$(uname -m)"
    case "$arch" in
        amd64|x86*)
            arch=x64;;
        arm64)
    	arch=a64;;
        arm*|evbarm)
            arch=a32;;
        aarch64)
            arch=a64;;
        riscv64)
            arch=r64;;
        *)
            echo " unsupported architecture: $arch"
            exit 1;;
    esac
    arch0=$arch
    arch="$arch$flavor"
    echo $arch
fi

if [ -z "$prolog" ]; then
    printf "searching for GNU Prolog ... "
    pc="$(command -v gplc)"
    if [ -n "$pc" ]; then
        prolog=gnu
        echo $pc
    else
        echo "not found"
        printf "searching for SWI Prolog ... "
        pc="$(command -v swipl)"
        if [ -n "$pc" ]; then
            prolog=swi
            echo $pc
        else
            echo "not found"
            printf "searching for YAP ... "
            pc="$(command -v yap)"
            if [ -n "$pc" ]; then
                prolog=yap
                echo $pc
            else
                echo "not found"
                exit 1
            fi
        fi
    fi
fi
gplc=gplc
swipl=swipl
yap=yap
case "$prolog" in
    */gplc|*/gprolog)
        gplc="$prolog"
        prolog=gnu;;
    */swipl)
        swipl="$prolog"
        prolog=swi;;
    */yap)
        yap="$prolog"
        prolog=yap;;
    yap) prolog=yap;;
    gnu|gplc|GNU|gnu-prolog|gprolog) prolog=gnu;;
    swi|swipl|SWI|swi-prolog) prolog=swi;;
    *)
        echo "unsupported prolog implementation - use either GNU, YAP or SWI Prolog" >&2
        exit 1;;
esac

printf "locating version file ... "
if [ -f ../version.ghc ]; then
    vfile=../version.ghc
else
    echo "version.ghc not found" >&2
    exit 1
fi
cp "$vfile" version.pl
echo " $vfile"

echo "generating compiler loader c.pl ..."
cat >c.pl <<EOF
:- include('version.pl').
:- include('top.pl').
:- include('compiler.pl').
:- include('lfa.pl').
:- include('asm.pl').
EOF
if [ -f ${arch0}-generic.pl ]; then
cat >>c.pl <<EOF
:- include('${arch0}-generic.pl').
EOF
fi
cat >>c.pl <<EOF
:- include('${arch}.pl').
:- include('../entrypoints.ghc').
EOF

echo "generating config.sh ..."
cat > config.sh <<EOF
ARCH="$arch"
PROLOG="$prolog"
GPLC="$gplc"
YAP="$yap"
SWIPL="$swipl"
EOF
