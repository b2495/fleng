/* FLENG - header-file for C part of runtime */

#ifndef FLENG_H
#define FLENG_H

#include <stdio.h>
#include <sys/time.h>

#ifdef __linux__
#include <poll.h>
#endif

#define FL_PORT_SIZE    0x4000
#define FL_GLOBAL_TABLE 256
#define FL_HEAP_STAT_COUNT  7

#define FL_TAG_MASK     0xff000000
#define FL_COUNT_MASK   0x00ffffff
#define FL_INT_BIT      1
#define FL_ATOM_BIT     2
#define FL_BITS_MASK    3

#define FL_LIST_TAG     0x01000000
#define FL_FLOAT_TAG    0x02000000
#define FL_VECTOR_TAG	0x03000000
#define FL_VAR_TAG      0x04000000
#define FL_PORT_TAG     0x05000000
#define FL_TASK_TAG     0x06000000
#define FL_REF_TAG      0x07000000
#define FL_ARRAY_TAG    0x08000000
#define FL_RTASK_TAG    0x09000000
#define FL_MODULE_TAG   0x0a000000
#define FL_BOX_INT_TAG  0x0b000000
#define FL_BOX_DOUBLE_TAG 0x0c000000
#define FL_BOX_CHAR_TAG 0x0d000000
#define FL_BOX_SHORT_TAG 0x0e000000
#define FL_BOX_LONG_TAG 0x0f000000

#define FL_CONSTANT     0x7e000000
#define FL_INTERNED     0x7f000000

#define FL_TUPLE_BIT    0x80000000

#define FL_BROKEN_HEART 0xdead0000

#define FL_CHAR_ARRAY   1
#define FL_SHORT_ARRAY   2
#define FL_INT_ARRAY    3
#define FL_DOUBLE_ARRAY 4
#define FL_LONG_ARRAY    5
#define FL_KL1_STRING	6

#define FL_ARRAY_IMMUTABLE  0x08

typedef void *FL_VAL;

#define FL_FWD      (-1)
#define FL_BWD      (-2)
#define FL_NORTH    (-3)
#define FL_EAST     (-4)
#define FL_SOUTH    (-5)
#define FL_WEST     (-6)
#define FL_ALL      (-7)

#define FL_MAX_ARGS     4

typedef struct {
    unsigned long tag;
    FL_VAL car, cdr;
} FL_CELL;

typedef struct {
    void *addr;
    FL_VAL args[ FL_MAX_ARGS ];
    char *info;
    FL_VAL task;
} FL_GOAL;

typedef struct fl_htnode {
    FL_VAL key, val;
    struct fl_htnode *next;
} FL_HTNODE;

typedef struct fl_tcb_struct {
    FL_GOAL *goal;
    FL_VAL *sstackp;
    long tailcalls;
    long timeslice;
    long ticks;
    void *stackbase;
    FL_CELL *freelist;
    FL_CELL *heap, *heapend;
    long suspended, active, used;
    FL_GOAL **qstart, **qend;
    FL_GOAL **queue;
    long max_goals;
    FL_GOAL *freegoals;
    FL_GOAL *goal_buffer;
    FL_VAL *sstack;
    long ordinal;
    long logging;
    long goals;
#if defined(__aarch64__) || defined(__riscv)
    void *sp0;
#else
    void *unused1;
#endif
    long *counters;		/* #23 */
    long candidates;		/* #24 */
    long selected;	/* #25 */
    /* the slots above are accessed from asm and must keep their position
       and size */
    FL_VAL idle;
    long epoch;
    char *port;
    FL_VAL events;
    FL_VAL listening;
#ifdef __linux__
    struct pollfd *pollfds;
#else
    int event_queue;
#endif
    int peers[ 6 ]; /* fwd, bwd, north, east, south, west */
    long delay;  /* microseconds << 8 */
    int state;
#define FL_BUSY             0
#define FL_IDLE             1
#define FL_FINISHING        2
    int defer;
    int detached;
    int loggingx;
    char *mportfile;
    FL_VAL deferred, deferred_tail;
    void *thread;
    FL_HTNODE **globals;
    int write_limit;
    int occurs_check;
    char *cbuf;
    int cbuf_len;
    FL_VAL *trailbuf;
    FL_CELL *heaptop;
    void (*wakeup_hook)(struct fl_tcb_struct *);
    long average, peak, scounts;
    struct timeval latency;
    long counter;
} FL_TCB;

typedef struct {
    FL_VAL name;
    long arity, exported;
    void *addr;
} FL_PDEF;

typedef struct fl_profinfo {
    char *goalname;
    long *counters;
    struct fl_profinfo *next;
} FL_PROFINFO;

typedef struct fl_module {
    struct fl_module *next;
    long flags;
#define FL_INTERACTIVE	1
    long npdefs;
    FL_VAL name;
    FL_VAL *atoms;
    char *names;
    FL_VAL **ratoms;
    FL_PROFINFO *profinfo;
    FL_PDEF pdefs[0];
} FL_MODULE;

#define FL_NO_ERROR         0
#define FL_NOT_AN_ATOM      1
#define FL_NOT_A_CELL       2
#define FL_NOT_AN_INT       3
#define FL_NOT_A_LIST       4
#define FL_NOT_A_TUPLE      5
#define FL_NOT_A_VAR        6
#define FL_QUEUE_FULL       7
#define FL_NO_MODULE        8
#define FL_NO_PDEF          9
#define FL_NO_CELLS         10
#define FL_NOT_A_NUMBER     11
#define FL_DIV_BY_ZERO      12
#define FL_CANT_CONVERT     13
#define FL_NOT_A_STRING     14
#define FL_IO_ERROR         15
#define FL_NOT_A_PORT       16
#define FL_PORT_CLOSED      17
#define FL_BAD_INDEX        18
#define FL_CANT_UNIFY       19
#define FL_CANT_FORWARD     20
#define FL_CANT_UNFORWARD   21
#define FL_BAD_PEER         22
#define FL_NOT_A_MODULE     23
#define FL_OUT_OF_MEMORY    24
#define FL_BAD_SIGNAL       25
#define FL_TOO_MANY_GOALS   26
#define FL_NO_GLOBAL        27
#define FL_OCCURS           28
#define FL_BAD_STRUCT       29
#define FL_TRAIL_FULL       30
#define FL_NOT_AN_ARRAY     31
#define FL_NOT_AN_INT_ARRAY 32
#define FL_NOT_A_CHAR_ARRAY 33
#define FL_NOT_A_SHORT_ARRAY 34
#define FL_NOT_A_DOUBLE_ARRAY 35
#define FL_IMMUTABLE        36
#define FL_BAD_THREAD       37
#define FL_NOT_A_LONG_ARRAY 38
#define FL_UNINITIALIZED		39
#define FL_NOT_A_VECTOR	40

#define FL_INPUT            1
#define FL_SIGNAL           2
#define FL_TIMEOUT          3
#define FL_CHILD            4
#define FL_CLOCK            5
#define FL_OUTPUT		6

#define FL_DETACHED         (-1)

#ifdef __cplusplus
extern "C" {
#endif

extern char *fl_argv[], *fl_prgname;
extern FL_TCB fl_tcbs[];
extern FL_VAL fl_nil, fl_true, fl_false, fl_colon, fl_error, fl_slash, fl_dot;
extern FL_VAL fl_stdin, fl_stdout, fl_stderr;
extern void fl_rt_error(FL_TCB *tcb, FL_VAL x, int error);
extern void fl_rt_error2(FL_TCB *tcb, FL_VAL x, FL_VAL y, int error);
extern void fl_assign(FL_TCB *tcb, FL_VAL x, FL_VAL var);
extern void fl_d_assign(FL_TCB *tcb, FL_VAL x, FL_VAL var);
extern void fl_unify_result(FL_TCB *tcb, FL_VAL val, FL_VAL dest);
extern void fl_release(FL_TCB *tcb, FL_VAL x);
extern void fl_io_error(FL_TCB *tcb, FL_VAL var);
extern void fl_abort(FL_TCB *tcb, FL_VAL err);
extern char *fl_stringify_next(FL_TCB *tcb, char **pp, FL_VAL str, int *len);
extern FL_VAL fl_alloc_cell(FL_TCB *tcb, FL_VAL car, FL_VAL cdr);
extern FL_VAL fl_alloc_float(FL_TCB *tcb, double x);
extern void fl_write(FL_TCB *tcb, FILE *fp, FL_VAL x);
extern void fl_fail(FL_TCB *tcb, int arity);
extern void fl_logfmt(FL_TCB *tcb, char *fstr, ...);
extern void fl_log(FL_TCB *tcb, FILE *fp, FL_VAL msg);
extern void fl_log_entry(FL_TCB *tcb, long arity);
extern FL_VAL fl_intern(char *np);
extern void fl_out_of_memory(void);
extern FL_VAL fl_mkstring(char *str, int len);
extern void fl_init_module(FL_MODULE *mod);
extern void fl_init_goal(FL_PDEF *pdef);
extern void fl_resolve_pdef(FL_TCB *tcb, long arity, FL_VAL mod, FL_VAL name,
    void *cache, FL_GOAL *ng);
extern FL_VAL fl_unify_rec(FL_TCB *tcb, FL_VAL x, FL_VAL y);
extern FL_VAL fl_d_unify_rec(FL_TCB *tcb, FL_VAL x, FL_VAL y);
extern void fl_unify_rec_checked(FL_TCB *tcb, FL_VAL x, FL_VAL y);
extern void fl_d_unify_rec_checked(FL_TCB *tcb, FL_VAL x, FL_VAL y);
extern void fl_yield(FL_TCB *tcb);
extern void fl_enter(FL_TCB *tcb);
extern void fl_suspend(FL_TCB *tcb);
extern void fl_d_suspend(FL_TCB *tcb);
extern void fl__init(void);
extern long fl_ordering(FL_TCB *tcb, FL_VAL x, FL_VAL y, long *suspend);
extern FL_VAL fl_ordered_below(FL_TCB *ignored, FL_VAL x, FL_VAL y);
extern FL_VAL fl_chase_tail(FL_VAL st);
extern void fl_add_event(FL_TCB *tcb, FL_VAL event);
extern char *fl_forward_rec(FL_TCB *tcb, FL_VAL x, int dest, char *ptr, char *limit, int *error);
extern int fl_forward(FL_TCB *tcb, int id, FL_VAL x);
extern int fl_forward_to_port(FL_TCB *tcb, char *port, FL_VAL x, int dest);
extern char *fl_unforward_rec(FL_TCB *tcb, char *ptr, FL_VAL *result);
extern FL_VAL fl_unforward(FL_TCB *tcb);
extern void fl_send_to_port(FL_TCB *tcb, FL_VAL port, FL_VAL x);
extern int fl_file(FL_TCB *tcb, FL_VAL file);
extern int fl_nthreads(void);
extern int fl_create_event_queue(FL_TCB *tcb);
extern int fl_wait_for_events(FL_TCB *tcb, long ms);
extern int fl_add_event_to_queue(FL_TCB *tcb, int event, FL_VAL data, FL_VAL var, FL_VAL eid);
extern int fl_cancel_timer(FL_TCB *tcb, FL_VAL id);
extern int fl_restart_timer(FL_TCB *tcb, FL_VAL id, int ms);
extern FL_MODULE *fl_find_module(FL_VAL name);
extern FL_VAL fl_all_modules(FL_TCB *tcb);
extern double fl_random(void);
extern long fl_random_integer(long n);
extern void fl_set_random_seed(char *buf, int n);
extern void fl_atexit(void (*proc)(int));
extern void fl_terminate(int code);
extern FL_VAL fl_get_global(FL_TCB *tcb, FL_VAL key, FL_VAL def);
extern void fl_set_global(FL_TCB *tcb, FL_VAL key, FL_VAL val);
extern int fl_resolve_signal(char *name);
extern void fl_heap_statistics(FL_TCB *tcb, long *data);
extern void fl_dump_heap_graph(char *fname);
extern FL_VAL fl_unify_safe1(FL_TCB *tcb, FL_VAL x, FL_VAL y);
extern void fl_init_signals(void);
extern FL_VAL fl_clone(FL_TCB *tcb, FL_VAL a);
extern FL_VAL fl_unbox_cell(FL_TCB *tcb, FL_VAL b);
extern void fl_force_yield(FL_TCB *tcb);
extern int fl_match_facts(FL_TCB *tcb, int arity, FL_VAL tree);

#ifdef __cplusplus
}
#endif

#endif
