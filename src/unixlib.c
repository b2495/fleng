/* FLENG - C library for "unix" module */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/times.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <signal.h>
#include <sys/un.h>
#include <errno.h>
#include <sys/stat.h>
#include <time.h>
#include "fleng-util.h"

static char *add_xs(char *str)
{
	int len = strlen(str);
	char *d = malloc(len + 7);
	assert(d != NULL);
	memcpy(d, str, len);
	char *p = d + strlen(str) - 1;
	int xs = 0;
	for(char *p2 = p; p2 >= d; --p2) {
		if(*p2 == 'X') ++xs;
		else break;
	}
	if(xs < 6) {
		while(xs < 6) *(p++) = 'X';
	}
	*p = '\0';
	return d;
}

void fl_unix_mktemp_3(FL_TCB *tcb, FL_VAL tmp, FL_VAL fn, FL_VAL tl)
{
	char *t = add_xs(stringify(tcb, tmp, NULL));
	int fd = mkstemp(t);
	if(fd != -1) close(fd);
	fl_assign(tcb, mkcharlist(tcb, t, strlen(t), tl), fn);
	free(t);
}

void fl_unix_mkstemp_4(FL_TCB *tcb, FL_VAL tmp, FL_VAL f, FL_VAL fn, FL_VAL tl)
{
	char *t = add_xs(stringify(tcb, tmp, NULL));
	int fd = mkstemp(t);
	fl_assign(tcb, MKINT(fd), f);
	fl_assign(tcb, mkcharlist(tcb, t, strlen(t), tl), fn);
	free(t);
}

void fl_unix_access_3(FL_TCB *tcb, FL_VAL path, FL_VAL mode, FL_VAL r)
{
	CHECK_INT(mode);
	char *s = stringify(tcb, path, NULL);
	fl_assign(tcb, MKINT(access(s, INT(mode))), r);
}

void fl_unix_umask_1(FL_TCB *tcb, FL_VAL mask)
{
	mode_t um = umask(022);
	umask(um);
	fl_assign(tcb, MKINT(um), mask);
}

void fl_unix_umask_2(FL_TCB *tcb, FL_VAL mask, FL_VAL new)
{
	CHECK_INT(new);
	mode_t um = umask(INT(new));
	fl_assign(tcb, MKINT(um), mask);
}

void fl_unix_putenv_2(FL_TCB *tcb, FL_VAL val, FL_VAL r)
{
	char *str = stringify(tcb, val, NULL);
	fl_assign(tcb, MKINT(putenv(str)), r);
}

void fl_unix_fsync_2(FL_TCB *tcb, FL_VAL file, FL_VAL done)
{
	CHECK_INT(file);
	fsync(INT(file));
	fl_assign(tcb, fl_nil, done);
}

void fl_unix_times_4(FL_TCB *tcb, FL_VAL u, FL_VAL s, FL_VAL cu, FL_VAL cs)
{
#ifdef CLK_TCK
	static int ct = CLK_TCK;
#else
	static int ct = 0;
	if(ct == 0) ct = sysconf(_SC_CLK_TCK);
#endif
	struct tms tm;
	if(times(&tm) < 0) fl_io_error(tcb, u);
	fl_assign(tcb, MKINT(tm.tms_utime * 1000 / ct), u);
	fl_assign(tcb, MKINT(tm.tms_stime * 1000 / ct), s);
	fl_assign(tcb, MKINT(tm.tms_cutime * 1000 / ct), cu);
	fl_assign(tcb, MKINT(tm.tms_cutime * 1000 / ct), cs);
}

void fl_unix_connect_4(FL_TCB *tcb, FL_VAL s1, FL_VAL s2, FL_VAL f, FL_VAL ok)
{
	int af;
	struct sockaddr_in addr;
	struct sockaddr_un uaddr;
	struct sockaddr *pa;
	int pasz;
	if(s2 == fl_false) {
		int len;
		char *un = stringify(tcb, s1, &len);
		af = AF_UNIX;
		memset(&uaddr, 0, sizeof(uaddr));
		uaddr.sun_family = AF_UNIX;
		memcpy(uaddr.sun_path, un, len);
		pa = (struct sockaddr *)&uaddr;
		pasz = sizeof(struct sockaddr_un);
	} else {
		af = AF_INET;
		memset(&addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(INT(s2));
		if(s1 == fl_false)
			addr.sin_addr.s_addr = htonl(INADDR_ANY);
		else if(TAG(s1) == FL_ARRAY_TAG && ARRAY_TYPE(s1) != FL_KL1_STRING)
			addr.sin_addr.s_addr = htonl(*((int *)ARRAY_POINTER(s1)));
		else {
			char *hn = stringify(tcb, s1, NULL);
			struct hostent *h = gethostbyname(hn);
			if(h == NULL) goto fail;
			addr.sin_addr = *((struct in_addr *)h->h_addr);
		}
		pa = (struct sockaddr *)&addr;
		pasz = sizeof(struct sockaddr_in);
	}
#ifndef SOCK_NONBLOCK
# define SOCK_NONBLOCK		0
#endif
	int sockfd = socket(af, SOCK_STREAM | SOCK_NONBLOCK, 0);
	if(sockfd < 0) goto fail;
	if(af == AF_INET) {
		int yes = 1;
		if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const char *)&yes, sizeof(int)) != 0)
			goto fail;
	}
	int val = fcntl(sockfd, F_GETFL, 0);
	if(val < 0) goto fail;
	if(fcntl(sockfd, F_SETFL, val | O_NONBLOCK) < 0) goto fail;
	if(connect(sockfd, pa, pasz) < 0) {
		if(errno != EINPROGRESS) goto fail;
	}
	if(fl_add_event_to_queue(tcb, FL_OUTPUT, MKINT(sockfd), ok, NULL) == -1)
		fl_io_error(tcb, ok);
	fl_assign(tcb, MKINT(sockfd), f);
	return;
fail:
	fflush(stdout);
	fl_assign(tcb, fl_false, f);
}

void fl_unix_connect_check_2(FL_TCB *tcb, FL_VAL s, FL_VAL r)
{
	int v;
	socklen_t len = sizeof(int);
	if(getsockopt(INT(s), SOL_SOCKET, SO_ERROR, &v, &len) < 0)
		fl_assign(tcb, fl_false, r);
	fl_assign(tcb, v ? fl_false : fl_true, r);
}

void fl_unix_bind_3(FL_TCB *tcb, FL_VAL s1, FL_VAL s2, FL_VAL f)
{
	int af;
	struct sockaddr_in addr;
	struct sockaddr_un uaddr;
	struct sockaddr *pa;
	int pasz;
	if(s2 == fl_false) {
		int len;
		char *un = stringify(tcb, s1, &len);
		af = AF_UNIX;
		unlink(un);
		memset(&uaddr, 0, sizeof(uaddr));
		uaddr.sun_family = AF_UNIX;
		memcpy(uaddr.sun_path, un, len);
		pa = (struct sockaddr *)&uaddr;
		pasz = sizeof(struct sockaddr_un);
	} else {
		af = AF_INET;
		memset(&addr, 0, sizeof(addr));
		addr.sin_family = AF_INET;
		addr.sin_port = htons(INT(s2));
		if(s1 == fl_false)
			addr.sin_addr.s_addr = htonl(INADDR_ANY);
		else if(TAG(s1) == FL_ARRAY_TAG)
			addr.sin_addr.s_addr = htonl(*((int *)ARRAY_POINTER(s1)));
		else {
			char *hn = stringify(tcb, s1, NULL);
			struct hostent *h = gethostbyname(hn);
			if(h == NULL) goto fail;
			addr.sin_addr = *((struct in_addr *)h->h_addr);
		}
		pa = (struct sockaddr *)&addr;
		pasz = sizeof(struct sockaddr_in);
	}
	int sockfd = socket(af, SOCK_STREAM, 0);
	if(sockfd < 0) goto fail;
	int val = fcntl(sockfd, F_GETFL, 0);
	if(val < 0) goto fail;
	if(fcntl(sockfd, F_SETFL, val | O_NONBLOCK) < 0) goto fail;
	if(af == AF_INET) {
		int yes = 1;
		if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const char *)&yes, sizeof(int)) != 0)
			goto fail;
	}
	if(bind(sockfd, pa, pasz) < 0) goto fail;
	if(listen(sockfd, 100) < 0) goto fail;
	fl_assign(tcb, MKINT(sockfd), f);
	return;
fail:
	fl_assign(tcb, fl_false, f);
}

void fl_unix_accept_2(FL_TCB *tcb, FL_VAL f1, FL_VAL f2)
{
	int sockfd = INT(f1);
	int fd;
	while(1) {
		fd = accept(sockfd, NULL, NULL);
		if(fd < 0) {
			if(errno == EWOULDBLOCK || errno == EAGAIN) {
				fl_assign(tcb, fl_true, f2);
				return;
			} else if(errno != EINTR) {
				fl_assign(tcb, fl_false, f2);
				return;
			}
		} else break;
	}
	fl_assign(tcb, MKINT(fd), f2);
}
