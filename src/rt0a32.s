# FLENG - asm part of arm32 runtime

    .section .text

    .global fl_check_atom
    @ r0=val
fl_check_atom:
    and r5, r0, #3
    cmp r5, #2
    bne l1
    mov pc, lr
l1:
    mov r1, r0
    mov r0, r8
    mov r2, #1          @ FL_NOT_AN_ATOM
    b fl_rt_error

    .global fl_check_cell
    @ r0=val
fl_check_cell:
    tst r0, #3
    bne l2
    mov pc, lr
l2:
    mov r1, r0
    mov r0, r8
    mov r2, #2          @ FL_NOT_A_CELL
    b fl_rt_error

    .global fl_check_integer
    @ r0=val
fl_check_integer:
    tst r0, #1         @ FL_INT_BIT
    beq l3
    mov pc, lr
l3:
    mov r1, r0
    mov r0, r8
    mov r2, #3          @ FL_NOT_AN_INT
    b fl_rt_error

    .global fl_check_list
    @ r0=val
fl_check_list:
    tst r0, #3
    bne l4
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x01000000 @ FL_LIST_TAG
    cmp r5, r6
    bne l4
    mov pc, lr
l4:
    mov r1, r0
    mov r0, r8
    mov r2, #4          @ FL_NOT_A_LIST
    b fl_rt_error

    .global fl_check_module
    @ r0=val
fl_check_module:
    tst r0, #3
    bne l4b
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x0a000000 @ FL_MODULE_TAG
    cmp r5, r6
    bne l4b
    mov pc, lr
l4b:
    mov r1, r0
    mov r0, r8
    mov r2, #23          @ FL_NOT_A_MODULE
    b fl_rt_error

    .global fl_check_tuple
    @ r0=val
fl_check_tuple:
    tst r0, #3
    bne l5
    ldr r5, [r0]
    mov r6, #0x80000000  @ FL_TUPLE_BIT
    tst r5, r6
    bne l5
    mov pc, lr
l5:
    mov r1, r0
    mov r0, r8
    mov r2, #5          @ FL_NOT_A_TUPLE
    b fl_rt_error

    .global fl_check_var
    @ r0=val
fl_check_var:
    tst r0, #3
    bne l6
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x04000000 @ FL_VAR_TAG
    cmp r5, r6
    beq l6
    mov r6, #0x07000000 @ FL_REF_TAG
    cmp r5, r6
    beq l6
    mov r1, r0
    mov r0, r8
    mov r2, #6          @ FL_NOT_A_VAR
    b fl_rt_error
l6:
    mov pc, lr

    .global fl_check_number
    @ r0=val
fl_check_number:
    tst r0, #1
    beq l7
    mov pc, lr
l7:
    tst r0, #2
    bne l8
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x02000000 @ FL_FLOAT_TAG
    cmp r5, r6
    bne l8
    mov pc, lr
l8:
    mov r1, r0
    mov r0, r8
    mov r2, #11          @ FL_NOT_A_NUMBER
    b fl_rt_error

    @ r1=val -> d0
float_arg_1:
    tst r1, #1
    beq l9
    asr r1, r1, #1
    vmov s0, r1
    vcvt.f64.s32 d0, s0
    mov pc, lr
l9:
    vldr d0, [r1, #4]
    mov pc, lr

    @ r2=val -> d1
float_arg_2:
    tst r2, #1
    beq l10
    asr r2, r2, #1
    vmov s2, r2
    vcvt.f64.s32 d1, s2
    mov pc, lr
l10:
    vldr d1, [r2, #4]
    mov pc, lr

    .global fl_add
    @ r1=num, r2=num -> r0
fl_add:
    tst r1, #1
    beq l11
    tst r2, #1
    beq l11
    sub r0, r1, #1
    add r0, r0, r2
    mov pc, lr
l11:
    push {lr}
    bl float_arg_1
    bl float_arg_2
    vadd.f64 d0, d0, d1
    pop {lr}
    b fl_mkfloat

    .global fl_sub
    @ r1=num, r2=num -> r0
fl_sub:
    tst r1, #1
    beq l12
    tst r2, #1
    beq l12
    sub r1, r1, r2
    add r0, r1, #1
    mov pc, lr
l12:
    push {lr}
    bl float_arg_1
    bl float_arg_2
    vsub.f64 d0, d0, d1
    pop {lr}
    b fl_mkfloat

    .global fl_mul
    @ r1=num, r2=num -> r0
fl_mul:
    tst r1, #1
    beq l13
    tst r2, #1
    beq l13
    asr r1, r1, #1
    asr r2, r2, #1
    mul r0, r1, r2
    lsl r0, r0, #1
    orr r0, r0, #1
    mov pc, lr
l13:
    push {lr}
    bl float_arg_1
    bl float_arg_2
    vmul.f64 d0, d0, d1
    pop {lr}
    b fl_mkfloat

divmod:
    @ lhs=r0, rhs=r1, div=r2, mod=r3
    eors r5, r0, r1   @ divsgn
    movs r6, r0       @ modsgn
    rsbmi r0, r0, #0
    teq r1, #0
    rsbmi r1, r1, #0
    b udiv32
sdiv32r:
    teq r5, #0
    rsbmi r2, r2, #0
    teq r6, #0
    mov pc, lr
udiv32:
    mov r2, #0
    mov r3, #0
    mov r4, #32
udivlp1:
    subs r4, r4, #1
    beq sdiv32r
    movs r0, r0, lsl #1
    bpl udivlp1
udiv32_2:
    movs r0, r0, lsl #1
    adc r3, r3, r3
    cmp r3, r1
    subcs r3, r3, r1
    adc r2, r2, r2
    subs r4, r4, #1
    bne udiv32_2
    b sdiv32r

    .global fl_pow
    @ r1=num, r2=num -> r0
fl_pow:
    push {lr}
    bl float_arg_1
    bl float_arg_2
    bl pow
    pop {lr}
    b fl_mkfloat

    .global fl_div
    @ r1=num, r2=num -> r0
fl_div:
    cmp r2, #1
    bne l14
    mov r0, r8
    mov r2, #12         @ FL_DIV_BY_ZERO
    b fl_rt_error
l14:
    tst r1, #1
    beq l15
    tst r2, #1
    beq l15
    asr r0, r1, #1
    asr r1, r2, #1
    push {lr}
    bl divmod
    pop {lr}
    mov r0, r2
    lsl r0, r0, #1
    orr r0, r0, #1
    mov pc, lr
l15:
    push {lr}
    bl float_arg_1
    bl float_arg_2
    vdiv.f64 d0, d0, d1
    pop {lr}
    b fl_mkfloat

    .global fl_mod
    @ r1=int, r2=int -> r0
fl_mod:
    cmp r2, #1
    bne l14b
    mov r0, r8
    mov r2, #12         @ FL_DIV_BY_ZERO
    b fl_rt_error
l14b:
    asr r0, r1, #1
    asr r1, r2, #1
    push {r0, lr}
    bl divmod
    pop {r0, lr}
    cmp r0, #0
    rsbmi r3, r3, #0
    mov r0, r3
    lsl r0, r0, #1
    orr r0, r0, #1
    mov pc, lr

    .global fl_sqrt
    @ r1=num -> r0
fl_sqrt:
    push {lr}
    bl float_arg_1
    vsqrt.f64 d0, d0
    pop {lr}
    b fl_mkfloat

    .global fl_sin
    @ r1=num -> r0
fl_sin:
    push {lr}
    bl float_arg_1
    bl sin
    pop {lr}
    b fl_mkfloat

    .global fl_cos
    @ r1=num -> r0
fl_cos:
    push {lr}
    bl float_arg_1
    bl cos
    pop {lr}
    b fl_mkfloat

    .global fl_tan
    @ r1=num -> r0
fl_tan:
    push {lr}
    bl float_arg_1
    bl tan
    pop {lr}
    b fl_mkfloat

    .global fl_asin
    @ r1=num -> r0
fl_asin:
    push {lr}
    bl float_arg_1
    bl asin
    pop {lr}
    b fl_mkfloat

    .global fl_acos
    @ r1=num -> r0
fl_acos:
    push {lr}
    bl float_arg_1
    bl atan
    pop {lr}
    b fl_mkfloat

    .global fl_atan
    @ r1=num -> r0
fl_atan:
    push {lr}
    bl float_arg_1
    bl atan
    pop {lr}
    b fl_mkfloat

    .global fl_loge
    @ r1=num -> r0
fl_loge:
    push {lr}
    bl float_arg_1
    bl log
    pop {lr}
    b fl_mkfloat

    .global fl_exp
    @ r1=num -> r0
fl_exp:
    push {lr}
    bl float_arg_1
    bl exp
    pop {lr}
    b fl_mkfloat

    .global fl_truncate
    @ r1=num -> r0
fl_truncate:
    push {lr}
    bl float_arg_1
    bl trunc
    pop {lr}
    b fl_mkfloat

    .global fl_round
    @ r1=num -> r0
fl_round:
    push {lr}
    bl float_arg_1
    bl round
    pop {lr}
    b fl_mkfloat

    .global fl_floor
    @ r1=num -> r0
fl_floor:
    push {lr}
    bl float_arg_1
    bl floor
    pop {lr}
    b fl_mkfloat

    .global fl_ceiling
    @ r1=num -> r0
fl_ceiling:
    push {lr}
    bl float_arg_1
    bl ceil
    pop {lr}
    b fl_mkfloat

    .global fl_shl
    @ r1=num, r2=num -> r0
fl_shl:
    lsr r2, r2, #1
    bic r1, r1, #1
    lsl r0, r1, r2
    orr r0, #1
    mov pc, lr

    .global fl_shr
    @ r1=num, r2=num -> r0
fl_shr:
    lsr r2, r2, #1
    asr r0, r1, r2
    orr r0, #1
    mov pc, lr

    .global fl_int
    @ r1=num -> r0
fl_int:
    tst r1, #1
    beq l16
    mov r0, r1
    mov pc, lr
l16:
    vldr d0, [r1, #4]
    vcvt.s32.f64 s0, d0
    vmov r0, s0
    lsl r0, r0, #1
    orr r0, r0, #1
    mov pc, lr

    .global fl_float
    @ r1=num -> r0
fl_float:
    asr r1, r1, #1
    vmov s0, r1
    vcvt.f64.s32 d0, s0
    b fl_mkfloat

true:
    ldr r0, =fl_true
    ldr r0, [r0]
    mov pc, lr

false:
    ldr r0, =fl_false
    ldr r0, [r0]
    mov pc, lr

    .global fl_sametype
    @ r1=val, r2=val -> r0
fl_sametype:
    and r0, r1, #3
    and r5, r2, #3
    cmp r0, r5
    bne false
    tst r0, #3
    bne true
    ldr r0, [r1]
    ldr r5, [r2]
    mov r6, #0x80000000    @ FL_TUPLE_BIT
    tst r0, r6
    beq l19
    tst r5, r6
    bne false
    b true
l19:
    tst r5, r6
    bne false
    mov r6, #0xff000000     @ FL_TAG_MASK
    and r0, r0, r6
    and r5, r5, r6
    cmp r0, r5
    bne false
    b true

    .global fl_eq
    @ r1=val, r2=val -> r0
fl_eq:
    cmp r1, r2
    beq true
    b false

    .global fl_equal
    @ r1=val, r2=val -> r0
    @ must preserve P0-P3
fl_equal:
    cmp r1, r2
    beq true
    tst r1, #1
    beq l22
    tst r2, #1
    beq l23
    b false
l22:
    tst r2, #2
    bne false
    ldr r5, [r1]
    mov r6, #0xff000000     @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x02000000     @ FL_FLOAT_TAG
    cmp r5, r6
    bne false
l23:
    tst r2, #2
    bne false
    tst r2, #1
    bne l25
    ldr r5, [r2]
    mov r6, #0xff000000     @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x02000000     @ FL_FLOAT_TAG
    cmp r5, r6
    bne false
l25:
    push {lr}
    bl float_arg_1
    bl float_arg_2
    pop {lr}
    vcmp.f64 d0, d1
    vmrs APSR_nzcv, fpscr
    bne false
    b true

    .global fl_pf_equal
    @ principal functor equality
    @ r1=val, r2=val -> r0
    @ must preserve P0-P3
fl_pf_equal:
    cmp r1, r2
    beq true
    tst r1, #3
    bne false
    tst r2, #3
    bne false
    ldr r5, [r1]
    mov r4, #0xff000000		@ FL_TAG_MASK
    and r5, r5, r4
    ldr r6, [r2]
    and r6, r6, r4
    cmp r5, r6
    bne false
    mov r4, #0x80000000		@ FL_TUPLE_BIT
    tst r5, r4
    beq true
    ldr r5, [r1, #4]
    ldr r6, [r2, #4]
    cmp r5, r6
    bne false
    b true

    .global fl_less
    @ r1=val, r2=val -> r0
    @ must preserve P0-P3
fl_less:
    tst r1, #1
    beq l26
    tst r2, #1
    beq l26
    cmp r1, r2
    bge false
    b true
l26:
    push {lr}
    bl float_arg_1
    bl float_arg_2
    pop {lr}
    vcmp.f64 d0, d1
    vmrs APSR_nzcv, fpscr
    bmi true
    b false

first:
    mov r0, r1
    mov pc, lr

second:
    mov r0, r2
    mov pc, lr

    .global fl_max
    @ r1=val, r2=val -> r0
    @ must preserve P0-P3
fl_max:
    tst r1, #1
    beq l26a
    tst r2, #1
    beq l26a
    cmp r1, r2
    ble second
    b first
l26a:
    push {r1, r2, lr}
    bl float_arg_1
    bl float_arg_2
    pop {r1, r2, lr}
    vcmp.f64 d0, d1
    vmrs APSR_nzcv, fpscr
    bmi second
    b first

    .global fl_min
    @ r1=val, r2=val -> r0
    @ must preserve P0-P3
fl_min:
    tst r1, #1
    beq l26i
    tst r2, #1
    beq l26i
    cmp r1, r2
    ble first
    b second
l26i:
    push {r1, r2, lr}
    bl float_arg_1
    bl float_arg_2
    pop {r1, r2, lr}
    vcmp.f64 d0, d1
    vmrs APSR_nzcv, fpscr
    bmi first
    b second

    .global fl_sign
    @ r1=val -> r0
fl_sign:
    tst r1, #1
    bne lsign1
    push {lr}
    bl float_arg_1
    pop {lr}
    ldr r0, =zero
    vldr.f64 d1, [r0]
    vcmp.f64 d0, d1
    vmrs APSR_nzcv, fpscr
    beq lsign3
    bmi lsign2
lsign4:
    mov r0, #3
    mov pc, lr
lsign2:
    mov r0, #-1
    mov pc, lr
lsign3:
    mov r0, #1
    mov pc, lr
lsign1:
    cmp r0, #1
    bmi lsign2
    bne lsign4
    mov r0, r1
    mov pc, lr
zero: .double 0

    .global fl_abs
    @ r1=val -> r0
fl_abs:
    tst r1, #1
    bne labs1
    push {lr}
    bl float_arg_1
    pop {lr}
    vabs.f64 d0, d0
    b fl_mkfloat
labs1:
    cmp r1, #0
    bmi labs2
    mov r0, r1
    mov pc, lr
labs2:
    neg r0, r1
    add r0, r0, #2
    mov pc, lr

    .global fl_rnd
    @ r1=val -> r0
fl_rnd:
    mov r0, r1
    b fl_random_int

    .global fl_fip
    @ r1=val -> r0
fl_fip:
    tst r1, #1
    beq lfip1
    mov r0, r1
    mov pc, lr
lfip1:
    push {lr}
    sub sp, sp, #8
    bl float_arg_1
    mov r0, sp
    bl modf
    vldr.f64 d0, [sp]
    add sp, sp, #8
    pop {lr}
    b fl_mkfloat

    .global fl_ffp
    @ r1=val -> r0
fl_ffp:
    tst r1, #1
    beq lffp1
    mov r0, #1
    mov pc, lr
lffp1:
    push {lr}
    sub sp, sp, #8
    bl float_arg_1
    mov r0, sp
    bl modf
    add sp, sp, #8
    pop {lr}
    b fl_mkfloat

    .global fl_iless
    @ r1=val, r2=val -> r0
fl_iless:
    cmp r1, r2
    blt true
    b false

    .global fl_fless
    @ r1=val, r2=val -> r0
fl_fless:
    push {r1, r2, lr}
    bl float_arg_1
    bl float_arg_2
    pop {r1, r2, lr}
    vcmp.f64 d0, d1
    vmrs APSR_nzcv, fpscr
    bmi true
    b false

    .global fl_below
    @ r1=val, r2=val -> r0
fl_below:
    push {r1, r2, r3, lr}
    sub sp, sp, #4
    mov r0, r8
    mov r3, sp
    bl fl_ordering
    pop {r5}
    pop {r1, r2, r3, lr}
    tst r5, #1      @ suspend?
    beq lbel1
    ldr r0, =fl_nil
    ldr r0, [r0]
    mov pc, lr
lbel1:
    tst r0, #2     @ -1
    bne true
    b false

    .global fl_below_or_equal
    @ r1=val, r2=val -> r0
fl_below_or_equal:
    push {r1, r2, r3, lr}
    sub sp, sp, #4
    mov r0, r8
    mov r3, sp
    bl fl_ordering
    pop {r5}
    pop {r1, r2, r3, lr}
    tst r5, #1     @ suspend?
    beq lbeleq1
    ldr r0, =fl_nil
    ldr r0, [r0]
    mov pc, lr
lbeleq1:
    cmp r0, #1
    bne true
    b false

    .global fl_above
    @ r1=val, r2=val -> r0
fl_above:
    push {r1, r2, r3, lr}
    sub sp, sp, #4
    mov r0, r8
    mov r3, sp
    bl fl_ordering
    pop {r5}
    pop {r1, r2, r3, lr}
    tst r5, #1
    beq labo1
    ldr r0, =fl_nil
    ldr r0, [r0]
    mov pc, lr
labo1:
    cmp r0, #1
    bne false
    b true

    .global fl_above_or_equal
    @ r1=val, r2=val -> r0
fl_above_or_equal:
    push {r1, r2, r3, lr}
    sub sp, sp, #4
    mov r0, r8
    mov r3, sp
    bl fl_ordering
    pop {r5}
    pop {r1, r2, r3, lr}
    tst r5, #1
    beq laboeq1
    ldr r0, =fl_nil
    ldr r0, [r0]
    mov pc, lr
laboeq1:
    cmp r0, #-1
    beq false
    b true

    .global fl_deref
    @ r0=val -> r0
    @ must preserve P0-P3
fl_deref:
    tst r0, #3
    beq l27
l28:
    mov pc, lr
l27:
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x04000000 @ FL_VAR_TAG
    cmp r5, r6
    bne l28
    ldr r5, [r0, #4]
    cmp r0, r5
    beq l28
    mov r0, r5
    b fl_deref

    .global fl_unbox
    @ r0=val -> r0
fl_unbox:
    tst r0, #3
    beq lu27
lu28:
    mov pc, lr
lu27:
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x04000000 @ FL_VAR_TAG
    cmp r5, r6
    bne lu29
    ldr r5, [r0, #4]
    cmp r0, r5
    beq lu28
    mov r0, r5
    b fl_unbox
lu29:
    mov r1, r0
    mov r0, r8
    b fl_unbox_cell

    .global fl_box
    @ r1=val -> A
fl_box:
    tst r1, #1
    bne box_l1
    mov pc, lr
box_l1:
    push {lr}
    ldr r0, =fl_nil
    ldr r0, [r0]
    mov r5, r0
    bl cons
    mov r6, #0x0f000000     @ FL_BOX_LONG_TAG
    str r6, [r0]
    asr r1, #1
    str r1, [r0, #4]
    pop {lr}
    mov pc, lr

    .global fl_unref
    @ r5=val
    @ must preserve A + P0-P3
fl_unref:
    tst r5, #3
    beq l29
    mov pc, lr
l29:
    push {r0}
    ldr r0, =0x00ffffff      @ FL_COUNT_MASK
    ldr r6, [r5]
    and r6, r6, r0
    cmp r6, r0
    beq l30a
    ldr r6, [r5]
    sub r6, r6, #1
    tst r6, r0
    beq l30
    str r6, [r5]
l30a:
    pop {r0}
    mov pc, lr
l30:
    push {r1, r2, r3, lr}
    mov r0, r8
    mov r1, r5
    bl fl_release
    pop {r1, r2, r3, lr}
    pop {r0}
    mov pc, lr

setcarry:
    msr CPSR_f, #1 << 29
    mov pc, lr

clrcarry:
    mrs r5, CPSR
    and r5, r5, #~(1 << 29)
    msr CPSR, r5
    mov pc, lr

    .global fl_force
    @ r0=val -> r0, CF is 1 when suspending
fl_force:
    tst r0, #3
    bne clrcarry
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x07000000 @ FL_REF_TAG
    cmp r5, r6
    beq l33
    mov r6, #0x04000000 @ FL_VAR_TAG
    cmp r5, r6
    bne clrcarry
    ldr r5, [r0, #4]
    cmp r0, r5
    beq l33
    mov r0, r5
    b fl_force
l33:
    ldr r5, [r8, #4]   @ tcb->sstackp
    str r0, [r5]
    ldr r0, [r10]      @ goal->addr
    str r0, [r5, #4]
    add r5, r5, #8
    str r5, [r8, #4]   @ tcb->sstackp
    b setcarry

    .global fl_match
    @ r1=val1, r2=val2 -> r0
    @ assumes args are forced (allows initial quick eq check)
fl_match:
    cmp r1, r2
    beq true
    mov r0, r8
    push {r1, r2, lr}
    bl fl_match_rec
    pop {r1, r2, lr}
    mov pc, lr

    .global fl_islist_t
    @ r0=val, set CF if not a list
fl_islist_t:
    tst r0, #3
    bne setcarry
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x01000000 @ FL_LIST_TAG
    cmp r5, r6
    beq clrcarry
    b setcarry

    .global fl_istuple_t
    @ r0=val, set CF if not a tuple
fl_istuple_t:
    tst r0, #3
    bne setcarry
    ldr r5, [r0]
    mov r6, #0x80000000 @ FL_TUPLE_BIT
    tst r5, r6
    bne clrcarry
    b setcarry

    @ r1=addr -> Carry (fail)
trynext:
    str r1, [r10]      @ goal->addr
    b setcarry

    .global fl_isval
    @ r0=val, r2=atom, r1=addr -> Carry (fail)
fl_isval:
    cmp r0, r2
    bne trynext
    b clrcarry

    .global fl_isfloat
    @ r0=val, d0=float, r1=addr -> Carry (fail)
fl_isfloat:
    tst r0, #3
    bne trynext
    vldr.f64 d1, [r0, #4]
    vcmp.f64 d0, d1
    vmrs APSR_nzcv, fpscr
    bne trynext
    b clrcarry

    .global fl_islist
    @ r0=val, r1=addr -> r0(car), Carry (fail)
    @ push cdr on stack
fl_islist:
    push {lr}
    bl fl_islist_t
    pop {lr}
    bcs trynext
    ldr r5, [r0, #8]
    push {r5}
    ldr r0, [r0, #4]
    b clrcarry

    .global fl_isvector
    @ r0=val, r1=addr, r2=len -> r0(M), Carry (fail)
    @ push vector on stack
fl_isvector:
    tst r0, #3
    bne trynext
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x03000000 @ FL_VECTOR_TAG
    cmp r5, r6
    bne trynext
    ldr r5, [r0, #8]
    ldr r6, [r5, #4]
    lsr r6, r6, #1
    cmp r6, r2
    bne trynext
    push {r0}
    b clrcarry

    .global fl_velement
    @ r1=index(fixnum), stack=vector -> r0=item
fl_velement:
    ldr r0, [sp]
    ldr r0, [r0, #4]		@ get map
    push {lr}
    bl fl_deref
    ldr r4, =fl_nil
    ldr r4, [r4]
lie1:
    cmp r0, r4
    bne lie4
    ldr r0, [sp, #4]
    ldr r0, [r0, #8]
    ldr r0, [r0, #8]		@ default
    pop {lr}
    mov pc, lr
lie4:
    ldr r0, [r0, #8]		@ get part of "t" tuple holding k, v
    ldr r0, [r0, #8]
    ldr r5, [r0, #4]		@ key
    cmp r1, r5
    blt lie2
    bgt lie3
    ldr r0, [r0, #8]
    ldr r0, [r0, #4]
    pop {lr}
    mov pc, lr
lie2:
    ldr r0, [r0, #8]		@ left part
    ldr r0, [r0, #8]
    ldr r0, [r0, #4]
    bl fl_deref
    b lie1
lie3:
    ldr r0, [r0, #8]		@ right part
    b lie2

   .global fl_isarrayval
    @ r0=val, r1=addr, r2=literal_array -> carry (fail)
fl_isarrayval:
    tst r0, #3
    bne trynext
    ldr r5, [r0]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r5, r5, r6
    mov r6, #0x08000000 @ FL_ARRAY_TAG
    cmp r5, r6
    bne trynext
    ldr r5, [r0, #4]		@ CAR(A) = [ptr len/type]
    ldr r6, [r2, #4]		@ CAR(P1) = [ptr len/type]
    ldr r4, [r5, #8]
    bic r4, r4, #8		@ clear FL_ARRAY_IMMUTABLE bit
    ldr r9, [r6, #8]
    bic r9, r9, #8
    cmp r4, r9
    bne trynext
    asr r4, r4, #4
    ldr r5, [r5, #4]
    ldr r6, [r6, #4]
liav1:
    cmp r4, #0
    beq clrcarry
    ldrb r12, [r5]
    ldrb r9, [r6]
    cmp r12, r9
    bne trynext
    add r5, r5, #1
    add r6, r6, #1
    sub r4, r4, #1
    b liav1

    .global fl_istuple
    @ r0=val, r1=addr, r2=arity -> r0(head), Carry (fail)
    @ push arglist on stack
fl_istuple:
    tst r0, #3
    bne trynext
    ldr r5, [r0]
    mov r6, #0x80000000 @ FL_TUPLE_BIT
    tst r5, r6
    beq trynext
    lsr r5, r5, #24
    and r5, r5, #0x7f
    cmp r5, r2
    bne trynext
    ldr r3, [r0, #8]
    push {r3}
    ldr r0, [r0, #4]
    b clrcarry

    @ r5=car, r0=cdr -> r0
    @ must preserve parameter registers
cons:
    ldr r6, [r8, #6 * 4]   @ tcb->freelist
    cmp r6, #0
    bne l34
    push {r0, r1, r2, r3, r5, lr}
    mov r0, r8
    bl fl_new_chunk
    pop {r0, r1, r2, r3, r5, lr}
    ldr r6, [r8, #6 * 4]   @ tcb->freelist
l34:
    push {r11, lr}
    ldr r11, [r6, #8]
    str r11, [r8, #6 * 4]  @ tcb->freelist
    bl fl_addref
    str r0, [r6, #8]
    mov r0, r5
    bl fl_addref
    str r0, [r6, #4]
    ldr r11, [r8, #11 * 4]  @ tcb->used
    add r11, r11, #1
    str r11, [r8, #11 * 4]    @ tcb->used
    mov r0, r6
    pop {r11, lr}
    mov pc, lr

    .global fl_mkfloat
    @ d0=float64 -> r0
    @ must preserve parameter registers
fl_mkfloat:
    add r6, r8, #6 * 4   @ &tcb->freelist
    ldr r0, [r6]
    cmp r0, #0
    bne l34b
    push {r1, r2, r3, r5, lr}
    mov r0, r8
    bl fl_new_chunk
    pop {r1, r2, r3, r5, lr}
    add r6, r8, #6 * 4   @ &tcb->freelist
    ldr r0, [r6]
l34b:
    tst r0, #4
    beq l34c
    ldr r5, [r0, #8]
    str r5, [r6]
    vstr.f64 d0, [r0, #4]
    mov r5, #0x02000000     @ FL_FLOAT_TAG
    str r5, [r0]
    ldr r5, [r8, #11 * 4]  @ tcb->used
    add r5, r5, #1
    str r5, [r8, #11 * 4]    @ tcb->used
    mov pc, lr
l34c:
    add r6, r0, #8
    ldr r0, [r6]
    b l34b

    .pool

    .global fl_mklist
    @ r0=cdr, <stack>=car -> r0
fl_mklist:
    pop {r5}
    push {lr}
    bl cons
    mov r5, #0x01000000      @ FL_LIST_TAG
    str r5, [r0]
    pop {lr}
    mov pc, lr

    .global fl_mktuple
    @ r0=args, r5=arity, r6=atom -> r0
fl_mktuple:
    push {r5, lr}
    mov r5, r6
    bl cons
    pop {r5, lr}
    @ fall through ...

    .global fl_mktuple0
    @ r0=args, r5=arity -> r0
fl_mktuple0:
    lsl r5, r5, #24
    mov r6, #0x80000000 @ FL_TUPLE_BIT
    orr r5, r5, r6
    str r5, [r0]
    mov pc, lr

    .global fl_tovector
    @ r0=lst -> r0
fl_tovector:
    mov r6, #0x03000000       @ FL_VECTOR_TAG
    str r6, [r0]
    mov pc, lr

    .global fl_mkvar
    @ -> r0
fl_mkvar:
    push {lr}
    mov r5, #1
    ldr r0, =fl_nil
    ldr r0, [r0]
    bl cons
    str r0, [r0, #4]
    mov r5, #0x04000000   @ FL_VAR_TAG
    str r5, [r0]
    pop {lr}
    mov pc, lr

    @ r11=goal
enqueue:
    ldr r0, [r8, #13 * 4]     @ tcb->qend
    str r11, [r0]
    add r0, r0, #4
    ldr r3, [r8, #12 * 4]     @ tcb->qstart
    cmp r0, r3
    bne l40
    ldr r1, =fl_nil
    ldr r0, [r0]
    mov r2, #7                  @ FL_QUEUE_FULL
    mov r0, r8
    b fl_rt_error
l40:
    ldr r5, [r8, #15 * 4]     @ tcb->max_goals
    lsl r5, r5, #2
    ldr r6, [r8, #14 * 4]     @ tcb->queue
    add r5, r5, r6
    cmp r0, r5
    blo l41
    ldr r0, [r8, #14 * 4]     @ tcb->queue
l41:
    str r0, [r8, #13 * 4]     @ tcb->qend
    ldr r5, [r8, #10 * 4]     @ tcb->active
    add r5, r5, #1
    str r5, [r8, #10 * 4]    @ tcb->active
    mov pc, lr

    .global fl_opengoal
    @ -> r11
fl_opengoal:
    ldr r11, [r8, #16 * 4]     @ tcb->freegoals
    cmp r11, #0
    bne lg1
    mov r2, #26                  @ FL_TOO_MANY_GOALS
    mov r0, r8
    b fl_rt_error
lg1:
    ldr r5, [r11]               @ goal->addr
    ldr r0, [r10, #6 * 4]       @ goal->task
    push {lr}
    bl fl_addref
    pop {lr}
    str r0, [r11, #6 * 4]        @ goal->task
    str r5, [r8, #16 * 4]      @ tcb->freegoals
    ldr r5, [r8, #21 * 4]    @ tcb->goals
    add r5, r5, #1
    str r5, [r8, #21 * 4]    @ tcb->goals
    b enqueue

    .global fl_dropgoal
fl_dropgoal:
    ldr r5, [r8]               @ tcb->goal
    ldr r6, [r8, #16 * 4]     @ tcb->freegoals
    str r6, [r5]               @ goal->addr
    str r5, [r8, #16 * 4]      @ tcb->freegoals
    ldr r0, [r8, #21 * 4]    @ tcb->goals
    sub r0, r0, #1
    str r0, [r8, #21 * 4]    @ tcb->goals
    mov r0, #0
    str r0, [r5, #5 * 4]     @ goal->info
    ldr r6, =fl_nil
    ldr r6, [r6]
    ldr r0, [r5, #6 * 4]     @ goal->task
    str r6, [r5, #6 * 4]     @ goal->task
    mov r5, r0
    b fl_unref

    .global fl_addref
    @ r0=val
    @ may not change any registers
fl_addref:
    tst r0, #3
    beq l42
    mov pc, lr
l42:
    push {r5, r6}
    ldr r6, =0x00ffffff     @ FL_COUNT_MASK
    ldr r5, [r0]
    and r5, r5, r6
    cmp r5, r6
    beq l42a
    ldr r5, [r0]
    add r5, r5, #1
    str r5, [r0]
l42a:
    pop {r5, r6}
    mov pc, lr

    .global fl_puttailarg
    @ r0=list
fl_puttailarg:
    ldr r5, [r0, #8]
    tst r5, #3
    beq l43
    ldr r0, [r0, #4]
l43:
    push {lr}
    bl fl_addref
    str r0, [r11, #4 * 4]   @ goal->args[ 3 ]
    pop {lr}
    mov pc, lr

    .global fl_reify
    @ replace cdr of pair with deref'd element
    @ r0=pair
fl_reify:
    push {lr}
    bl fl_deref
    push {r0}
    ldr r0, [r0, #8]
    push {r0}
    bl fl_deref
    bl fl_addref
    pop {r5}
    bl fl_unref
    pop {r5}
    str r0, [r5, #8]
    pop {lr}
    mov pc, lr

    .global fl_resolve
    @ r4=cache, r1=arity, r2=module, r3=name
fl_resolve:
    ldr r5, [r4]
    cmp r5, #0
    beq l44
    str r5, [r11]        @ goal->addr
    mov pc, lr
l44:
    mov r0, r8
    push {r11}
    push {r4}
    b fl_resolve_pdef

    .global fl_replace
    @ r0=val, r2=argaddr
fl_replace:
    ldr r5, [r2]
    cmp r0, r5
    bne l45
    mov pc, lr
l45:
    ldr r5, [r2]
    push {lr}
    bl fl_addref
    pop {lr}
    str r0, [r2]
    b fl_unref

    .global fl_tailgoal
    @ r2=addr -> r11(N)
fl_tailgoal:
    ldr r3, [r8, #8] @ tcb->tailcalls
    ldr r1, [r8, #12] @ tcb->timeslice
    cmp r3, r1
    blt l_tg_1
    push {r2, lr}
    bl fl_opengoal
    pop {r2, lr}
    str r2, [r11]      @ goal->addr
    mov pc, lr
l_tg_1:
    mov r11, r10
    str r2, [r11]      @ goal->addr
    mov pc, lr

    .global fl_tail_jump
    @ r7=frame
fl_tail_jump:
    ldr r3, [r8, #8] @ tcb->tailcalls
    ldr r1, [r8, #12] @ tcb->timeslice
    cmp r3, r1
    blt l46
    mov r0, #0
    str r0, [r8, #8] @ tcb->tailcalls
    mov pc, lr
l46:
    add r3, r3, #1
    str r3, [r8, #8] @ tcb->tailcalls
    @ fall through ...

    .global fl_invoke_jump
    @ r7=frame
fl_invoke_jump:
    mov sp, r7
    mov r10, r11
    str r11, [r8]      @ tcb->goal
    ldr r0, [r11]      @ goal->addr
    mov pc, r0

    .global fl_test_integer
    @ r1=val -> r0 (bool)
fl_test_integer:
    tst r1, #1
    bne true
    b false

    .global fl_test_number
    @ r1=val -> r0 (bool)
fl_test_number:
    tst r1, #1
    bne true
    mov r5, #0x02000000      @ FL_FLOAT_TAG
    b fl_test_tag

    .global fl_test_atom
    @ r1=val -> r0 (bool)
fl_test_atom:
    mov r0, r1
    and r0, r0, #3
    cmp r0, #2
    bne false
    b true

    .global fl_test_tag
    @ r1=val, r5=tag -> r0 (bool)
fl_test_tag:
    tst r1, #3
    bne false
    ldr r0, [r1]
    mov r6, #0xff000000 @ FL_TAG_MASK
    and r0, r0, r6
    cmp r0, r5
    bne false
    b true

    .global fl_test_var
    @ r1=val -> r0 (bool)
fl_test_var:
    tst r1, #3
    bne false
    ldr r0, [r1]
    mov r5, #0xff000000 @ FL_TAG_MASK
    and r0, r0, r5
    mov r5, #0x07000000 @ FL_REF_TAG
    cmp r0, r5
    beq true
    mov r5, #0x04000000 @ FL_VAR_TAG
    cmp r0, r5
    bne false
    ldr r0, [r1, #4]
    cmp r0, r1
    bne false
    b true

    .global fl_test_remote
    @ r1=val -> r0 (bool)
fl_test_remote:
    tst r1, #3
    bne false
    ldr r0, [r1]
    mov r5, #0xff000000 @ FL_TAG_MASK
    and r0, r0, r5
    mov r5, #0x07000000 @ FL_REF_TAG
    cmp r0, r5
    beq true
    mov r5, #0x05000000 @ FL_PORT_TAG
    cmp r0, r5
    bne false
    ldr r0, [r1, #4]
    tst r0, #1
    bne true
    b false

    .global fl_test_tuple
    @ r1=val -> r0 (bool)
fl_test_tuple:
    tst r1, #3         @ FL_BITS_MASK
    bne false
    ldr r0, [r1]
    mov r5, #0x80000000 @ FL_TUPLE_BIT
    tst r0, r5
    beq false
    b true

    .global fl_test_struct
    @ r1=val, r2=name, r3=arity -> r0 (bool)
fl_test_struct:
    tst r1, #3         @ FL_BITS_MASK
    bne false
    ldr r0, [r1]
    lsr r5, r0, #24
    and r5, r5, #0xff
    lsr r3, r3, #1
    orr r3, r3, #0x80	 @ FL_TUPLE_BIT
    cmp r5, r3
    bne false
    ldr r0, [r1, #4]
    cmp r0, r2
    bne false
    b true

    .global fl_test_array
    @ r1=val -> r0 (bool)
fl_test_array:
    tst r1, #3         @ FL_BITS_MASK
    bne false
    ldr r0, [r1]
    mov r5, #0xff000000     @ FL_TAG_MASK
    and r0, r0, r5
    mov r5, #0x08000000     @ FL_ARRAY_TAG
    cmp r0, r5
    bne false
    b true

    .global fl_test_kl1string
    @ r1=val -> r0 (bool)
fl_test_kl1string:
    tst r1, #3         @ FL_BITS_MASK
    bne false
    ldr r0, [r1]
    mov r5, #0xff000000     @ FL_TAG_MASK
    and r0, r0, r5
    mov r5, #0x08000000     @ FL_ARRAY_TAG
    cmp r0, r5
    bne false
    ldr r0, [r1, #4]
    ldr r0, [r0, #8]
    and r0, r0, #0x7
    cmp r0, #6			@ FL_KL1_STRING
    bne false
    b true

    .global fl_test_vector
    @ r1=val -> r0 (bool)
fl_test_vector:
    tst r1, #3         @ FL_BITS_MASK
    bne false
    ldr r0, [r1]
    mov r5, #0xff000000     @ FL_TAG_MASK
    and r0, r0, r5
    mov r5, #0x03000000     @ FL_VECTOR_TAG
    cmp r0, r5
    bne false
    b true

    .global fl_enter
    @ r0=tcb
fl_enter:
    mov r8, r0
    ldr sp, [r8, #5 * 4]  @ tcb->stackbase
    mov r7, sp
    ldr r10, [r8]      @ tcb->goal
    ldr r0, [r10]      @ goal->addr
    mov pc, r0

    .global fl_atomic
    @ r1=val -> r0 (bool)
fl_atomic:
    tst r1, #3         @ FL_BITS_MASK
    bne true
    ldr r0, [r1]
    mov r5, #0xff000000     @ FL_TAG_MASK
    and r0, r0, r5
    mov r5, #0x02000000     @ FL_FLOAT_TAG
    cmp r0, r5
    bne false
    b true

    .global fl_atomic_nonfloat
    @ r1=val -> r0 (bool)
fl_atomic_nonfloat:
    tst r1, #3         @ FL_BITS_MASK
    bne true
    b false

    .global fl_entry
    @ rsi=arity
fl_entry:
    ldr r5, =fl_nil
    ldr r5, [r5]
    ldr r0, [r10, #6 * 4]    @ goal->task
    cmp r0, r5
    beq l50a
    ldr r0, [r0, #4]
    ldr r0, [r0, #4]
    ldr r0, [r0, #4]
    cmp r0, r5
    bne l50
l50a:
    ldr r0, [r8, #20 * 4]     @ tcb->logging
    tst r0, #1
    bne l50
    mov pc, lr
l50:
    mov r0, r8
    b fl_log_entry

    .global fl_assign0
    @ r1=val, r2=val -> r0 (bool)
    @ must preserve P0-P3
fl_assign0:
    push {r1, r2, r3, lr}
    mov r0, r8
    bl fl_assign
    pop {r1, r2, r3, lr}
    mov pc, lr

    .global fl_d_assign0
    @ r1=val, r2=val -> r0 (bool)
    @ must preserve P0-P3
fl_d_assign0:
    push {r1, r2, r3, lr}
    mov r0, r8
    bl fl_d_assign
    pop {r1, r2, r3, lr}
    mov pc, lr

    .global fl_unify
    @ r1=val, r2=val -> r0 (bool)
    @ must preserve P0-P3
fl_unify:
    push {r1, r2, r3, lr}
    mov r0, r8
    bl fl_unify_rec
    pop {r1, r2, r3, lr}
    mov pc, lr

    .global fl_d_unify
    @ r1=val, r2=val -> r0 (bool)
    @ must preserve P0-P3
fl_d_unify:
    push {r1, r2, r3, lr}
    mov r0, r8
    bl fl_d_unify_rec
    pop {r1, r2, r3, lr}
    mov pc, lr

    .global fl_unify_safe
    @ r1=val, r2=val -> r0 (bool)
    @ must preserve P0-P3
fl_unify_safe:
    push {r1, r2, r3, lr}
    mov r0, r8
    bl fl_unify_safe1
    pop {r1, r2, r3, lr}
    mov pc, lr

    .global fl_unify_checked
    @ r1=val, r2=val
    @ must preserve P0-P3
fl_unify_checked:
    push {r1, r2, r3, lr}
    mov r0, r8
    bl fl_unify_rec_checked
    pop {r1, r2, r3, lr}
    mov pc, lr

    .global fl_d_unify_checked
    @ r1=val, r2=val
    @ must preserve P0-P3
fl_d_unify_checked:
    push {r1, r2, r3, lr}
    mov r0, r8
    bl fl_d_unify_rec_checked
    pop {r1, r2, r3, lr}
    mov pc, lr

    .global fl_lookup
    @ A, X0: table [count, int, label, ..., label]
fl_lookup:
    mov r1, r5
    ldr r6, [r5], #4
llok1:
    ldr r4, [r5]
    cmp r0, r4
    beq llok2
    add r5, r5, #8
    subs r6, r6, #1
    bne llok1
    sub r5, r5, #4
llok2:
    ldr r5, [r5, #4]
    add pc, r1, r5

    .global fl_goto
    @ A, X0: table [min, max*, flabel, label1, ...]
    @ max: mark bit is 0
fl_goto:
    mov r1, r5
    ldr r6, [r5]
    sub r0, r0, r6
    ldr r6, [r5, #4]
    cmp r0, r6
    bhi lgoto1
    add r0, r0, #6
    lsl r0, r0, #1
    add r0, r0, r5
    ldr r0, [r0]
    add pc, r0, r5
lgoto1:
    ldr r0, [r5, #8]
    add pc, r0, r5

    .global fl_prepare_c_arg
    @ A: Arg
fl_prepare_c_arg:
    tst r0, #2      @ FL_ATOM_BIT
    beq lprep1
    add r0, r0, #1
    mov pc, lr
lprep1:
    ldr r5, [r0]
    and r5, r5, #0xff000000 @ FL_TAG_MASK
    mov r6, #0x08000000 @ FL_ARRAY_TAG
    cmp r5, r6
    bne lprep2
    ldr r0, [r0, #4]
    ldr r0, [r0, #4]
    mov pc, lr
lprep2:
    add r0, r0, #4
    mov pc, lr

    .global fl_new_task
    @ G, r8=tcb, A = status(var)
fl_new_task:
    push {lr}
    mov r1, r0          @ save status in P0
    ldr r5, =fl_nil
    ldr r5, [r5]
    ldr r0, [r10, #6*4]   @ goal->task
    cmp r0, r5
    bne new_task_l1
    @ cons new env/parent cell [[]|[]]]
    bl cons
    mov r6, #0x01000000   @ FL_LIST_TAG
    str r6, [r0]
    mov r5, r0
    b new_task_l2
new_task_l1:
    ldr r0, [r0, #4]
    ldr r5, [r0, #4]            @ env
new_task_l2:
    ldr r0, [r10, #6*4]            @ goal->task (parent)
    bl cons
    mov r5, r0
    mov r0, r1
    bl cons                       @ task: [env/parent|st]
    mov r6, #0x06000000         @ FL_TASK_TAG|1
    orr r6, r6, #1
    str r6, [r0]
    ldr r5, [r10, #6*4]            @ goal->task (old)
    bl fl_unref
    str r0, [r10, #6*4]            @ goal->task
    pop {lr}
    mov pc, lr

    .global fl_box_int_ref
    .global fl_box_long_ref
    @ A = box -> A
fl_box_int_ref:
fl_box_long_ref:
    ldr r5, [r0]
    and r5, r5, #0xff000000 @ FL_TAG_MASK
    mov r6, #0x08000000 @ FL_ARRAY_TAG
    cmp r5, r6
    bne lbir1
    ldr r0, [r0, #4]
    ldr r0, [r0, #4]
lbir1:
    ldr r0, [r0, #4]
fix:
    lsl r0, r0, #1
    orr r0, #1
    mov pc, lr

    .global fl_box_char_ref
    @ A = box -> A
fl_box_char_ref:
    ldr r5, [r0]
    and r5, r5, #0xff000000 @ FL_TAG_MASK
    mov r6, #0x08000000 @ FL_ARRAY_TAG
    cmp r5, r6
    bne lbbr1
    ldr r0, [r0, #4]
    ldr r0, [r0, #4]
lbbr1:
    ldrb r0, [r0, #4]
    b fix

    .global fl_box_short_ref
    @ A = box -> A
fl_box_short_ref:
    ldr r5, [r0]
    and r5, r5, #0xff000000 @ FL_TAG_MASK
    mov r6, #0x08000000 @ FL_ARRAY_TAG
    cmp r5, r6
    bne lbsr1
    ldr r0, [r0, #4]
    ldr r0, [r0, #4]
lbsr1:
    ldrsh r0, [r0, #4]
    b fix

    .global fl_box_double_ref
    @ T, A = box -> A
fl_box_double_ref:
    ldr r5, [r0]
    and r5, r5, #0xff000000 @ FL_TAG_MASK
    mov r6, #0x08000000 @ FL_ARRAY_TAG
    cmp r5, r6
    bne lbdr1
    ldr r0, [r0, #4]
    ldr r0, [r0, #4]
lbdr1:
    vldr d0, [r0, #4]
    b fl_mkfloat

    .global fl_factset
    @ r1=arity, r2=tree
    @ must preserve P0-P3
fl_factset:
    push {r1, r2, r3, lr}
    mov r0, r8
    bl fl_match_facts
    pop {r1, r2, r3, lr}
    tst r0, #1
    beq setcarry
    b clrcarry

    .global fl_candidate
    @ T -> set carry if pre-selecting fair choice candidate
fl_candidate:
    ldr r0, [r8, #24*4]		@ tcb->candidates
    ldr r5, [r8, #25*4]		@ tcb->selected
    tst r5, #1
    bne lcand1
    @ case 1: the clause is a possible candidate (selected == 0)
    add r0, r0, #1		@ ++tcb->candidates
    str r0, [r8, #24*4]
    b setcarry			@ skip
lcand1:
    cmp r0, #0
    bne lcand2
    @ case 2: the clause is the selected candidate (selected == 1, candidates == 0)
    str r0, [r8, #25*4]		@ tcb->selected = 0
    b clrcarry				@ execute candidates
lcand2:
    @ case 3: the clause not the selected candidate (selected == 1, candidates > 0)
    ldr r6, [r8, #24*4]		@ --tcb->candidates
    sub r6, r6, #1
    str r6, [r8, #24*4]
    b setcarry

    .global fl_select
    @ T, X0=cell address
    @ clobbers P0, P1
fl_select:
    ldr r1, [r8, #24*4]			@ tcb->candidates
    cmp r1, #0
    bne lsel1
    mov pc, lr					@ no candidates, normal suspend
lsel1:
    mov r6, #1
    str r6, [r8, #25*4]			@ tcb->selected = 1
    ldr r6, [r5]
    cmp r6, #0
    bne lsel2
    add r6, r6, #1
lsel2:
    lsr r4, r6, #8
    add r0, r6, r4
    push {r5, r6}
    bl divmod
    pop {r5, r6}
    ldr r0, =1664525			@ LCG
    mul r6, r6, r0
    ldr r0, =1013904223
    add r6, r6, r0
    str r6, [r5]					@ seed = (1664525 * seed) + 1013904223
    str r3, [r8, #24*4]			@ tcb->candidates = tcb->candidates % (<cell> + 1)
    ldr r0, [r10]					@ goal->addr
    mov pc, r0					@ reenter goal
