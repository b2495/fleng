% FLENG compiler - local analysis

% analyze primitive expressions and collect types + temporaries
%
% builds an environment holding variable information, where the following
% elements are defined:
%
%   temp(VARNUM)        temporary result variable
%   type(VARNUM, TYPE)  variable hold value of known type

analyze((X, Y), Bound1, Bound, Env1, Env) :-
    analyze(X, Bound1, Bound2, Env1, Env2),
    analyze(Y, Bound2, Bound, Env2, Env).
analyze(foreign_call(Term), B1, B, E1, E) :-
    callable(Term),
    Term =.. [_|Args],
    analyze_args(Args, any, Term, B1, B, E1, E).
analyze('$assign'(X, '$VAR'(V)), B, [V|B], E, [type(V, T)|E]) :-
    literal_type(X, T).
analyze(Term, B1, [R|B], E1, E) :-
    Term = compute(Op, X, Y, '$VAR'(R)),
    computation(Op, [TA, TB, TR]),
    immediate_result_temp(TR, R, [type(R, TR)|E1], E2),
    analyze_args([Op, X, Y], [a, TA, TB], Term, B1, B, E2, E),
    \+memberchk(R, B).
analyze(Term, B1, B, E1, E) :-
    Term = compute(Op, X, Y, R),
    analyze_args([Op, X, Y, R], [a, x, x, x], Term, B1, B, E1, E).
analyze(Term, B1, [R|B], E1, E) :-
    Term = compute(Op, X, '$VAR'(R)),
    computation(Op, [TA, TR]),
    immediate_result_temp(TR, R, [type(R, TR)|E1], E2),
    analyze_args([Op, X], [a, TA], Term, B1, B, E2, E),
    \+memberchk(R, B).
analyze(Term, B1, B, E1, E) :-
    Term = compute(Op, X, R),
    analyze_args([Op, X, R], [a, x, x], Term, B1, B, E1, E).
analyze(Term, B, [R|B], E, [temp(R), type(R, a)|E]) :-
    Term = '$idle'('$VAR'(R)).
analyze(T, Bound1, Bound, Env1, Env) :-
    T =.. [N|Args],
    length(Args, A),
    primitive(N/A, _, R, Types),
    analyze_primitive(R, Args, Types, T, Bound1, Bound, Env1, Env).
analyze(_, B, B, E, E).

analyze_primitive(result, Args, Types, Term, B1, [R|B], E1, E) :-
    split_args(Args, Args2, '$VAR'(R)),
    split_args(Types, _, RT),
    immediate_result_temp(RT, R, [type(R, RT)|E1], E2),
    analyze_args(Args2, Types, Term, B1, B, E2, E),
    \+memberchk(R, B).
analyze_primitive(_, Args, Types, Term, B1, B, E1, E) :-
    analyze_args(Args, Types, Term, B1, B, E1, E).

analyze_args(Args, Types, Term, B1, B, E1, E) :-
    analyze_args(Args, Types, 1, Term, B1, B, E1, E).

analyze_args([], _, _, _, B, B, E, E).
analyze_args(Args, any, Pos, Term, B1, B, E1, E) :-
    analyze_args(Args, [x|any], Pos, Term, B1, B, E1, E).
analyze_args([X|Args], [T|Types], Pos, Term, B1, B, E1, E) :-
    check_type(X, T, Pos, Term, E1),
    !,
    analyze_arg(X, B1, B2),
    Pos2 is Pos + 1,
    analyze_args(Args, Types, Pos2, Term, B2, B, E1, E).

analyze_arg(A, B, B) :- atomic(A), !.
analyze_arg('$VAR'(I), Bound, [I|Bound]) :- integer(I), !.
analyze_arg([T1|As], Bound1, Bound) :-
    !,
    analyze_arg(T1, Bound1, Bound2),
    analyze_arg(As, Bound2, Bound).
analyze_arg(T, Bound1, Bound) :-
    T =.. [_|Args],
    !,
    analyze_arg(Args, Bound1, Bound).
analyze_arg(_, B, B).

split_args([R], [], R).
split_args([X|L], [X|L2], R) :-
    split_args(L, L2, R).

literal_type(I, i) :- integer(I), !.
literal_type(F, f) :- float(F), !.
literal_type([], a) :- !.
literal_type(S, a) :- atom(S), !.
literal_type(T, t) :- callable(T), !.

check_type(A, T, Pos, Term, _) :-
    atom(A),
    check_type2(a, T, Pos, Term).
check_type(N, T, Pos, Term, _) :-
    integer(N),
    check_type2(i, T, Pos, Term).
check_type(N, T, Pos, Term, _) :-
    float(N),
    check_type2(n, T, Pos, Term).
check_type('$VAR'(V), T, Pos, Term, E) :-
    memberchk(type(V, T2), E),
    check_type2(T, T2, Pos, Term).
check_type(_, _, _, _, _).

check_type2(T, T, _, _) :- !.
check_type2(n, i, _, _) :- !.
check_type2(n, f, _, _) :- !.
check_type2(i, n, _, _) :- !.
check_type2(f, n, _, _) :- !.
check_type2(x, _, _, _) :- !.
check_type2(_, x, _, _) :- !.
check_type2(T1, T2, Pos, Term) :-
    type_name(T1, TN1),
    type_name(T2, TN2),
    format(user_error, 'Warning: argument #~d in term `~q\' has type ~a, but was expected to be of type ~a\n', [Pos, Term, TN2, TN1]).

immediate_result_temp(T, V, E1, [temp(V)|E1]) :- immediate_result(T), !.
immediate_result_temp(_, _, E, E).

immediate_result(a).
immediate_result(i).

% tables

type_name(a, atom).
type_name(i, integer).
type_name(t, tuple).
type_name(n, number).
type_name(c, 'tuple or list').
type_name(l, 'list').
type_name(v, variable).
type_name(f, float).
type_name(p, port).
type_name(m, module).

computation(+, [n, n, n]).
computation(-, [n, n, n]).
computation(*, [n, n, n]).
computation(/, [n, n, n]).
computation(mod, [i, i, i]).
computation(and, [i, i, i]).
computation(or, [i, i, i]).
computation(xor, [i, i, i]).
computation(shl, [i, i, i]).
computation(shr, [i, i, i]).
computation(=, [x, x, a]).
computation(<, [n, n, a]).
computation(>, [n, n, a]).
computation(max, [n, n, n]).
computation(min, [n, n, n]).
computation(sametype, [x, x, a]).
computation(**, [n, n, f]).
computation(sqrt, [n, f]).
computation(sin, [n, f]).
computation(cos, [n, f]).
computation(tan, [n, f]).
computation(asin, [n, f]).
computation(acos, [n, f]).
computation(atan, [n, f]).
computation(log, [n, f]).
computation(exp, [n, f]).
computation(sign, [n, i]).
computation(rnd, [i, i]).
computation(abs, [n, n]).
computation(real_integer_part, [n, n]).
computation(real_fractional_part, [n, n]).
computation(integer, [n, i]).
computation(real, [n, f]).
computation(truncate, [n, f]).
computation(floor, [n, f]).
computation(round, [n, f]).
computation(ceiling, [n, f]).

primitive('$above'/3, above, result, [x, x, a]).
primitive('$above_or_equal'/3, above_or_equal, result, [x, x, a]).
primitive('$assign'/2, assign, none, [x, v]).
primitive('$atomic'/2, atomic, result, [x, a]).
primitive('$below'/3, below, result, [x, x, a]).
primitive('$below_or_equal'/3, below_or_equal, result, [x, x, a]).
primitive('$error'/1, error, none, [x]).
primitive('$var'/2, test_var, result, [x, a]).
primitive('$remote'/2, test_remote, result, [x, a]).
primitive('$unify'/3, unify, result, [x, x, a]).
primitive('$unify_safe'/3, unify_safe, result, [x, x, a]).
primitive('$unify_checked'/2, unify_checked, none, [x, x]).
primitive('$integer'/2, test_integer, result, [x, a]).
primitive('$float'/2, test_float, result, [x, a]).
primitive('$array'/2, test_array, result, [x, a]).
primitive('$kl1string'/2, test_kl1string, result, [x, a]).
primitive('$vector'/2, test_vector, result, [x, a]).
primitive('$number'/2, test_number, result, [x, a]).
primitive('$tuple'/2, test_tuple, result, [x, a]).
primitive('$atom'/2, test_atom, result, [x, a]).
primitive('$match'/3, match, result, [x, x, a]).
primitive('$list'/2, test_list, result, [x, a]).
primitive('$module'/2, test_module, result, [x, a]).
primitive('$port'/2, test_port, result, [x, a]).
