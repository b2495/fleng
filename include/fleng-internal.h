/* FLENG - system- and configuration-specific wrappers */

#ifndef FLENG_INTERNAL_H
#define FLENG_INTERNAL_H

#include "fleng-config.h"

#ifdef HAVE_THREADS

#include <pthread.h>

#define DECLARE_MUTEX(m)    static pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER

#define LOCK            pthread_mutex_lock
#define UNLOCK          pthread_mutex_unlock
#define JOIN(t)         pthread_join((pthread_t)(t), NULL)

#define THREAD_INIT(t) \
    pthread_attr_t _attr; \
    pthread_attr_init(&_attr); \
    pthread_attr_setstacksize(&_attr, T_STACK_SIZE); \
    pthread_attr_setdetachstate(&_attr, PTHREAD_CREATE_JOINABLE)
#define THREAD_CREATE(t, p, a)     pthread_create((pthread_t *)(t), &_attr, p, a)
#define THREAD_EXIT		pthread_exit(NULL)
#define SIGMASK     pthread_sigmask

#define COMPARE_AND_SWAP    __sync_bool_compare_and_swap
#define ADD_AND_FETCH       __sync_add_and_fetch
#define SUB_AND_FETCH       __sync_sub_and_fetch
#define SYNCHRONIZE         __sync_synchronize()

#if defined(__APPLE__) && defined(__MACH__)
# define THREAD_CLOCK_ID(t, id)   *(id) = CLOCK_MONOTONIC /*A BIG LIE*/
#else
# define THREAD_CLOCK_ID(t, id)   pthread_getcpuclockid((pthread_t)(t), id)
#endif

#else

/* no threads */
#define DECLARE_MUTEX(m)

#define LOCK(m)
#define UNLOCK(m)
#define JOIN(t)

#define THREAD_INIT(t)
#define THREAD_CREATE(t, p, a)
#define THREAD_EXIT		exit(0)

#define SIGMASK    sigprocmask

#define COMPARE_AND_SWAP(l, o, n)   1
#define ADD_AND_FETCH(p, n)     (*(p) += (n))
#define SUB_AND_FETCH(p, n)     (*(p) -= (n))
#define SYNCHRONIZE

#define THREAD_CLOCK_ID(t, id)   *(id) = CLOCK_MONOTONIC

#endif

#endif
