/* build system as described in the book
   "Concurrent Programming in ML" by John Reppy,
   translated from CML into PCN

   takes a makefile with the following format:

   <target> : <dependency> ...
   <cmdline>
   :

   whitespace between ":" is mandatory, filenames for targets and
   dependencies are whitespace-delimited. The cmdline is always is
   limited to a single line
*/

main(_, argv, _)
{?
    argv ?= [_, mf, root] ->
        {|| parse_makefile(mf, rules),
            make(rules, root, signalch, rootch),
            signalch = [true],
            wait(rootch) },
    default -> error("usage: cbuild FILENAME ROOT")
}

wait(ch)
{?
    ch ?= [r|_] ->
        {; writeln(r),
           if(r == "error") halt(1)
           else halt(0) }
}

make(rules, root, signalch, rootch)
{||
    make_graph(signalch, root, rules, rootch)
}

make_graph(signalch, root, rules, ch)
{||
    insert_targets(rules, [], t1),
    insert_nds(rules, signalch, t1, t2),
    map:lookup(root, t2, r),
    if(r ?= {"def", ch1}) ch = ch1
    else error("missing root definition")
}

insert_targets(rules, m1, m)
{?
    rules ?= [{t, al, cmd}|rules2] ->
        {|| map:insert(t, {"undef", {t, al, cmd}}, m1, m2),
            insert_targets(rules2, m2, m) },
    default -> m = m1
}

insert_nds(rules, signalch, m1, m)
{?
    rules ?= [{t, _, _}|rules2] ->
        {|| ins_nd(t, signalch, m1, m2),
            insert_nds(rules2, signalch, m2, m) },
    default -> m = m1
}

ins_nd(target, signalch, m1, m)
{||
    map:lookup(target, m1, t),
    {? t ?= {"def", _} -> {},
       t == "mark" -> error({"cyclic dependency", target}),
       t ?= {"undef", nd} -> add_nd(nd, signalch, m1, m),
       default -> add_leaf(target, signalch, m1, m) }
}

add_leaf(target, signalch, m1, m)
{||
    make_leaf(signalch, target, [], ch),
    map:insert(target, {"def", ch}, m1, m)
}

add_nd(nd, signalch, m1, m)
{?
    nd ?= {t, [], cmd} ->
        {|| make_leaf(signalch, t, cmd, ch),
            map:insert(t, ch, m1, m) },
    nd ?= {t, al, cmd}, al != [] ->
        {|| map:insert(t, "mark", m1, m2),
            app:foldl(`(t, m1, m) -> ins_nd(t, signalch, m1, m), al, m2, m3),
            app:maplist(`(t, r) -> get_nd(t, m3, r), al, chs),
            make_node(t, chs, cmd, ch),
            map:insert(t, {"def", ch}, m3, m) }
}

get_nd(objname, m, ch)
{||
    map:lookup(objname, m, nd),
    if(nd ?= {"def", ch1}) ch = ch1
    else error({"missing definition", objname})
}

make_node(t, a_chs, cmd, ch)
{||
    recv_stamps(a_chs, a_chs2, 0, max),
    {? max == "error" ->
        {|| ch = ["error"|ch2],
            make_node(t, a_chs2, cmd, ch2) },
       default ->
            {; mt = file_status(t),
               {? mt == false -> do_action(t, cmd, ch, ch2),
                  int(mt), mt <= max -> do_action(t, cmd, ch, ch2),
                  default -> ch = [mt|ch2] },
               make_node(t, a_chs2, cmd, ch2) }
    }
}

recv_stamps(chs, chs_out, max1, max)
{?
    chs ?= [[stamp|tail]|chs2] ->
        {|| chs_out = [tail|chs_out2],
            {? stamp == "error" -> max = "error",
               int(stamp), stamp > max1 ->
                    recv_stamps(chs2, chs_out2, stamp, max),
               default -> recv_stamps(chs2, chs_out2, max1, max) }
        },
    default -> {|| chs_out = [], max = max1 }
}

do_action(t, cmd, ch1, ch)
{||
    if(cmd != []) {;
        fmt:format("  ~s\n", [cmd]),
        proc:shell(cmd, status)
    } else status = 0,
    {? status == 0 ->
            {|| file_modification_time(t, mt),
                ch1 = [mt|ch] },
       default -> {; fmt:format(2, "error making \"~s\"\n", [t]),
                     ch1 = ["error"|ch] }
    }
}

function file_status(fname)
{||
    file_exists(fname, exists),
    {? exists == true ->
        {|| file_modification_time(fname, mtime),
            return(mtime) },
       default -> return(false) }
}

make_leaf(signalch, target, cmd, ch)
{?
    signalch ?= [_|sigch2] ->
        {|| do_action(target, cmd, ch, ch2),
            make_leaf(sigch2, target, cmd, ch2) },
    default -> ch = []
}

parse_makefile(fname, rules)
{;
    open_file(fname, "r", file),
    io:read_lines(file, lines),
    parse_rules(lines, fname, 1, rules),
    close_file(file)
}

parse_rules(lines, fname, n, rules)
{?
    lines ?= [line|more] -> parse_rule(line, more, fname, n, rules),
    default -> rules = []
}

parse_rule(line, lines, fname, n, rules)
{||
    scan:whitespace(line, l2),
    scan:word(t, l2, l3),
    list_to_string(t, ts),
    scan:whitespace(l3, l4),
    if(l4 ?= [':'|rest]) {;
        parse_deps(rest, al),
        parse_cmd(lines, lines2, fname, n + 1, n2, cmd),
        rules = [{ts, al, cmd}|rules2],
        parse_rules(lines2, fname, n2, rules2)
    } else {;
        fmt:format(2, "~s:~d: expected \":\"\n", [fname, n]),
        error("failed")
    }
}

parse_deps(line, al)
{||
    scan:whitespace(line, line2),
    if(line2 == []) al = []
    else {||
        scan:word(dep, line2, line3),
        list_to_string(dep, deps),
        al = [deps|al2],
        parse_deps(line3, al2)
    }
}

parse_cmd(lines1, lines, fname, n1, n, cmd)
{?
    lines1 ?= [cmdl|lines2] ->
        {|| lines = lines2,
            n = n1 + 1,
            cmd = cmdl },
    default -> {; fmt:format(2, "~s:~d: expected command\n", [fname, n1]),
                  error("failed") }
}
