.Dd Dec 20, 2022
.Dt FLENGMAKE 1
.Os
.Sh NAME
.Nm flengmake
.Nd A build tool for programs written in FGHC, Strand, PCN or FLENG
.Sh SYNOPSIS
.Nm
.Op Ar [MODE]
.Op Ar OPTION
.Op Ar ARGUMENT ...
.Sh DESCRIPTION
.Nm
is a tool to build projects consisting of programs and libraries implemented
in one or more languages supported by the FLENG compiler. The tool
assumes a certain convention of structuring code and build executables
automatically using the minimal number of build steps.
.Pp
.Ar MODE
can be either
.Qq clean
to remove all build artifacts,
.Qq show
to generate
a graph suitable for visualization via
.Xr dot 1
or
.Qq build
(the default) to rebuild one or more parts of the project, if
necessary.
.Ar OPTION
should be one of the following:
.Pp
.Bl -tag -width Ds
.It Fl h
Show usage information and exit.
.It --version
Show FLENG system version and exit.
.It Fl v
Be verbose, show explanations for the build steps taken.
.It Fl n
Do not actually rebuild, just show the steps that would be taken.
.It Fl r Ar DIRECTORY
Specifies the root directory of the project. If this option is not given,
the current working directory
is assumed to be the root directory. Note that the actual working
directory is not changed.
.It Fl OPTION VALUE
Set a project-wide build option to the value given. See below for more
information about build options.
.Pp
.El
.Sh PRINCIPLE OF OPERATION
.Nm
traverses the root directory and locates any source files with
the extensions
.Qq .ghc
,
.Qq .fghc
,
.Qq .strand
,
.Qq .st
,
.Qq .pcn
,
.Qq .kl1
,
.Qq .fl
and
.Qq .fleng
assuming these to be code written in one of the languages supported
by the FLENG system. The code is then scanned and module-uses and
uses of included files are analyzed to infer dependencies between
the different modules and programs, resulting in an internal list of
targets.
.Nm
knows the following types of targets:
.Bl -tag -width Ds
.It programs
Top-level programs that compile to an executable. A file is considered
to represent a program if it has no.
.Qq module
declaration and is not included in any
other file in the project.
.It modules
Separately compiled modules used in other programs, modules and
libraries. A file is considered to represent a module if it has a
.Qq module
declaration and is not included in any other file in the project.
.It libraries
A collection of modules linked into a static library. A directory is considered
to represent a library if all source files in the directory contain a
.Qq module
declaration or are included in other files.
.It include files
Files included in modules or programs.
.El
.Pp
A project can have multiple programs and libraries. Programs are
named after the basename of the main source module, libraries
are named
.Qq lib<DIRECTORY>.a
, where <DIRECTORY> is the name
of the directory containing all the modules that make up a library.
This implies that library sources need to reside in a distinct directory.
.Pp
Once the root directory is analyzed, the selected operation (build, clean
or show) will be performed.
.Pp
In
.Qq build
mode, all components of the project are
compiled if the target objects do not exist, if their source files changed,
if included files or modules used by program or module have changed.
A program will be
linked with all modules in the same directory and with all libraries in
any subdirectory. After the build completes, the current state will
be written to a file named ".flengmake" in the root directory. This
state is used in subsequent runs to reduce the necessity to scan
source files repeatedly.
.Pp
In
.Qq clean
mode, all artifacts that would normally be produced by a
build are deleted should they exist.
.Pp
In
.Qq show
mode, a textual description of the dependency graph in
.Xr dot 1
format will be written to stdout.
.Pp
.Sh BUILD OPTIONS
The build operations for targets can be customized using
.Qq build options
which can be either given on the command-line or stored in
files. If a file named
.Qq options
exist, then the option given in it apply to all targets in the same
directory and any sub-directories.
Target-specific options can be defined by having a file with the
same basename as the target but the file extension
.Qq .options
in the same directory.
.Pp
Options given on the command-line apply to all targets in the project
and are not retained between invocations of
.Nm
\&.
Options in files are always given one per line, beginning with the option
name and separated by one or more whitespace characters in the remainder
of the line. Elements in the value part of an option specification may be
enclosed in single or double quotes to allow embedding whitespace
characters. Lines preceded by
.Qq #
are ignored and references of the form
.Qq $VARIABLE
or
.Qq ${VARIABLE}
perform substitution of environment variables unless enclosed in
single quotes. Use the backslash character to escape
and include dollar characters without treating them as a substitution
operation.
.Pp
The following options can be used:
.Bl -tag -width Ds
.It name TARGET SOURCE
Rename target for SOURCE to TARGET, currently only applicable to
program targets.
.It target TARGET DEPENDENCY ...
Define a custom target with the given name and files that this target
depends on.
.It depends TARGET DEPENDENCY ...
Define additional dependencies for TARGET.
.It options FLAG ...
Define compile flags for the
.Xl fleng 1
compiler driver that should be passed when compiling a module or program.
.It ignore PATTERN ...
While traversing the root directory, files matching one of the given
patterns should be ignored. The pattern syntax follows the one defined by the
.Qq match
library (enter
.Qq flengdoc match:
for more information).
.It fleng FILENAME
Override the compiler driver used for compiling modules or programs.
.It build BUILDER
Specify a custom build script instead of using the
.Xl fleng 1
compiler driver. See below for more information about custom builders.
.El
.Pp
.Sh USER DEFINED BUILDERS
Custom builders are shell scripts or programs that should be used
when building a target. The builder is invoked as
.Qq <BUILDER> <TARGET> <DEPENDENCIES> ...
and should exit with non-zero exit status in case of failure. During
execution of the build the environment variables
.Qq FLENG
is set to the default compiler driver and
.Qq FLENG_OPTIONS
to the flags that would normally be passed when compiling a target.
.Pp
.Sh ENVIRONMENT
The following environment variables change the behaviour of
.Nm Ns :
.Bl -tag -width Ds
.It Ev FLENGMAKE_FLAGS
Flags that are implicitly given on the
.Nm
command line.
.It Ev FLENGMAKE_ROOT
Holds the root directory of the current invocation of
.Nm
and so is accessible in custom build scripts.
.It Ev FLENG_PREFIX
Contains the directory where the default FLENG tools are installed.
If this variable is not set, the tools are located in the path specified
during installation.
.El
.Sh EXIT STATUS
.Ex -std
.Sh SEE ALSO
.Xr fleng 1
.Xr dot 1
.Pp
More information can be found in the User's Manual and at
.Lk http://www.call-with-current-continuation.org/fleng/fleng.html
.Sh AUTHORS
.An Felix L. Winkelmann
.Sh BUGS
Parallel builds are currently not supported.
.Pp
Submit bug reports by e-mail to
.Mt felix@call-with-current-continuation.org
