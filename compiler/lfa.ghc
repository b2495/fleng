% FLENG compiler - local analysis

% analyze primitive expressions and collect types + temporaries
%
% builds an environment holding variable information, where the following
% elements are defined:
%
%   temp(VARNUM)        temporary result variable
%   type(VARNUM, TYPE)  variable hold value of known type

-module(lfa).

analyze((X, Y), EP, Bound1, Bound, Env1, Env) :-
    analyze(X, EP, Bound1, Bound2, Env1, Env2),
    analyze(Y, EP, Bound2, Bound, Env2, Env).
analyze(foreign_call(Term), EP, B1, B, E1, E) :-
    term:functor_parts(Term, _, _, Args),
    analyze_args(Args, any, Term, EP, B1, B, E1, E).
analyze('$assign'(X, '$VAR'(_, V, _)), _, B1, B, E1, E) :-
    integer(V) |
    analyze_assign(X, V, B1, B, E1, E).
analyze(compute(Op, X, Y, R), EP, B1, B, E1, E) :-
    computation(Op/3, Types),
    analyze_computation(Types, Op, [X, Y, R], EP, B1, B, E1, E).
analyze(compute(Op, X, R), EP, B1, B, E1, E) :-
    computation(Op/2, Types),
    analyze_computation(Types, Op, [X, R], EP, B1, B, E1, E).
analyze('$box_ref'(T, X, R), EP, B1, B, E1, E) :-
    box_type_code(T, TC),
    analyze_primitive(result, [X, R], [x, TC], '$box_ref'(T, X, R), EP, B1, B, E1, E).
analyze('$idle'('$VAR'(_, R, _)), _, B1, B, E1, E) :-
    B = [R|B1],
    E = [temp(R), type(R, a)|E1].
analyze(T, EP, Bound1, Bound, Env1, Env) :-
    otherwise |
    term:functor_parts(T, N, A, Args),
    primitive(N/A, _, R, Types),
    (Types == []  ->
        Bound = Bound1,
        Env = Env1
    ;
        analyze_primitive(R, Args, Types, T, EP, Bound1, Bound, Env1, Env)
    ).

analyze_assign('$VAR'(_, V2,_), _, B1, B, E1, E) :-
    integer(V2) | B = B1, E = E1.
analyze_assign(X, V, B1, B, E1, E) :-
    otherwise |
    literal_type(X, T),
    (T == []  ->
        B = B1,
        E = E1
    ;
        B = [V|B1],
        E = [type(V, T)|E1]
    ).

analyze_computation([TA, TB, TR], Op, [X, Y, '$VAR'(_, R, VN)], EP, B1, B,
    E1, E) :-
    integer(R) |
    immediate_result_temp(TR, R, [type(R, TR)|E1], E2, F),
    (F == true  ->
        list_to_tuple([compute, Op, X, Y, '$VAR'(0, R, VN)], Term),
        analyze_args([Op, X, Y], [a, TA, TB], Term, EP, B1, B2, E2, E3),
        list:member(R, B2, F2),
        analyze_computation_finalize(F2, R, B1, B2, B, E1, E3, E)
    ;
        analyze_computation2(Op, [X, Y, '$VAR'(_, R, _)], EP, B1, B, E1, E)
    ).
analyze_computation([TA, TR], Op, [X, '$VAR'(_, R, VN)], EP, B1, B, E1,
    E) :-
    integer(R) |
    immediate_result_temp(TR, R, [type(R, TR)|E1], E2, F),
    (F == true  ->
        list_to_tuple([compute, Op, X, '$VAR'(0, R, VN)], Term),
        analyze_args([Op, X], [a, TA], Term, EP, B1, B2, E2, E3),
        list:member(R, B2, F2),
        analyze_computation_finalize(F2, R, B1, B2, B, E1, E3, E)
    ;
        analyze_computation2(Op, [X, '$VAR'(_, R, _)], EP, B1, B, E1, E)
    ).
analyze_computation(_, Op, Args, EP, B1, B, E1, E) :-
    otherwise |
    analyze_computation2(Op, Args, EP, B1, B, E1, E).

analyze_computation2(Op, Args, EP, B1, B, E1, E) :-
    All = [Op|Args],
    term:make_functor(All, Term),
    app:maplist(make_x, Args, Types),
    analyze_args(All, [a|Types], Term, EP, B1, B, E1, E).

make_x(_, R) :- R = x.

analyze_computation_finalize(true, _, B1, _, B, E1, _, E) :-
    B = B1, E = E1.
analyze_computation_finalize(false, R, _, B2, B, _, E2, E) :-
    B = [R|B2],
    E = E2.

analyze_primitive(result, Args, Types, Term, EP, B1, B, E1, E) :-
    term:split_result(Args, Args2, R),
    term:split_result(Types, _, RT),
    analyze_primitive_result(R, RT, Types, Args, Args2, Term, EP, B1, B, E1, E).
analyze_primitive(_, Args, Types, Term, EP, B1, B, E1, E) :-
    otherwise |
    analyze_args(Args, Types, Term, EP, B1, B, E1, E).

analyze_primitive_result('$VAR'(_, R, _), RT, Types, Args, Args2, Term, EP,
    B1, B, E1, E) :-
    immediate_result_temp(RT, R, [type(R, RT)|E1], E2, F1),
    analyze_primitive_result2(F1, R, Types, Args, Args2, Term, EP, B1, B,
        E2, E).
analyze_primitive_result(_, _, _, _, _, _, _, B1, B, E1, E) :-
    otherwise | B = B1, E = E1.

analyze_primitive_result2(true, R, Types, Args, Args2, Term, EP, B1, B,
    E1, E) :-
    analyze_args(Args2, Types, Term, EP, B1, B2, E1, E3),
    list:member(R, B2, F),
    (F == true  ->
        analyze_args(Args, Types, Term, EP, B1, B, E1, E)
    ;
        E = E3,
        B = [R|B2]
    ).
analyze_primitive_result2(false, _, _, _, _, _, _, B1, B, E1, E) :-
    B = B1, E = E1.

analyze_args(Args, Types, Term, EP, B1, B, E1, E) :-
    analyze_args2(Args, Types, 1, Term, EP, B1, B, E1, E).

analyze_args2([], _, _, _, _, B1, B, E1, E) :- B = B1, E = E1.
analyze_args2(Args, any, Pos, Term, EP, B1, B, E1, E) :-
    list(Args) |
    analyze_args2(Args, [x|any], Pos, Term, EP, B1, B, E1, E).
analyze_args2([X|Args], [T|Types], Pos, Term, EP, B1, B, E1, E) :-
    otherwise |
    check_type(X, T, EP, Pos, Term, E1),
    analyze_arg(X, B1, B2),
    Pos2 is Pos + 1,
    analyze_args2(Args, Types, Pos2, Term, EP, B2, B, E1, E).

analyze_arg(A, B1, B) :- atomic(A) | B = B1.
analyze_arg(A, B1, B) :- array(A) | B = B1.
analyze_arg(A, B1, B) :- vector(A) | B = B1.
analyze_arg('$VAR'(_, I, _), B1, B) :- integer(I) | B = [I|B1].
analyze_arg([T1|As], Bound1, Bound) :-
    analyze_arg(T1, Bound1, Bound2),
    analyze_arg(As, Bound2, Bound).
analyze_arg(T, Bound1, Bound) :-
    otherwise |
    term:functor_parts(T, _, _, Args),
    analyze_arg(Args, Bound1, Bound).

-mode(literal_type(?, ^)).

literal_type(I, i) :- integer(I) | true.
literal_type(F, f) :- real(F) | true.
literal_type(S, a) :- string(S) | true.
literal_type(T, t) :- tuple(T) | true.
literal_type(_, []) :- otherwise | true.

check_type(A, T, EP, Pos, Term, _) :-
    string(A) |
    check_type2(a, T, EP, Pos, Term).
check_type(N, T, EP, Pos, Term, _) :-
    integer(N) |
    check_type2(i, T, EP, Pos, Term).
check_type(N, T, EP, Pos, Term, _) :-
    real(N) |
    check_type2(n, T, EP, Pos, Term).
check_type('$VAR'(_, V, _), T, EP, Pos, Term, E) :-
    check_type_var(E, V, T, EP, Pos, Term).
check_type(_, _, _, _, _, _) :- otherwise | true.

check_type_var([], _, _, _, _, _).
check_type_var([type(V, T2)|_], V, T, EP, Pos, Term) :-
    check_type2(T, T2, EP, Pos, Term).
check_type_var([_|E], V, T, EP, Pos, Term)  :-
    otherwise |
    check_type_var(E, V, T, EP, Pos, Term).

check_type2(T, T, _, _, _).
check_type2(n, i, _, _, _).
check_type2(n, f, _, _, _).
check_type2(i, n, _, _, _).
check_type2(f, n, _, _, _).
check_type2(x, _, _, _, _).
check_type2(_, x, _, _, _).
check_type2(T1, T2, EP, Pos, Term) :-
    otherwise |
    type_name(T1, TN1),
    type_name(T2, TN2),
    send(EP, warning('argument #~d in term `~q\' has type ~a, but was expected to be of type ~a', [Pos, Term, TN2, TN1])).

immediate_result_temp(T, V, E1, E, F) :-
    immediate_result(T, F),
    (F == true  ->
        E = [temp(V)|E1]
    ;
        E = E1
    ).

immediate_result(a, F) :- F = true.
immediate_result(i, F) :- F = true.
immediate_result(_, F) :- otherwise | F = false.

infer_types([], _, Types) :- Types = [].
infer_types([X|Args], AEnv, Types) :-
    infer_type(X, AEnv, T),
    Types = [T|Types2],
    infer_types(Args, AEnv, Types2).

infer_type('$VAR'(_, V, _), AEnv, T) :-
    integer(V) |
    find_type(V, AEnv, T).
infer_type(X, _, T) :- otherwise | literal_type(X, T).

find_type(_, [], T) :- T = x.
find_type(V, [type(V, T1)|_], T) :- T = T1.
find_type(V, [_|E], T) :- otherwise | find_type(V, E, T).

% tables

-mode(box_type_code(?, ^)).
box_type_code(int, i).
box_type_code(long, i).
box_type_code(double, f).
box_type_code(char, i).
box_type_code(byte, i).

-mode(type_name(?, ^)).
type_name(a, atom).
type_name(i, integer).
type_name(t, tuple).
type_name(n, number).
type_name(c, 'tuple or list').
type_name(l, 'list').
type_name(v, variable).
type_name(f, float).
type_name(p, port).
type_name(m, module).

-mode(computation(?, ^)).
computation(('+')/3, [n, n, n]).
computation(('-')/3, [n, n, n]).
computation(('*')/3, [n, n, n]).
computation(('/')/3, [n, n, n]).
computation(('mod')/3, [i, i, i]).
computation(and/3, [i, i, i]).
computation(or/3, [i, i, i]).
computation(xor/3, [i, i, i]).
computation(shl/3, [i, i, i]).
computation(shr/3, [i, i, i]).
computation(('=')/3, [x, x, a]).
computation(('i=i')/3, [i, i, a]).
computation(('f=f')/3, [f, f, a]).
computation(('<')/3, [n, n, a]).
computation(('i<i')/3, [i, i, a]).
computation(('f<f')/3, [f, f, a]).
computation(('>')/3, [n, n, a]).
computation(('i>i')/3, [i, i, a]).
computation(('f>f')/3, [f, f, a]).
computation(('p=p')/3, [x, x, a]).
computation(max/3, [n, n, n]).
computation(min/3, [n, n, n]).
computation(sametype/3, [x, x, a]).
computation(('**')/3, [n, n, f]).
computation(sqrt/2, [n, f]).
computation(sin/2, [n, f]).
computation(cos/2, [n, f]).
computation(tan/2, [n, f]).
computation(asin/2, [n, f]).
computation(acos/2, [n, f]).
computation(atan/2, [n, f]).
computation(log/2, [n, f]).
computation(exp/2, [n, f]).
computation(sign/2, [n, i]).
computation(rnd/2, [i, i]).
computation(abs/2, [n, n]).
computation(real_integer_part/2, [n, n]).
computation(real_fractional_part/2, [n, n]).
computation(integer/2, [n, i]).
computation(real/2, [n, f]).
computation(truncate/2, [n, f]).
computation(floor/2, [n, f]).
computation(round/2, [n, f]).
computation(ceiling/2, [n, f]).
computation(_, []) :- otherwise | true.

-mode(specialize(?, ?, ^)).
specialize('+', [i, i], 'i+i').
specialize('-', [i, i], 'i-i').
specialize(Op, _, R) :- otherwise | R = Op.

-mode(primitive(?, ^, ^, ^)).
primitive('$above'/3, above, result, [x, x, a]).
primitive('$above_or_equal'/3, above_or_equal, result, [x, x, a]).
primitive('$assign'/2, assign, none, [x, v]).
primitive('$atomic'/2, atomic, result, [x, a]).
primitive('$atomic_nofloat'/2, atomic_nonfloat, result, [x, a]).
primitive('$below'/3, below, result, [x, x, a]).
primitive('$below_or_equal'/3, below_or_equal, result, [x, x, a]).
primitive('$error'/1, error, none, [x]).
primitive('$var'/2, test_var, result, [x, a]).
primitive('$remote'/2, test_remote, result, [x, a]).
primitive('$unify'/3, unify, result, [x, x, a]).
primitive('$unify_safe'/3, unify_safe, result, [x, x, a]).
primitive('$unify_checked'/2, unify_checked, none, [x, x]).
primitive('$integer'/2, test_integer, result, [x, a]).
primitive('$float'/2, test_float, result, [x, a]).
primitive('$number'/2, test_number, result, [x, a]).
primitive('$tuple'/2, test_tuple, result, [x, a]).
primitive('$struct'/4, test_struct, result, [x, a, i, a]).
primitive('$array'/2, test_array, result, [x, a]).
primitive('$kl1string'/2, test_kl1string, result, [x, a]).
primitive('$vector'/2, test_vector, result, [x, a]).
primitive('$atom'/2, test_atom, result, [x, a]).
primitive('$match'/3, match, result, [x, x, a]).
primitive('$list'/2, test_list, result, [x, a]).
primitive('$module'/2, test_module, result, [x, a]).
primitive('$port'/2, test_port, result, [x, a]).
primitive(_, _, [], []) :- otherwise | true.
