%= "map" module: Key-value maps
%
% Maps based on AVL trees. The maps allow random-access and are fully
% functional: inserting, deleting or replacing a value will create a
% new map that will share contents with the old one, but both new and old
% map are fully independent. The empty map is equivalent to the empty
% list ("[]").
%
% Keys may be of any type and are ordered according to the total order
% of values as defined by "compare/3" and "@</2".
%
% Use "insert" and "replace" to add values, "lookup" to search the map for
% a key and "delete" to delete entries. "keys" and "values" retrieve
% lists of all keys and values of a map, respectively. "list_to_map" and"
% "map_to_list" convert maps to and from lists.
%
% The implementation of this module was inspired by:
% https://two-wrongs.com/purely-functional-avl-trees-in-common-lisp.html
%
%-
% map:node(KEY?, VAL?, TREE^)
%       Create a new TREE with a single entry.
%
% map:insert(KEY?, VAL?, TREE?, RTREE^)
%       Insert new entry into TREE, assigning the new tree to RTREE.
%       If KEY already exists, the existing node is replaced.
%
% map:delete(KEY?, TREE?, RTREE^)
% map:delete(KEY?, VAL^, TREE?, RTREE^)
%       Delete entry from TREE and assign new tree to RTREE, optionally
%       assigning old existing value (or []) to VAL.
%
% map:replace(KEY?, VAL?, TREE?, RTREE^)
% map:replace(KEY?, VAL?, OLDVAL^, TREE?, RTREE^)
%       Replace existing entry in TREE with new value, optionally assigning
%       the old value (or []) to OLDVAL. RTREE holds the final result tree.
%       If the entry does not yet exist, create a new one.
%
% map:collect(KEY?, VAL?, TREE?, RTREE^)
%       Adds VAL to the list of values stored for KEY in TREE and assigns the
%       new list to RTREE. If no previous value existed for KEY, a new list with
%       the single element VAL is stored.
%
% map:lookup(KEY?, TREE?, VAL^)
% map:lookup(KEY?, TREE?, VAL^, DEFAULT?)
%       Unifies the value for the entry with the given KEY in TREE to VAL,
%       unifies [] (or DEFAULT) if no such entry exists.
%
% map:keys(TREE?, KEYS^)
% map:keys(TREE?, KEYS^, TAIL?)
%       Collects the keys of all entries and assigns the list of keys to KEYS.
%       The returned list holds the keys in ascending order.
%
% map:values(TREE?, VALS^)
% map:values(TREE?, VALS^, TAIL?)
%       Collect the values of all entries and assigns the list to VALS.
%       The returned list holds the values in the ascending order of the map's keys.
%
% map:list_to_map(LIST?, MAP^)
% map:list_to_map(LIST?, MAP1?, MAP^)
%       Insert elements from LIST into MAP1 (or into a new map) and
%       unifies MAP with the new map, LIST should have elements of the
%       form "{'-', <KEY>, <VALUE>}" or "{<KEY>, <VALUE>}".
%
% map:map_to_list(MAP?, LIST^)
% map:map_to_list(MAP?, LIST^, TAIL?)
%       Collects keys and values into a list with elements of the form
%       "{'-', <KEY>, <VALUE>}". The list will be in sorted key order,
%       ascending.
%
% map:root(MAP?, KEY^, VALUE^)
%       Assigns KEY and VALUE the key and value of the root node of MAP.
%       If the map is empty, the predicate fails.
%
% map:above(MAP?, RMAP^)
% map:below(MAP?, RMAP^)
%       Unifies RMAP with the submap holding the elements above or
%       below the root node of MAP.

-module(map).

root(t(_, K, V, _, _), RK, RV) :- RK = K, RV = V.

below([], M) :- M := [].
below(t(_, _, _, L, _), M) :- M = L.

above([], M) :- M := [].
above(t(_, _, _, _, R), M) :- M = R.

height([], H) :- H := 0.
height(t(H0, _, _, _, _), H) :- H = H0.

balance([], B) :- B := 0.
balance(t(_, _, _, L, R), B) :-
    height(L, HL), height(R, HR), B is HR - HL.

rotate_l(t(_, K, V, L, t(_, KR, VR, LR, RR)), T) :-
    node(K, V, L, LR, T1),
    node(KR, VR, T1, RR, T).

rotate_r(t(_, K, V, t(_, KL, VL, LL, RL), R), T) :-
    node(K, V, RL, R, T1),
    node(KL, VL, LL, T1, T).

node(K, V, T) :- node(K, V, [], [], T).

node(K, V, L, R, T) :-
    height(L, HL), height(R, HR), B is HR - HL,
    H is 1 + max(HL, HR),
    node2(B, H, K, V, L, R, T).

node2(-2, H, K, V, L, R, T) :-
    balance(L, BL),
    node_il(BL, H, K, V, L, R, T).
node2(2, H, K, V, L, R, T) :-
    balance(R, BR),
    node_ir(BR, H, K, V, L, R, T).
node2(_, H, K, V, L, R, T) :-
    otherwise | T = t(H, K, V, L, R).

node_il(N, H, K, V, L, R, T) :-
    N < 0 |
    rotate_r(t(H, K, V, L, R), T).
node_il(N, _, K, V, L, R, T) :-
    N > 0 |
    rotate_l(L, L1),
    node(K, V, L1, R, T).
node_il(0, H, K, V, L, R, T) :-
    T = t(H, K, V, L, R).

node_ir(N, _, K, V, L, R, T) :-
    N < 0 |
    rotate_r(R, R1),
    node(K, V, L, R1, T).
node_ir(N, H, K, V, L, R, T) :-
    N > 0 |
    rotate_l(t(H, K, V, L, R), T).
node_ir(0, H, K, V, L, R, T) :-
    T = t(H, K, V, L, R).

insert(K, V, [], T) :-
    node(K, V, T).
insert(K, V, t(_, KT, VT, L, R), T) :-
    K @< KT |
    insert(K, V, L, TL),
    node(KT, VT, TL, R, T).
insert(K, V, t(_, KT, VT, L, R), T) :-
    K @> KT |
    insert(K, V, R, TR),
    node(KT, VT, L, TR, T).
insert(K, V, t(_, _, _, L, R), T) :-
    otherwise | node(K, V, L, R, T).

delete(K, T1, T) :- delete(K, _, T1, T).

delete(_, V, [], T) :- T := [], V := [].
delete(K, V, t(_, KT, VT, L, R), T) :-
    K @< KT |
    delete(K, V, L, TL),
    node(KT, VT, TL, R, T).
delete(K, V, t(_, KT, VT, L, R), T) :-
    K @> KT |
    delete(K, V, R, TR),
    node(KT, VT, L, TR, T).
delete(_, V, t(_, _, V1, L, R), T) :-
    otherwise |
    V = V1, delete2(L, R, T).

delete2([], R, T) :- T = R.
delete2(L, [], T) :- T = L.
delete2(L, R, T) :-
    otherwise |
    min_node(R, MR),
    delete3(MR, L, R, T).

delete3(t(_, K, V, _, _), L, R, T) :-
    delete(K, R, DR),
    node(K, V, L, DR, T).

min_node(t(H, K, V, [], R), T) :- T = t(H, K, V, [], R).
min_node(t(_, _, _, L, _), T) :- otherwise | min_node(L, T).

replace(K, V, T1, T) :- replace1(T1, K, _, V, T).

replace(K, V, V0, T1, T) :- replace1(T1, K, V, V0, T).

replace1(T1, K, V, V0, T) :-
    replace2(T1, K, V, V0, T2, OK1),
    replace_check(OK1, K, V, T1, T2, T).

replace_check(false, K, V, T1, _, T) :-
    insert(K, V, T1, T).
replace_check(_, _, _, _, T2, T) :-
    otherwise | T = T2.

replace2([], _, _, V0, _, OK) :-
    V0 = [], OK := false.
replace2(t(H, K1, V1, L, R), K, V, V0, T, OK) :-
    K1 @> K |
    replace2(L, K, V, V0, L1, OK),
    T = t(H, K1, V1, L1, R).
replace2(t(H, K1, V1, L, R), K, V, V0, T, OK) :-
    K1 @< K |
    replace2(R, K, V, V0, R1, OK),
    T = t(H, K1, V1, L, R1).
replace2(t(H, K, V1, L, R), _, V, V0, T, OK) :-
    otherwise |
    V0 = V1, OK := [], T = t(H, K, V, L, R).

collect(K, V, M1, M) :- replace(K, [V|Old], Old, M1, M).

lookup(K, T, X) :- lookup(K, T, X, []).

lookup(_, [], X, D) :- X = D.
lookup(K1, t(_, K, _, L, _), X, D) :-
    K1 @< K | lookup(K1, L, X, D).
lookup(K1, t(_, K, _, _, R), X, D) :-
    K1 @> K | lookup(K1, R, X, D).
lookup(_, t(_, _, V, _, _), X, _) :- otherwise | X = V.

keys(T, KS) :- keys(T, KS, []).

keys([], KS, TL) :- KS = TL.
keys(t(_, K, _, L, R), KS, TL) :-
    keys(L, KS, [K|KS3]),
    keys(R, KS3, TL).

values(T, VS) :- values(T, VS, []).

values([], VS, TL) :- VS = TL.
values(t(_, _, V, L, R), VS, TL) :-
    values(L, VS, [V|VS3]),
    values(R, VS3, TL).

list_to_map(L, M) :- list_to_map(L, [], M).

list_to_map([], M1, M) :- M = M1.
list_to_map([{K, V}|L], M1, M) :-
    insert(K, V, M1, M2),
    list_to_map(L, M2, M).
list_to_map([K-V|L], M1, M) :-
    insert(K, V, M1, M2),
    list_to_map(L, M2, M).
list_to_map([X|_], _,_) :-
    otherwise |
    error('list_to_map: invalid entry'(X)).

map_to_list(T, L) :- map_to_list(T, L, []).

map_to_list([], KS, TL) :- KS = TL.
map_to_list(t(_, K, V, L, R), KS, TL) :-
    map_to_list(L, KS, [K-V|KS3]),
    map_to_list(R, KS3, TL).
