/* X11 color table */

#include "fleng.h"
#include "fleng-util.h"
#include <stdlib.h>
#include <string.h>
#include <strings.h>

struct color {
    char *name;
    int r, g, b;
};

static struct color table[] = {
#include "src/colortable.c"
    { NULL, 0, 0, 0 }
};

void x11_color_4(FL_TCB *tcb, FL_VAL name, FL_VAL r, FL_VAL g, FL_VAL b)
{
    char *n = stringify(tcb, name, NULL);
    for(struct color *c = table; c->name != NULL; ++c) {
        if(strlen(n) == strlen(c->name) && !strcasecmp(c->name, n)) {
            fl_assign(tcb, MKINT(c->r), r);
            fl_assign(tcb, MKINT(c->g), g);
            fl_assign(tcb, MKINT(c->b), b);
            return;
        }
    }
    fl_assign(tcb, fl_false, r);
}

void x11_color_by_index_2(FL_TCB *tcb, FL_VAL i, FL_VAL name)
{
    int n = INT(i);
    if(n < 0 || n > sizeof(table) / sizeof(table[0]))
        fl_assign(tcb, fl_nil, name);
    else {
        FL_VAL str = fl_mkstring(table[ n ].name, strlen(table[ n ].name));
        fl_assign(tcb, str, name);
    }
}

void x11_all_colors_1(FL_TCB *tcb, FL_VAL all)
{
    FL_VAL lst, tl = fl_nil;
    for(struct color *c = table; c->name != NULL; ++c) {
        FL_VAL name = mkcharlist(tcb, c->name, strlen(c->name), fl_nil);
        FL_VAL n = mklist(tcb, name, fl_nil);
        if(tl != fl_nil) CDR(tl) = addref(n);
        else lst = n;
        tl = n;
    }    
    fl_unify_result(tcb, lst, all);
}
