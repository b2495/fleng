#!/bin/sh
# configuration script for FLENG

prolog=
target=
prefix=/usr/local
bflags=
as=as
ccname="$CC"
cxx="$CXX"
test -z "$ccname" && ccname=cc
test -z "$cxx" && cxx=c++
profile=
flflags=
nosdl=
nopcn=
nokl1=
arch=
threads=1

if [ -z "$CFLAGS" ]; then
    cflags="-O2"
else
    cflags="$CFLAGS"
fi

usage () {
    cat >&2 << EOF
usage: $0 [OPTION ...]

    --help                  show this message.
    --debug                 build runtime libraries with debug
                            information.
    --target=TARGET         force compilation target
    --prefix=DIRECTORY      specifies installation directory and
                            defaults to "/usr/local".
    --profile               compile libraries with profiling information.
    --with-prolog=PROLOG    select what Prolog implementation should
                            be used, use either "swi", "yap" or "gnu".
    --disable-sdl           compile without SDL2 support.
    --disable-threads       compile without OS-thread support.
    --disable-pcn			compile without support for the PCN language
    --disable-kl1			compile without support for the KL1 language

    the environment variables CC and CFLAGS can be set to override
    the default C compiler, preprocessor and C compiler flags.
EOF
    exit $1
}

try_compile () {
    tmpfile=$(mktemp tmp.XXXXXX)
    printf "$1\n" > "$tmpfile"
    if "$ccname" -x c $2 "$tmpfile" -o "$tmpfile.out" 2>/dev/null; then
        s=0
    else
        s=1
    fi
    rm -f "$tmpfile" "$tmpfile.out"
    return $s
}

while [ -n "$1" ]; do
    case "$1" in
        --prefix)
            shift
            prefix="$1";;
        --prefix=*)
            prefix=$(echo "$1" | awk -F= '{print $2}' -);;
        --debug)
            flflags="$flflags -d"
            cflags="-g -DLOGGING -DXLOGGING -O0 -Wno-cpp";;
        --with-prolog)
            shift
            prolog="$1";;
        --with-prolog=*)
            prolog=$(echo "$1" | awk -F= '{print $2}' -);;
        --target)
            shift
            arch="$1";;
        --target=*)
            arch=$(echo "$1" | awk -F= '{print $2}' -);;
        --profile)
            flflags="$flflags -p"
            profile=1;;
        --disable-sdl)
            nosdl=1;;
        --disable-threads)
            threads=;;
        --disable-pcn)
        	nopcn=1;;
        --disable-kl1)
        	nokl1=1;;
        -h|-help|--help)
            usage 0
            exit;;
        *) usage 1;;
    esac
    shift
done

cflags="$cflags -fno-strict-aliasing -fwrapv -fno-stack-protector -Wno-unused-result"
conlyflags="-std=gnu99"
bflags="-DENABLE_PROFILING -I."
ldflags="$LDFLAGS -lm"
test -n "$threads" && ldflags="$ldflags -pthread"
cc="$ccname"
asflags=
oscflags=
manflags="-l"

printf "checking operating system ... "
os="$(uname)"
flavor=
case "$os" in
    *BSD|DragonFly)
        # later versions of FreeBSD have no as(1)
        if ! command -v as >/dev/null; then
            as="$ccname -c -x assembler"
        fi
        os=bsd;;
    Darwin)
        flavor=m
        manflags="--"
        os=bsd;;
    Linux)
        test -n "$threads" && cflags="$cflags -pthread"
        ldflags="$ldflags -z noexecstack"
        asflags="--noexecstack"
        os=linux;;
    *)
        echo "unsupported ($os)" >&2
        exit 1;;
esac
echo $os

if [ -z "$arch" ]; then
    printf "checking architecture ... "
    case "$(uname -m)" in
        amd64|x86*)
            arch=x64;;
        arm64)
            arch=a64;;
        arm*|evbarm)
            arch=a32
            asflags="$asflags -march=armv6 -mfloat-abi=hard -mfpu=vfp";;
        aarch64)
            arch=a64;;
        riscv64)
            arch=r64
            asflags="$asflags -march=rv64imafdc";;
        *)
            echo " unsupported architecture: $arch"
            exit 1;;
    esac
    arch="$arch$flavor"
fi

case "$(uname)$arch" in
    OpenBSDx64)
        if [ "$(uname -r)" '>' 7.3 ]; then
            arch="x64b"
        fi
        as="$ccname -c -x assembler";;
    OpenBSDa64)
        if [ "$(uname -r)" '>' 7.3 ]; then
            arch="a64b"
        fi
        as="$ccname -c -x assembler"
        arch=a64b;;
esac
echo $arch

sdllibs=
extramods=
extracmods=
rm -f fleng-config.h
touch fleng-config.h
if [ -z "$nosdl" ]; then
    nosdl=1
    printf "checking SDL2 availability ... "
    if command -v sdl2-config >/dev/null; then
        if try_compile '#include "SDL.h"\nint main() {SDL_Init(0);}' "$(sdl2-config --cflags --libs)"; then
            echo yes
            printf "checking SDL2_image availability ... "
            if try_compile '#include "SDL_image.h"\nint main() {IMG_Init(0);}' "$(sdl2-config --cflags --libs) -lSDL2_image"; then
                echo yes
                nosdl=
                sdllibs="-lSDL2_image"
                echo '#define HAVE_SDL2' >>fleng-config.h
                echo '#define HAVE_SDL2_image' >>fleng-config.h
                printf "checking SDL2_ttf availability ... "
                if try_compile '#include "SDL_ttf.h"\nint main() {TTF_Init();}' "$(sdl2-config --cflags --libs) -lSDL2_ttf"; then
                    echo yes
                    echo '#define HAVE_SDL2_ttf' >>fleng-config.h
                    sdllibs="$sdllibs -lSDL2_ttf"
                else
                    echo no
                fi
                printf "checking SDL2_mixer availability ... "
                if try_compile '#include "SDL_mixer.h"\nint main() {Mix_Init(0);}' "$(sdl2-config --cflags --libs) -lSDL2_mixer"; then
                    echo yes
                    echo '#define HAVE_SDL2_mixer' >>fleng-config.h
                    sdllibs="$sdllibs -lSDL2_mixer"
                else
                    echo no
                fi
            else
                echo no
                echo 'SDL2 and SDL2_image are required for graphics support'
            fi
        else
            echo no
        fi
    else
        echo no
        echo 'SDL2 and SDL2_image are required for graphics support'
    fi
fi
test -n "$threads" && echo "#define HAVE_THREADS" >>fleng-config.h

if [ -z "$nosdl" ]; then
    extramods="sdl ezd"
    test -z "$nopcn" && extramods="$extramods bb bbedit"
    extracmods="ezdlib sdllib"
    ldflags="$(sdl2-config --libs) $sdllibs $ldflags"
    bflags="$bflags $(sdl2-config --cflags)"
    printf "checking whether the linker accepts -Wl,--as-needed ... "
    if try_compile 'int main() {}' '-Wl,--as-needed'; then
        echo yes
        ldflags="-Wl,--as-needed $ldflags"
    else
        echo no
    fi
fi

echo "linker flags: $ldflags"

printf "checking whether -fno-lto is accepted ... "
if try_compile 'int main() {}' '-fno-lto'; then
    echo yes
    ldflags="-fno-lto $ldflags"
else
    echo no
fi

echo "building cdo ..."
"$ccname" cdo.c -o cdo

echo "generating driver script ..."
version=$(cat doc/VERSION | cut -f1 -d' ')
rm -f fleng
cat > fleng <<EOF
#!/bin/sh
w=
d=
c=
p=
epmode=
incs=
estage=
keep=
verbose=
oname=
fname=
mname=
objs=
cobjs=
cppobjs=
ccopts="$cflags"
clibs=
prefix="$prefix"
tmps=
link=
checking=
lang=
defs=
ldcc="$cc"
foreign=
iprefix=\$prefix/include/fleng
xprefix=\$prefix/bin
lprefix=\$prefix/lib
lxprefix=\$prefix/lib/fleng
fwcflags=
if [ -n "\$FLENG_PREFIX" ]; then
    iprefix="\$FLENG_PREFIX/include"
    xprefix="\$FLENG_PREFIX"
    lprefix="\$FLENG_PREFIX"
    lxprefix="\$FLENG_PREFIX"
fi
usage () {
    echo "usage: fleng [--version] [-whkdceptai] [-c++] [-l LANGUAGE] [-foreign FILENAME] [-link FILENAME] [-cflags] [-libs] [-cc] [-ld LINKER] [-DDEFINE] [CCOPT ...] [-m MODULE] [-check] [-I DIRECTORY] FILENAME [-o OUTFILE] [OBJECTS ...]" >&2
    exit \$1
}
invoke () {
    test -n "\$verbose" && echo "\$@"
    "\$@"
}
fail () {
    if [ -n "\$1" ]; then
        rm -f \$tmps "$@"
    fi
    exit 1
}
while [ -n "\$1" ]; do
    case "\$1" in
        --version)
            echo \$(\$xprefix/fghc2fl --version 2>&1) $arch
            exit;;
        -k) keep=1;;
        -v) verbose=1;;
        -i) d="\$d -i";;
        -w) w=-w;;
        -d)
        	d="\$d -d"
        	fwcflags="-g -O0";;
        -c++) d="\$d -c++";;
        -c) c=1;;
        -check)
            w="\$w -check"
            checking=1;;
        -t) d="\$d -t";;
        -p) p=-p;;
        -a) d="\$d -a";;
        -e) epmode=-e;;
        -foreign)
            shift
            foreign="\$1";;
        -cc)
            echo $cc
            exit;;
        -cflags)
            echo "$cflags -I\$iprefix"
            exit;;
        -libs)
            echo "\$lprefix/libfleng.a $ldflags"
            exit;;
        -ld)
            shift
            ld="\$1";;
        -o)
            shift
            oname="\$1";;
        -m)
            test -n "\$mname" && usage 1
            shift
            mname="\$1";;
        -link)
            shift
            link="\$link -link \$1";;
        -l)
            shift
            lang=".\$1";;
        -D*)
            defs="\$defs \$1"
            ccopts="\$ccopts \$1";;
        -l*)
            clibs="\$clibs \$1";;
        -I)
            shift
            incs="-I \$1 \$incs";;
        -I*)
            shift
            incs="\$1 \$incs";;
        -*) ccopts="\$ccopts \$1";;
        *.o|*.a|*.so) objs="\$objs \$1";;
        *.c) cobjs="\$cobjs \$1";;
        *.cpp|*.cxx|*.c++) cppobjs="\$cppobjs \$1";;
        *)
            test -n "\$fname" && usage 1
            fname="\$1";;
    esac
    shift
done
test -z "\$fname\$cobjs\$cppobjs" && usage 1
case "\$oname" in
    *.fl|*.fleng) estage=f;;
    *.s|*.S|*.asm) estage=s;;
    *.o) estage=o;;
    "")
        if [ -n "\$c" ]; then
            estage=o
        else
            estage=x
            test -z "\$epmode" && oname=a.out
        fi;;
    *) estage=x;;
esac
step () {
    prev="\$next"
    if [ -n "\$keep" ]; then
        next="\$1"
    else
        next=\$(mktemp -t fleng.XXXXXX)
        tmps="\$tmps \$next"
    fi
}
check () {
    if [ "\$estage" = "\$1" ]; then
        if [ -n "\$oname" ] && [ "\$oname" != "\$next" ]; then
            invoke mv "\$next" "\$oname"
        fi
        rm -f \$tmps
        exit
    fi
}
if [ -n "\$fname" ]; then
    next="\$fname"
    test -z "\$lang" && lang="\$fname"
    bname=\$(echo "\$fname" | sed -e 's,\.[^./]*$,,')
    case "\$lang" in
        *.pcn|*.PCN)
            if ! test -f "\$fname"; then
                echo "file not found: \$fname" >&2
                exit 1
            fi
            step "\$bname.x"
            invoke $cc -E -fdollars-in-identifiers -DPCN=$version -include "\$iprefix/pcn.h" -I\$iprefix \$incs \$defs -x c \
                "\$fname" -o "\$next" || fail "\$next"
            step "\$bname.fl"
	     if [ -n "\$mname" ]; then
		mname="-m \$mname"
     	     fi
            invoke "\$xprefix/pcn2fl" -source "\$fname" \$w $mname "\$prev" "\$next" || \
                fail "\$next"
            test -n "\$checking" && exit
            mname=
            w="\$w -w";;
        *.fghc|*.ghc|*.FGHC|*.st|*.strand|*.graphics)
            step "\$bname.fl"
            invoke \$xprefix/fghc2fl \$w \$incs "\$fname" "\$next" || \
                fail "\$next"
            test -n "\$checking" && exit
            w="\$w -w";;
         *.kl1)
            step "\$bname.fl"
            invoke \$xprefix/kl12fl \$w \$incs "\$fname" "\$next" || \
                fail "\$next"
            test -n "\$checking" && exit
            w="\$w -w";;
       *.fl|*.fleng|*.FLENG)
            next="\$fname";;
        *)
            echo 'unknown source language - use the "-l" option to specify' >&2
            exit 1;;
    esac
    sname1="\$fname"
    if [ -n "\$c" ]; then
        estage=o
        test -z "\$oname" && oname="\$bname.o"
    fi
    check f
    step "\$bname.s"
    if [ -n "\$mname" ]; then
        mname="-m \$mname"
    fi
    fwf=
    fwfx=.c
    test -z "\$foreign" && foreign="\${bname}~foreign"
    rm -f "\${foreign}.c" "\${foreign}.cpp"
    invoke \$xprefix/fl2$arch \$epmode \$d \$w \$p \$link \$incs \$mname \
        -foreign "\${foreign}" "\$prev" "\$next" || fail "\$next"
    test -n "\$checking" && exit
    if [ -f "\${foreign}.c" ] && [ "\${foreign}.c" -nt "\$sname1" ]; then
        fwf="\${foreign}"
        objs="\$objs \$fwf.o"
        fwcc="$cc"
        fwcflags="\$fwcflags $conlyflags"
    elif [ -f "\${foreign}.cpp" ] && [ "\${foreign}.cpp" -nt "\$sname1" ]; then
        fwf="\${foreign}"
        objs="\$objs \$fwf.o"
        fwfx=".cpp"
        fwcc="$cxx"
        ldcc="$cxx"
    fi
    check s
    if [ -n "\$epmode" ]; then
        test -z "\$oname" && oname="\$bname.link"
        invoke mv "\$next" "\$oname"
        exit
    fi
    step "\$bname.o"
    invoke $as $asflags "\$prev" -o "\$next" || fail "\$next"
    if [ -n "\$fwf" ]; then
        invoke \$fwcc $oscflags -I\$iprefix \$incs $copts \$ccopts \$fwcflags \
            -c "\$fwf\$fwfx" -o "\$fwf.o" || fail "\$fwf.o"
        test -z "\$keep" && invoke rm -f "\$fwf\$fwfx"
    fi
fi
for x in \$cobjs; do
    bx=\$(echo "\$x" | sed -e 's,\.[^./]*$,,')
    bo="\$bx.o"
    invoke $cc $oscflags $conlyflags \$cflags -I\$iprefix \$incs $copts \$ccopts \
        -c "\$x" -o "\$bo" || fail "\$bo"
    objs="\$objs \$bo"
done
for x in \$cppobjs; do
    bx=\$(echo "\$x" | sed -e 's,\.[^./]*$,,')
    bo="\$bx.o"
    invoke $cxx $oscflags \$cflags -I\$iprefix \$incs $copts \$ccopts \
        -c "\$x" -o "\$bo" || fail "\$bo"
    objs="\$objs \$bo"
done
test -z "\$fname" && exit
check o
step "\$bname"
invoke \$ldcc $oscflags \$ccopts "\$prev" \$objs \$lprefix/libfleng.a \
    $ldflags \$clibs -o "\$next" || fail "\$next"
if [ -z "\$keep" ] && [ -n "\$fwf" ]; then
    invoke rm -f "\$fwf.o"
fi
check x
EOF
chmod a+x fleng

if ! [ -f version.ghc ]; then
    echo "generating version.ghc ..."
    ./mkversion
fi

printf "checking distribution files ... "
if [ -d asm ]; then
    echo "available"
elif [ -n "$prolog" ]; then
    echo "bootstrapping with $prolog"
    mkdir -p boot
    cd boot
    ./configure --with-prolog "$prolog" --target "$arch" || exit 1
    cd ..
else
    echo
fi

echo "generating config.sh ..."
langlibs=
if [ -z "$nopcn" ]; then
	langlibs="pcn"
	pcnlibs="pcnlex pcnparse"
fi
if [ -z "$nokl1" ]; then
	langlibs="$langlibs kl1"
	kl1libs="kl1lex kl1parse"
fi
cat > config.sh <<EOF
OS="$os"
ARCH="$arch"
PREFIX="$prefix"
CC="$ccname"
AS="$as"
AR="ar"
CFLAGS="$cflags $conlyflags $bflags"
ASFLAGS="$asflags"
LDFLAGS="$ldflags"
FLFLAGS="$flflags"
MANFLAGS="$manflags"
EXTRAMODULES="$extramods"
EXTRACMODULES="$extracmods"
THREADS="$threads"
LANGLIBS="$langlibs"
PCNLIBS="$pcnlibs"
KL1LIBS="$kl1libs"
EOF

echo "generating version.h ..."
printf 'static char *version = "%s\\n"\n' "$(cat version.ghc)" > version.h
awk '{gsub(/"/, "", $0); print "\"" $0 "\\n\""}' config.sh >> version.h
echo ';' >> version.h

echo "generating Makefile ..."
cat >Makefile <<EOF
.POSIX:
.PHONY: all bootstrap clean install check bench documentation spotless examples
all:
	./build default
clean:
	-test -x flengmake && env FLENG_PREFIX="\$(PWD)" ./flengmake -r examples clean
	./build clean
spotless:
	-test -x flengmake && env FLENG_PREFIX="\$(PWD)" ./flengmake -r examples clean
	./build spotless
install:
	./build install
bootstrap:
	./build bootstrap
documentation:
	./build documentation
check:
	./build check
bench:
	./build bench
examples:
	 env FLENG_PREFIX="\$(PWD)" ./flengmake -r examples
EOF

if [ -d examples ]; then
	echo "generating examples/options ..."
	cat >examples/options <<EOF
name hello1 hello.ghc
name hello2 hello.pcn
EOF
fi

if [ -d examples ]; then
	if [ -z "$nosdl" ] ; then
		cat >>examples/options <<EOF
target font_data.o Apl385.ttf
depends 2048 font_data.o
depends floodit font_data.o
EOF
	else
		cat >>examples/options <<EOF
ignore 7GUIs snow.pcn 2048.ghc floodit.ghc clock.pcn plot.ghc nodes.ghc
EOF
	fi
fi

echo "generating config2.ghc ..."
cat >config2.ghc <<EOF
fleng_prefix('$prefix').
fleng_cc('$cc $cflags -I\'~s\'', '$prefix/include/fleng').
os_type('$os').
flavor('$flavor').
architecture('$arch').
EOF
