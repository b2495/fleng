/* mandelbrot - C part (for use with PCN)
   taken from: https://gist.github.com/andrejbauer/7919569
*/

#include <string.h>

void mb_iterate(int *px, int *py, int *w, int *h, long *maxiter,
    double *ymax, double *xmin, double *dx, double *dy, unsigned char *buf)
{
  double x, y;
  double u, v;
  int pxn = *px, pyn = *py;
  int wn = *w, hn = *h;
  double dxn = *dx, dyn = *dy;
  double ymaxn = *ymax, xminn = *xmin;
  int maxitern = *maxiter;
  int i, j, k;
  unsigned char *locp = buf + (pyn * wn * 3 + pxn) * 6;
  for (j = 0; j < hn; j++) {
    y = ymaxn - (j + pyn) * dyn;
    for(i = 0; i < wn; i++) {
      double u = 0.0;
      double v = 0.0;
      double u2 = u * u;
      double v2 = v * v;
      x = xminn + (i + pxn) * dxn;
      /* iterate the point */
      for (k = 1; k < maxitern && (u2 + v2 < 4.0); k++) {
            v = 2 * u * v + y;
            u = u2 - v2 + x;
            u2 = u * u;
            v2 = v * v;
      };
      /* compute  pixel color and write it to file */
      if (k >= maxitern) {
        /* interior */
        const unsigned char black[] = {0, 0, 0, 0, 0, 0};
        memcpy(locp, black, 6);
      }
      else {
        /* exterior */
        unsigned char color[6];
        color[0] = k >> 8;
        color[1] = k & 255;
        color[2] = k >> 8;
        color[3] = k & 255;
        color[4] = k >> 8;
        color[5] = k & 255;
        memcpy(locp, color, 6);
      }
      locp += 6;
    }
    locp += wn * 2 * 6;
  }
}
