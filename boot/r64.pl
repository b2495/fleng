% FLENG compiler - RISCV64 backend
%
%   A: a0
%   T: s1 (callee save)
%   C: a0
%   G: s2 (callee save)
%   N: s3 (callee save)
%   E0-E6: s4-s10 (callee save)
%   E<n> (n >= 7) stack, F points to E7, ...
%   F: fp
%   P0-P3: a1-a4 (tcb is passed in a0)
%   SP: s11, on entry set to fp
%   X0, X1: t3, t4
%
%   t0, t1 and t5 are used as assembler temporary.
%   t5 is used as carry flag to indicate fail/suspend.
%   fa0 is used for float arguments to rt0 operations.

:- dynamic([floats/2]).

target_architecture(r64).

float_label(N, L) :- floats(N, L), !.
float_label(N, L) :-
    g_read(flabel, L1),
    !,
    L is L1 + 1,
    g_assign(flabel, L),
    assertz(floats(N, L)).
float_label(N, 1) :-
    assertz(flabel(1)),
    assertz(floats(N, 1)).

d_setup(_) :- g_assign(flabel, 1).

d_finalize(Out) :-
    emit('  .section .data\n  .balign 8\n', Out),
    floats(N, L),
    emit('f~d: .double ~f\n', [L, N], Out),
    fail.
d_finalize(_).

emit_external(_, _).

d_global(Name, Out) :- emit('  .global ~a\n', [Name], Out).
d_section(Name, Out) :- emit('  .section ~a\n', [Name], Out).
d_comment(Str, Out) :- emit('# ~a\n', [Str], Out).
d_wreserve(N, Out) :- emit('  .space ~d*8\n', [N], Out).

d_function_entry(Out) :- 
    emit('  addi sp, sp, -16\n  sd ra, 0(sp)\n', Out).
d_function_exit(Out) :- 
    emit('  ld ra, 0(sp)\n  addi sp, sp, 16\n', Out).
d_endfunction(_).

c_prepare_foreign_call(_, _).
c_finalize_foreign_call(_, _).

d_align(code, Out) :- emit('  .balign 8\n', Out).
d_align(even, Out) :- emit('  .balign 2\n', Out).
d_align(halfword, Out) :- emit('  .balign 4\n', Out).
d_align(word, Out) :- emit('  .balign 8\n', Out).
d_align(float, Out) :- emit('  .balign 8\n', Out).

d_equ(S, X, Out) :- emit('  .equ ~a, ~d\n', [S, X], Out).
d_equ(S, Index, X, Out) :- emit('  .equ ~a~d, ~d\n', [S, Index, X], Out).

d_bdata([B], Out) :- !, emit('  .byte ~w\n', [B], Out).
d_bdata([B|L], Out) :- 
    emit('  .byte ~w', [B], Out),
    d_data1(L, Out).

d_wdata([B], Out) :- !, emit('  .quad ~w\n', [B], Out).
d_wdata([B|L], Out) :- 
    emit('  .quad ~w', [B], Out),
    d_data1(L, Out).

d_fdata([N], Out) :- !, emit('  .double ~f\n', [N], Out).
d_fdata([N|L], Out) :- 
    emit('  .double ~f, ', [N], Out),
    d_data1(L, Out).

d_data1([], Out) :- emit('\n', Out).
d_data1([B|L], Out) :- 
    emit(', ~w', [B], Out),
    d_data1(L, Out).

d_label(Str, Out) :- emit('~a:\n', [Str], Out).
d_label(Prefix, Index, Out) :- emit('~a~d:\n', [Prefix, Index], Out).

dg_label(Str, Out) :- d_label(Str, Out).

a_call(Name, Out) :- emit('  call ~a\n', [Name], Out).

r_reg(a, a0).
r_reg(c, a0).
r_reg(t, s1).
r_reg(g, s2).
r_reg(n, s3).
r_reg(fp, fp).
r_reg(sp, s11).
r_reg(e(N), R) :- e_reg(N, R).
r_reg(p(N), R) :- p_reg(N, R).
r_reg(x(N), R) :- x_reg(N, R).

p_reg(0, a1).
p_reg(1, a2).
p_reg(2, a3).
p_reg(3, a4).

x_reg(0, t3).
x_reg(1, t4).

e_reg(0, s4).
e_reg(1, s5).
e_reg(2, s6).
e_reg(3, s7).
e_reg(4, s8).
e_reg(5, s9).
e_reg(6, s10).

r_move_r(NR, R, Out) :-
    r_reg(R, RR),
    emit('  mv ~a, ~a\n', [RR, NR], Out).

r_move(Src, e(N), Out) :- 
    N > 6, 
    !,
    N2 is N - 7,
    r_store(Src, fp, N2, Out).
r_move(e(N), Dest, Out) :- 
    N > 6, 
    !,
    N2 is N - 7,
    r_load(fp, N2, Dest, Out).
r_move(Src, Dest, Out) :-
    r_reg(Src, SR),
    r_reg(Dest, DR),
    emit('  mv ~a, ~a\n', [DR, SR], Out).

r_load(RI, I, RD, Out) :-
    r_reg(RI, RIR),
    r_reg(RD, RDR),
    I2 is I * 8,
    r_load2(I2, RIR, RDR, Out).

r_load2(I, RIR, RDR, Out) :-
    I >= -2048, I =< 2047, 
    !,
    emit('  ld ~a, ~d(~a)\n', [RDR, I, RIR], Out).
r_load2(I, RIR, RDR, Out) :-
    emit('  lui t0, %hi(~d)\n  addi t0, t0, %lo(~d)\n  add t0, ~a, t0\n  ld ~a, (t0)\n', 
        [I, I, RIR, RDR], Out).

a_load(A, I, RD, Out) :-
    r_reg(RD, RDR),
    I2 is I * 8,
    emit('  la ~a, ~a\n', [RDR, A], Out),
    r_load2(I2, RDR, RDR, Out).

r_store(RS, RI, I, Out) :-
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    I2 is I * 8,
    emit('  sd ~a, ~d(~a)\n', [RSR, I2, RIR], Out).

la_load(L, RD, Out) :-
    r_reg(RD, RDR),
    emit('  la ~a, ~a\n', [RDR, L], Out).
la_load(L, Index, RD, Out) :-
    r_reg(RD, RDR),
    emit('  la ~a, ~a~d\n', [RDR, L, Index], Out).

la_loadm(L, Index, RD, Out) :-
    r_reg(RD, RDR),
    emit('  la ~a, ~a~d\n  ori ~a, ~a, 1\n', [RDR, L, Index, RDR, RDR], Out).

lag_load(L, RD, Out) :- la_load(L, RD, Out).

i_load(I, RD, Out) :-
    r_reg(RD, RDR),
    emit('  li ~a, ~w\n', [RDR, I], Out).

f_load(F, Out) :-
    float_label(F, L),
    emit('  la t0, f~d\n  fld fa0, (t0)\n', [L], Out).

r_push(R, Out) :-
    r_reg(R, RR),
    emit('  addi s11, s11, -8\n  sd ~a, (s11)\n', [RR], Out).

r_pop(R, Out) :-
    r_reg(R, RR),
    emit('  ld ~a, (s11)\n  addi s11, s11, 8\n', [RR], Out).

g_return(Out) :- emit('  ret\n', Out).

ra_inc(R, Out) :-
    r_reg(R, RR),
    emit('  ld t0, (~a)\n  addi t0, t0, 1\n  sd t0, (~a)\n', [RR, RR], Out).

la_inc(L, Out) :- 
    emit('  la t0, ~a\n  ld t1, (t0)\n  addi t1, t1, 1\n  sd t1, (t0)\n',
        [L], Out).

sp_align(_).

ra_add(RS, N, RD, Out) :-
    r_reg(RS, RSR),
    r_reg(RD, RDR),
    N2 is N * 8,
    emit('  addi ~a, ~a, ~d\n', [RDR, RSR, N2], Out).

r_r_op(OP, RA, RB, RD, Out) :- 
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  ~a ~a, ~a, ~a\n', [OP, RDR, RAR, RBR], Out).

r_and(RA, RB, RD, Out) :- r_r_op(and, RA, RB, RD, Out).
r_or(RA, RB, RD, Out) :- r_r_op(or, RA, RB, RD, Out).
r_xor(RA, RB, RD, Out) :- 
    % special case
    r_reg(RA, RAR),
    r_reg(RB, RBR),
    r_reg(RD, RDR),
    emit('  xor ~a, ~a, ~a\n  addi ~a, ~a, 1\n', [RAR, RAR, RBR, RDR, RAR], Out).

i_mask(R, M, Out) :-
    r_reg(R, RR),
    emit('  andi ~a, ~a, ~w\n', [RR, RR, M], Out).

i_or(M, R, Out) :-
    r_reg(R, RR),
    emit('  ori ~a, ~a, ~w\n', [RR, RR, M], Out).

j_if_carry(L, Out) :- emit('  bne t5, zero, ~a\n', [L], Out).
j_if_carry(L, Index, Out) :- emit('  bne t5, zero, ~a~d\n', [L, Index], Out).

j_compare_if_equal(RS, RI, I, L, Index, Out) :- 
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  ld t0, ~d*8(~a)\n  beq ~a, t0, ~a~d\n', [I, RIR, RSR, L, Index], Out).

j_r_compare_if_not_equal(RS, RI, L, Index, Out) :- 
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  bne ~a, ~a, ~a~d\n', [RSR, RIR, L, Index], Out).

j_a_compare_if_not_equal(RS, A, L, Index, Out) :- 
    r_reg(RS, RSR),
    emit('  lui t0, %hi(~a)\n  addi t0, t0, %lo(~a)\n', [A, A], Out),
    emit('  ld t0, (t0)\n  bne ~a, t0, ~a~d\n', [RSR, L, Index], Out).

j_compare_if_not_equal(RS, RI, I, L, Index, Out) :- 
    r_reg(RI, RIR),
    r_reg(RS, RSR),
    emit('  ld t0, ~d*8(~a)\n  bne ~a, t0, ~a~d\n', [I, RIR, RSR, L, Index], Out).

j_if_test(R, I, L, Index, Out) :-
    r_reg(R, RR),
    emit('  andi t0, ~a, ~d\n  beq t0, zero, ~a~d\n', [RR, I, L, Index], Out).
j_if_test(R, I, L, Out) :-
    r_reg(R, RR),
    emit('  andi t0, ~a, ~d\n  beq t0, zero, ~a\n', [RR, I, L], Out).

j_if_not_test(R, I, L, Index, Out) :-
    r_reg(R, RR),
    emit('  andi t0, ~a, ~d\n  bne t0, zero, ~a~d\n', [RR, I, L, Index], Out).
j_if_not_test(R, I, L,  Out) :-
    r_reg(R, RR),
    emit('  andi t0, ~a, ~d\n  bne t0, zero, ~a\n', [RR, I, L], Out).

j_always(L, Out) :- emit('  j ~a\n', [L], Out).
j_always(P, Index, Out) :- emit('  j ~a~d\n', [P, Index], Out).

sp_reset(Out) :-
    r_reg(t, R),
    emit('  ld sp, 22*8(~a)\n', [R], Out).  % tcb->sp0

b_target(_, _).
