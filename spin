#!/usr/bin/rc
i = 1
count = 99999
if(~ $1 [0-9]*) {
	count = $1
	shift
}
while(test $i -lt $count) {
    printf '%d ' $i
    if(! $*) exit 1
    echo
    i = `{expr $i +  1}
}
