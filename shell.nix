{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
    buildInputs = with pkgs.buildPackages; [ gcc SDL2 SDL2_ttf SDL2_image SDL2_mixer gprolog ];
}
