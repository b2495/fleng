% FLENG compiler - aarch64 backend - BTI ("b") flavor

target_architecture(a64b).

sym_prefix('').

relocation(Name, Name, Lo) :-
    format_to_atom(Lo, ':lo12:~a', [Name]).

relocation(Name, I, Hi, Lo) :-
    format_to_atom(Hi, '~a~d', [Name, I]),
    format_to_atom(Lo, ':lo12:~a~d', [Name, I]).

b_target(jump, Out) :- emit('  bti j\n', Out).
b_target(call, Out) :- emit('  bti c\n', Out).
b_target(all, Out) :- emit('  bti jc\n', Out).
