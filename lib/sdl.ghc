%= "sdl" module: basic SDL2 graphics and sound interface
%
% This module provides a stream-based interface to libSDL2 for displaying
% graphics, render fonts and perform basic audio operations.
%
% Use "sdl:init/{1,2,3}" to initialize the SDL subsystem and obtain a
% port which accepts messages to show graphics and perform other tasks.
% Messages interpreted in the SDL stream are any of the following:
%
%   listen(EVENT?, PORT^)     (EVENT may also be a list)
%   ignore(EVENT?)
%   close
%   draw_image(X?, Y?, IMAGE?)
%   draw_image(X?, Y?, IMAGE?, FLIP?)
%   draw_image(X?, Y?, IMAGE?, ANGLE?, CX?, CY?)
%   draw_image(X?, Y?, IMAGE?, ANGLE?, CX?, CY?, FLIP?)
%   draw_text(X?, Y?, TEXT?)
%   draw_text(X?, Y?, TEXT?, FONT?)
%   draw_text(X?, Y?, TEXT?, FONT?, WIDTH?)
%   redraw
%   clear
%   clear(R?, G?, B?)
%   draw_line(X1?, Y1?, X2?, Y2?)
%   draw_polygon(X?, Y?, SIDES?, RADIUS?, ANGLE?)
%   fill_polygon(X?, Y?, SIDES?, RADIUS?, ANGLE?)
%   draw_box(X?, Y?, X2?, Y2?)
%   fill_box(X?, Y?, X2?, Y2?)
%   clip(off)
%   clip(X1?, Y1?, X2?, Y2?)
%   color(R?, G?, B?)
%   volume(CHANNEL?, VOL?)  (Channel: 0-7, Volume: 0-128)
%   play(CHAN?, SAMPLE?)
%   play(CHAN?, SAMPLE?, LOOPS?)
%   play(CHAN?, SAMPLE?, LOOPS?, TICKS?)
%   fade_in(CHAN?, SAMPLE?, MS?)
%   fade_in(CHAN?, SAMPLE?, MS?, TICKS?)
%   fade_in(CHAN?, SAMPLE?, LOOPS?, MS?, TICKS?)
%   fade_out(CHAN?, MS?)
%   pause(CHAN?)
%   resume(CHAN?)
%   set_mouse(FLAG?)
%   mouse_cursor(CURSOR?)
%   warp_mouse(X?, Y?)
%
%   FONT may be "[]" to specify the builtin 8*16 bitmap font.
%   CURSOR may be one of the strings "default", "arrow", "ibeam", "wait", "crosshair",
%   "waitarrow", "size_nwse", "size_nesw", "size_we", "size_ns", "size_all",  "no",
%   or "hand".
%   FLAG is one of the strings "true" and "false".
%
% Event types reported by the "listen" message:
%
%   Type:                   Event sent to port:
%
%   mouse                   mouse(X, Y)                 (mouse movement)
%   keyup                   keyup(K)
%   keydown                 keydown(K)
%   buttonup(B)             buttonup(B, X, Y)           (mouse button press/release)
%   buttondown(B)           buttondown(B, X, Y)
%   joybuttonup             joybuttonup(B, JOYSTICK)    (joystick)
%   joybuttondown           joybuttondown(B, JOYSTICK)
%   joymotion               joymotion(JOYSTICK, AXIS, VALUE)
%   controllerbuttonup      controllerbuttonup(B, JOYSTICK) (game controller)
%   controllerbuttondown    controllerbuttondown(B, JOYSTICK)
%   controllermotion        controllermotion(JOYSTICK, AXIS, VALUE)
%   resize                  resize(W, H)                (window resizing)
%   quit                    quit                        (window close)
%   expose                  expose                      (window exposed/shown)
%   textinput               textinput(CODE)             (textual input from keyboard)
%
%   "K": keycode (ASCII character or control key
%   "JOYSTICK": joystick indicator
%   "B": button number
%   "CODE": UNICODE code point
%
% With the exception of "sdl:wake_up/1", all predicates in this module _must_
% be called from the main thread.
%
%-
% sdl:init(STREAM^)
% sdl:init(PARAMS?, STREAM^)
% sdl:init(PARAMS?, STREAM^, DONE^)
%       Creates a resizable window of given size and returns a stream
%       for communicating with SDL. PARAMS may be a list of the following
%       elements:
%_
%       size(WIDTH?, HEIGHT?)
%       fixed
%       fullscreen
%       handle(PORT^)       expects events are read via "next_event/1" and
%                           sent to PORT
%_
%       Unless "handle(PORT)" is passed, an event loop will be started
%       and run in the background, processing events as they are delivered
%       from input devices. Events are processing once the thread is idle,
%       so it is advisable to react to I/O events, signals and timers in a
%       different thread to avoid blocking the SDL event loop processing
%       user-events.
%_
%       DONE is assigned the empty list once initialization is complete.
%
% sdl:next_event(EVENT^)
%       Unify EVENT with the next available SDL event or the empty list if
%       no event is pending.
%
% sdl:image_size(IMAGE?, W^, H^)
%       Unify W and H with the width and height of IMAGE.
%
% sdl:text_size(TEXT?, W^, H^)
% sdl:text_size(TEXT?, FONT?, W^, H^)
%       Unify W and H with the width and height of TEXT (which can be a string,
%       character list or "char" array) rendered using FONT. FONT defaults to the
%       built-in bitmap font. The sizes are given in pixels.
%
%+ sdl:load_image load_sample load_font
% sdl:load_image(NAME?, IMAGE^)
% sdl:load_sample(NAME?, SAMPLE^)
% sdl:load_font(NAME?, SIZE?, FONT^)
%       Load external asset, unifies result with the empty list if loading fails.
%
%+ sdl:load_image_from_bundle load_sample_from_bundle load_font_from_bundle
% sdl:load_image_from_bundle(ARRAY?,  IMAGE^)
% sdl:load_sample_from_bundle(ARRAY?,  SAMPLE^)
% sdl:load_font_from_bundle(ARRAY?,  SIZE?, FONT^)
%       Load external asset from memory, unifies result with the empty list if
%       loading fails.
%
%+ sdl:release_image release_sample release_font
% sdl:release_image(IMAGE?, DONE^)
% sdl:release_sample(SAMPLE?, DONE^)
% sdl:release_font(FONT?, DONE^)
%       Release loaded resource and unify DONE with the empty list.
%
% sdl:window_size(W^, H^)
%       Returns the current window size.
%
% sdl:resize_window(W?, H?, DONE^)
%       Resize window to given size.
%
% sdl:wake_up(DONE^)
%       If the SDL event loop is currently blocked by waiting for events, then
%       invoking this operation from another thread will trigger re-entry of the
%       loop and give other processes in the thread the chance to run.
%
% sdl:window_title(STRING?, DONE^)
%       Set the title of the window to STRING, which should be a string or a
%       character list. DONE is unified with the empty list when the operation
%       is completed.
%
% sdl:font_metrics(FONT?, METRICS^)
%       Unifies metrics with a tuple of the form {HEIGHT, ASCENT, DESCENT, LINESKIP}
%       for the given font.
%
% sdl:mouse_position(X^, Y^)
%       Unifies X and Y with the current mouse position.
%
% sdl:mouse_buttons(B^)
%       Unifies B with a bit-mask where bits #0 to #2 are set when the
%       mouse buttons 1 to 3 are pressed.
%
% sdl:shift_state(STATE^)
% sdl:shift_state(EVENT^, STATE1?, STATE2^)
%       The first form initializes a new, unset shift state object. The
%       second form updates the current shift state according to the
%       passed event object. If the event does not designate a keyboard
%       event, the state is unchanged.
%
% sdl:get_shift_state(SHIFT?, STATE?, FLAG^)
%       Unifies FLAG with the string "true" or "false", if the shift
%       state STATE represenents the keys given in SHIFT, which may
%       be one of the strings "shift", "control" or "alt", or a binary
%       operator tuple like "control+shift".
%
% sdl:message_box(MESSAGE?, ITEMS?, RESPONSE^)
% sdl:message_box(TITLE?, MESSAGE?, ITEMS?, RESPONSE^)
%       Shows a message box. Title may begin with the characters "!" or "?"
%       to mark the message as a warning or an error. ITEMS should hold a list
%       of strings or character lists with button texts. Each item may be
%       preceded by the characters "*" or "^" to mark a button that should
%       be selected when RETURN or ESCAPE is pressed, respectively.
%       After the box is confirmed, RESPONSE will be assigned the index
%       of the selected button (starting from 1). During display of the
%       message box execution in the current thread is suspended.

-module(sdl).

init(S) :- init([], S).

init(Props, S) :- init(Props, 640/480, 0, idle, S, _).

init(Props, S, Ok) :- init(Props, 640/480, 0, idle, S, Ok).

init([], !W / !H, !Flags, Pt, S, Ok) :-
    open_port(PE, SE),
    foreign_call(sdl_init(W, H, Flags, Ok1)) & Ok = [],
    when(Ok1, (init_events(Pt, PE), init_resources(Ok2))),
    event_loop(SE, []),
    when(Ok2, loop(S, PE)).
init([size(W, H)|Props], _, Flags, Pt, S, Ok) :-
    init(Props, W/H, Flags, Pt, S, Ok).
init([fixed|Props], WH, Flags, Pt, S, Ok) :-
    F2 is Flags \/ 4,
    init(Props, WH, F2, Pt, S, Ok).
init([handle(P)|Props], WH, Flags, _, S, Ok) :-
    init(Props, WH, Flags, handle(P), S, Ok).
init([fullscreen|Props], WH, Flags, Pt, S, Ok) :-
    F2 is Flags \/ 1,
    init(Props, WH, F2, Pt, S, Ok).
init([X|_], _, _, _, _, _) :-
    otherwise | error('sdl: invalid init option'(X)).

init_resources(Ok) :-
    foreign_call(sdl_default_image(I)),
    foreign_call(sdl_default_font(P)),
    foreign_call(sdl_init_resources(P, I, Ok)).

loop([], _) :- foreign_call(sdl_exit).
loop([close|_], PE) :- loop([], PE).
loop([listen(Evt, Out)|S], PE) :-
    add_listener(Evt, Out, PE),
    loop(S, PE).
loop([ignore(Evt)|S], PE) :-
    send(PE, drop(Evt)),
    loop(S, PE).
loop([draw_image(!X, !Y, !Img)|S], PE) :-
    foreign_call(sdl_draw_image(X, Y, Img, [0, 0, 0, ''|Ok])),
    loop(Ok, S, PE).
loop([draw_image(!X, !Y, !Img, !Angle, !Cx, !Cy)|S], PE) :-
    foreign_call(sdl_draw_image(X, Y, Img, [Angle, Cx, Cy, ''|Ok])),
    loop(Ok, S, PE).
loop([draw_image(!X, !Y, !Img, !Flip)|S], PE) :-
    foreign_call(sdl_draw_image(X, Y, Img, [0, 0, 0, Flip|Ok])),
    loop(Ok, S, PE).
loop([draw_image(!X, !Y, !Img, !Angle, !Cx, !Cy, !Flip)|S], PE) :-
    foreign_call(sdl_draw_image(X, Y, Img, [Angle, Cx, Cy, Flip|Ok])),
    loop(Ok, S, PE).
loop([draw_box(!X1, !Y1, !X2, !Y2)|S], PE) :-
    foreign_call(sdl_draw_box(X1, Y1, X2, [Y2|Ok])),
    loop(Ok, S, PE).
loop([draw_line(!X1, !Y1, !X2, !Y2)|S], PE) :-
    foreign_call(sdl_draw_line(X1, Y1, X2, [Y2|Ok])),
    loop(Ok, S, PE).
loop([draw_polygon(!X, !Y, !N, !R, !A)|S], PE) :-
    foreign_call(sdl_draw_polygon(X, Y, N, [R, A|Ok])),
    loop(Ok, S, PE).
loop([fill_polygon(!X, !Y, !N, !R, !A)|S], PE) :-
    foreign_call(sdl_fill_polygon(X, Y, N, [R, A|Ok])),
    loop(Ok, S, PE).
loop([fill_box(!X1, !Y1, !X2, !Y2)|S], PE) :-
    foreign_call(sdl_fill_box(X1, Y1, X2, [Y2|Ok])),
    loop(Ok, S, PE).
loop([draw_text(!X, !Y, Text)|S], PE) :-
    deref(Text, Ok),
    when(Ok, foreign_call(sdl_draw_text(X, Y, Text, [[], []|Ok2]))),
    loop(Ok2, S, PE).
loop([draw_text(!X, !Y, Text, !Fnt)|S], PE) :-
    deref(Text, Ok),
    when(Ok, foreign_call(sdl_draw_text(X, Y, Text, [Fnt, []|Ok2]))),
    loop(Ok2, S, PE).
loop([draw_text(!X, !Y, Text, !Fnt, !W)|S], PE) :-
    deref(Text, Ok),
    when(Ok, foreign_call(sdl_draw_text(X, Y, Text, [Fnt, W|Ok2]))),
    loop(Ok2, S, PE).
loop([redraw|S], PE) :-
    foreign_call(sdl_redraw(Ok)),
    loop(Ok, S, PE).
loop([clear|S], PE) :-
    foreign_call(sdl_clear(Ok)),
    loop(Ok, S, PE).
loop([clear(!R, !G, !B)|S], PE) :-
    foreign_call(sdl_clear(R, G, B, Ok)),
    loop(Ok, S, PE).
loop([clip(off)|S], PE) :-
    foreign_call(sdl_no_clip(Ok)),
    loop(Ok, S, PE).
loop([clip(!X1, !Y1, !X2, !Y2)|S], PE) :-
    foreign_call(sdl_clip(X1, Y1, X2, [Y2|Ok])),
    loop(Ok, S, PE).
loop([color(!R, !G, !B)|S], PE) :-
    foreign_call(sdl_color(R, G, B, Ok)),
    loop(Ok, S, PE).
loop([volume(!Ch, !V)|S], PE) :-
    foreign_call(sdl_volume(Ch, V, Ok)),
    loop(Ok, S, PE).
loop([play(!Ch, !Smp)|S], PE) :-
    foreign_call(sdl_play(Ch, Smp, 0, [[]|Ok])),
    loop(Ok, S, PE).
loop([play(!Ch, !Smp, !L)|S], PE) :-
    foreign_call(sdl_play(Ch, Smp, L, [[]|Ok])),
    loop(Ok, S, PE).
loop([play(!Ch, !Smp, !L, !T)|S], PE) :-
    foreign_call(sdl_play(Ch, Smp, L, [T|Ok])),
    loop(Ok, S, PE).
loop([fade_in(!Ch, !Smp, !FT)|S], PE) :-
    foreign_call(sdl_fade_in(Ch, Smp, 0, [FT, []|Ok])),
    loop(Ok, S, PE).
loop([fade_in(!Ch, !Smp, !FT, !T)|S], PE) :-
    foreign_call(sdl_fade_in(Ch, Smp, 0, [FT, T|Ok])),
    loop(Ok, S, PE).
loop([fade_in(!Ch, !Smp, !L, !FT, !T)|S], PE) :-
    foreign_call(sdl_fade_in(Ch, Smp, L, [FT, T|Ok])),
    loop(Ok, S, PE).
loop([fade_out(!Ch, !FT)|S], PE) :-
    foreign_call(sdl_fade_out(Ch, FT, Ok)),
    loop(Ok, S, PE).
loop([pause(!Ch)|S], PE) :-
    foreign_call(sdl_pause(Ch, Ok)),
    loop(Ok, S, PE).
loop([resume(!Ch)|S], PE) :-
    foreign_call(sdl_resume(Ch, Ok)),
    loop(Ok, S, PE).
loop([set_mouse(!F)|S], PE) :-
    foreign_call(sdl_set_mouse(F, Ok)),
    loop(Ok, S, PE).
loop([mouse_cursor(!C)|S], PE) :-
    foreign_call(sdl_mouse_cursor(C, Ok)),
    loop(Ok, S, PE).
loop([warp_mouse(!X, !Y)|S], PE) :-
    foreign_call(sdl_warp_mouse(X, Y, Ok)),
    loop(Ok, S, PE).
loop([M|_], _) :- otherwise | error(unknown_sdl_event(M)).

loop([], S, PE) :- loop(S, PE).

init_events(idle, PE) :- wait_for_events(PE).
init_events(handle(P), PE) :- otherwise | P := PE.

next_event(Evt) :- foreign_call(sdl_next_event(false, Evt)).

wait_for_events(P) :-
    idle |
    foreign_call(sdl_next_event(true, Evt)),
    send(P, Evt) &
    wait_for_events(P).

event_loop([], _).
event_loop([drop(E)|S], LL) :-
    drop_listener(E, LL, LL2),
    event_loop(S, LL2).
event_loop([E-ES|S], LL) :-
    event_loop(S, [E-ES|LL]).
event_loop([[]|S], LL) :-
    event_loop(S, LL).
event_loop([E|S], LL) :-
    otherwise | process_event(E, S, LL).

process_event(E, S, LL) :-
    activate_listeners(LL, E, E),
    event_loop(S, LL).

activate_listeners([], _, _).
activate_listeners([mouse-EP|LL], [0'M, X, Y], E) :-
    send(EP, mouse(X, Y)),
    activate_listeners(LL, E, E).
activate_listeners([joymotion-EP|LL], [0'A, A, J, V], E) :-
    send(EP, joymotion(J, A, V)),
    activate_listeners(LL, E, E).
activate_listeners([controllermotion-EP|LL], [0'X, A, J, V], E) :-
    send(EP, controllermotion(J, A, V)),
    activate_listeners(LL, E, E).
activate_listeners([textinput-EP|LL], [0'T, C], E) :-
    send(EP, textinput(C)),
    activate_listeners(LL, E, E).
activate_listeners([keyup-EP|LL], [0'K, K, false], E) :-
    send(EP, keyup(K)),
    activate_listeners(LL, E, E).
activate_listeners([keydown-EP|LL], [0'K, K, true], E) :-
    send(EP, keydown(K)),
    activate_listeners(LL, E, E).
activate_listeners([buttonup(B)-EP|LL], [0'B, B, X, Y, false], E) :-
    send(EP, buttonup(B, X, Y)),
    activate_listeners(LL, E, E).
activate_listeners([buttondown(B)-EP|LL], [0'B, B, X, Y, true], E) :-
    send(EP, buttondown(B, X, Y)),
    activate_listeners(LL, E, E).
activate_listeners([joybuttonup-EP|LL], [0'J, B, J, false], E) :-
    send(EP, joybuttonup(B, J)),
    activate_listeners(LL, E, E).
activate_listeners([joybuttondown-EP|LL], [0'J, B, J, true], E) :-
    send(EP, joybuttondown(B, J)),
    activate_listeners(LL, E, E).
activate_listeners([controllerbuttonup-EP|LL], [0'C, B, J, false], E) :-
    send(EP, controllerbuttonup(B, J)),
    activate_listeners(LL, E, E).
activate_listeners([controllerbuttondown-EP|LL], [0'C, B, J, true], E) :-
    send(EP, controllerbuttondown(B, J)),
    activate_listeners(LL, E, E).
activate_listeners([resize-EP|LL], [0'R, W, H], E) :-
    send(EP, resize(W, H)),
    activate_listeners(LL, E, E).
activate_listeners([quit-EP|LL], [0'Q], E) :-
    send(EP, quit, Ok),
    when(Ok, activate_listeners(LL, E, E)).
activate_listeners([expose-EP|LL], [0'X], E) :-
    send(EP, expose, Ok),
    when(Ok, activate_listeners(LL, E, E)).
activate_listeners([_|LL], E, _) :-
    otherwise | activate_listeners(LL, E, E).

drop_listener([], _, LL) :- LL = [].
drop_listener([E-_|LL1], E, LL) :- LL = LL1.
drop_listener([E1|LL1], E, LL) :-
    otherwise |
    LL = [E1|LL2],
    drop_listener(LL1, E, LL2).

add_listener([Evt|L], Out, PE) :-
    send(PE, Evt-Out),
    add_listener(L, Out, PE).
add_listener(Evt, Out, PE) :- otherwise | send(PE, Evt-Out).

image_size(!Img, W, H) :- foreign_call(sdl_image_size(Img, W, H)).

text_size(T, W, H) :- text_size(T, [], W, H).

text_size([], _, W, H) :- W = 0, H = 0.
text_size(Text, !Fnt, W, H) :-
    otherwise |
    deref(Text) &
    foreign_call(sdl_text_size(Text, Fnt, W, H)).

load_image(Name, Img) :-
    deref(Name, Ok),
    when(Ok, foreign_call(sdl_load_image(Name, Img))).

load_sample(Name, Smp) :-
    deref(Name, Ok),
    when(Ok, foreign_call(sdl_load_sample(Name, Smp))).

load_font(Name, !Sz, Fnt) :-
    deref(Name, Ok),
    when(Ok, foreign_call(sdl_load_font(Name, Sz, Fnt))).

load_image_from_bundle(!Ptr, Img) :-
    foreign_call(sdl_load_image_from_bundle(Ptr, Img)).

load_sample_from_bundle(!Ptr, Smp) :-
    foreign_call(sdl_load_sample_from_bundle(Ptr, Smp)).

load_font_from_bundle(!Ptr, !Sz, Fnt) :-
    foreign_call(sdl_load_font_from_bundle(Ptr, Sz, Fnt)).

window_size(W, H) :- foreign_call(sdl_window_size(W, H)).

resize_window(!W, !H, Ok) :- foreign_call(sdl_resize_window(W, H, Ok)).

wake_up(Ok) :- foreign_call(sdl_wakeup(Ok)).

release_font(Fnt, Ok) :- foreign_call(sdl_release_font(Fnt, Ok)).
release_sample(Smp, Ok) :- foreign_call(sdl_release_sample(Smp, Ok)).
release_image(Img, Ok) :- foreign_call(sdl_release_image(Img, Ok)).

window_title(Str, Ok) :-
    deref(Str) & foreign_call(sdl_window_title(Str, Ok)).

font_metrics(!Fnt, M) :- foreign_call(sdl_font_metrics(Fnt, M)).

mouse_position(X, Y) :- foreign_call(sdl_mouse_state(X, Y, _)).

mouse_buttons(B) :- foreign_call(sdl_mouse_state(_, _, B)).

shift_state(KS) :- KS = {0, 0, 0}.    % {SHIFT, CTRL, ALT}

shift_state(keydown(0x400000e0), {S, C, A}, KS) :-   % LCTRL
    C2 is C \/ 0b10, KS = {S, C2, A}.
shift_state(keydown(0x400000e1), {S, C, A}, KS) :-   % LSHIFT
    S2 is S \/ 0b10, KS = {S2, C, A}.
shift_state(keydown(0x400000e2), {S, C, A}, KS) :-   % LALT
    A2 is A \/ 0b10, KS = {S, C, A2}.
shift_state(keydown(0x400000e4), {S, C, A}, KS) :-   % RCTRL
    C2 is C \/ 0b01, KS = {S, C2, A}.
shift_state(keydown(0x400000e5), {S, C, A}, KS) :-   % RSHIFT
    S2 is S \/ 0b01, KS = {S2, C, A}.
shift_state(keydown(0x400000e6), {S, C, A}, KS) :-   % RALT
    A2 is A \/ 0b01, KS = {S, C, A2}.
shift_state(keyup(0x400000e0), {S, C, A}, KS) :-   % LCTRL
    C2 is C /\ 0b01, KS = {S, C2, A}.
shift_state(keyup(0x400000e1), {S, C, A}, KS) :-   % LSHIFT
    S2 is S /\ 0b01, KS = {S2, C, A}.
shift_state(keyup(0x400000e2), {S, C, A}, KS) :-   % LALT
    A2 is A /\ 0b01, KS = {S, C, A2}.
shift_state(keyup(0x400000e4), {S, C, A}, KS) :-   % RCTRL
    C2 is C /\ 0b10, KS = {S, C2, A}.
shift_state(keyup(0x400000e5), {S, C, A}, KS) :-   % RSHIFT
    S2 is S /\ 0b10, KS = {S2, C, A}.
shift_state(keyup(0x400000e6), {S, C, A}, KS) :-   % RALT
    A2 is A /\ 0b10, KS = {S, C, A2}.
shift_state(_, KS1, KS) :- otherwise | KS = KS1.

get_shift_state({_, X, Y}, KS, F) :-
    get_shift_state(X, KS, F1),
    (F1 == true -> get_shift_state(Y, KS, F); F = false).
get_shift_state(control, {_, C, _}, F) :- C =\= 0 | F = true.
get_shift_state(shift, {S, _, _}, F) :- S =\= 0 | F = true.
get_shift_state(alt, {_, _, A}, F) :- A =\= 0 | F = true.
get_shift_state(_, _, F) :- otherwise | F = false.

message_box(Msg, Items, B) :-
    message_box('', Msg, Items, B).

message_box(Title, Msg, Items, B) :-
    deref([Title, Msg, Items], Ok),
    when(Ok, foreign_call(sdl_message_box(Title, Msg, Items, B))).
