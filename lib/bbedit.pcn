/* FLENG - "bbedit" module, used internally by "bb"
*/

#include "bb.h"

-module("bbedit")

function text_to_lines(text) {?
	string(text), text != [] -> return(split_lines(string_to_list(text)));
	list(text) -> return(split_lines(text));
	array(text) -> return(array_to_lines(text));
	default -> return(text)
}

function split_lines(text) {
	list:split(text, 10, lines);
	split_lines2(lines, 1, [], map);
	return(map)
}

split_lines2(lines, n, :map)
lines ?= [ln|more] -> {
	map:insert(n, ln, :map);
	split_lines2(more, n + 1, :map)
}

function array_to_lines(text) {||
	array_to_lines2(text, 1, 0, [], map);
	return(map)
}

array_to_lines2(text, n, offset, :map) {||
	array:search(NL, offset, text, pos);
	if(pos == false) {||
		len = length(text);
		if(len > offset) {||
			array:view(offset, length(text) - offset, text, line);
			map:insert(n, line, :map)
		}
	} else {||
		len = pos - offset;
		if(len == 0) line = []
		else array:view(offset, pos - offset, text, line);
		map:insert(n, line, :map);
		array_to_lines2(text, n + 1, pos + 1, :map)
	}
}

lines_to_text(m, text)  {
	map:values(m, lines);
	total_length(0, len, lines, lines2);
	array:make("char", len, text);
	fill_text(lines2, 0, text)
}

total_length(n, len, lns1, lns) {?
	lns1 == [] -> {
		len = n;
		lns = []
	};
	lns1 ?= [ln|more] -> {
		if(array(ln)) {
			lns = [ln|lns2];
			total_length(n + length(ln) + 1, len, more, lns2)
		} else {
			utf_encode(ln, lnu);
			lns = [lnu|lns2];
			total_length(n + length(lnu) + 1, len, more, lns2)
		}
	}
}

fill_text(lines, p, a)
char a[];
lines ?= [ln|more] -> {
	n = length(ln);
	if(array(ln)) array:copy(ln, a, 0, p, n, p2)
	else {
		utf_encode(ln, lnu);
		array:put(p, lnu, a, p2)
	};
	array:put(p2, NL, a);
	fill_text(more, p2 + 1, a)
}

draw_rows(id, i, tr, tc, x, y, w, h, r, c, font, col, ccol, tw, th, ev, state, :ezd)
y + th < h -> {
	row = getline(tr, state.lines);
	tabexpand(row, c, ec, rowt);
	if(tr == r) draw_cursor_row(id, i, tc, x, y, w, ec, font, col, ccol, tw, th, rowt, ev, :ezd)
	else draw_normal_row(id, i, tc, x, y, w, font, col, rowt, :ezd);
	draw_rows(id, i + 1, tr + 1, tc, x, y + th, w, h, r, c, font, col, ccol, tw, th, ev,
		state, :ezd)
}

draw_normal_row(id, i, tc, x, y, w, font, col, row, :ezd) {
	offset_row(row, tc - 1, row2);
	ezd ++ {"object", {id, i}, [{"text", x, y, row2, [{"anchor", "nw"}, {"width", w},
		{"font", font}, {"color", col}]}]}
}

offset_row(row, offset, part) {
	if(offset == 0) part = row
	else list:drop(offset, row, part)
}

draw_cursor_row(id, i, tc, x, y, w, c, font, col, ccol, tw, th, row, ev, :ezd) {
	partition_row(row, tc - 1, c - tc, vrow, prefix, rest);
	sdl:text_size(prefix, font, pw, _);
	if(rest == []) cw = tw
	else {
		ch = rest[ 0 ];
		sdl:text_size([ch], font, cw, _)
	};
	cx = pw + x;
	cx2 = cx + cw;
	cy2 = y + th;
	if(cx < w) ezd ++ {"object", {id, "cursor"}, [{"box", cx, y, cx2, cy2, ccol}]};
	ezd ++ {"object", {id, i}, [{"text", x, y, vrow, [{"anchor", "nw"},  {"width", w},
			{"font", font}, {"color", col}]}]}
		++ {"when", {id, "cursor"}, "textinput", ev}
		++ {"when", {id, "cursor"}, "keydown", ev}
		++ {"when", {id, "cursor"}, "keyup", ev}
}

// partition into visible row, prefix before the cursor and rest
partition_row(row, offset, plen, vrow, prefix, rest) {
	list:drop(offset, row, vrow);
	list:cut(plen, vrow, [], prefix, rest)
}

// expand tabs, add additional whitespace to compute effective column
tabexpand(row, c, ec, exp) {
	if(array(row)) {
		array:search(TAB, 0, row, pos);
		if(pos) {
			array:get(0, length(row), row, text);
			utf_decode(text, utext);
			tabexpand_list(utext, 1, 0, c, c, ec, exp)
		} else exp = row
	} else tabexpand_list(row, 1, 0, c, c, ec, exp)
}

tabexpand_list(row, i, p, c, ec1, ec, exp) {?
	row == [] -> {
		exp = [];
		ec = ec1
	};
	row ?= [TAB|more] -> {
		n = TABWIDTH - p % TABWIDTH;
		if(i < c) ec2 = ec1 + n - 1
		else ec2 = ec1;
		list:make(n, SPACE, exp, exp2);
		tabexpand_list(more, i + 1, p + n, c, ec2, ec, exp2)
	};
	row ?= [x|more], x != TAB -> {
		exp = [x|exp2];
		tabexpand_list(more, i + 1, p + 1, c, ec1, ec, exp2)
	}
}

delete_edit_rows(id, row, rows, :ezd)
row <= rows -> {
	ezd ++ {"object", {id, row}, []};
	delete_edit_rows(id, row + 1, rows, :ezd)
}

#define CONTROL		1
#define SHIFT		2
#define ALT			4

#define KC_UP			0x40000052
#define KC_DOWN		0x40000051
#define KC_LEFT			0x40000050
#define KC_RIGHT		0x4000004f

#define KC_LCONTROL	0x400000e0
#define KC_RCONTROL 	0x400000e4
#define KC_LALT			0x400000e2
#define KC_RALT 			0x400000e6
#define KC_LSHIFT		0x400000e1
#define KC_RSHIFT 		0x400000e5
#define KC_PGUP			0x4000004b
#define KC_PGDN			0x4000004e
#define KC_HOME			0x4000004a
#define KC_END			0x4000004d

#define touch(exp)		{ exp; modified = true }
#define notouch(exp)	{ exp; modified = false }

edit_input_dispatch(e, mks, dot, modified, :state) {?
	e ?= {"keydown", KC_LCONTROL} -> notouch(state.modkeys |= CONTROL);
	e ?= {"keydown", KC_RCONTROL} -> notouch(state.modkeys |= CONTROL);
	e ?= {"keydown", KC_LSHIFT} -> notouch(state.modkeys |= SHIFT);
	e ?= {"keydown", KC_RSHIFT} -> notouch(state.modkeys |= SHIFT);
	e ?= {"keydown", KC_LALT} -> notouch(state.modkeys |= ALT);
	e ?= {"keydown", KC_RALT} -> notouch(state.modkeys |= ALT);
	e ?= {"keyup", KC_LCONTROL} -> notouch(state.modkeys &= ~CONTROL);
	e ?= {"keyup", KC_RCONTROL} -> notouch(state.modkeys &= ~CONTROL);
	e ?= {"keyup", KC_LSHIFT} -> notouch(state.modkeys &= ~SHIFT);
	e ?= {"keyup", KC_RSHIFT} -> notouch(state.modkeys &= ~SHIFT);
	e ?= {"keyup", KC_LALT} -> notouch(state.modkeys &= ~ALT);
	e ?= {"keyup", KC_RALT} -> notouch(state.modkeys &= ~ALT);
	e ?= {"keydown", CR} -> touch(insert_nl(:state));
	e ?= {"keydown", 'a'}, mks & CONTROL != 0 -> notouch(bol(:state));
	e ?= {"keydown", 'e'}, mks & CONTROL != 0 -> notouch(eol(:state));
	e ?= {"keydown", 'd'}, mks & CONTROL != 0 -> touch(del(:state));
	e ?= {"keydown", DEL} -> touch(del(:state));
	e ?= {"keydown", KC_UP} -> notouch(up(:state));
	e ?= {"keydown", KC_DOWN} -> notouch(down(:state));
	e ?= {"keydown", KC_LEFT} -> notouch(left(:state));
	e ?= {"keydown", KC_RIGHT} -> notouch(right(:state));
	e ?= {"keydown", KC_PGUP} -> notouch(pgup(:state));
	e ?= {"keydown", KC_PGDN} -> notouch(pgdn(:state));
	e ?= {"keydown", KC_HOME} -> notouch(home(:state));
	e ?= {"keydown", KC_END} -> notouch(end(:state));
	e ?= {"keydown", TAB} -> touch(insert1([TAB], :state));
	e ?= {"keydown", BACKSPACE} -> {
		if(dot != {1, 1}) {
			left(:state);
			del(:state);
			modified = true
		} else modified = false
	};
	e ?= {"keydown", 'k'}, mks & CONTROL != 0 -> {
		let dot ?= {r, c};
		m = state.lines;
		row = getline(r, m);
		list:cut(c - 1, row, [], row2, rest);
		if(rest == []) del(:state)
		else {
			if(list(rest)) state.cut <-- rest;
			map:insert(r, row2, m, m2);
			state.lines <-- m2
		};
		modified = true
	};
	e ?= {"keydown", 'u'}, mks & CONTROL != 0 -> {
		let dot ?= {r, c};
		m = state.lines;
		if(c == 1) {
			if(r > 1) {
				r2 = r - 1;
				row = getline(r2, m);
				c2 = length(row) + 1;
				state.dot <-- {r2, c2};
				del(:state);
				modified = true
			} else modified = false;
		} else {
			row = getline(r, m);
			list:cut(c - 1, row, [], cut, row2);
			if(list(cut)) state.cut <-- cut;
			map:insert(r, row2, m, m2);
			state.lines <-- m2;
			bol(:state);
			modified = true
		}
	};
	e ?= {"keydown", 'y'}, mks & CONTROL != 0 -> {
		if(list(state.cut)) {
			insert(state.cut, :state);
			modified= true
		} else modified = false
	};
	e ?= {"textinput", code} -> touch(insert1([code], :state));
	default -> modified = false
}

add_modifiers(mks, :key) {
	if(mks & SHIFT != 0) key = ["shift"|key];
	if(mks & CONTROL != 0) key = ["control"|key];
	if(mks & ALT != 0) key = ["alt"|key];
}

shift_lines(pivot, offset, :node) {?
	node ?= {"t", h, k, v, l, r}, k >= pivot -> {||
		shift_lines(pivot, offset, l, l2);
		shift_lines(pivot, offset, r, r2);
		k2 = k + offset;
		node = {"t", h, k2, v, l2, r2}
	};
	node ?= {"t", h, k, v, l, r}, k < pivot -> {||
		shift_lines(pivot, offset, r, r2);
		node = {"t", h, k, v, l, r2}
	}
}

bol(:state) {
	dot = state.dot;
	let dot ?= {r, _};
	state.dot <-- {r, 1}
}

eol(:state) {
	dot = state.dot;
	let dot ?= {r, _};
	c = length(getline(r, state.lines)) + 1;
	state.dot <-- {r, c}
}

up(:state) {
	dot = state.dot;
	let dot ?= {r, c};
	if(r > 1) {
		r2 = r - 1;
		c2 = min(c, length(getline(r2, state.lines)) + 1);
		state.dot <-- {r2, c2}
	}
}

down(:state) {
	dot = state.dot;
	let dot ?= {r, c};
	r2 = r + 1;
	c2 = min(c, length(getline(r2, state.lines)) + 1);
	state.dot <-- {r2, c2}
}

left(:state) {
	dot = state.dot;
	let dot ?= {r, c};
	if(c > 1) {
		c2 = c - 1;
		state.dot <-- {r, c2}
	} else if(r > 1) {
		up(:state);
		eol(:state)
	}
}

right(:state) {
	dot = state.dot;
	let dot ?= {r, c};
	n = length(getline(r, state.lines));
	if(c > n) {
		down(:state);
		bol(:state)
	} else {
		c2 = c + 1;
		state.dot <-- {r, c2}
	}
}

pgup(:state) {
	dot = state.dot;
	let dot ?= {r, c};
	r2 = max(1, r - state.rows);
	n = length(getline(r2, state.lines)) + 1;
	if(c > n) c2 = n
	else c2 = c;
	state.dot <-- {r2, c2}
}

pgdn(:state) {
	dot = state.dot;
	let dot ?= {r, c};
	r2 = r + state.rows;
	n = length(getline(r2, state.lines)) + 1;
	if(c > n) c2 = n
	else c2 = c;
	state.dot <-- {r2, c2}
}

home(:state) {
	state.dot <-- {1, 1};
	state.top <-- {1, 1}
}

end(:state) {||
	map:keys(state.lines, lines);
	if(lines == []) home(:state)
	else {
		list:last(lines, r);
		c = length(getline(r, state.lines)) + 1;
		state.dot <-- {r, c}
	}
}

show(:state) {
	dot = state.dot;
	top = state.top;
	rows = state.rows;
	cols = state.columns;
	let dot ?= {r, c};
	let top ?= {tr, tc};
	{?
		r < tr -> tr2 = max(1, r - 2);
		r >= tr + rows -> tr2 = r - rows + 2;
		default -> tr2 = tr
	};
	{?
		c < tc -> tc2 = max(1, c - 2);
		c >= tc + cols -> tc2 = c - cols + 2;
		default -> tc2 = tc
	};
	//log({"top", r, c, rows, cols, tr2, tc2});//XXX
	state.top <-- {tr2, tc2}
}

function getline(r, m) {
	map:lookup(r, m, ln);
	if(array(ln)) {
		array:get(0, length(ln), ln, ln2);
		utf_decode(ln2, ln3);
		return(ln3)
	} else return(ln)
}

function replaceline(r, new, :m) {
	map:replace(r, new, old, :m);
	if(array(old)) {
		array:get(0, length(old), old, ln2);
		utf_decode(ln2, ln3);
		return(ln3)
	} else return(old)
}

edit_delete_line(:state) {
	dot = state.dot;
	let dot ?= {r, _};
	map:delete(r, state.lines, m);
	shift_lines(r + 1, -1, m, m2);
	state.lines <-- m2;
	state.dot <-- {r, 1}
}

del(:state) {
	dot = state.dot;
	m = state.lines,
	let dot ?= {r, c};
	old = replaceline(r, new, m, m2);
	list:cut(c - 1, old, end, new, rest);
	if(rest == []) {
		end = getline(r + 1, m2);
		map:delete(r + 1, m2, m3);
		shift_lines(r + 1, -1, m3, m4)
	} else {
		end = tail(rest);
		m4 = m2
	};
	map:insert(r, new, m4, m5);
	state.lines <-- m5
}

insert(text, :state) {
	list:characters(text, text2);
	list:split(text2, NL, lines);
	insert2(lines, :state)
}

insert2(lines, :state)
lines ?= [ln|more] -> {
	insert1(ln, :state);
	if(list(more)) {
		insert_nl(:state);
		insert2(more, :state)
	}
}

insert1(text, :state) {
	m = state.lines;
	list:characters(text, text2);
	dot = state.dot;
	let dot ?= {r, c};
	old = replaceline(r, new, m, m2);
	list:cut(c - 1, old, text3, new, rest);
	list:append(text2, rest, text3);
	state.lines <-- m2;
	c2 = length(text) + c;
	state.dot <-- {r, c2}
}

insert_nl(:state) {
	m = state.lines;
	dot = state.dot;
	let dot ?= {r, c};
	old = replaceline(r, new, m, m2);
	shift_lines(r + 1, 1, m2, m3);
	list:cut(c - 1, old, [], new, rest);
	map:insert(r + 1, rest, m3, m4);
	state.lines <-- m4;
	r2 = r + 1;
	state.dot <-- {r2, 1}
}

dot_at(x, y, pos, size, font, state, rc) {
	let pos ?= {x1, y1};
	let size ?= {w, h};
	if(x >= x1, y >= y1, x < x1 + w, y < y1 + h) {
		top = state.top;
		let top ?= {tr, tc};
		lh = h / state.rows;
		rc = {r, c};
		r = tr + (y - y1) / lh;
		row = getline(r, state.lines);
		tabexpand(row, 0, _, rowt);
		offset_row(rowt, tc - 1, vrow);
		sdl:text_size(vrow, font, tw, _);
		xo = x - x1;
		if(xo > tw) c = length(vrow) + 1
		else {
			search_column(vrow, font, xo, tw, 0, length(vrow), c1);
			c2 = tc + c1;
			tabadjust(row, 1, 1, c2, c)
		}
	} else rc = false
}

search_column(row, font, x, xmax, c1, len, c) {
	if(len <= 1) c = c1
	else {
		list:cut(len / 2, row, [], pre, post);
		sdl:text_size(pre, font, w, _);
		if(x < w) search_column(pre, font, x, w, c1, length(pre), c)
		else search_column(post, font, x - w, xmax - w, c1 + len / 2, length(post), c)
	}
}

tabadjust(row, c, ec, c1, c2) {?
	ec >= c1 -> c2 = c;
	row ?= [TAB|more], ec < c1 -> {
		n = TABWIDTH - ec % TABWIDTH;
		if(ec + n + 1 > c1) c2 = c
		else tabadjust(more, c + 1, ec + n + 1, c1, c2)
	};
	default -> tabadjust(tail(row), c + 1, ec + 1, c1, c2)
}

search(text, rs, cs, re, ce, m, found) {
	map:lookup(rs, m, row, false);
	{?
		row == false -> found = false;
		array(row) -> {
			if(rs == re) len = ce - cs
			else len = length(row);
			array:search(text, cs - 1, len, row, p);
			if(p == false) search(text, rs + 1, 1, re, ce, m, found)
			else {
				pc = p + 1;
				found = {rs, pc}
			}
		};
		default -> {
			if(rs == re) {
				off = ce - 1;
				list:take(off, row, row2)
			} else {
				off = 0;
				row2 = row
			};
			list:search(text, row2, p);
			if(p == 0) search(text, rs + 1, 1, re, ce, m, found)
			else {
				pc = p + off;
				found = {rs, pc}
			}
		}
	}
}
