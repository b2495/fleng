%= "color" module: Access to named colors
%
% This module provides RGB values for all named X11 colors as normally found
% in "rgb.txt".
%
%-
% color:get(NAME?, RGB^)
%        Unifies RGB with a 3-tuple holding the red/green/blue components
%        of the color named NAME. If no color with that name exists, RGB
%        will be unified with "false".
%
% color:get_indexed(INDEX?, NAME^)
%        Retrieve color by numeric index from 0-N, or the empty list of
%        INDEX is out of range.
%
% color:all(COLORS^)
%        Unifies COLORS with a list of all color known names.

-module(color).

get(Name, RGB) :-
    deref(Name, Ok),
    when(Ok, foreign_call(x11_color(Name, R, G, B))),
    check(R, G, B, RGB).

check(false, _, _, RGB) :- RGB = false.
check(R, G, B, RGB) :- otherwise | RGB = {R, G, B}.

get_indexed(N, Name) :- foreign_call(x11_color_by_index(N, Name)).

all(All) :- foreign_call(x11_all_colors(All)).
