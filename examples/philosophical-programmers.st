% The "Philosophical Programmers Problem"
% from: Strand - New Concepts in Parallel Programming (p. 143)

-initialization(go).

go :-
    prog(ponder, 1, P1), prog(ponder, 2, P2),
    prog(ponder, 3, P3), prog(ponder, 4, P4),
    merger([merge(P1), merge(P2), merge(P3)|P4], Rs),
    monitor(Rs).

monitor(In) :- initial_resources(Cs), monitor(In, 0, Rq, Rq, Cs).

initial_resources(Rs) :- Rs := [set, set].

monitor([req(R)|In], N, Rf, Rb, []) :-
    Rb := [R|Rb1], N1 is N + 1,
    monitor(In, N1, Rf, Rb1, []).
monitor([req(R1)|In], N, Rf, Rb, [R|Cs]) :-
    R1 := R,
    monitor(In, N, Rf, Rb, Cs).
monitor([rel(R)|In], N, [R1|Rf], Rb, Cs) :-
    R1 := R, N1 is N - 1,
    monitor(In, N1, Rf, Rb, Cs).
monitor([rel(R)|In], 0, Rf, Rb, Cs) :-
    monitor(In, 0, Rf, Rb, [R|Cs]).
monitor([], 0, _, _, _).

prog(Mode, P, Rs) :-
    fmt:format('#~d: ~a\n', [P, Mode]) &
    prog0(Mode, P, Rs).

prog0(ponder, P, Rs) :- prog(inspired, P, Rs).
prog0(inspired, P, Rs) :- Rs := [req(R)|Rs1], prog1(inspired, P, Rs1, R).
prog0(program, P, Rs) :- Rs := [rel(set)|Rs1], prog(ponder, P, Rs1).

prog1(inspired, P, Rs, set) :- prog(program, P, Rs).
