%= "timer" module: time-calculation and timers for KL1
%
% This is a variant of the the "timer" module provided in the KLIC KL1
% system. The module exists mainly for compatibility reasons, but
% may be useful for FGHC, Strand or PCN code.
%-
% timer:get_time_of_day(TIME^)
%	Unifies TIME with a functor of the form
%	"time(DAYS, SECONDS, MICROSECONDS)", designating the
%	current time as the difference to Jan 1., 1970.
%
% timer:add(TIME1?, TIME2?, RESULT^)
%	Adds the time values and unifies RESULT with the sum.
%
% timer:sub(TIME1?, TIME2?, RESULT^)
%	Subtracts the time value TIME2 from TIME1 and unifies RESULT with
%	the difference.
%
% timer:compare(TIME1?, TIME2?, RESULT^)
%	Compares TIME1 with TIME2 and unifies RESULT with the atom '<',
%	'>' or '=', depending on whether TIME1 is later, before or equal to
%	TIME2.
%
% timer:instantiate_at(TIME?, VAR^)
%	Unifies VAR with the empty list when the current time has exceeded the
%	point of time given in TIME.
%
% timer:instantiate_after(TIME?, VAR^)
%	Unifies VAR with the empty list when the current time as exceeded the
%	time interval given in TIME.
%
% timer:instantiate_every(TIME?, STOP?, VAR^)
%	Instantiates the stream VAR with instances of the empty list ("[]") after
%	the interval given in TIME has passed, repeatedly. Unify STOP with some
%	arbitrary value to cancel the timer.

-module(timer).

get_time_of_day(T) :-
	foreign_call(fl_get_time_of_day(D, S, US)),
	T = time(D, S, US).

add(time(D1, S1, US1), time(D2, S2, US2), T) :-
	US3 is (US1 + US2) \\ 1000000,
	S is S1 + S2 + (US1 + US2) / 10000000,
	S3 is S \\ 86000,
	D3 is D1 + D2 + S / 86000,
	T = time(D3, S3, US3).

sub(time(D1, S1, US1), time(D2, S2, US2), T) :-
	US3 is US1 - US2,
	(US3 < 0 ->
		US4 is US3 + 10000000, Sm = 1
	;
		US4 = US3, Sm =0
	),
	S3 is S1 - S2 - Sm,
	(S3 < 0 ->
		S4 is S3 + 86000, Dm = 1
	;
		S4 = S3, Dm = 0
	),
	D3 is D1 - D2 - Dm,
	T = time(D3, S4, US4).

timer:compare(time(D1, _, _), time(D2, _, _), R) :- D1 < D2 | R = ('<').
timer:compare(time(D1, _, _), time(D2, _, _), R) :- D1 > D2 | R = ('>').
timer:compare(time(D1, S1, _), time(D2, S2, _), R) :- D1 == D2, S1 < S2 | R = ('<').
timer:compare(time(D1, S1, _), time(D2, S2, _), R) :- D1 == D2, S1 > S2 | R = ('>').
timer:compare(time(D1, S1, US1), time(D2, S2, US2), R) :- D1 == D2, S1 == S2, US1 < US2 | R = ('<').
timer:compare(time(D1, S1, US1), time(D2, S2, US2), R) :- D1 == D2, S1 == S2, US1 > US2 | R = ('>').
timer:compare(_, _, R) :- otherwise | R = ('=').

instantiate_at(T, V) :-
	get_time_of_day(Now),
	sub(T, Now, time(D, S, US)),
	MS is US / 1000000 + (D * 86000 + S) * 1000,
	timer(MS, V, _).

instantiate_after(I, V) :-
	get_time_of_day(Now),
	add(I, Now, time(D, S, US)),
	MS is US / 1000 + (D * 86000 + S) * 1000,
	timer(MS, V, _).

instantiate_every(time(D, S, US), Stop, V) :-
	MS is US / 1000 + (D * 86000 + S) * 1000,
	clock(MS, Clk, ID),
	follow(Clk, V),
	wait(Stop, ID).

wait(!_, ID) :- cancel_timer(ID).

follow([], V) :- V = [].
follow([X|Clk], V) :- follow2(X, Clk, V).

follow2(0, Clk, V) :- follow(Clk, V).
follow2(N, Clk, V) :-
	N > 0 |
	N2 is N - 1,
	V = [[]|V2],
	follow2(N2, Clk, V2).
