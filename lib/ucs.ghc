%= "ucs" module: unicode character classification and conversion
%
% Provides character classification (uppercase, lowercase, letter, digit,
% whitespace, alphanumeric), case conversion and folding. The operations
% in this module are all UNICODE aware (in the Basic Multlingual Plane).
%
%+ ucs:upper lower alpha space digit
% ucs:upper(CODE?, FLAG^)
% ucs:lower(CODE?, FLAG^)
% ucs:alpha(CODE?, FLAG^)
% ucs:alphanumeric(CODE?, FLAG^)
% ucs:space(CODE?, FLAG^)
% ucs:digit(CODE?, FLAG^)
%   Unify FLAG with the string "true" or "false", depending on whether
%   the character code is of the given class.
%
% ucs:digit_value(CODE?, NUM^)
%    Unify NUM with the numeric value of the digit represented by CODE or
%    the string "false" if CODE is not a digit.
%
%+ ucs:tolower toupper
% ucs:tolower(CODE?, RESULT^)
% ucs:toupper(CODE?, RESULT^)
%   Unify RESULT with the lower or uppercase conversion of the character
%   code given by CODE.
%
% ucs:foldcase(CODE?, RESULT^)
% ucs:foldcase(CODE?, RESULT^, TAIL?)
%   Unify RESULT with the list of character codes resulting from the
%   case-folding of CODE, optionally terminated by TAIL.

-module(ucs).

foldcase(C, R) :- foldcase(C, R, []).

foldcase(!C, R, T) :- foreign_call(fl_ucs_foldcase(C, R, T)).

upper(!C, F) :- foreign_call(fl_ucs_isupper(C, F)).

lower(!C, F) :- foreign_call(fl_ucs_islower(C, F)).

alpha(!C, F) :- foreign_call(fl_ucs_isalpha(C, F)).

alphanumeric(!C, F) :-
	foreign_call(fl_ucs_isalpha(C, F1)),
	(F1 == false -> digit(C, F); F = true).

space(!C, F) :- foreign_call(fl_ucs_isspace(C, F)).

digit(!C, F) :-
	foreign_call(fl_ucs_isdigit(C, F1)),
	(F1 == false -> F = false; F = true).

digit_value(!C, N) :- foreign_call(fl_ucs_isdigit(C, N)).

tolower(!C, R) :- foreign_call(fl_ucs_downcase(C, R)).

toupper(!C, R) :- foreign_call(fl_ucs_upcase(C, R)).
