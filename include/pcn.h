/* Default include file for PCN-compiled programs */

#define false   "false"
#define true    "true"

#define stdin		"stdin"
#define stdout		"stdout"
#define stderr		"stderr"

#ifdef unix
// clashes with module of the same name
# undef unix
#endif

-fleng_library([app, io, lex, parse, map, match, pcnlex, pcnparse,
    scan, sys, find, set, ucs, fmt, list, path, proc, sort, lib, array, sec, sdl,
    ezd, color, binfmt, "9p", json, eval, rx])

-formatter(fmt:format("*", "?"), fmt:format_chunked(1, "!"))
-formatter(fmt:format(f, "*", "?"), fmt:format_chunked(f, "!"))
-formatter(fmt:format(f, "*", "?", d), fmt:format_chunked(f, "!", d))
-formatter(fmt:format_chars("*", "?", o), fmt:format_chars_chunked("!", o, []))
-formatter(fmt:format_chars("*", "?", o, t), fmt:format_chars_chunked("!", o, t))
-formatter(binfmt:format("*", "?"), binfmt:format_chunked("!"))
-formatter(binfmt:format(f, "*", "?"), binfmt:format_chunked(f, "!"))
-formatter(binfmt:format(f, "*", "?", d), binfmt:format_chunked(f, "!", d))
-formatter(binfmt:format(f, "*", "?", e, d), binfmt:format_chunked(f, "!", e, d))
-formatter(binfmt:format_bytes("*", "?", o), binfmt:encode("!", o, []))
-formatter(binfmt:format_bytes("*", "?", o, t), binfmt:encode("!", o, t))
-formatter(binfmt:format_bytes("*", "?", e, o, t), binfmt:encode("!", e, o, t))
-formatter(binfmt:scan_bytes("*", "?", o, t), binfmt:decode("!", "little", o, t))
-formatter(binfmt:scan_bytes("*", "?", e, o, t), binfmt:decode("!", e, o, t))
-formatter(scan:format("*", "?", o, t), scan:decode("!", o, t))
