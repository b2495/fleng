/* plasma
   Ported from: https://github.com/patsie75/awk-plasma/blob/main/plasma.awk
*/

function clamp(val, minx, maxx) {?
    val < minx -> return(minx),
    val > maxx -> return(maxx),
    default -> return(val)
}

draw(src, w, h, palette, screen, sp)
int src[], sp, x, y, yp, up, dn; char screen[];
{
    sp := 0,
    y over 0 .. h - 1 :: {
        yp := y / 2 + 1,
        fmt_line(screen, yp, sp),
        x over 0 .. w - 1 :: {
            up := src[ y * w + x ],
            dn := src[ (y + 1) * w + x ],
            fmt_pixel(screen, palette, up, dn, sp)
        },
        y := y + 1
    }
}

fmt_line(screen, y, p) 
char screen[]; int p;
{
    fmt:format_chars("\x1b[~d;1H", [y], t),
    array:put(p, t, screen, p2),
    p := p2
}

fmt_pixel(screen, palette, u, d, p)
char screen[]; int p;
{
    map:lookup(u, palette, palu),
    map:lookup(d, palette, pald),
    fmt:format_chars("\x1b[38;2;~s;48;2;~sm", [palu, pald], t),
    array:put(p, t, screen, p2),
    array:put(p2, [226, 150, 128], screen, p3),
    p := p3
}

paletteGen(x, p1, palette) {?
    x >= 256 -> palette = p1,
    default -> {||
        r = clamp(integer(128 + 127 * sin(3.14159265 * x / 32.0)), 0, 255),
        g = clamp(integer(128 + 127 * sin(3.14159265 * x / 64.0)), 0, 255),
        b = clamp(integer(128 + 127 * sin(3.14159265 * x / 128.0)), 0, 255),
        fmt:format_chars("~d;~d;~d", [r, g, b], t),
        map:insert(x, t, p1, p2),
        paletteGen(x + 1, p2, palette)
  }
}

plasma003(plasma, w, h, now) 
int plasma[]; double color;
{
    y over 0 .. h - 1 ::
        x over 0 .. w - 1 :: {
            color := (128.0 + (128.0 * sin((x / 8.0) - cos(now/2) ))
                    + 128.0 + (128.0 * sin((y / 16.0) - sin(now)*2 ))
                    + 128.0 + (128.0 * sin(sqrt((x - w / 2.0) * (x - w / 2.0) + (y - h / 2.0) * (y - h / 2.0)) / 4.0))
                    + 128.0 + (128.0 * sin((sqrt(x * x + y * y) / 4.0) - sin(now/4) )))
                    / 4,
            plasma[ y * w + x ] := integer(color)
        }
}

main() {
    proc:capture(["stty", "size"], line),
    scan:integer(h, line, rest),
    scan:whitespace(rest, rest2),
    scan:integer(w, rest2, _),
    list_to_number(w, wn),
    list_to_number(h, hn),
    if(hn == 0) error("can not retrieve terminal size"),
    h2 = hn * 2,
    paletteGen(0, [], palette),
    array:make("int", wn * h2, plasma),
    array:make("int", wn * h2, buffer),
    array:make("char", 1000000, screen),
    loop(plasma, buffer, palette, screen, wn, h2, 0)
}

loop(plasma, buffer, palette, screen, w, h, now)
int plasma[], buffer[], len; char screen[];
{
    plasma003(plasma, w, h, now),
    paletteShift = now * 100,
    y over 0 .. h - 1 ::
        x over 0 .. w - 1 ::
            buffer[ y * w + x ] := integer(plasma[ y * w + x ] + paletteShift) % 256,
    draw(buffer, w, h, palette, screen, len),
    array:write(0, len, screen, 1),
    loop(plasma, buffer, palette, screen, w, h, now + 0.1)
}
