
     _______        _______ __   _  ______
     |______ |      |______ | \  | |  ____
     |       |_____ |______ |  \_| |_____|


    Version: 23


I. Introduction

This software is an implementation of "FLENG", a low level concurrent
logic programming language descended from Prolog. A compiler is
provided to translate programs written in FLENG into assembly language
which can subsequently be compiled and linked into a binary
executable. As FLENG is quite minimal in features, translators from
"Flat Guarded Horn Clauses (FGHC)", "Strand", "KL1" and "PCN" (other,
higher- level concurrent logic languages) into FLENG are also
available.

FLENG programs allow for massive parallelism at a very fine grained
level - goals are executed concurrently and communicate solely via
single-assignment logic variables. The use of logic variables for
inter-process communication offers an easy to use and convenient
abstraction to implement many common patterns of synchronization
and message passing in a natural manner. This implementation
additionally can distribute pools of parallel processes over native
OS level threads, thus taking advantage of multicore architectures.
Locking overhead should be small, as data is normally not shared
among processes executing on different threads. Automatic management
of memory is provided using a reference-counting scheme of fixed
size cells, which avoid fragmentation, avoids garbage collection
pauses and keeps the overall memory required to hold live data at
any point of time minimal, while providing relatively good locality.

Interfacing to native code is straightforward and compiled FLENG
code and the run time library support can be linked with other
modules written in C or C++ without problems. The compilation tools
allow cross compilation, provided the architecture is supported.

The compiler generates native code, but does currently not perform
a lot of optimizations, so performance-sensitive code should probably
be written in C or another language. FLENG is particularly well
suited as a coordination language for providing intercommunication
and synchronization with reasonable overhead while calling out to
C or C++ code for low-level or speed-critical operations.

Suggestions for improvement or patches providing enhancements and
corrections are very welcome, see the user's manual for information
on how to contact the author.

This software was written by Felix L. Winkelmann and has been released
into the public domain. See the file "doc/LICENSE" for more information.

A git[4] repository containing the sources can be found at the projects
gitlab site[5].


II. Build and Installation

* Requirements:

The run-time system for FLENG is written in C99 with GNU extensions
and should be compilable with most C compilers. The FGHC-to-FLENG
and PCN-to-FLENG translators and the FLENG compiler are written in
FGHC and require either a set of precompiled assembler files (included
in the distribution tarball), a previously built FLENG system or a
Prolog implementation that can be used to bootstrap the system.
Currently, SWI[1] Prolog , GNU[2] Prolog and YAP[3] are supported.

You additionally need a POSIX compliant sh(1) and optionally make(1)
(GNU or BSD).

The system is known to run on Linux and BSD-based UNIX systems.
Supported target architectures are x86-64 armv6 (hardfp), AArch64,
Apple M1 and riscv64 (R64IMAFD). If you are interested
in ports to other platforms, please contact me.

Support for graphical applications is provided by libraries interfacing
to SDL2[6]. If available on the installation system, these libraries
will be built and added to the module library. A higher level
structured graphics toolkit with a design based on Joel Bartletts
"EZDraw" system[7] provides more convenient means for programming
graphical applications and is built on top of the SDL interface.

At least SDL2 and SDL2_image are needed for graphics programming,
SDL2_ttf and SDL2_mixer are optional. If these can't be found
during the configuration phase of the build, then the system falls
back on a built-in 16x8 bitmap font and font and sound-related
operations silently fail.

* Configuration:

First run the "configure" shell script, optionally providing
arguments. Enter

    ./configure --help

to see a list of options for specifying the installation location
and possibly selecting the host Prolog implementation to use. By
default, the system will be installed in "/usr/local" and use GNU
Prolog, if found, or, alternatively SWI Prolog.

"configure" adds the contents of the environment variables "CC",
"CFLAGS" and "LDFLAGS" to the default compiler and linker
invocations, if set.

If the libraries for graphics support are not required, pass the
"--disable-sdl" option to the invocation of "configure".

* Building:

Once configuration has been done, you can run make(1) to build the
compiler and run-time system.

First, run

    make

(If you build directly from the sources of the development
repository[5], you should first run

    make spotless

to ensure no old translated files from previous check outs pollute
the build.)

If no errors occur, you can optionally enter

    make check

to run the test suite.

    make bench

Runs a number of test programs and measures their run time.

When building from a distribution tarball, the system is usually
directly generated from precompiled assembler files. By removing
or renaming the "asm" directory you can force building the system
completely from sources with one of the supported Prolog systems
by entering

    make bootstrap

you will force a bootstrap from the baseline Prolog compiler.
A suitable Prolog system will be searched, you can override the
automatic search by using the "--with-prolog" option for the
"configure" script, giving it the full path to the Prolog compiler
intended to use for bootstrapping.

Building the system in a different directory than the one containing
the source code is not supported.

By setting the environment variable "FLENG_PREFIX" to the current
working directory, you can use the generated "fleng" driver script
in-place to use or test the system without installation.

* Installation:

To install, enter

    make install

If you do not have permissions to access the installation location,
use sudo(1) or doas(1) when invoking "make install". The variable
"$DESTDIR" will be respected when defined to further customize the
installation destination.

The following files will be installed:

    $PREFIX                             Defaults to /usr/local
    |-- bin
    |   |-- fghc2fl                     FHGC->FLENG compiler
    |   |-- pcn2fl                      PCN->FLENG compiler
    |   |-- kl12fl						KL1->FLENG compiler
    |   |-- fl2<ARCH>                   FLENG->assembler compiler
    |   |-- flengdoc                    Documentation tool
    |   |-- flengmake                   Build tool
    |   |-- flengbundle                 Tool for integration data
    |   |-- flengdraw                   Graphics editor
    |   `-- fleng                       Driver script
    |-- include
    |   `-- fleng
    |       |-- fleng-util.h            Utility header for C extensions
    |       |-- fleng.h                 Main header for C extensions
    |       `-- pcn.h                   Default include file for PCN code
    |-- share
    |   |-- doc
    |   |   `-- fleng
    |   |       |-- MANUAL              User's manual
    |   |       |-- INTERNALS           Documentation on some internals
    |   |       |-- LIBRARY             Documentation for all libraries
    |   |       |-- README              This document
    |   |       |-- LICENSE             License
    |   |       |-- PCN-tutorial.txt    PCN tutorial
    |   |       `-- DOCDB               "flengdoc" database
    |   |           `-- ...
    |   `-- man
    |       |-- man1
    |       |   |-- fleng.1             Manual page for driver script
    |       |   |-- flengdoc.1          Manual page for documentation tool
    |       |   |-- flengmake.1         Manual page for build tool
    |       |   |-- flengbundle.1       Manual page for data tool
    |       |   `-- flengdraw.1         Manual page for graphics editor
    |       `-- man7
    |           |-- fleng.7             Manual page for runtime options
    |           `-- flengdoc.7          Manual page describing documentation format
    `-- lib
        `-- libfleng.a                  FLENG runtime library


III. Documentation

The documentation is available in textual form in the files
"doc/MANUAL", "LIBRARY" and "doc/INTERNALS". The "flengdoc" tool can
be used to query the documentation reference database, which is
created and installed along with the rest of the system. See

    man flengdoc

for more information.


IV. Get into Contact

In case you need help, have suggestions or ideas, please don't
hesitate to contact the author at

    felix AT call-with-current-continuation DOT org

There is the IRC channel "#fleng" on https://libera.chat/ where
you can ask questions, suggest improvements or if you just want
to chat.


V. References

[1] http://www.swi-prolog.org
[2] http://www.gprolog.org
[3] https://github.com/vscosta/yap
[4] https://git-scm.com/
[5] https://gitlab.com/b2495/fleng
[6] https://www.libsdl.org
[7] https://archive.org/download/bitsavers_dectechrep_144635/WRL-91-6.pdf
