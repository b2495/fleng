/* FLENG - random numbers

   Taken more or less completely from CHICKEN, with code from Kooda 
   and Riastradh.

   WELL512 pseudo random number generator, see also:
   https://en.wikipedia.org/wiki/Well_equidistributed_long-period_linear
   http://lomont.org/Math/Papers/2008/Lomont_PRNG_2008.pdf

  Copyright (c) 2007-2021, The CHICKEN Team
  All rights reserved.
  
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. The name of the authors may not be used to endorse or promote products
     derived from this software without specific prior written permission.
  
  THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <math.h>
#include <stdint.h>
#include <string.h>

#define RANDOM_STATE_SIZE   (16 * sizeof(unsigned long))

static unsigned long random_state[ RANDOM_STATE_SIZE / sizeof(unsigned long) ]; 
static int random_state_index = 0;

static unsigned long random_word(void)
{ 
  unsigned long a, b, c, d, r; 
  a  = random_state[random_state_index]; 
  c  = random_state[(random_state_index+13)&15]; 
  b  = a^c^(a<<16)^(c<<15); 
  c  = random_state[(random_state_index+9)&15]; 
  c ^= (c>>11); 
  a  = random_state[random_state_index] = b^c;  
  d  = a^((a<<5)&0xDA442D24UL);  
  random_state_index = (random_state_index + 15)&15; 
  a  = random_state[random_state_index]; 
  random_state[random_state_index] = a^b^d^(a<<2)^(b<<18)^(c<<28); 
  r = random_state[random_state_index];
  return r;
} 

/*
 * C_a_i_random_real: Generate a stream of bits uniformly at random and
 * interpret it as the fractional part of the binary expansion of a
 * number in [0, 1], 0.00001010011111010100...; then round it.
 * More information on https://mumble.net/~campbell/2014/04/28/uniform-random-float
 */

static inline uint64_t random64() {
#if defined(__LP64__) || defined(_LP64)
    return random_word();
#else
    uint64_t v = 0;
    v |= ((uint64_t) random_word()) << 32;
    v |= (uint64_t) random_word();
    return v;
#endif
}

#ifdef __GNUC__
# define	clz64	__builtin_clzll		
#else
/* https://en.wikipedia.org/wiki/Find_first_set#CLZ */
static const unsigned char clz_table_4bit[16] = { 4, 3, 2, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 };

int clz32(C_u32 x)
{
  int n;
  if ((x & 0xFFFF0000) == 0) {n  = 16; x <<= 16;} else {n = 0;}
  if ((x & 0xFF000000) == 0) {n +=  8; x <<=  8;}
  if ((x & 0xF0000000) == 0) {n +=  4; x <<=  4;}
  n += (int)clz_table_4bit[x >> (32-4)];
  return n;
}

int clz64(C_u64 x) 
{
    int y = clz32(x >> 32);
    if(y == 32) return y + clz32(x);
    return y;
}
#endif

double fl_random(void) 
{
  int exponent = -64;
  uint64_t significand;
  unsigned shift;
  while((significand = random64()) == 0) {
    exponent -= 64;
    if (exponent < -1074) return 0;
  }
  shift = clz64(significand);
  if (shift != 0) {
    exponent -= shift;
    significand <<= shift;
    significand |= (random64() >> (64 - shift));
  }
  significand |= 1;
  return ldexp((double)significand, exponent);
}

long fl_random_integer(long n) 
{
    return random64() % n;
}

void fl_set_random_seed(char *buf, int n)
{
  int i, nsu = n / sizeof(unsigned long);
  int off = 0;
  memset(random_state, 0, RANDOM_STATE_SIZE);
  for(i = 0; i < (RANDOM_STATE_SIZE / sizeof(unsigned long)); ++i) {
    if(off >= nsu) off = 0;
    random_state[ i ] = *((unsigned long *)buf + off);
    ++off;
  }
  random_state_index = 0;
}
