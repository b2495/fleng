% parse all source files and generate dot(1) graph from its dependencies
% by distributing files over available threads.

-initialization(main).

main :- 
    command_line(Args),
    threads(N),
    length(Args, FN),
    P is integer(ceiling(real(FN) / N)),
    schedule(Args, P, DL),
    report_dependencies(DL).

schedule([], _, DL) :- DL = [].
schedule(Args, P, DL) :-
    otherwise |
    list:cut(P, Args, [], Files, Rest),
    length(Files, PN),
    list:make(PN, DL, DL2),
    parse_files(Files, DL),
    schedule(Rest, P, DL2)@fwd.

parse_files([], _).
parse_files([File|L], [R|RL]) :-
    log(parse(File)),
    process_file(File, DL),
    file_to_module(File, M),
    R = M-DL,
    deref(DL, Ok),
%    log(deps(DL)),
    when(Ok, parse_files(L, RL)).   % only process one file at a time

process_file(Name, DL) :-
    open_port(P, S),
    handle_errors(S, Name),
    open_file(Name, r, File),
    parse:parse_terms(File, Forms, P),
    grovel(Forms, [], Deps),
    map:keys(Deps, DL).

handle_errors([], _).
handle_errors([error(Ln, Fmt, Args, Code)|S], Name) :-
    fmt:format(2, 'Error: (~s:~d) ~?\n', [Name, Ln, Fmt, Args], _) &
    handle_errors(Code, S, Name).

handle_errors(0, S, Name) :- handle_errors(S, Name).
handle_errors(N, _, _) :- otherwise | halt(N).

grovel([], Deps1, Deps) :- Deps = Deps1.
grovel([X|S], Deps1, Deps) :-
%    log(grovel(X)),
    walk_top(X, Deps1, Deps2),
    grovel(S, Deps2, Deps).

walk_top(-uses(L), Deps1, Deps) :- add_uses(L, Deps1, Deps).
walk_top((_ :- _ | B), Deps1, Deps) :- walk_body(B, Deps1, Deps).
walk_top((_ :- B), Deps1, Deps) :- otherwise | walk_body(B, Deps1, Deps).
walk_top(_, Deps1, Deps) :- otherwise | Deps = Deps1.

walk_body((X, Y), Deps1, Deps) :-
    walk_body(X, Deps1, Deps2),
    walk_body(Y, Deps2, Deps).
walk_body((X & Y), Deps1, Deps) :-
    walk_body(X, Deps1, Deps2),
    walk_body(Y, Deps2, Deps).
walk_body(call(M:_, _), Deps1, Deps) :- 
    string(M) | map:insert(M, true, Deps1, Deps).
walk_body(call(M:_, _, _), Deps1, Deps) :- 
    string(M) | map:insert(M, true, Deps1, Deps).
walk_body(when(_, X), Deps1, Deps) :- walk_body(X, Deps1, Deps).
walk_body(M:_, Deps1, Deps) :- 
    string(M) | map:insert(M, true, Deps1, Deps).
walk_body(_, Deps1, Deps) :- otherwise | Deps = Deps1.

add_uses([], D1, D) :- D = D1.
add_uses([M|L], D1, D) :- 
    map:insert(M, true, D1, D2),
    add_uses(L, D2, D).
add_uses(M, D1, D) :- otherwise | add_uses([M], D1, D).

report_dependencies(DL) :-
    open_file('deps.dot', w, File),
    fmt:format(File, 'digraph deps {\n', [], Ok),
    report_dependencies(Ok, DL, File).

report_dependencies([], [], File) :- 
    fmt:format(File, '}\n', [], Ok),
    when(Ok, close_file(File, _)).
report_dependencies([], [M-L|DL], File) :-
    write_deps(L, M, File, Ok),
    report_dependencies(Ok, DL, File).

write_deps([], _, _, Done) :- Done = [].
write_deps([M|L], Name, File, Done) :-
    fmt:format(File, '"~s" -> "~s";\n', [Name, M], Ok),
    when(Ok, write_deps(L, Name, File, Done)).

file_to_module(Name, M) :- 
    string_to_list(Name, L, []),
    list:split(L, 0'., [M1|_], []),
    list_to_string(M1, M).
