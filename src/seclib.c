/* Runtime support for sec.ghc */

#include "fleng.h"
#include "fleng-util.h"
#include <unistd.h>
#include <pwd.h>
#include <grp.h>

void sec_drop_privileges_2(FL_TCB *tcb, FL_VAL user, FL_VAL done)
{
    struct passwd *pw;
    if(BITS(user) == 2) {
        char *n = stringify(tcb, user, NULL);
        pw = getpwnam(n);
    } else {
        CHECK_INT(user);
        pw = getpwuid(INT(user));
    }
    if(pw == NULL) {
        fprintf(stderr, "sec: unknown user: ");
        fl_abort(tcb, user);
    }
    if(initgroups(pw->pw_name, pw->pw_gid) != 0 ||
        setgid(pw->pw_gid) != 0 || setuid(pw->pw_uid) != 0)
	fl_rt_error(tcb, user, FL_IO_ERROR);
    fl_assign(tcb, fl_nil, done);
}

void sec_unveil_3(FL_TCB *tcb, FL_VAL path, FL_VAL perm, FL_VAL done)
{
#ifdef __OpenBSD__
    char *p = path == fl_nil ? NULL : stringify(tcb, path, NULL);
    char *pe = perm == fl_nil ? NULL : stringify(tcb, perm, NULL);
    if(unveil(p, pe) < 0) fl_io_error(tcb, done);
#endif
    fl_assign(tcb, fl_nil, done);
}

void sec_pledge_3(FL_TCB *tcb, FL_VAL proms, FL_VAL xproms, FL_VAL done)
{
#ifdef __OpenBSD__
    char *p = proms == fl_nil ? NULL : stringify(tcb, proms, NULL);
    char *xp = proms == fl_nil ? NULL : stringify(tcb, xproms, NULL);
    if(pledge(p, xp) < 0) fl_io_error(tcb, done);
#endif
    fl_assign(tcb, fl_nil, done);
}
