/* Base64 encoding/decoding

   Adapted from: https://github.com/zhicheng/base64

   This is a public domain base64 implementation written by WEI Zhicheng.
*/

#include "fleng.h"
#include "fleng-util.h"

#define BASE64_PAD '='
#define BASE64DE_FIRST '+'
#define BASE64DE_LAST 'z'

/* BASE 64 encode table */
static const char base64en[] = {
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
	'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
	'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
	'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
	'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
	'w', 'x', 'y', 'z', '0', '1', '2', '3',
	'4', '5', '6', '7', '8', '9', '+', '/',
};

/* ASCII order for BASE 64 decode, 255 in unused character */
static const unsigned char base64de[] = {
	/* nul, soh, stx, etx, eot, enq, ack, bel, */
	   255, 255, 255, 255, 255, 255, 255, 255,

	/*  bs,  ht,  nl,  vt,  np,  cr,  so,  si, */
	   255, 255, 255, 255, 255, 255, 255, 255,

	/* dle, dc1, dc2, dc3, dc4, nak, syn, etb, */
	   255, 255, 255, 255, 255, 255, 255, 255,

	/* can,  em, sub, esc,  fs,  gs,  rs,  us, */
	   255, 255, 255, 255, 255, 255, 255, 255,

	/*  sp, '!', '"', '#', '$', '%', '&', ''', */
	   255, 255, 255, 255, 255, 255, 255, 255,

	/* '(', ')', '*', '+', ',', '-', '.', '/', */
	   255, 255, 255,  62, 255, 255, 255,  63,

	/* '0', '1', '2', '3', '4', '5', '6', '7', */
	    52,  53,  54,  55,  56,  57,  58,  59,

	/* '8', '9', ':', ';', '<', '=', '>', '?', */
	    60,  61, 255, 255, 255, 255, 255, 255,

	/* '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', */
	   255,   0,   1,  2,   3,   4,   5,    6,

	/* 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', */
	     7,   8,   9,  10,  11,  12,  13,  14,

	/* 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', */
	    15,  16,  17,  18,  19,  20,  21,  22,

	/* 'X', 'Y', 'Z', '[', '\', ']', '^', '_', */
	    23,  24,  25, 255, 255, 255, 255, 255,

	/* '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', */
	   255,  26,  27,  28,  29,  30,  31,  32,

	/* 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', */
	    33,  34,  35,  36,  37,  38,  39,  40,

	/* 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', */
	    41,  42,  43,  44,  45,  46,  47,  48,

	/* 'x', 'y', 'z', '{', '|', '}', '~', del, */
	    49,  50,  51, 255, 255, 255, 255, 255
};

static unsigned int
base64_encode(const unsigned char *in, unsigned int inlen, int s, unsigned char l,
    char *out, int *so, unsigned char *lo)
{
	unsigned int i;
	unsigned int j;
	unsigned char c;

	for (i = j = 0; i < inlen; i++) {
		c = in[i];

		switch (s) {
		case 0:
			s = 1;
			out[j++] = base64en[(c >> 2) & 0x3F];
			break;
		case 1:
			s = 2;
			out[j++] = base64en[((l & 0x3) << 4) | ((c >> 4) & 0xF)];
			break;
		case 2:
			s = 0;
			out[j++] = base64en[((l & 0xF) << 2) | ((c >> 6) & 0x3)];
			out[j++] = base64en[c & 0x3F];
			break;
		}
		l = c;
	}

        *so = s;
        *lo = l;
        return j;
}

static int
base64_encode_end(int s, unsigned char l, char *out)
{
	switch (s) {
	case 1:
		*(out++) = base64en[(l & 0x3) << 4];
		*(out++) = BASE64_PAD;
		*(out++) = BASE64_PAD;
		return 3;
	case 2:
		*(out++) = base64en[(l & 0xF) << 2];
		*(out++) = BASE64_PAD;
		return 2;
	}

	return 0;
}

void
fl_base64_encode_4(FL_TCB *tcb, FL_VAL input, FL_VAL state, FL_VAL output, FL_VAL rest)
{
#define MAX_BUF 16
    unsigned char buf[ MAX_BUF ], out[ MAX_BUF * 2 ];
    LIST(state, s, l);
    LIST(rest, outstate, rest2);
    LIST(rest2, in2, tail);
    int n, so;
    unsigned char lo;
    int len = 0;
    input = deref(input);
    while(BITS(input) == 0 && TAG(input) == FL_LIST_TAG) {
        FL_VAL c = deref(CAR(input));
        if(BITS(c) == 0) break;
        CHECK_INT(c);
        buf[ len++ ] = INT(c);
        input = deref(CDR(input));
        if(len >= MAX_BUF) break;
    }
    n = base64_encode(buf, len, INT(s), INT(l), (char *)out, &so, &lo);
    FL_VAL r = tail;
    for(int i = n - 1; i >= 0; --i)
        r = mklist(tcb, MKINT(out[ i ]), r);
    fl_assign(tcb, input, in2);
    fl_assign(tcb, r, output);
    fl_assign(tcb, mklist(tcb, MKINT(so), MKINT(lo)), outstate);
}

void
fl_base64_encode_end_3(FL_TCB *tcb, FL_VAL state, FL_VAL output, FL_VAL tail)
{
    unsigned char buf[ 3 ];
    LIST(state, s, l);
    int n = base64_encode_end(INT(s), INT(l), (char *)buf);
    FL_VAL r = tail;
    for(int i = n - 1; i >= 0; --i)
        r = mklist(tcb, MKINT(buf[ i ]), r);
    fl_assign(tcb, r, output);
}

void
fl_base64_decode_4(FL_TCB *tcb, FL_VAL in, FL_VAL state, FL_VAL output, FL_VAL rest)
{
    LIST(state, pos, val);
    LIST(rest, tail, outstate);
    CHECK_INT(in);
    int b = INT(in);
    if (b == BASE64_PAD) {
        fl_assign(tcb, tail, output);
        fl_assign(tcb, state, outstate);
        return;
    }
    int p1 = INT(pos);
    unsigned char o1 = INT(val);
    if(b < BASE64DE_FIRST || b > BASE64DE_LAST) goto fail;
    unsigned char c = base64de[(unsigned char)b];
    if (c == 255) goto fail;
    char o2;
    switch (p1 & 0x3) {
    case 0:
        o1 = (c << 2) & 0xFF;
        fl_assign(tcb, tail, output);
        fl_assign(tcb, mklist(tcb, MKINT(p1 + 1), MKINT(o1)), outstate);
        return;
    case 1:
        o1 |= (c >> 4) & 0x3;
        o2 = (c & 0xF) << 4;
        fl_assign(tcb, mklist(tcb, MKINT(o1), tail), output);
        fl_assign(tcb, mklist(tcb, MKINT(p1 + 1), MKINT(o2)), outstate);
        return;
    case 2:
        o1 |= (c >> 2) & 0xF;
        o2 = (c & 0x3) << 6;
        fl_assign(tcb, mklist(tcb, MKINT(o1), tail), output);
        fl_assign(tcb, mklist(tcb, MKINT(p1 + 1), MKINT(o2)), outstate);
        return;
    default:
        o1 |= c;
        fl_assign(tcb, mklist(tcb, MKINT(o1), tail), output);
        fl_assign(tcb, mklist(tcb, MKINT(p1 + 1), MKINT(o1)), outstate);
        return;
    }
fail:
    fl_assign(tcb, fl_error, outstate);
}
