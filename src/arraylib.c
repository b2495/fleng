/* FLENG - mutable numeric array library - C part */

#include "fleng.h"
#include "fleng-util.h"
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>

static void put(FL_TCB *tcb, unsigned char *p, long t, FL_VAL x)
{
    switch(t) {
    case FL_KL1_STRING:
    case FL_CHAR_ARRAY:
        CHECK_INT(x);
        *p = INT(x) & 255;
        break;
    case FL_INT_ARRAY:
        CHECK_INT(x);
        *((int *)p) = INT(x);
        break;
    case FL_LONG_ARRAY:
        CHECK_INT(x);
        *((long *)p) = INT(x);
        break;
    case FL_SHORT_ARRAY:
        CHECK_INT(x);
         *((short *)p) = INT(x);
        break;
    case FL_DOUBLE_ARRAY: {
        double y;
        CHECK_NUMBER(x);
        if(ISINT(x)) y = INT(x);
        else y = FLOATVAL(x);
        *((double *)p) = y;
        break; }
    }
}

void fl_array_make_long_2(FL_TCB *tcb, FL_VAL size, FL_VAL var)
{
    CHECK_INT(size);
    void *ptr = malloc(INT(size) * sizeof(long));
    if(ptr == NULL) fl_assign(tcb, fl_false, var);
    else fl_assign(tcb, mkarray(tcb, FL_LONG_ARRAY, ptr, INT(size)), var);
}

void fl_array_make_int_2(FL_TCB *tcb, FL_VAL size, FL_VAL var)
{
    CHECK_INT(size);
    void *ptr = malloc(INT(size) * sizeof(int));
    if(ptr == NULL) fl_assign(tcb, fl_false, var);
    else fl_assign(tcb, mkarray(tcb, FL_INT_ARRAY, ptr, INT(size)), var);
}

void fl_array_make_short_2(FL_TCB *tcb, FL_VAL size, FL_VAL var)
{
    CHECK_INT(size);
    void *ptr = malloc(INT(size) * sizeof(short));
    if(ptr == NULL) fl_assign(tcb, fl_false, var);
    else fl_assign(tcb, mkarray(tcb, FL_SHORT_ARRAY, ptr, INT(size)), var);
}

void fl_array_make_char_2(FL_TCB *tcb, FL_VAL size, FL_VAL var)
{
    CHECK_INT(size);
    void *ptr = malloc(INT(size));
    if(ptr == NULL) fl_assign(tcb, fl_false, var);
    else fl_assign(tcb, mkarray(tcb, FL_CHAR_ARRAY, ptr, INT(size)), var);
}

void fl_array_make_kl1string_2(FL_TCB *tcb, FL_VAL size, FL_VAL var)
{
    CHECK_INT(size);
    void *ptr = malloc(INT(size));
    if(ptr == NULL) fl_assign(tcb, fl_false, var);
    else fl_assign(tcb, mkarray(tcb, FL_KL1_STRING, ptr, INT(size)), var);
}

void fl_array_make_double_2(FL_TCB *tcb, FL_VAL size, FL_VAL var)
{
    CHECK_INT(size);
    void *ptr = malloc(INT(size) * sizeof(double));
    if(ptr == NULL) fl_assign(tcb, fl_false, var);
    else fl_assign(tcb, mkarray(tcb, FL_DOUBLE_ARRAY, ptr, INT(size)), var);
}

void fl_array_put_4(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL elts,
    FL_VAL done)
{
    CHECK_ARRAY(array);
    CHECK_INT(index);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    int alen = ARRAY_LENGTH(array);
    int i = INT(index);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    if(elts == fl_nil) goto done;
    if(i < 0 || i >= alen)
        fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    if(ISINT(elts) || (ISCELL(elts) && TAG(elts) == FL_FLOAT_TAG)) {
        put(tcb, p, t, elts);
        ++i;
    } else {
        while(ISCELL(elts) && TAG(elts) == FL_LIST_TAG) {
            if(i++ >= alen) break;
            FL_VAL x = deref(CAR(elts));
            elts = deref(CDR(elts));
            put(tcb, p, t, x);
            p += es;
        }
    }
done:
    fl_assign(tcb, MKINT(i), done);
}

void fl_array_get_4(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL len,
    FL_VAL more)
{
    CHECK_ARRAY(array);
    CHECK_INT(index);
    CHECK_INT(len);
    LIST(more, vals, tail);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    int alen = ARRAY_LENGTH(array);
    int i = INT(index);
    if(i < 0 || i >= alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    int n = INT(len);
    if(n < 1) {
        fl_unify_result(tcb, tail, vals);
        return;
    }
    FL_VAL lst = tail;
    if(i + n > alen) n -= (i + n) - alen;
    p += (n - 1) * es;
    while(n-- > 0) {
        FL_VAL x;
        switch(t) {
        case FL_KL1_STRING:
        case FL_CHAR_ARRAY: x = MKINT(*p); break;
        case FL_INT_ARRAY: x = MKINT(*((int *)p)); break;
        case FL_LONG_ARRAY: x = MKINT(*((long *)p)); break;
        case FL_SHORT_ARRAY: x = MKINT(*((short *)p)); break;
        case FL_DOUBLE_ARRAY: x = fl_alloc_float(tcb, *((double *)p)); break;
        }
        lst = mklist(tcb, x, lst);
        p -= es;
    }
    fl_unify_result(tcb, lst, vals);
}

void fl_array_write_4(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL len,
    FL_VAL more)
{
    CHECK_ARRAY(array);
    CHECK_INT(len);
    CHECK_INT(index);
    LIST(more, fd, done);
    int ffd = fl_file(tcb, fd);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    int alen = ARRAY_LENGTH(array);
    int i = INT(index);
    if(i < 0 || i > alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    int l = INT(len);
    if(l + i >= alen) l = alen - i;
    int r;
    if(l > 0) r = write(ffd, p, l * es);
    else r = 0;
    fl_assign(tcb, MKINT(r), done);
}

void fl_array_write_utf_4(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL len,
    FL_VAL more)
{
    CHECK_ARRAY_TYPE(array, FL_INT_ARRAY);
    CHECK_INT(len);
    CHECK_INT(index);
    LIST(more, fd, done);
    int ffd = fl_file(tcb, fd);
    int alen = ARRAY_LENGTH(array);
    int i = INT(index);
    if(i < 0 || i > alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned int *p = (unsigned int *)ARRAY_POINTER(array) + i;
    int l = INT(len);
    if(l + i >= alen) l = alen - i;
    int r;
    if(l > 0) {
        char *buf = malloc(l * 5);
        char *pb = buf;
        if(buf == NULL) fl_rt_error(tcb, array, FL_OUT_OF_MEMORY);
        while(l--) pb = utf8_encode(*(p++), pb);
        r = write(ffd, buf, pb - buf);
    } else r = 0;
    fl_assign(tcb, MKINT(r), done);
}

void fl_array_read_4(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL len,
    FL_VAL more)
{
    CHECK_ARRAY(array);
    CHECK_INT(len);
    CHECK_INT(index);
    LIST(more, fd, done);
    int ffd = fl_file(tcb, fd);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    int alen = ARRAY_LENGTH(array);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    int i = INT(index);
    if(i < 0 || i >= alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    int l = INT(len);
    if(l + i >= alen) l = alen - i;
    int r = read(ffd, p, l * es);
    fl_assign(tcb, MKINT(r / es), done);
}

void fl_array_view_4(FL_TCB *tcb, FL_VAL array, FL_VAL vt, FL_VAL index,
    FL_VAL more)
{
    CHECK_ARRAY(array);
    CHECK_INT(index);
    LIST(more, len, r);
    CHECK_INT(len);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    int alen = ARRAY_LENGTH(array);
    int i = INT(index);
    if(i < 0 || i >= alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    int ves = array_element_size(INT(vt));
    int l = INT(len);
    if(i + l * ves > alen * es) l = ((alen - i) * es) / ves;
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    FL_VAL a = mkarray(tcb, t, p, l);
    CDR(a) = addref(array);
    fl_assign(tcb, a, r);
}

void fl_array_fill_4(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL len,
    FL_VAL more)
{
    CHECK_ARRAY(array);
    CHECK_INT(len);
    CHECK_INT(index);
    LIST(more, x, done);
    int alen = ARRAY_LENGTH(array);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    int i = INT(index);
    if(i >= alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    int n = INT(len);
    while(i++ <= alen && n--) {
        put(tcb, p, t, x);
        p += es;
    }
    fl_assign(tcb, fl_nil, done);
}

void fl_array_copy_4(FL_TCB *tcb, FL_VAL from, FL_VAL to, FL_VAL s1, FL_VAL more)
{
    CHECK_ARRAY(from);
    CHECK_ARRAY(to);
    CHECK_INT(s1);
    LIST(more, s2, m2);
    LIST(m2, count, done);
    CHECK_INT(s2);
    CHECK_INT(count);
    int flen = ARRAY_LENGTH(from);
    int t = ARRAY_TYPE(from);
    int fes = array_element_size(t);
    int fs = INT(s1);
    int ts = INT(s2);
    int c = INT(count);
    if(fs < 0 || fs >= flen) fl_rt_error2(tcb, s1, from, FL_BAD_INDEX);
    unsigned char *fp = (unsigned char *)ARRAY_POINTER(from) + fs * fes;
    int tlen = ARRAY_LENGTH(to);
    t = ARRAY_TYPE(to);
    if(ARRAY_IMMUTABLE(to)) fl_rt_error(tcb, to, FL_IMMUTABLE);
    int tes = array_element_size(t);
    if(ts < 0 || ts >= tlen) fl_rt_error2(tcb, s2, to, FL_BAD_INDEX);
    unsigned char *tp = (unsigned char *)ARRAY_POINTER(to) + ts * tes;
    /*XXX only copy to from/to max length */
    memmove(tp, fp, fes * c);
done:
    fl_assign(tcb, MKINT(c + ts), done);
}

void fl_array_length_2(FL_TCB *tcb, FL_VAL array, FL_VAL len)
{
    if(!ISCELL(array) || TAG(array) != FL_ARRAY_TAG)
        fl_assign(tcb, MKINT(1), len);
    else fl_assign(tcb, MKINT(ARRAY_LENGTH(array)), len);
}

void fl_array_check_char_1(FL_TCB *tcb, FL_VAL x)
{
    if(!ISCELL(x) || TAG(x) != FL_ARRAY_TAG ||
        ARRAY_TYPE(x) != FL_CHAR_ARRAY)
        fl_rt_error(tcb, x, FL_NOT_A_CHAR_ARRAY);
}

void fl_array_check_int_1(FL_TCB *tcb, FL_VAL x)
{
    if(!ISCELL(x) || TAG(x) != FL_ARRAY_TAG ||
        ARRAY_TYPE(x) != FL_INT_ARRAY)
        fl_rt_error(tcb, x, FL_NOT_AN_INT_ARRAY);
}

void fl_array_check_short_1(FL_TCB *tcb, FL_VAL x)
{
    if(!ISCELL(x) || TAG(x) != FL_ARRAY_TAG ||
        ARRAY_TYPE(x) != FL_SHORT_ARRAY)
        fl_rt_error(tcb, x, FL_NOT_A_SHORT_ARRAY);
}

void fl_array_check_long_1(FL_TCB *tcb, FL_VAL x)
{
    if(!ISCELL(x) || TAG(x) != FL_ARRAY_TAG ||
        ARRAY_TYPE(x) != FL_LONG_ARRAY)
        fl_rt_error(tcb, x, FL_NOT_A_LONG_ARRAY);
}

void fl_array_check_double_1(FL_TCB *tcb, FL_VAL x)
{
    if(!ISCELL(x) || TAG(x) != FL_ARRAY_TAG ||
        ARRAY_TYPE(x) != FL_DOUBLE_ARRAY)
        fl_rt_error(tcb, x, FL_NOT_A_DOUBLE_ARRAY);
}

void fl_array_long_set_3(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL x)
{
    /* assumes array is valid */
    CHECK_INT(index);
    CHECK_INT(x);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    int i = INT(index);
    if(i < 0 || i >= ARRAY_LENGTH(array))
        fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    ((long *)ARRAY_POINTER(array))[ i ] = INT(x);
}

void fl_array_int_set_3(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL x)
{
    /* assumes array is valid */
    CHECK_INT(index);
    CHECK_INT(x);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    int i = INT(index);
    if(i < 0 || i >= ARRAY_LENGTH(array))
        fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    ((unsigned int *)ARRAY_POINTER(array))[ i ] = INT(x);
}

void fl_array_char_set_3(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL x)
{
    /* assumes array is valid */
    CHECK_INT(index);
    CHECK_INT(x);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    int i = INT(index);
    if(i < 0 || i >= ARRAY_LENGTH(array))
        fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    ((unsigned char *)ARRAY_POINTER(array))[ i ] = INT(x);
}

void fl_array_short_set_3(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL x)
{
    /* assumes array is valid */
    CHECK_INT(index);
    CHECK_INT(x);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    int i = INT(index);
    if(i < 0 || i >= ARRAY_LENGTH(array))
        fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    ((short *)ARRAY_POINTER(array))[ i ] = INT(x);
}

void fl_array_double_set_3(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL x)
{
    /* assumes array is valid */
    CHECK_INT(index);
    double y;
    CHECK_NUMBER(x);
    if(ISINT(x)) y = INT(x);
    else y = FLOATVAL(x);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    int i = INT(index);
    if(i < 0 || i >= ARRAY_LENGTH(array))
        fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    ((double *)ARRAY_POINTER(array))[ i ] = y;
}

void fl_array_set_3(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL x)
{
    CHECK_ARRAY(array);
    CHECK_INT(index);
    int alen = ARRAY_LENGTH(array);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    if(ARRAY_IMMUTABLE(array))
        fl_rt_error(tcb, array, FL_IMMUTABLE);
    int i = INT(index);
    if(i < 0 || i >= alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    put(tcb, p, t, x);
}

void fl_index_ref_3(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL r)
{
    CHECK_INT(index);
    int i = INT(index);
    if(i < 0) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    if(array != fl_nil && ISSTRING(array)) {
        char *p = STRING(array);
        unsigned int c;
        while(i-- >= 0) {
            if(*p == '\0') fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
            p = utf8_decode(p, &c);
        }
        fl_assign(tcb, MKINT(c), r);
        return;
    }
    if(ISCELL(array)) {
        FL_VAL lst = array;
        if(TAG(lst) == FL_LIST_TAG) {
            while(ISCELL(lst) && TAG(lst) == FL_LIST_TAG) {
                if(i-- == 0) {
                    fl_assign(tcb, CAR(lst), r);
                    return;
                }
                lst = deref(CDR(lst));
            }
            fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
        }
        if((TAG(array) & FL_TUPLE_BIT) != 0) {
            FL_VAL x;
            if(i > TUPLE_LENGTH(array))
                fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
            while(i--) array = CDR(array);
            fl_assign(tcb, CAR(array), r);
            return;
        }
    }
    CHECK_ARRAY(array);
    int alen = ARRAY_LENGTH(array);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    if(i >= alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    switch(t) {
    case FL_KL1_STRING:
    case FL_CHAR_ARRAY:
        fl_assign(tcb, MKINT(*p), r);
        break;
    case FL_LONG_ARRAY:
        fl_assign(tcb, MKINT(*((long *)p)), r);
        break;
    case FL_INT_ARRAY:
        fl_assign(tcb, MKINT(*((int *)p)), r);
        break;
    case FL_SHORT_ARRAY:
        fl_assign(tcb, MKINT(*((short *)p)), r);
        break;
    case FL_DOUBLE_ARRAY:
        fl_assign(tcb, fl_alloc_float(tcb, *((double *)p)), r);
        break;
    }
}

void fl_array_type_2(FL_TCB *tcb, FL_VAL array, FL_VAL t)
{
    CHECK_ARRAY(array);
    fl_assign(tcb, MKINT(ARRAY_TYPE(array)), t);
}

void fl_array_element_3(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL r)
{
    CHECK_INT(index);
    int i = INT(index);
    if(i < 0) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    CHECK_ARRAY(array);
    int alen = ARRAY_LENGTH(array);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    if(i >= alen) fl_rt_error2(tcb, index, array, FL_BAD_INDEX);
    unsigned char *p = (unsigned char *)ARRAY_POINTER(array) + i * es;
    int im = ARRAY_IMMUTABLE(array) ? FL_ARRAY_IMMUTABLE : 0;
    FL_VAL a = mkarray(tcb, t | im, p, 1);
    CDR(a) = addref(array);
    fl_assign(tcb, a, r);
}

void fl_array_pack_4(FL_TCB *tcb, FL_VAL x, FL_VAL array, FL_VAL index,
    FL_VAL rindex)
{
    CHECK_INT(index);
    CHECK_ARRAY(array);
    if(ARRAY_TYPE(array) != FL_LONG_ARRAY)
        fl_rt_error(tcb, array, FL_NOT_A_LONG_ARRAY);
    long *p = (long *)ARRAY_POINTER(array) + INT(index);
    long *limit = p + ARRAY_LENGTH(array) * sizeof(long);
    int err = 0;
    char *p2 = fl_forward_rec(tcb, x, FL_DETACHED, (char *)p, (char *)limit,
        &err);
    if(p2 == NULL) {
        if(err == 0) fl_assign(tcb, fl_nil, rindex);
        else fl_assign(tcb, fl_error, rindex);
    } else fl_assign(tcb, MKINT((long *)p2 - p), rindex);
}

void fl_array_unpack_4(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL rindex,
    FL_VAL x)
{
    CHECK_INT(index);
    CHECK_ARRAY(array);
    if(ARRAY_TYPE(array) != FL_LONG_ARRAY)
        fl_rt_error(tcb, array, FL_NOT_A_LONG_ARRAY);
    long *p = (long *)ARRAY_POINTER(array) + INT(index);
    FL_VAL r;
    char *p2 = fl_unforward_rec(tcb, (char *)p, &r);
    fl_assign(tcb, MKINT((long *)p2 - p), rindex);
    fl_unify_result(tcb, r, x);
    unref(tcb, r);
}

void fl_array_resize_4(FL_TCB *tcb, FL_VAL array, FL_VAL len, FL_VAL fill, FL_VAL r)
{
    CHECK_INT(len);
    CHECK_ARRAY(array);
    int nlen = INT(len);
    int alen = ARRAY_LENGTH(array);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    int osz = es * alen;
    int sz = es * nlen;
    void *old = ARRAY_POINTER(array);
    if(sz == osz) {
        fl_assign(tcb, array, r);
        return;
    }
    unsigned char *buf = malloc(sz);
    if(buf == NULL) {
        fl_assign(tcb, fl_false, r);
        return;
    }
    FL_VAL na = mkarray(tcb, t, buf, sz);
    if(sz < osz) memcpy(buf, old, sz);
    else {
        memcpy(buf, old, osz);
        buf += osz;
        for(int i = alen; i <= nlen; ++i) {
            put(tcb, buf, t, fill);
            buf += es;
        }
    }
    fl_assign(tcb, na, r);
}

void fl_array_search_4(FL_TCB *tcb, FL_VAL array, FL_VAL seq, FL_VAL index, FL_VAL more)
{
    CHECK_ARRAY(array);
    CHECK_INT(index);
    LIST(more, len, found);
    CHECK_INT(len);
    int alen = ARRAY_LENGTH(array);
    int llen = INT(len);
    int t = ARRAY_TYPE(array);
    int es = array_element_size(t);
    int sz = es * alen;
    int i = INT(index);
    int limit = llen + i > alen ? alen : llen + i;
    if(ISCELL(seq) && TAG(seq) == FL_ARRAY_TAG) {
        int slen = ARRAY_LENGTH(seq);
        int ses = array_element_size(ARRAY_TYPE(seq));
        int ssz = ses * slen;
        limit -= ssz;
        char *sp = ARRAY_POINTER(seq);
        for(char *p = ARRAY_POINTER(array) + INT(index) * es; i < limit; ++i) {
            if(memcmp(sp, p, ssz) == 0) {
                fl_assign(tcb, MKINT(i), found);
                return;
            }
            p += es;
        }
    } else {
        int slen = 0;
        FL_VAL lst;
        for(lst = seq; ISCELL(lst) && TAG(lst) == FL_LIST_TAG; lst = deref(CDR(lst))) {
            if(t == FL_DOUBLE_ARRAY) CHECK_NUMBER(deref(CAR(lst)))
            else CHECK_INT(deref(CAR(lst)))
            ++slen;
        }
        limit -= slen - 1;
        if(slen == 1 && (t == FL_CHAR_ARRAY || t == FL_KL1_STRING)) {
            char *p = ARRAY_POINTER(array);
            char *pf = memchr(p + i, INT(deref(CAR(seq))), limit - i);
            if(pf == NULL) fl_assign(tcb, fl_false, found);
            else fl_assign(tcb, MKINT(pf - p), found);
            return;
        }
        for(char *p = ARRAY_POINTER(array) + i * es; i < limit; ++i) {
            lst = seq;
            int f;
            char *p1 = p;
            for(int j = slen; j > 0; --j) {
                FL_VAL x = deref(CAR(lst));
                switch(t) {
                case FL_INT_ARRAY: f = *((int *)p1) == INT(x); break;
                case FL_LONG_ARRAY: f = *((long *)p1) == INT(x); break;
                case FL_KL1_STRING:
                case FL_CHAR_ARRAY: f = *((unsigned char *)p1) == INT(x); break;
                case FL_SHORT_ARRAY: f = *((short *)p1) == INT(x); break;
                default:
                    f = *((double *)p1) == (ISINT(x) ? INT(x) : FLOATVAL(x));
                    break;
                }
                if(!f) break;
                lst = deref(CDR(lst));
                p1 += es;
            }
            if(f) {
                fl_assign(tcb, MKINT(i), found);
                return;
            }
            p += es;
        }
    }
    fl_assign(tcb, fl_false, found);
}

void fl_array_map_4(FL_TCB *tcb, FL_VAL fd, FL_VAL flags, FL_VAL type, FL_VAL more)
{
    CHECK_INT(fd);
    LIST(more, len, more2);
    LIST(more2, off, done);
    CHECK_INT(len);
    int prot = 0, fl = 0;
    while(ISCELL(flags) && TAG(flags) == FL_LIST_TAG) {
        FL_VAL x = deref(CAR(flags));
        CHECK_STRING(x);
        char *f = STRING(x);
        if(!strcmp(f, "PROT_EXEC")) prot |= PROT_EXEC;
        else if(!strcmp(f, "PROT_READ")) prot |= PROT_READ;
        else if(!strcmp(f, "PROT_WRITE")) prot |= PROT_WRITE;
        else if(!strcmp(f, "MAP_PRIVATE")) fl |= MAP_PRIVATE;
        else if(!strcmp(f, "MAP_SHARED")) fl |= MAP_SHARED;
        else if(!strcmp(f, "MAP_ANON")) fl |= MAP_ANON;
        else {
            fl_assign(tcb, fl_error, done);
            return;
        }
        flags = deref(CDR(flags));
    }
    void *p = mmap(NULL, INT(len), prot, fl, INT(fd), INT(off));
    if(p == MAP_FAILED) fl_io_error(tcb, done);
    else {
        FL_VAL ax = mkarray(tcb, INT(type), p, INT(len));
        CDR(ax) = fl_false;
        fl_assign(tcb, ax, done);
    }
}

void fl_array_unmap_2(FL_TCB *tcb, FL_VAL array, FL_VAL done)
{
    CHECK_ARRAY(array);
    int n = array_element_size(ARRAY_TYPE(array)) * ARRAY_LENGTH(array);
    if(munmap(ARRAY_POINTER(array), n) == 0) fl_assign(tcb, fl_true, done);
    else fl_io_error(tcb, done);
}

void fl_array_sync_4(FL_TCB *tcb, FL_VAL array, FL_VAL index, FL_VAL len, FL_VAL done)
{
    CHECK_ARRAY(array);
    CHECK_INT(index);
    CHECK_INT(len);
    FL_VAL ret = fl_true;
    if(CDR(array) == fl_false) {
        int es = array_element_size(ARRAY_TYPE(array));
        int i = INT(index);
        int alen = ARRAY_LENGTH(array);
        if(i >= 0 && i < alen) {
            int n = INT(len);
            if(n + i > alen) n = alen - i;
            int p = es * INT(index);
            if(msync((char *)ARRAY_POINTER(array) + p, n * es, MS_SYNC) != 0)
                fl_io_error(tcb, done);
        }
    }
    fl_assign(tcb, ret, done);
}

void fl_array_printable_2(FL_TCB *tcb, FL_VAL a, FL_VAL f)
{
    unsigned char *p = ARRAY_POINTER(a);
    int len = ARRAY_LENGTH(a);
    while(len--) {
        if(*p > 128 || (*p < 32 && *p != '\n' && *p != '\t')) {
            fl_assign(tcb, fl_false, f);
            return;
        }
        ++p;
    }
    fl_assign(tcb, fl_true, f);
}

void fl_array_search_utf_4(FL_TCB *tcb, FL_VAL a, FL_VAL w, FL_VAL s, FL_VAL more)
{
	CHECK_ARRAY(a);
	LIST(more, e, more2);
	LIST(more2, o, r);
	CHECK_INT(s);
	CHECK_INT(e);
	CHECK_INT(o);
	int wlen;
	char *what = stringify(tcb, w, &wlen);
	char *str = ARRAY_POINTER(a) + INT(o);
	int i = 0;
	int c;
	while(i < INT(s)) {
		str = utf8_decode(str, &c);
		++i;
	}
	while(i < INT(e)) {
		if(!memcmp(what, str, wlen)) {
			fl_assign(tcb, MKINT(i), r);
			return;
		}
		++i;
		str = utf8_decode(str, &c);
	}
	fl_assign(tcb, fl_false, r);
}
