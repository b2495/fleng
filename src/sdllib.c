/* C library interface for basic SDL usage */

#include "SDL.h"
#include "SDL_image.h"

#include "fleng-config.h"

#ifdef HAVE_SDL2_ttf
#include <SDL_ttf.h>
#endif

#ifdef HAVE_SDL2_mixer
#include <SDL_mixer.h>
#endif

#include "fleng.h"
#include "fleng-util.h"
#include <stdio.h>

static SDL_Window *window = NULL;
static SDL_Renderer *renderer;
static SDL_Color bg_color = { 255, 255, 255, SDL_ALPHA_OPAQUE };
static SDL_Color fg_color = { 0, 0, 0, SDL_ALPHA_OPAQUE };
static int wakeup_event;
static SDL_Texture *unifont, *defimage;
static SDL_mutex *wakeup_mutex;

#define FULLSCREEN_FLAG         1
#define FIXED_FLAG              4

#define PI                      3.1415926535897932

static void error(FL_TCB *tcb, char *msg)
{
    if(msg != NULL) fprintf(stderr, "%s: ", msg);
    const char *err = SDL_GetError();
    fl_abort(tcb, fl_mkstring((char *)err, strlen(err)));
}

static void (*old_hook)(FL_TCB *);

static void wakeup_hook(FL_TCB *tcb)
{
    SDL_Event e;
    SDL_LockMutex(wakeup_mutex);
    e.type = wakeup_event;
    SDL_PushEvent(&e);
    SDL_UnlockMutex(wakeup_mutex);
    if(old_hook != NULL) old_hook(tcb);
}

void sdl_init_4(FL_TCB *tcb, FL_VAL w, FL_VAL h, FL_VAL bits, FL_VAL done)
{
    if(window != NULL) error(tcb, "SDL already initialized");
    if(tcb->ordinal != 1) fl_rt_error(tcb, fl_nil, FL_BAD_THREAD);
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK) < 0)
        error(tcb, "can not initialize SDL");
    int flags = 0;
    if((INT(bits) & FULLSCREEN_FLAG) != 0) flags = SDL_WINDOW_FULLSCREEN_DESKTOP;
    else if((INT(bits) & FIXED_FLAG) == 0) flags = SDL_WINDOW_RESIZABLE;
    window = SDL_CreateWindow("", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        INT(w), INT(h), flags);
    if(!window) error(tcb, "can not create window");
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
    if(!renderer) error(tcb, "can not create renderer");
    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);
#ifdef HAVE_SDL2_ttf
    if(TTF_Init() == -1) error(tcb, "can not initialize TTF font rendering");
#endif
#ifdef HAVE_SDL2_mixer
    if(Mix_Init(MIX_INIT_FLAC | MIX_INIT_MOD | MIX_INIT_MP3 | MIX_INIT_OGG) == 0)
        error(tcb, "can not initialize mixer");
    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
        error(tcb, "can not initialize mixer");
    Mix_AllocateChannels(8);
#endif
    wakeup_mutex = SDL_CreateMutex();
    wakeup_event = SDL_RegisterEvents(1);
    old_hook = tcb->wakeup_hook;
    tcb->wakeup_hook = wakeup_hook;
    SDL_StartTextInput();
    fl_assign(tcb, fl_nil, done);
}

void sdl_init_resources_3(FL_TCB *tcb, FL_VAL ptr, FL_VAL img, FL_VAL done)
{
    SDL_RWops *rw = SDL_RWFromConstMem(ARRAY_POINTER(ptr),
    	array_size_in_bytes(ptr));
    unifont = IMG_LoadTexture_RW(renderer, rw, 1);
    rw = SDL_RWFromConstMem(ARRAY_POINTER(img),
    	array_size_in_bytes(img));
    defimage = IMG_LoadTexture_RW(renderer, rw, 1);
    fl_assign(tcb, fl_nil, done);
}

void sdl_window_title_2(FL_TCB *tcb, FL_VAL str, FL_VAL done)
{
    char *txt = stringify(tcb, str, NULL);
    SDL_SetWindowTitle(window, txt);
    fl_assign(tcb, fl_nil, done);
}

void sdl_resize_window_3(FL_TCB *tcb, FL_VAL w, FL_VAL h, FL_VAL done)
{
    SDL_SetWindowSize(window, INT(w), INT(h));
    fl_assign(tcb, fl_nil, done);
}

void sdl_window_size_2(FL_TCB *tcb, FL_VAL w, FL_VAL h)
{
    int ww, wh;
    SDL_GetWindowSize(window, &ww, &wh);
    fl_assign(tcb, MKINT(ww), w);
    fl_assign(tcb, MKINT(wh), h);
}

void sdl_exit_0(FL_TCB *tcb)
{
    Mix_Quit();
    TTF_Quit();
    SDL_Quit();
}

void sdl_next_event_2(FL_TCB *tcb, FL_VAL wait, FL_VAL event)
{
    SDL_Event e;
    if(wait != fl_false) {
    	SDL_WaitEvent(&e);
    }
    else if(!SDL_PollEvent(&e)) {
        fl_assign(tcb, fl_nil, event);
        return;
    }
    switch(e.type) {
    case SDL_TEXTINPUT: {
        unsigned int c;
        utf8_decode(e.text.text, &c);
        fl_assign(tcb, mklist(tcb, MKINT('T'), mklist(tcb, MKINT(c), fl_nil)), event);
        return; }
    case SDL_KEYDOWN:
    case SDL_KEYUP:
        fl_assign(tcb, mklist(tcb, MKINT('K'), mklist(tcb, MKINT(e.key.keysym.sym),
            mklist(tcb, e.key.state == SDL_PRESSED ? fl_true : fl_false, fl_nil))),
            event);
        return;
    case SDL_MOUSEMOTION:
        fl_assign(tcb, mklist(tcb, MKINT('M'), mklist(tcb, MKINT(e.motion.x),
            mklist(tcb, MKINT(e.motion.y), fl_nil))), event);
        return;
    case SDL_JOYAXISMOTION:
        fl_assign(tcb, mklist(tcb, MKINT('A'), mklist(tcb, MKINT(e.jaxis.axis),
            mklist(tcb, MKINT(e.jaxis.which),
            mklist(tcb, MKINT(e.jaxis.value), fl_nil)))), event);
        return;
    case SDL_CONTROLLERAXISMOTION:
        fl_assign(tcb, mklist(tcb, MKINT('X'), mklist(tcb, MKINT(e.caxis.axis),
            mklist(tcb, MKINT(e.caxis.which),
            mklist(tcb, MKINT(e.caxis.value), fl_nil)))), event);
        return;
    case SDL_JOYBUTTONDOWN:
    case SDL_JOYBUTTONUP:
        fl_assign(tcb, mklist(tcb, MKINT('J'), mklist(tcb, MKINT(e.jbutton.button),
            mklist(tcb, MKINT(e.jbutton.which),
            mklist(tcb, e.button.state == SDL_PRESSED ? fl_true : fl_false,
            fl_nil)))), event);
        return;
    case SDL_CONTROLLERBUTTONDOWN:
    case SDL_CONTROLLERBUTTONUP:
        fl_assign(tcb, mklist(tcb, MKINT('C'), mklist(tcb, MKINT(e.cbutton.button),
            mklist(tcb, MKINT(e.cbutton.which),
            mklist(tcb, e.cbutton.state == SDL_PRESSED ? fl_true : fl_false,
            fl_nil)))), event);
        return;
    case SDL_MOUSEBUTTONDOWN:
    case SDL_MOUSEBUTTONUP:
        fl_assign(tcb, mklist(tcb, MKINT('B'), mklist(tcb, MKINT(e.button.button),
            mklist(tcb, MKINT(e.button.x), mklist(tcb, MKINT(e.button.y),
            mklist(tcb, e.button.state == SDL_PRESSED ? fl_true : fl_false,
            fl_nil))))), event);
        return;
    case SDL_WINDOWEVENT:
        switch(e.window.event) {
        case SDL_WINDOWEVENT_EXPOSED:
            fl_assign(tcb, mklist(tcb, MKINT('X'), fl_nil), event);
            return;
        case SDL_WINDOWEVENT_RESIZED:
            fl_assign(tcb, mklist(tcb, MKINT('R'), mklist(tcb, MKINT(e.window.data1),
                mklist(tcb, MKINT(e.window.data2), fl_nil))),
                event);
            return;
        }
        break;
    case SDL_QUIT:
        fl_assign(tcb, mklist(tcb, MKINT('Q'), fl_nil), event);
        return;
    default:
        fl_force_yield(tcb);
    }
    fl_assign(tcb, fl_nil, event);
}

void sdl_wakeup_1(FL_TCB *tcb, FL_VAL done)
{
    wakeup_hook(tcb);
    fl_assign(tcb, fl_nil, done);
}

void sdl_load_image_2(FL_TCB *tcb, FL_VAL name, FL_VAL img)
{
    char *fn = stringify(tcb, name, NULL);
    SDL_Texture *t = IMG_LoadTexture(renderer, fn);
    if(t == NULL) {
        fprintf(stderr, "Warning: image file %s not found\n", fn);
        fl_assign(tcb, fl_nil, img);
    } else fl_assign(tcb, MARK(t), img);
}

void sdl_load_image_from_bundle_2(FL_TCB *tcb, FL_VAL mem, FL_VAL img)
{
    SDL_RWops *rw = SDL_RWFromConstMem(ARRAY_POINTER(mem),
    	array_size_in_bytes(mem));
    SDL_Texture *t = IMG_LoadTexture_RW(renderer, rw, 1);
    if(t == NULL) fl_assign(tcb, fl_nil, img);
    else fl_assign(tcb, MARK(t), img);
}

void sdl_release_image_2(FL_TCB *tcb, FL_VAL img, FL_VAL done)
{
    SDL_DestroyTexture((SDL_Texture *)UNMARK(img));
    fl_assign(tcb, fl_nil, done);
}

void sdl_load_sample_2(FL_TCB *tcb, FL_VAL name, FL_VAL smp)
{
#ifdef HAVE_SDL2_mixer
    char *fn = stringify(tcb, name, NULL);
    Mix_Chunk *s = Mix_LoadWAV(fn);
    if(s == NULL) {
        fprintf(stderr, "Warning: sample file %s not found\n", fn);
        fl_assign(tcb, fl_nil, smp);
    } else fl_assign(tcb, MARK(s), smp);
#else
    fl_assign(tcb, fl_nil, smp);
#endif
}

void sdl_load_sample_from_bundle_2(FL_TCB *tcb, FL_VAL mem, FL_VAL smp)
{
#ifdef HAVE_SDL2_mixer
    SDL_RWops *rw = SDL_RWFromConstMem(ARRAY_POINTER(mem),
    	array_size_in_bytes(mem));
    Mix_Chunk *s = Mix_LoadWAV_RW(rw, 1);
    if(s == NULL) fl_assign(tcb, fl_nil, smp);
    else fl_assign(tcb, MARK(s), smp);
#else
    fl_assign(tcb, fl_nil, smp);
#endif
}

void sdl_release_sample_2(FL_TCB *tcb, FL_VAL smp, FL_VAL done)
{
#ifdef HAVE_SDL2_mixer
    Mix_FreeChunk((Mix_Chunk *)UNMARK(smp));
#endif
    fl_assign(tcb, fl_nil, done);
}

void sdl_load_font_3(FL_TCB *tcb, FL_VAL name, FL_VAL sz, FL_VAL fnt)
{
    char *fn = stringify(tcb, name, NULL);
#ifdef HAVE_SDL2_ttf
    TTF_Font *f = TTF_OpenFont(fn, INT(sz));
    if(f == NULL) {
        fprintf(stderr, "Warning: font file %s not found\n", fn);
        fl_assign(tcb, fl_nil, fnt);
    } else fl_assign(tcb, MARK(f), fnt);
#else
    fl_assign(tcb, fl_nil, fnt);
#endif
}

void sdl_load_font_from_bundle_3(FL_TCB *tcb, FL_VAL mem,  FL_VAL sz,
    FL_VAL fnt)
{
#ifdef HAVE_SDL2_ttf
    SDL_RWops *rw = SDL_RWFromConstMem(ARRAY_POINTER(mem),
    	array_size_in_bytes(mem));
    TTF_Font *f = TTF_OpenFontRW(rw, 1, INT(sz));
    if(f == NULL) fl_assign(tcb, fl_nil, fnt);
    else fl_assign(tcb, MARK(f), fnt);
#else
    fl_assign(tcb, fl_nil, fnt);
#endif
}

void sdl_release_font_2(FL_TCB *tcb, FL_VAL font, FL_VAL done)
{
#ifdef HAVE_SDL2_ttf
    TTF_CloseFont((TTF_Font *)UNMARK(font));
#endif
    fl_assign(tcb, fl_nil, done);
}

void sdl_color_4(FL_TCB *tcb, FL_VAL r, FL_VAL g, FL_VAL b, FL_VAL done)
{
    fg_color.r = INT(r);
    fg_color.g = INT(g);
    fg_color.b = INT(b);
    fl_assign(tcb, fl_nil, done);
}

void sdl_draw_box_4(FL_TCB *tcb, FL_VAL x1, FL_VAL y1, FL_VAL x2, FL_VAL more)
{
    LIST(more, y2, done);
    SDL_Rect r;
    SDL_SetRenderDrawColor(renderer, fg_color.r, fg_color.g, fg_color.b,
        fg_color.a);
    r.x = INT(x1);
    r.y = INT(y1);
    r.w = INT(x2) - INT(x1);
    r.h = INT(y2) - INT(y1);
    SDL_RenderDrawRect(renderer, &r);
    fl_assign(tcb, fl_nil, done);
}

void sdl_draw_line_4(FL_TCB *tcb, FL_VAL x1, FL_VAL y1, FL_VAL x2, FL_VAL more)
{
    LIST(more, y2, done);
    SDL_SetRenderDrawColor(renderer, fg_color.r, fg_color.g, fg_color.b,
        fg_color.a);
    SDL_RenderDrawLine(renderer, INT(x1), INT(y1), INT(x2), INT(y2));
    fl_assign(tcb, fl_nil, done);
}

void sdl_draw_polygon_4(FL_TCB *tcb, FL_VAL x, FL_VAL y, FL_VAL sides, FL_VAL more)
{
    LIST(more, radius, m2);
    LIST(m2, angle, done);
    int n = INT(sides);
    SDL_Point *ps = malloc(sizeof(SDL_Point) * (n + 1));
    int r = INT(radius);
    CHECK_NUMBER(angle);
    FLOAT(angle, a);
    a = a * PI / 180;
    SDL_Point *pp = ps;
    for(int i = 0; i < n; ++i) {
        pp->x = round(sin(a) * r) + INT(x);
        pp->y = round(cos(a) * r) + INT(y);
        a += 360 / n * PI / 180;
        ++pp;
    }
    pp->x = ps->x;
    pp->y = ps->y;
    SDL_SetRenderDrawColor(renderer, fg_color.r, fg_color.g, fg_color.b,
        fg_color.a);
    SDL_RenderDrawLines(renderer, ps, n + 1);
    free(ps);
    fl_assign(tcb, fl_nil, done);
}

/* Polygon fill routines from SDL_gfx
*/

static int hlineColor(SDL_Surface * dst, int x1, int x2, int y, Uint32 color)
{
	int left, right, top, bottom;
	unsigned char *pixel, *pixellast;
	int dx;
	int pixx, pixy;
	int w;
	int xtmp;
	int result = -1;

	/*
	* Check visibility of clipping rectangle
	*/
	if ((dst->clip_rect.w==0) || (dst->clip_rect.h==0)) {
		return(0);
	}

	/*
	* Swap x1, x2 if required to ensure x1<=x2
	*/
	if (x1 > x2) {
		xtmp = x1;
		x1 = x2;
		x2 = xtmp;
	}

	/*
	* Get clipping boundary and
	* check visibility of hline
	*/
	left = dst->clip_rect.x;
	if (x2<left) {
		return(0);
	}
	right = dst->clip_rect.x + dst->clip_rect.w - 1;
	if (x1>right) {
		return(0);
	}
	top = dst->clip_rect.y;
	bottom = dst->clip_rect.y + dst->clip_rect.h - 1;
	if ((y<top) || (y>bottom)) {
		return (0);
	}

	/*
	* Clip x
	*/
	if (x1 < left) {
		x1 = left;
	}
	if (x2 > right) {
		x2 = right;
	}

	/*
	* Calculate width
	*/
	w = x2 - x1;

	/*
	* Lock the surface
	*/
	if (SDL_MUSTLOCK(dst)) {
		if (SDL_LockSurface(dst) < 0) {
			return (-1);
		}
	}

	/*
	* More variable setup
	*/
	dx = w;
	pixx = dst->format->BytesPerPixel;
	pixy = dst->pitch;
	pixel = ((Uint8 *) dst->pixels) + pixx * (int) x1 + pixy * (int) y;

	/*
	* Draw
	*/
	// assumes BitsPerPixel is 4:

		dx = dx + dx;
		pixellast = pixel + dx + dx;
		for (; pixel <= pixellast; pixel += pixx) {
			*(Uint32 *) pixel = color;
		}

	/*
	* Unlock surface
	*/
	if (SDL_MUSTLOCK(dst)) {
		SDL_UnlockSurface(dst);
	}

	/*
	* Set result code
	*/
	result = 0;

	return (result);
}

static int _gfxPrimitivesCompareInt(const void *a, const void *b)
{
	return (*(const int *) a) - (*(const int *) b);
}

static int filledPolygonColorMT(const int * vx, const int * vy, int n)
{
	int result;
	int i;
	int y, xa, xb;
	int minx, maxx, miny, maxy;
	int x1, y1;
	int x2, y2;
	int ind1, ind2;
	int ints;
	int *gfxPrimitivesPolyInts = NULL;
	char* tmp;
	SDL_Surface * dst;

	/*
	* Sanity check number of edges
	*/
	if (n < 3) {
		return -1;
	}

	/*
	* Allocate temp array, only grow array
	*/
	gfxPrimitivesPolyInts = (int *) malloc(sizeof(int) * n);

	/*
	* Check temp array again
	*/
	if (gfxPrimitivesPolyInts==NULL) {
		return(-1);
	}

	/*
	* Determine X/Y maxima
	*/
	minx = vx[0];
	maxx = vx[0];
	miny = vy[0];
	maxy = vy[0];
	for (i = 1; (i < n); i++) {
		if (vx[i] < minx) {
			minx = vx[i];
		} else if (vx[i] > maxx) {
			maxx = vx[i];
		}
		if (vy[i] < miny) {
			miny = vy[i];
		} else if (vy[i] > maxy) {
			maxy = vy[i];
		}
	}

	// create surface
	int h = maxy - miny;
	// Little-endian-specific:
	dst = SDL_CreateRGBSurface(SDL_SWSURFACE, maxx - minx, h, 32,
		0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
	if(dst == NULL) {
		free(gfxPrimitivesPolyInts);
		return -1;
	}
	Uint32 color = SDL_MapRGB(dst->format, fg_color.r, fg_color.g, fg_color.b);
	SDL_FillRect(dst, NULL, SDL_MapRGBA(dst->format, bg_color.r, bg_color.g, bg_color.b,
		SDL_ALPHA_TRANSPARENT));
	SDL_SetSurfaceAlphaMod(dst, SDL_ALPHA_OPAQUE);

	/*
	* Draw, scanning y
	*/
	result = 0;
	for (y = 0; (y <= h); y++) {
		ints = 0;
		for (i = 0; (i < n); i++) {
			if (!i) {
				ind1 = n - 1;
				ind2 = 0;
			} else {
				ind1 = i - 1;
				ind2 = i;
			}
			y1 = vy[ind1] - miny;
			y2 = vy[ind2] - miny;
			if (y1 < y2) {
				x1 = vx[ind1] - minx;
				x2 = vx[ind2] - minx;
			} else if (y1 > y2) {
				y2 = vy[ind1] - miny;
				y1 = vy[ind2] - miny;
				x2 = vx[ind1] - minx;
				x1 = vx[ind2] - minx;
			} else {
				continue;
			}
			if ( ((y >= y1) && (y < y2)) || ((y == maxy) && (y > y1) && (y <= y2)) ) {
				gfxPrimitivesPolyInts[ints++] = ((65536 * (y - y1)) / (y2 - y1)) * (x2 - x1) + (65536 * x1);
			}
		}

		qsort(gfxPrimitivesPolyInts, ints, sizeof(int), _gfxPrimitivesCompareInt);

		for (i = 0; (i < ints); i += 2) {
			xa = gfxPrimitivesPolyInts[i] + 1;
			xa = (xa >> 16) + ((xa & 32768) >> 15);
			xb = gfxPrimitivesPolyInts[i+1] - 1;
			xb = (xb >> 16) + ((xb & 32768) >> 15);
			result |= hlineColor(dst, xa, xb, y, color);
		}
	}

	free(gfxPrimitivesPolyInts);

	if(result != 0) {
		SDL_FreeSurface(dst);
		return result;
	}

       SDL_Texture *tx = SDL_CreateTextureFromSurface(renderer, dst);
       SDL_Rect dest;
       dest.x = minx;
       dest.y = miny;
       SDL_QueryTexture(tx, NULL, NULL, &dest.w, &dest.h);
       SDL_SetTextureBlendMode(tx, SDL_BLENDMODE_BLEND);
	SDL_RenderCopy(renderer, tx, NULL, &dest);
	SDL_DestroyTexture(tx);
	SDL_FreeSurface(dst);
	return (result);
}

void sdl_fill_polygon_4(FL_TCB *tcb, FL_VAL x, FL_VAL y, FL_VAL sides, FL_VAL more)
{
    LIST(more, radius, m2);
    LIST(m2, angle, done);
    int n = INT(sides);
    int *px = malloc(sizeof(int) * n);
    int *py = malloc(sizeof(int) * n);
    int r = INT(radius);
    int *px0 = px, *py0 = py;
    CHECK_NUMBER(angle);
    FLOAT(angle, a);
    a = a * PI / 180;
    for(int i = 0; i < n; ++i) {
        *(px++) = round(sin(a) * r) + INT(x);
        *(py++) = round(cos(a) * r) + INT(y);
        a += 360 / n * PI / 180;
    }
    filledPolygonColorMT(px0, py0, n);
    free(px0);
    free(py0);
    fl_assign(tcb, fl_nil, done);
}

void sdl_fill_box_4(FL_TCB *tcb, FL_VAL x1, FL_VAL y1, FL_VAL x2, FL_VAL more)
{
    LIST(more, y2, done);
    SDL_Rect r;
    SDL_SetRenderDrawColor(renderer, fg_color.r, fg_color.g, fg_color.b,
        fg_color.a);
    r.x = INT(x1);
    r.y = INT(y1);
    r.w = INT(x2) - INT(x1);
    r.h = INT(y2) - INT(y1);
    SDL_RenderFillRect(renderer, &r);
    fl_assign(tcb, fl_nil, done);
}

void sdl_draw_image_4(FL_TCB *tcb, FL_VAL x, FL_VAL y, FL_VAL img, FL_VAL more)
{
    LIST(more, angle, l1);
    LIST(l1, cx, l2);
    LIST(l2, cy, l3);
    LIST(l3, axis, done);
    SDL_Texture *t;
    if(img == fl_nil) t = defimage;
    else t = (SDL_Texture *)UNMARK(img);
    SDL_Rect dest;
    dest.x = INT(x);
    dest.y = INT(y);
    char *a = stringify(tcb, axis, NULL);
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    while(*a) {
        switch(*(a++)) {
        case 'h': flip |= SDL_FLIP_HORIZONTAL; break;
        case 'v': flip |= SDL_FLIP_VERTICAL; break;
        }
    }
    SDL_Point c;
    c.x = -INT(cx);
    c.y = -INT(cy);
    double an = 0;
    if(ISINT(angle)) an = INT(angle);
    else an = FLOATVAL(angle);
    SDL_QueryTexture(t, NULL, NULL, &dest.w, &dest.h);
    SDL_RenderCopyEx(renderer, t, NULL, &dest, an, &c, flip);
    fl_assign(tcb, fl_nil, done);
}

void sdl_draw_text_4(FL_TCB *tcb, FL_VAL x, FL_VAL y, FL_VAL text, FL_VAL more)
{
    LIST(more, font, more2);
    LIST(more2, w, done);
    char *t = stringify(tcb, text, NULL);
    if(*t != '\0') {
        SDL_Rect dest, sr;
        dest.x = INT(x);
        dest.y = INT(y);
        dest.h = 16;
        if(font == fl_nil) {
            int wp = 0xffff;
            SDL_SetTextureColorMod(unifont, fg_color.r, fg_color.g, fg_color.b);
            if(w != fl_nil) wp = INT(w);
            while(*t != '\0' && wp > 0) {
                unsigned int c;
                t = utf8_decode(t, &c);
                if(c >= 0x1f00) c = 0;
                sr.x = c * 8;
                sr.y = 0;
                dest.w = sr.w = wp >= 8 ? 8 : wp;
                sr.h = 16;
                SDL_RenderCopy(renderer, unifont, &sr, &dest);
                wp -= 8;
                dest.x += 8;
            }
        }
#ifdef HAVE_SDL2_ttf
        else {
            SDL_Surface *s = TTF_RenderUTF8_Blended((TTF_Font *)UNMARK(font), t, fg_color);
            if(s == NULL) return;
            SDL_Texture *tx = SDL_CreateTextureFromSurface(renderer, s);
            SDL_QueryTexture(tx, NULL, NULL, &dest.w, &dest.h);
            if(w != fl_nil) {
                sr.x = sr.y = 0;
                sr.w = INT(w);
                sr.h = dest.h;
                dest.w = sr.w;
                SDL_RenderCopy(renderer, tx, &sr, &dest);
            } else SDL_RenderCopy(renderer, tx, NULL, &dest);
            SDL_DestroyTexture(tx);
            SDL_FreeSurface(s);
        }
#endif
    }
    fl_assign(tcb, fl_nil, done);
}

void sdl_redraw_1(FL_TCB *tcb, FL_VAL done)
{
    SDL_RenderPresent(renderer);
    fl_assign(tcb, fl_nil, done);
}

void sdl_clear_1(FL_TCB *tcb, FL_VAL done)
{
    SDL_SetRenderDrawColor(renderer, bg_color.r, bg_color.g, bg_color.b,
        bg_color.a);
    SDL_RenderClear(renderer);
    fl_assign(tcb, fl_nil, done);
}

void sdl_clear_4(FL_TCB *tcb, FL_VAL r, FL_VAL g, FL_VAL b, FL_VAL done)
{
    bg_color.r = INT(r);
    bg_color.g = INT(g);
    bg_color.b = INT(b);
    sdl_clear_1(tcb, done);
}

void sdl_volume_3(FL_TCB *tcb, FL_VAL channel, FL_VAL vol, FL_VAL done)
{
#ifdef HAVE_SDL2_mixer
    Mix_Volume(INT(channel), INT(vol));
#endif
    fl_assign(tcb, fl_nil, done);
}

void sdl_play_4(FL_TCB *tcb, FL_VAL chan, FL_VAL sample, FL_VAL loops, FL_VAL more)
{
    LIST(more, ticks, done);
#ifdef HAVE_SDL2_mixer
    if(sample != fl_nil) {
        if(ticks == fl_nil)
            Mix_PlayChannel(INT(chan), (Mix_Chunk *)UNMARK(sample), INT(loops));
        else Mix_PlayChannelTimed(INT(chan), (Mix_Chunk *)UNMARK(sample), INT(loops),
            INT(ticks));
    }
#endif
    fl_assign(tcb, fl_nil, done);
}

void sdl_fade_in_4(FL_TCB *tcb, FL_VAL chan, FL_VAL sample, FL_VAL loops, FL_VAL more)
{
    LIST(more, ms, more2);
    LIST(more2, ticks, done);
#ifdef HAVE_SDL2_mixer
    if(sample != fl_nil) {
        if(ticks == fl_nil)
            Mix_FadeInChannel(INT(chan), (Mix_Chunk *)UNMARK(sample), INT(loops),
                INT(ms));
        else Mix_FadeInChannelTimed(INT(chan), (Mix_Chunk *)UNMARK(sample), INT(loops),
            INT(ms), INT(ticks));
    }
#endif
    fl_assign(tcb, fl_nil, done);
}

void sdl_fade_out_3(FL_TCB *tcb, FL_VAL chan, FL_VAL ms, FL_VAL done)
{
#ifdef HAVE_SDL2_mixer
    Mix_FadeOutChannel(INT(chan), INT(ms));
#endif
    fl_assign(tcb, fl_nil, done);
}

void sdl_pause_2(FL_TCB *tcb, FL_VAL chan, FL_VAL done)
{
#ifdef HAVE_SDL2_mixer
    Mix_Pause(INT(chan));
#endif
    fl_assign(tcb, fl_nil, done);
}

void sdl_resume_2(FL_TCB *tcb, FL_VAL chan, FL_VAL done)
{
#ifdef HAVE_SDL2_mixer
    Mix_Resume(INT(chan));
#endif
    fl_assign(tcb, fl_nil, done);
}

void sdl_no_clip_1(FL_TCB *tcb, FL_VAL done)
{
    SDL_RenderSetClipRect(renderer, NULL);
    fl_assign(tcb, fl_nil, done);
}

void sdl_clip_4(FL_TCB *tcb, FL_VAL x1, FL_VAL y1, FL_VAL x2, FL_VAL more)
{
    LIST(more, y2, done);
    SDL_Rect r;
    r.x = INT(x1);
    r.y = INT(y1);
    r.w = INT(x2) - r.x;
    r.h = INT(y2) - r.y;
    SDL_RenderSetClipRect(renderer, &r);
    fl_assign(tcb, fl_nil, done);
}

void sdl_image_size_3(FL_TCB *tcb, FL_VAL img, FL_VAL w, FL_VAL h)
{
    if(img == fl_nil) {
        fl_assign(tcb, MKINT(0), w);
        fl_assign(tcb, MKINT(0), h);
        return;
    }
    SDL_Texture *t = (SDL_Texture *)UNMARK(img);
    SDL_Rect dest;
    SDL_QueryTexture(t, NULL, NULL, &dest.w, &dest.h);
    fl_assign(tcb, MKINT(dest.w), w);
    fl_assign(tcb, MKINT(dest.h), h);
}

void sdl_text_size_4(FL_TCB *tcb, FL_VAL txt, FL_VAL fnt, FL_VAL wd, FL_VAL hd)
{
    char *t = stringify(tcb, txt, NULL);
    int w, h;
    SDL_Color c;
    if(*t == '\0') {
        w = 0; h = 0;
    } else if(fnt == fl_nil) {
        unsigned int c, len = 0;
        while(*t != '\0') {
            t = utf8_decode(t, &c);
            ++len;
        }
        w = len * 8;
        h = 16;
    }
#ifdef HAVE_SDL2_ttf
    else {
        CHECK_INT(fnt);
        SDL_Surface *s = TTF_RenderUTF8_Blended((TTF_Font *)UNMARK(fnt), t, c);
        w = s->w;
        h = s->h;
        SDL_FreeSurface(s);
    }
#endif
    fl_assign(tcb, MKINT(w), wd);
    fl_assign(tcb, MKINT(h), hd);
}

void sdl_font_metrics_2(FL_TCB *tcb, FL_VAL fnt, FL_VAL r)
{
    if(fnt == fl_nil) {
        //XXX fix these
        FL_VAL m = mklist(tcb, MKINT(0), fl_nil);
        m = mklist(tcb, MKINT(1), m);
        m = mklist(tcb, MKINT(1), m);
        fl_unify_result(tcb,  mktuple(tcb, MKINT(16), 3, m), r);
        return;
    }
    TTF_Font *f = (TTF_Font *)UNMARK(fnt);
    FL_VAL m = mklist(tcb, MKINT(TTF_FontLineSkip(f)), fl_nil);
    m = mklist(tcb, MKINT(TTF_FontDescent(f)), m);
    m = mklist(tcb, MKINT(TTF_FontAscent(f)), m);
    fl_unify_result(tcb,  mktuple(tcb, MKINT(TTF_FontHeight(f)), 3, m), r);
}

void sdl_mouse_state_3(FL_TCB *tcb, FL_VAL x, FL_VAL y, FL_VAL b)
{
    int x1, y1;
    int bs = SDL_GetMouseState(&x1, &y1);
    fl_unify_result(tcb, MKINT(x1), x);
    fl_unify_result(tcb, MKINT(y1), y);
    fl_unify_result(tcb, MKINT(bs), b);
}

void sdl_set_mouse_2(FL_TCB *tcb, FL_VAL flag, FL_VAL done)
{
    SDL_ShowCursor(flag == fl_false ? SDL_DISABLE : SDL_ENABLE);
    fl_assign(tcb, fl_nil, done);
}

void sdl_mouse_cursor_2(FL_TCB *tcb, FL_VAL c, FL_VAL done)
{
    static SDL_Cursor *prev = NULL;
    SDL_Cursor *cursor;
    CHECK_STRING(c);
    char *name = (char *)c + 1;
    if(!strcmp(name, "default")) cursor = SDL_GetDefaultCursor();
    else {
        int cu;
        if(!strcmp(name, "arrow")) cu = SDL_SYSTEM_CURSOR_ARROW;
        else if(!strcmp(name, "ibeam")) cu = SDL_SYSTEM_CURSOR_IBEAM;
        else if(!strcmp(name, "wait")) cu = SDL_SYSTEM_CURSOR_WAIT;
        else if(!strcmp(name, "crosshair")) cu = SDL_SYSTEM_CURSOR_CROSSHAIR;
        else if(!strcmp(name, "waitarrow")) cu = SDL_SYSTEM_CURSOR_WAITARROW;
        else if(!strcmp(name, "size_nwse")) cu = SDL_SYSTEM_CURSOR_SIZENWSE;
        else if(!strcmp(name, "size_nesw")) cu = SDL_SYSTEM_CURSOR_SIZENESW;
        else if(!strcmp(name, "size_we")) cu = SDL_SYSTEM_CURSOR_SIZEWE;
        else if(!strcmp(name, "size_ns")) cu = SDL_SYSTEM_CURSOR_SIZENS;
        else if(!strcmp(name, "size_all")) cu = SDL_SYSTEM_CURSOR_SIZEALL;
        else if(!strcmp(name, "no")) cu = SDL_SYSTEM_CURSOR_NO;
        else if(!strcmp(name, "hand")) cu = SDL_SYSTEM_CURSOR_HAND;
        else {
            fl_assign(tcb, fl_nil, done);
            return;
        }
        cursor = SDL_CreateSystemCursor(cu);
        if(cursor != NULL) {
            if(prev != NULL) SDL_FreeCursor(prev);
            prev = cursor;
        }
    }
    SDL_SetCursor(cursor);
    fl_assign(tcb, fl_nil, done);
}

void sdl_message_box_4(FL_TCB *tcb, FL_VAL title, FL_VAL msg, FL_VAL lst, FL_VAL response)
{
    if(tcb->ordinal != 1) fl_rt_error(tcb, fl_nil, FL_BAD_THREAD);
    int n = 0;
    FL_VAL l = lst;
    while(ISCELL(l)) {
        l = deref(CDR(l));
        ++n;
    }
    SDL_MessageBoxData data;
    char *p = init_cbuf(tcb);
    SDL_MessageBoxButtonData *buttons = malloc(sizeof(SDL_MessageBoxButtonData) * n);
    data.flags = 0;
    data.title = fl_stringify_next(tcb, &p, title, NULL);
    data.message = fl_stringify_next(tcb, &p, msg, NULL);
    data.numbuttons = n;
    data.window = window;
    data.colorScheme = NULL;
    data.buttons = buttons;
    switch(*data.title) {
    case '?':
        ++data.title;
        data.flags = SDL_MESSAGEBOX_ERROR;
    case '!':
        ++data.title;
        data.flags = SDL_MESSAGEBOX_WARNING;
    default:
        data.flags = SDL_MESSAGEBOX_INFORMATION;
    }
    int i = 0;
    while(ISCELL(lst)) {
        FL_VAL x = deref(CAR(lst));
        char *t = fl_stringify_next(tcb, &p, x, NULL);
        buttons[ i ].flags = 0;
        if(*t == '*') {
            ++t;
            buttons[ i ].flags = SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT;
        } else if(*t == '^') {
            ++t;
            buttons[ i ].flags = SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT;
        }
        buttons[ i ].buttonid = i + 1;
        buttons[ i++ ].text = t;
        lst = deref(CDR(lst));
    }
    int b;
    if(SDL_ShowMessageBox(&data, &b) != 0) fl_assign(tcb, fl_false, response);
    else fl_assign(tcb, MKINT(b), response);
    free((void *)data.buttons);
}

void sdl_warp_mouse_3(FL_TCB *tcb, FL_VAL x, FL_VAL y, FL_VAL done) {
	CHECK_INT(x);
	CHECK_INT(y);
	SDL_WarpMouseInWindow(window, INT(x), INT(y));
	fl_assign(tcb, fl_nil, done);
}

