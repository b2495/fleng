/* FLENG - common header file for "bb" + "bbedit"
*/

#define TABWIDTH	8

#define TAB			9
#define NL			10
#define CR			13
#define SPACE		32
#define BACKSPACE   	8
#define ESCAPE      	27
#define DEL			127

-struct(edit_state(dot, top, lines, rows, columns, modkeys, id, cut))
