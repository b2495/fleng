% FLENG - build tool

-initialization(main).

main :-
    	command_line(Args),
	getenv('FLENGMAKE_FLAGS', FF),
	list:split(FF, 32, FF2),
	app:maplist(list_to_string, FF2, FF3),
	list:append(Args, FF3, Args2),
    	init_globals &
	parse_args(Args2, ".", Root, Options),
	put_global(root, Root) &
    	setenv('FLENGMAKE_ROOT', Root) &
	ensure_infodir(Root) &
	load_state(State),
	update_state([ignore([".flengmake", "options"])|Options], State, State2) &
	global(operation, Op),
	operate(Op, Root, State2).

init_globals :-
	getenv('FLENG_PREFIX', P1),
       config:fleng_prefix(P2),
    	init_prefix(P1, P2, P),
    	fmt:format_chars('~s/fleng', [P], Fleng),
    	put_global(prefix, P),
    	put_global(fleng, Fleng),
	put_global(operation, build).

operate(clean, Root, {TM, _, _}) :-
	clean(TM, Root).
operate(show, _, {TM, _, TS}) :-
	show(TM, TS).
operate(build, Root, State) :-
	build(State, State3) &
	save_state(Root, State3).

init_prefix([], P2, P) :- fmt:format_chars('~s/bin', [P2], P).
init_prefix(P1, _, P) :- otherwise | P = P1.

parse_args([], R1, R, Opts) :-
	Opts = [], R = R1.
parse_args(['--version'|_], _, _, _) :-
    	config:fleng_version(V),
    	writeln(V) &
    	halt.
parse_args(['-v'|Args], R1, R, Opts) :-
	put_global(verbose, true) &
	parse_args(Args, R1, R, Opts).
parse_args(['-n'|Args], R1, R, Opts) :-
	put_global(noaction, true) &
	parse_args(Args, R1, R, Opts).
parse_args(['-h'|_], _, _, _) :-
	usage(0).
parse_args([clean|Args], R1, R, Opts) :-
	put_global(operation, clean) &
	parse_args(Args, R1, R, Opts).
parse_args([show|Args], R1, R, Opts) :-
	put_global(operation, show) &
	parse_args(Args, R1, R, Opts).
parse_args([build|Args], R1, R, Opts) :-
	parse_args(Args, R1, R, Opts).
parse_args(['-r', Root|Args], _, R, Opts) :-
	string_to_list(Root, RS, []),
	path:normalize(RS, RSN),
	parse_args(Args, RSN, R, Opts).
parse_args([O], _, _, _) :-
	O =\= '-r', O =\= '-h', O =\= '-v', O =\= '-n', O =\= clean,
		O =\= show, O =\= build |
	usage(1).
parse_args([Opt, Val|Args], R1, R, Opts) :-
	otherwise |
	string_to_list(Opt, L, [32|V]),
	string_to_list(Val, V, []),
	parse_args2(L, Args, R1, R, Opts).

parse_args2([0'-|Opt], Args, R1, R, Opts) :-
	list(Opt) |
	parse_option(Opt, ".", 'command line', Spec),
	Opts = [Spec|Opts2],
	parse_args(Args, R1, R, Opts2).
parse_args2(_, _, _, _, _) :-
	otherwise | usage(1).

usage(Code) :-
	writeln('usage: flengmake [--version] [-v] [-h] [-n] [-r ROOT] [clean|show] -OPTION VAL ...') &
	halt(Code).

% Option parsing

parse_option(Line, Dir, Src, Opt) :-
	scan:whitespace(Line, L2),
	parse_option1(L2, Dir, Src, Opt).

parse_option1([0'#|_], _, _, Opt) :- Opt = [].
parse_option1(L2, Dir, Src, Opt) :-
	otherwise |
	scan:identifier(Setting, L2, Rest),
	list_to_string(Setting, O),
	parse_option2(O, Rest, Dir, Src, Opt).

parse_option2(name, Line, Dir, _, Opt) :-
	parse_rename_spec(Line, Dir, ROpt),
	Opt = ROpt.
parse_option2(target, Line, Dir, _, Opt) :-
	parse_file_list(Line, Dir, TL),
	Opt = target(TL).
parse_option2(depends, Line, Dir, _, Opt) :-
	parse_file_list(Line, Dir, DL),
	Opt = depends(DL).
parse_option2(options, Line, _, _, Opt) :-
	parse_compiler_options(Line, Cs),
	Opt = options(Cs).
parse_option2(ignore, Line, _, _, Opt) :-
	parse_compiler_options(Line, L),
	Opt = ignore(L).
parse_option2(fleng, Line, _, _, Opt) :-
	parse_single_argument(Line, Arg),
	Opt = fleng(Arg).
parse_option2(build, Line, Dir, _, Opt) :-
	parse_file_list(Line, Dir, Arg),
	Opt = build(Arg).
parse_option2(O, _, _, Src, _) :-
	otherwise |
	abort('invalid option: ~a (~s)', [O, Src]).

parse_single_argument(Data, Arg) :-
	scan:whitespace(Data, D2),
	Arg = D2.

parse_element(Arg, Data1, Data) :-
	scan:whitespace(Data1, D2),
	parse_element2(Arg, D2, Data).

parse_element2(Arg, [0''|Data1], Data) :-
	scan:delimited(0'', Arg, Data1, Data).
parse_element2(Arg, [0'"|Data1], Data) :-
	scan:delimited(0'", Arg, Data1, Data).
parse_element2(Arg, Data1, Data) :-
	otherwise |
	scan:not_from_set(" \t#", Arg, Data1, Data).

parse_compiler_options([], Cs) :- Cs = [].
parse_compiler_options(Data, Cs) :-
	scan:whitespace(Data, D2),
	(Data == [] ->
		Cs = []
	;
		parse_element(Opt, D2, D3),
		Cs = [Opt|Cs2],
		parse_compiler_options(D3, Cs2)
	).

check_empty_line(Data, Src) :-
	scan:whitespace(Data, D2),
	(list(D2) -> abort('unexpected arguments to option (~s)', [Src])).

parse_file_list([], _, L) :- L = [].
parse_file_list(Line, Dir, L) :-
	otherwise |
	scan:whitespace(Line, D2),
	parse_file_list2(D2, Dir, L).

parse_file_list2([0'#|_], _, L) :- L = [].
parse_file_list2(D2, Dir, L) :-
	otherwise |
	parse_element(FN, D2, D3),
	(list(FN) ->
		path:join([Dir, FN], FN2),
		L = [FN2|L2],
		parse_file_list(D3, Dir, L2)
	;
		L = []
	).

parse_rename_spec(Line, Dir, ROpt) :-
	parse_file_list(Line, Dir, Fs),
	parse_rename_spec2(Fs, Line, ROpt).

parse_rename_spec2([T, Src], _, ROpt) :- ROpt = rename(T, Src).
parse_rename_spec2(_, Line, _) :-
	otherwise |
	abort('invalid target specification: "~s"', [Line]).

load_options(Fname, Opts) :-
	dribble('loading options "~s"\n', [Fname]),
	open_file(Fname, r, File),
	io:read_lines(File, Lines),
	load_options2(Lines, Fname, 1, Opts) &
	close_file(File).

load_options2([], _, _, Opts) :- Opts = [].
load_options2([L|Lines], Fname, LN, Opts) :-
	fmt:format_chars('~s:~d', [Fname, LN], Src),
	path:dirname(Fname, Dir),
	substitute_vars(L, L2),
	parse_option(L2, Dir, Src, Opt),
	(Opt == [] ->
		Opts2 = Opts
	;
		Opts = [Opt|Opts2]
	),
	LN2 is LN + 1,
	load_options2(Lines, Fname, LN2, Opts2).

filter_options(_, [], R) :- R = [].
filter_options(Opt, [{Opt, X}|L], R) :-
	R = [X|R2],
	filter_options(Opt, L, R2).
filter_options(Opt, [_|L], R) :-
	otherwise |
	filter_options(Opt, L, R).

% misc

abort(Fmt, Args) :-
	fmt:format(2, 'Error: ~?\n', [Fmt, Args]) &
	halt(1).

dribble(Fmt, Args) :-
	get_global(verbose, F, false),
	(F == true -> fmt:format(2, Fmt, Args)).

state_filename(Root, FN) :-
	fmt:format_chars('~s/.flengmake/state', [Root], FN).

ensure_infodir(Root) :-
	fmt:format_chars('~s/.flengmake', [Root], ID),
	mkdir(ID).

dump_state(TM) :-
	map:map_to_list(TM, L),
	dump_state2(L).

dump_state2([]).
dump_state2([T-Spec|L]) :-
	get(type, Spec, Type),
	dribble('target "~s":\t~a\n', [T, Type]) &
	dump_state2(L).

cache_file(FN, Cache) :-
	counter(N),
	current_seconds(Secs),
	get_global(root, Root),
	path:tail(FN, BN),
	fmt:format_chars('~s/.flengmake/~d~~~d~~~s.fl', [Root, Secs, N, BN], Cache).

% reading/writing project state
%
% State = {TARGETMAP, FILEMAP, TIMESTAMP}
% TARGETMAP = TARGETNAME -> TARGET
% TARGET = target(map:ATTR -> VALUE)
% XXX explain these:
% ATTR = type, module, options,
%	uses, mtime, srcfile, deps, language, includes, level,
%	name, fobjects, cached
% FILEMAP = FILENAME -> TARGETNAME
% TYPE = program | library | module | include

get(Attr, target(T), X) :- map:lookup(Attr, T, X).
put(Attr, X, target(T1), T) :-
	map:insert(Attr, X, T1, T2),
	T = target(T2).

add_dependencies(Deps, T, T2) :-
	get(deps, T, Deps1),
	set:union(Deps1, Deps, Deps2),
	put(deps, Deps2, T, T2).

add_foreign_dependencies(FO, T, T2) :-
	get(fobjects, T, FO1),
	set:union(FO1, FO, FO2),
	put(fobjects, FO2, T, T2).

update_mtime(MT, T, T2) :- put(mtime, MT, T, T2).

load_state(State) :-
	get_global(root, Root),
	state_filename(Root, FN),
	file_exists(FN, F),
	(F == true ->
		read_state(FN, Root, State)
	;
		State = {[], [], 0}
	).

read_state(FN, Root, State) :-
	dribble('reading state from "~s"\n', [FN]),
	open_file(FN, r, File),
	open_port(P, S),
	parse:parse_terms(File, Forms, P) &
	close_file(File),
	handle_state_errors(S, FN),
	build_state(Forms, TS, Root, [], TM, [], FM),
	(TM == [] ->
		State = {[], [], 0}
	;
		State = {TM, FM, TS}
	).

handle_state_errors([], _).
handle_state_errors([error(LN, Fmt, Args, _)|_], FN) :-
	abort('error while loading state (~s:~d): ~?', [FN, LN, Fmt, Args]).

build_state([], _, _)-_-_.
build_state([root(R)|Forms], TS, R)-TM-FM :-
	build_state(Forms, TS, R)-TM-FM.
build_state([version(V)|Forms], TS, R)-TM-FM :-
	config:fleng_version(FV),
	(FV == V ->
	    build_state(Forms, TS, R)-TM-FM
	;
	    dribble('state file version mismatch (~a) - ignoring saved state\n', [V]),
	    TM <== []
	).
build_state([root(R)|_], _, R2)-TM-_ :-
	R =\= R2 |
	dribble('root directory changed - ignoring saved state\n', []),
	TM <== [].
build_state([status(TS1)|Forms], TS, Root)-TM-FM :-
	TS = TS1,
	build_state(Forms, _, Root)-TM-FM.
build_state([target(Map)|Forms], TS, Root)-TM-FM :-
	map:list_to_map(Map, T),
	map:lookup(srcfile, T, Src),
	build_state2(Src, T, Forms, TS, Root)-TM-FM.

build_state2({none}, _, Forms, TS, Root)-TM-FM :-
    % custom target
    build_state(Forms, TS, Root)-TM-FM.
build_state2(Src, T, Forms, TS, Root)-TM-FM :-
    otherwise |
    map:lookup(type, T, Type),
    map:lookup(name, T, Target),
    file_exists(Src, EF),
    check_source_exists(Type, EF, F),
    (F == false ->
	dribble('source file for stored target "~s" does not exist (dropped)\n', [Target])
    ;
	map:insert(Target, target(T))-TM,
	map:insert(Src, Target)-FM
    ),
    build_state(Forms, TS, Root)-TM-FM.

check_source_exists(program, true, F) :- F = true.
check_source_exists(module, true, F) :- F = true.
check_source_exists(_, _, F) :- otherwise | F = false.

save_state(Root, {TM, _}) :-
	TM =\= [] |
	state_filename(Root, FN),
	dribble('saving state in "~s"\n', [FN]),
	current_seconds(TS),
	open_file(FN, w, File),
	config:fleng_version(V),
	fmt:format(File, 'version(\'~a\').\nroot("~s").\nstatus(~d).\n', [V, Root, TS]) &
	map:values(TM, L, []),
	save_state2(L, File) &
	close_file(File).
save_state(_, _) :-
	otherwise |
	writeln('no targets').

save_state2([], _).
save_state2([target(TMap)|L], File) :-
	map:map_to_list(TMap, TL, []),
	fmt:format(File, '~q.\n', [target(TL)]) &
	save_state2(L, File).

% sync state with directory

update_state(Options, {TM, FM, TS}, State) :-
	get_global(root, Root),
	directory(Root, Dir),
	counter(L),
	load_dir_options(Root, Options, Opts2),
	ignored_files(Opts2, Ignored),
	walk_dir(Dir, Root, Ignored, Opts2, L/(-1), TS, TM, TM2, FM, FM2) &
	get_global(scan_failed, F1, false),
	(F1 == true -> halt(1)) &
	analyze_state(TM2, TM3, FM2, FM3),
	add_explicit_deps(Opts2, TM3, TM4),
	State = {TM4, FM3, TS},
	get_global(verbose, F, false),
	(F == true -> dump_state(TM3)).

rename_target(_, [], T1, T) :- T = T1.
rename_target(Fn, [rename(N, Fn)|_], _, T) :- T = N.
rename_target(Fn, [_|Opts], T1, T) :-
	otherwise | rename_target(Fn, Opts, T1, T).

load_dir_options(Dir, Opts1, Opts) :-
	path:join([Dir, "options"], OFn),
	file_exists(OFn, F),
	(F == true ->
		load_options(OFn, Opts2),
		list:append(Opts1, Opts2, Opts)
	;
		Opts = Opts1
	).

ignored(_, [], F) :- F = false.
ignored(Fn, [P|L], F) :-
	match:all(P, Fn, F1),
	(F1 == true -> F = true; ignored(Fn, L, F)).

walk_dir([], _, _, _, _, _)-_-_.
walk_dir([FN|Dir], Root, Ignored, Options, L, TS)-TM-FM :-
	path:join([Root, FN], FN2),
	ignored(FN, Ignored, F),
	(F == true ->
		dribble('ignoring "~s"\n', [FN2]),
		walk_dir(Dir, Root, Ignored, Options, L, TS)-TM-FM
	;
		path:join([Root, FN], Fn2),
		file_type(Fn2, T),
		walk_file(T, Fn2, Dir, Root, Ignored, Options, L, TS)-TM-FM
	).

walk_file(directory, Fn, Dir, Root, Ignored, Options, L/P, TS)-TM-FM :-
	load_dir_options(Fn, [], Opts2),
	directory(Fn, Dir2),
	list:append(Options, Opts2, Opts3),
	ignored_files(Opts3, Ignored2),
	counter(L2),
	walk_dir(Dir2, Fn, Ignored2, Opts3, L2/L, TS)-TM-FM,
	add_explicit_deps(Opts2)-TM,
	walk_dir(Dir, Root, Ignored, Options, L/P, TS)-TM-FM.
walk_file(link, Fn, Dir, Root, Ignored, Options, L, TS)-TM-FM :-
	readlink(Fn, Fn2),
	path:with_root(Root, Fn2, Fn3),
	% ignore symlinks outside of root dir:
	list:prefix(Root, Fn3, F),
	log(link(Root, Fn3, F)),
	file_exists(Fn3, EF),
	(F == true, EF == true ->
		file_type(Fn3, Type),
		walk_file(Type, Fn3, Dir, Root, Ignored, Options, L, TS)-TM-FM
	;
		walk_dir(Dir, Root, Ignored, Options, L, TS)-TM-FM
	).
walk_file(file, Fn, Dir, Root, Ignored, Options, LP, TS)-TM-FM :-
       maybe_load_options(Fn, Options, Opts2),
	scan_file(Fn, Opts2, LP)-TM-FM,
	walk_dir(Dir, Root, Ignored, Options, LP, TS)-TM-FM.
walk_file(_, _, _, _, _, _, _, _)-_-_ :-
	otherwise | true.

maybe_load_options(Fn)-Opts :-
	path:extension(Fn, Ext),
	maybe_load_options2(Ext, Fn)-Opts.

maybe_load_options2(Ext, Fn)-Opts :-
	list:member(Ext, ["ghc", "fghc", "pcn", "strand", "st", "graphics", "fl", "fleng", "kl1"], F),
	maybe_load_options3(F, Fn)-Opts.

maybe_load_options3(true, Fn, Opts1, Opts) :-
	otherwise |
	path:dirname(Fn, DN),
	path:basename(Fn, BN),
	fmt:format_chars('~s/~s.options', [DN, BN], OFn),
	file_exists(OFn, F),
	(F == true ->
		load_options(OFn, Opts2),
		list:append(Opts1, Opts2, Opts)
	;
		Opts = Opts1
	).
maybe_load_options3(false, _)-_.

add_explicit_deps([])-_.
add_explicit_deps([depends([T|TL])|Opts])-TM :-
	map:replace(T, Spec2, Spec)-TM,
	(Spec == [] ->
		abort('can not add dependencies for unknown target "~s"', [T])
	;
		add_dependencies(TL, Spec, Spec2),
		add_explicit_deps(Opts)-TM
	).
add_explicit_deps([_|Opts])-TM :-
	otherwise | add_explicit_deps(Opts)-TM.

ignored_files([], IF) :- IF = [].
ignored_files([ignore(FL)|L], IF) :-
	list:append(FL, IF2, IF),
	ignored_files(L, IF2).
ignored_files([_|L], IF) :-
	otherwise | ignored_files(L, IF).

% scanning

scan_file(Fn, Opts, L)-TM-FM :-
	path:extension(Fn, Ext),
	map:lookup(Fn, FM, TN1),
	rename_target(Fn, Opts, TN1, TN),
	(TN == [] ->
		T = []
	;
		map:lookup(TN, TM, T)
	),
	file_modification_time(Fn, MT),
	scan_file2(Ext, Fn, Opts, L, MT, T)-TM-FM.

scan_file2(Ext, Fn, Opts, L, MT, [])-TM-FM :-
	% not a registered normal target (module, program) or out of date
	find_custom_target(Fn, Opts, CT),
	scan_file_custom(CT, Ext, Fn, Opts, L, MT)-TM-FM.
scan_file2(Ext, Fn, Opts, L, MT, T)-TM-FM :-
	otherwise |
	get(mtime, T, TMT),
	get(options, T, TOpts),
	get(name, T, N),
	(MT =< TMT, Opts == TOpts ->
		put(level, L, T, T2),
		map:insert(N, T2)-TM,
		map:insert(Fn, N)-FM
	;
		delete_cache_file(T),
		scan_file2(Ext, Fn, Opts, L, MT, [])-TM-FM
	).

delete_cache_file(T) :-
	get(cached, T, CF),
	(CF =\= [] -> delete_file(CF)).

scan_file_custom(false, Ext, Fn, Opts, LP, MT)-TM-FM :-
	scan_file_by_ext(Ext, Fn, Opts, LP, MT, TN, T2),
	(T2 =\= false ->
		map:insert(TN, T2)-TM,
		map:insert(Fn, TN)-FM
	).
scan_file_custom([M|Deps], _, _, Opts, LP, MT)-TM-_ :-
	map:lookup(M, TM, F),
	(F == [] ->
	    maybe_load_options3(true, M, Opts, Opts2),
	    map:list_to_map([type-module, options-Opts2, mtime-MT, srcfile-{none},
             deps-Deps, level-LP, name-M], T1),
             map:insert(M, target(T1))-TM).

scan_file_by_ext("ghc", Fn, Opts, L, MT, TN, T) :-
	scan_file3(Fn, Opts, L, MT, fghc, TN, T).
scan_file_by_ext("fghc", Fn, Opts, L, MT, TN, T) :-
	scan_file3(Fn, Opts, L, MT, fghc, TN, T).
scan_file_by_ext("fleng", Fn, Opts, L, MT, TN, T) :-
	scan_fleng_file(Fn, Fn, Opts, L, MT, TN, T).
scan_file_by_ext("fl", Fn, Opts, L, MT, TN, T) :-
	scan_fleng_file(Fn, Fn, Opts, L, MT, TN, T).
scan_file_by_ext("strand", Fn, Opts, L, MT, TN, T) :-
	scan_file3(Fn, Opts, L, MT, fghc, TN, T).
scan_file_by_ext("st", Fn, Opts, L, MT, TN, T) :-
	scan_file3(Fn, Opts, L, MT, fghc, TN, T).
scan_file_by_ext("graphics", Fn, Opts, L, MT, TN, T) :-
	scan_file3(Fn, Opts, L, MT, fghc, TN, T).
scan_file_by_ext("pcn", Fn, Opts, L, MT, TN, T) :-
	scan_file3(Fn, Opts, L, MT, pcn, TN, T).
scan_file_by_ext("kl1", Fn, Opts, L, MT, TN, T) :-
	scan_file3(Fn, Opts, L, MT, kl1, TN, T).
scan_file_by_ext(_, _, _, _, _, _, T) :-
	otherwise | T = false.

scan_fleng_file(Fn, SFn, Opts, L, MT, TN, T) :-
	open_file(Fn, r, File),
	scan_fleng_forms(File, SFn, [], Mod, [], Uses, [], Incs, [], FO) &
	close_file(File),
	complete_target(SFn, SFn, MT, Mod, Opts, Uses, FO, Incs, fleng, L, TN, T).

scan_file3(Fn, Opts, L, MT, Lang, TN, T) :-
	path:dirname(Fn, Dir),
	target_fleng(Opts, Fleng),
	cache_file(Fn, Cache),
	compiler_options(Opts, COpts),
	list:append(['-I', Dir, Fn, '-o', Cache], COpts, Opts2),
	proc:execute([Fleng|Opts2], S),
	(S == 0 ->
		open_file(Cache, r, File),
		scan_fleng_forms(File, Fn, [], Mod, [], Uses, [], Incs, [], FO) &
		close_file(File),
		complete_target(Fn, Cache, MT, Mod, Opts, Uses, FO, Incs, Lang, L, TN, T)
	;
		put_global(scan_failed, true),
		T = false
	).

scan_fleng_forms(File, Fn)-M-U-I-F :-
	dribble('scanning file "~s"\n', [Fn]),
	open_port(P, S),
	handle_parse_errors(Fn, S),
	parse:parse_terms(File, Forms, P) &
	scan_exprs(Forms, Fn)-M-U-I-F.

complete_target(Fn, Cache, MT, Mod, Opts, Uses, FO, Incs, Lang, LP, TN, T) :-
	(Mod == [] ->
		Type = program,
		program_target(Fn, TN1)
	;
		Type = module,
		module_target(Fn, TN1)
	),
	rename_target(Fn, Opts, TN1, TN),
	map:list_to_map([type-Type, module-Mod, options-Opts, uses-Uses,
		mtime-MT, srcfile-Fn, language-Lang, includes-Incs, level-LP,
		name-TN, fobjects-FO, cached-Cache], T1),
	T = target(T1).

find_custom_target(_, [], R) :-R = false.
find_custom_target(Fn, [target([M|Deps])|Opts], R) :-
	list:member(Fn, Deps, F),
	(F == true ->
		R = [M|Deps]
	;
		find_custom_target(Fn, Opts, R)
	).
find_custom_target(Fn, [_|Opts], R) :-
	otherwise |
	find_custom_target(Fn, Opts, R).

program_target(Fn, TN) :-
	path:dirname(Fn, DN),
	path:basename(Fn, BN),
	path:join([DN, BN], TN).

module_target(Fn, TN) :-
	path:dirname(Fn, DN),
	path:basename(Fn, BN),
	fmt:format_chars('~s/~s.o', [DN, BN], TN).

library_target(Dir, TN) :-
	path:basename(Dir, BN),
	fmt:format_chars('~s/lib~s.a', [Dir, BN], TN).

handle_parse_errors(_, []).
handle_parse_errors(Fn, [error(LN, Fmt, Args, _)|_]) :-
	fmt:format(2, 'Error: (~s:~d) ~?\n', [Fn, LN, Fmt, Args]).

scan_exprs([], _)-_-_-_-_.
scan_exprs([-uses(Mods)|Forms], Fn, M1, M, U1, U)-I-F :-
	(list(Mods) -> Mods2 = Mods; Mods2 = [Mods]),
	list:append(Mods2, U1, U2),
	scan_exprs(Forms, Fn, M1, M, U2, U)-I-F.
scan_exprs([-module(M)|Forms], Fn, [], Mod)-U-I-F :-
	scan_exprs(Forms, Fn, M, Mod)-U-I-F.
scan_exprs([-foreign(Specs)|Forms], Fn)-M-U-I-F :-
	scan_foreign_specs(Specs, Fn)-F,
	scan_exprs(Forms, Fn)-M-U-I-F.
scan_exprs([-include(IF)|Forms], Fn)-M-U-I-F :-
	path:tail(IF, TL),
	(TL =\= "pcn.h" ->
		path:dirname(Fn, Dir),
		find_include(IF, Dir, IF2),
		path:normalize(IF2, Inc),
		Inc => I),
	scan_exprs(Forms, Fn)-M-U-I-F.
scan_exprs([-comment(included(IF))|Forms], Fn)-M-U-I-F :-
	list:characters(IF, IFL),
	(Fn == IFL ->
		scan_exprs(Forms, Fn)-M-U-I-F
	;
		scan_exprs([-include(IF)|Forms], Fn)-M-U-I-F
	).
scan_exprs([-module(_)|L], Fn, Old, M)-U-I-F :-
	Old =\= [] |
	fmt:format(2, 'Warning: file "~s" has multiple module declarations\n',
		[Fn]),
	scan_exprs(L, Fn, Old, M)-U-I-F.
scan_exprs([_|Forms], Fn)-M-U-I-F :-
	otherwise | scan_exprs(Forms, Fn)-M-U-I-F.

scan_foreign_specs([], _)-_.
scan_foreign_specs(S, Fn)-F :-
	string(S), S =\= [] | scan_foreign_specs([S], Fn)-F.
scan_foreign_specs([S|_], Fn)-F :-
	string(S) |
	scan_foreign_specs([verbatim(S)], Fn)-F.
scan_foreign_specs(T, Fn)-F :-
	tuple(T) |
	scan_foreign_specs([T], Fn)-F.
scan_foreign_specs([verbatim(_)|_], Fn)-F :-
	path:basename(Fn, Bn),
	path:dirname(Fn, Dn),
	fmt:format_chars('~s/~s~~foreign.o', [Dn, Bn], FFn),
	list:member(FFn, F, Fd),
	(Fd == false -> FFn => F).
scan_foreign_specs([_|L], Fn)-F :-
	otherwise | scan_foreign_specs(L, Fn)-F.

target_fleng([], F) :- global(fleng, F).
target_fleng([fleng(F1)|_], F) :- list:characters(F1, F).
target_fleng([_|Opts], F) :-
	otherwise | target_fleng(Opts, F).

find_include(Fn, Dir, IF) :-
    list:characters(Fn, Fn2),
    find_include2(Fn2, Dir, IF).

find_include2([0'/|Fn], _, IF) :- find_include3([0'/|Fn], IF).
find_include2(Fn, Dir, IF) :-
    otherwise |
    path:join([Dir, Fn], IF1),
    find_include3(IF1, IF).

find_include3(IF1, IF) :-
    file_exists(IF1, F1),
    (F1 == true ->
        dribble('located include file "~s"\n', [IF1])
    ;
        dribble('include file "~s" not found\n', [IF1])
    ) &
    IF = IF1.

% analysis

analyze_state-TM-FM :-
	map:map_to_list(TM, L),
	collect_includes(L, [], Incs),
	mark_included_targets(Incs, FM)-TM,
	partition_levels(TM, LM),
	map:values(LM, LML),
	create_targets(LML)-TM.

collect_includes([])-_.
collect_includes([_-Spec|L], Incs1, Incs) :-
	get(includes, Spec, Inc),
	set:union(Inc, Incs1, Incs2),
	collect_includes(L, Incs2, Incs).

mark_included_targets([], _)-_.
mark_included_targets([Inc|Incs], FM)-TM :-
	map:lookup(Inc, FM, TN),
	map:delete(TN, T)-TM,
	(T =\= [] ->
		mark_include(T, T2),
		get(name, T2, TN2),
		map:insert(TN2, T2)-TM),
	mark_included_targets(Incs, FM)-TM.

mark_include(T1, T) :-
	dribble('marking file "~s" as included file\n', [Src]),
	get(uses, T1, Uses),
	get(mtime, T1, MT),
	get(srcfile, T1, Src),
	get(language, T1, Lang),
	get(includes, T1, Incs),
	get(level, T1, LP),
	map:list_to_map([type-include, uses-Uses, mtime-MT, srcfile-Src,
		language-Lang, includes-Incs, level-LP, name-Src], T2),
	T = target(T2).
mark_include(T1, T) :- otherwise | T = T1.

partition_levels(TM1, LM) :-
	map:values(TM1, L1),
	partition_levels(L1, [], LM).

partition_levels([])-_.
partition_levels([Spec|L])-M :-
	get(level, Spec, Level/_),
	map:replace(Level, [Spec|Old], Old)-M,
	partition_levels(L)-M.

create_targets([])-_.
create_targets([Ts|LML])-TM :-
	programs_and_modules(Ts, Ps, Ms),
	create_library_targets(Ps, Ms)-TM,
	map_modules(Ms, [], MM),
	add_program_module_deps(Ps, MM)-TM,
	create_targets(LML)-TM.

map_modules([])-_.
map_modules([MSpec|L])-MM :-
	get(module, MSpec, Mod),
	(Mod =\= [] ->
		get(name, MSpec, MN),
		map:insert(Mod, MN)-MM),
	map_modules(L)-MM.

programs_and_modules([], Ps, Ms) :-
	Ps = [], Ms = [].
programs_and_modules([T|Ts], Ps, Ms) :-
	get(type, T, Type),
	programs_and_modules2(Type, T, Ts, Ps, Ms).

programs_and_modules2(program, T, Ts, Ps, Ms) :-
	Ps = [T|Ps2],
	programs_and_modules(Ts, Ps2, Ms).
programs_and_modules2(module, T, Ts, Ps, Ms) :-
	Ms = [T|Ms2],
	programs_and_modules(Ts, Ps, Ms2).
programs_and_modules2(_, _, Ts, Ps, Ms) :-
	otherwise |
	programs_and_modules(Ts, Ps, Ms).

create_library_targets([], [M1|Ms])-TM :-
	get(srcfile, M1, Src),
	path:dirname(Src, Dir),
	library_target(Dir, Ln),
	app:maplist(get(name), [M1|Ms], ONs),
	app:maplist(get(fobjects), [M1|Ms], FO1),
	list:append(FO1, FO),
	get(level, M1, L/P),
	map:list_to_map([type-library, mtime-0, deps-ONs, level-(L/P),
		name-Ln, fobjects-FO], T1),
	map:insert(Ln, target(T1))-TM,
	add_library_dep(P, Ln)-TM.
create_library_targets(_, _)-_ :-
	otherwise | true.

targets_at_level(L, TM, TL) :-
	map:values(TM, TL1),
	targets_at_level2(L, TL1, TL).

targets_at_level2(_, [], TL) :- TL = [].
targets_at_level2(L, [Spec|TL1], TL) :-
	get(level, Spec, L2/_),
	(L == L2 ->
		TL = [Spec|TL2],
		targets_at_level2(L, TL1, TL2)
	;
		targets_at_level2(L, TL1, TL)
	).

add_library_dep(L, LT)-TM :-
	targets_at_level(L, TM, TL),
	add_library_dep2(TL, LT)-TM.

add_library_dep2([], _)-_.
add_library_dep2([Spec|L], Ln)-TM :-
	get(type, Spec, Type),
	(Type == program ->
		get(name, Spec, T),
		add_dependencies([Ln], Spec, Spec2),
		map:insert(T, Spec2)-TM,
		add_library_dep2(L, Ln)-TM
	;
		add_library_dep2(L, Ln)-TM
	).

add_program_module_deps([], _)-_.
add_program_module_deps([P|L], MM)-TM :-
	get(uses, P, UL),
	resolve_uses(UL, [], MM, MNs, TM),
	add_dependencies(MNs, P, P2),
	app:maplist(get_fobjects(TM), MNs, FO1),
	list:append(FO1, FO),
	add_foreign_dependencies(FO, P2, P3),
	get(name, P, N),
	map:insert(N, P3)-TM,
	add_program_module_deps(L, MM)-TM.

get_fobjects(TM, MN, R) :-
    map:lookup(MN, TM, O),
    get(fobjects, O, R).

resolve_uses([], _, _, MNs, _) :- MNs = [].
resolve_uses([U|L], Seen, MM, MNs, TM) :-
	list:member(U, Seen, F),
	(F == true ->
		resolve_uses(L, Seen, MM, MNs, TM)
	;
		map:lookup(U, MM, MN),
		(MN == [] ->
			resolve_uses(L, [U|Seen], MM, MNs, TM)
		;
			MNs = [MN|MNs2],
			map:lookup(MN, TM, Mspec),
			get(uses, Mspec, U2),
			set:difference(U2, Seen, U3),
			list:append(U3, L, L2),
			resolve_uses(L2, [U|Seen], MM, MNs2, TM)
		)
	).

get_mtime(T, MT) :-
	file_exists(T, F),
	(F == true ->
		file_modification_time(T, MT)
	;
		MT = 0
	).

% clean

clean(TM, Root) :-
	map:values(TM, TL),
	clean2(TL),
	path:join([Root, ".flengmake"], FM),
	file_exists(FM, F),
	(F == true ->
		find:leaves(FM, Fs),
		app:foreach(lib:delete_file, Fs)).

clean2([]).
clean2([Spec|L]) :-
	get(name, Spec, N),
	get(type, Spec, Type),
	get(fobjects, Spec, FO),
	file_exists(N, F),
	clean_target(F, Type, N, FO)  &
	clean2(L).

clean_target(true, Type, N, FO) :-
	Type =\= include |
	clean_files([N|FO]).
clean_target(_, _, _, _) :- otherwise | true.

clean_files([]).
clean_files([N|L]) :-
	fmt:format('  rm \'~s\'\n', [N]) &
	get_global(noaction, F, false),
	(F == false ->
		delete_file(N) &
		clean_files(L)
	;
		clean_files(L)
	).

% build

build({TM, FM, TS}, State) :-
	map:keys(TM, TL),
	build2(TL, TS, TM, TM2, [], _, false, _),
	State = {TM2, FM}.

build2([], _)-_-_-_.
build2([T|TL], TS)-TM-Seen-RF :-
	map:lookup(T, Seen, FMT),
	map:lookup(T, TM, Spec),
	build_check(FMT, T, Spec, TS, TL)-TM-Seen-RF.

build_check(FMT, _, Spec, TS, TL)-TM-Seen-RF :-
	FMT =\= [], Spec =\= [] |
	% already seen and built and newer?
	(FMT > TS -> RF <== true),
	build2(TL, TS)-TM-Seen-RF.
build_check([], T, [], TS, TL)-TM-Seen-RF :-
	file_exists(T, F),
	(F == false ->
		abort('dependency "~s" does not exist', [T])
	;
		file_modification_time(T, TST),
		(TST > TS ->
			dribble('non-target dependency "~s" has changed\n', [T]),
			RF <== true),
		build2(TL, TS)-TM-Seen-RF
	).
build_check(_, T, Spec, TS, TL)-TM-Seen-RF :-
	otherwise |
	get(type, Spec, Type),
	build_type(Type, T, Spec)-TM-Seen-RF &
	build2(TL, TS)-TM-Seen-RF.

build_type(include, _, _)-_-_-_.
build_type(unknown, _, _)-_-_-_.
build_type(_, T, Spec)-TM-Seen-RF :-
	otherwise |
	file_exists(T, F),
	get(includes, Spec, Incs),
	get(deps, Spec, Deps),
	(F == true -> file_modification_time(T, MT); get(mtime, Spec, MT)),
	build2(Deps, MT)-TM-Seen+false+RF2 &
	build3(F, T, Incs, Spec)-TM+RF2+RF3,
	(RF3 == true -> RF <== true),
	map:lookup(T, TM, Spec2),
	get(mtime, Spec2, MT2),
	map:insert(T, MT2)-Seen.

build3(false, T, _, Spec, TM1, TM, _, RF) :-
	dribble('target "~s" does not exist\n', [T]),
	compile(Spec, TM1, TM),
	RF = true.
build3(true, T, _, Spec, TM1, TM, true, RF) :-
	dribble('dependency of target "~s" changed\n', [T]),
	compile(Spec, TM1, TM),
	RF = true.
build3(true, T, Incs, Spec, TM1, TM, false, RF) :-
	file_modification_time(T, MT2),
	get(mtime, Spec, MT),
	(MT > MT2 ->
		dribble('source file of target "~s" changed\n', [T]),
		compile(Spec, TM1, TM),
		RF = true
	;
		check_includes(Incs, MT2, TM1, F),
		(F == true ->
			dribble('files included by target "~s" changed\n', [T]),
			compile(Spec, TM1, TM),
			RF = true
		;
			TM = TM1,
			RF = false
		)
	).
build3(true, _, [], _)-_-_ :- otherwise | true.

check_includes([], _, _, F) :- F = false.
check_includes([Inc|L], MT, TM, F) :-
	map:lookup(Inc, TM, ISpec),
	check_includes2(ISpec, Inc, L, MT, TM, F).

check_includes2([], Inc, L, MT, TM, F) :-
	file_modification_time(Inc, IMT),
	check_includes3(IMT, L, MT, TM, F).
check_includes2(ISpec, _, L, MT, TM, F) :-
	otherwise |
	get(mtime, ISpec, IMT),
	check_includes3(IMT, L, MT, TM, F).

check_includes3(IMT, L, MT, TM, F) :-
	(IMT > MT ->
		F = true
	;
		check_includes(L, MT, TM, F)
	).

compile(Spec)-TM :-
	get_global(noaction, F, false),
	get(type, Spec, Type),
	build_command(Type, Spec, B),
	apply(B, [F, S]) &
	compile2(S, F, Spec)-TM.

compile2(_, true, _)-_.
compile2(0, false, Spec)-TM :-
	get(name, Spec, N),
	file_modification_time(N, MT),
	update_mtime(MT, Spec, Spec2),
	map:insert(N, Spec2)-TM.
compile2(S, _, _)-_ :-
	otherwise |
	fmt:format(2, 'build command terminated with status ~d\n', [S]) &
	halt(1).

build_command(library, Spec, Cmd) :-
	get(name, Spec, N),
	get(deps, Spec, Deps),
	get(fobjects, Spec, FO),
	Cmd = build_library(N, Deps, FO).
build_command(Type, Spec, Cmd) :-
	otherwise |
	get(options, Spec, Opts),
	get(name, Spec, N),
	get(deps, Spec, Deps),
	get(fobjects, Spec, FO),
	(Type == program, list(FO) ->
	    list:append(Deps, FO, Deps2)
	;
	    Deps2 = Deps
	),
	target_fleng(Opts, Fleng),
	path:dirname(N, Dir),
	compiler_options(Opts, COpts),
	get(srcfile, Spec, Src),
	(Type == module, list(FO) ->
	    path:dirname(Src, Dir),
	    path:basename(Src, Base),
	    fmt:format_chars('~s/~s~~foreign', [Dir, Base], FPrefix),
	    list:append(COpts, ["-foreign", FPrefix], COpts2)
	;
	    COpts2 = COpts
	),
	get(cached, Spec, Cache),
	get(includes, Spec, Incs),
	builder(Opts, ["-I", Dir|COpts2], Fleng, Src, Cache, Incs, N, Deps2, Cmd).

compiler_options([], Opts) :- Opts = [].
compiler_options([options(Opts1)|L], Opts) :-
	list:append(Opts1, T2, Opts),
	compiler_options(L, T2).
compiler_options([_|L], Opts) :-
	otherwise | compiler_options(L, Opts).

builder(Opts, Flags, Fleng, Src, Cache, Incs, T, Deps, B) :-
	filter_options(build, Opts, Bs),
	builder2(Bs, Flags, Fleng, Src, Cache, Incs, T, Deps, B).

builder2([Bld], Flags, Fleng, _, _, Incs, T, Deps, B) :-
	B = build_custom(Bld, Flags, Fleng, Incs, T, Deps).
builder2([], _, _, {none}, _, _, T, _, _) :-
	abort('custom target "~s" has no build instructions', [T]).
builder2([], Flags, Fleng, Src, Cache, _, T, Deps, B) :-
	Src =\= {none} |
	(Src == [] -> Src2 = []; Src2 = [Src]),
	B = build_fleng(Flags, Fleng, Src2, Cache, T, Deps).
builder2(Blds, _, _, _, _, _, T, _, _) :-
	otherwise |
	abort('conflicting build commands for target ~s: ~q', [T, Blds]).

build_custom([B1|Bld], Flags, Fleng, Incs, T, Deps, F, S) :-
	list:join(Flags, 32, FS, []),
	list:join([B1|Bld], 32, BCmd),
	list:append([[BCmd, T], Deps, Incs], Cmd),
	show_command(Cmd) &
	(F == false ->
		(setenv('FLENG', Fleng), setenv('FLENG_OPTIONS', FS)) &
		proc:execute(Cmd, S)).

build_fleng(Flags, Fleng, Src, [], T, Deps, F, S) :-
	list:append([[Fleng], Src, Deps, ["-o", T|Flags]], Cmd),
	show_command(Cmd) &
	(F == false -> proc:execute(Cmd, S)).
build_fleng(Flags, Fleng, Src, Cache, T, Deps, F, S) :-
	otherwise |
	list:append([[Fleng], ["-w", Cache], Deps, ["-o", T|Flags]], BCmd),
	list:append([[Fleng], Src, Deps, ["-o", T|Flags]], DCmd),
	show_command(DCmd) &
	(F == false -> proc:execute(BCmd, S)).

build_library(T, Deps, FO, F, S) :-
	list:append(Deps, FO, Objs),
	Cmd = ["ar", "cru", T|Objs],
	show_command(Cmd) &
	(F == false -> proc:execute(Cmd, S)).

show_command(Cmd) :-
	app:maplist(quote_arg, Cmd, Cmd2),
	list:join(Cmd2, 32, PCmd),
	fmt:format(2, '  ~s\n', [PCmd]).

quote_arg("", X) :- X = "''".
quote_arg(X, Y) :-
	set:intersection(X, " <|>", S),
	(S == [] -> Y = X; fmt:format_chars('\'~s\'', [X], Y)).
quote_arg(X, Y) :- otherwise | Y = X.

% graph generation

show(TM, TS) :-
	map:map_to_list(TM, TL),
	writeln('digraph {\n  node[shape=parallelogram];') &
	show2(TL, TS) &
	writeln('}').

show2([], _).
show2([T-Spec|TL], TS) :-
	get(type, Spec, Type),
	show_target(T, Type, TS, Spec) &
	show2(TL, TS).

mtime_color(T, TS, C) :-
	get_mtime(T, MT),
	(MT =\= 0, MT > TS ->
		C = ',fillcolor=gray75'
	;
		C = ''
	).

show_target(T, Type, TS, Spec) :-
	mtime_color(T, TS, C),
	target_shape(Type, Sh),
	fmt:format('  "~s"[shape=~s~s];\n', [T, Sh, C]) &
	get(deps, Spec, Deps),
	get(srcfile, Spec, Src),
	get(includes, Spec, Incs),
	get(options, Spec, Opts),
	filter_options(build, Opts, Bld),
	show_source(T, Src, Incs) &
	show_builder(T, Bld) &
	show_edges(T, Deps, solid).

target_shape(include, S) :- S = note.
target_shape(module, S) :- S = box.
target_shape(library, S) :- S = 'box,style=bold'.
target_shape(program, S) :- S = oval.

show_source(_, {none}, _).
show_source(T, Src, Incs) :-
	otherwise |
	fmt:format('  "~s"[shape=note];\n', [Src]) &
	(T =\= Src -> fmt:format('  "~s" -> "~s";\n', [T, Src])) &
	show_edges(Src, Incs, dotted).

show_edges(_, [], _).
show_edges(T, [X|L], Style) :-
	fmt:format('  "~s" -> "~s"[style=~s];\n', [T, X, Style]) &
	show_edges(T, L, Style).

show_builder(_, []).
show_builder(T, [[Bld]|_]) :-
	fmt:format('  "~s"[shape=octagon];\n  "~s" -> "~s"[style=dashed];\n',
		[Bld, T, Bld]).

% variable substitution

substitute_vars([], S) :- S = [].
substitute_vars([0'\\, C|S1], S) :-
    S = [C|S2],
    substitute_vars(S1, S2).
substitute_vars([0''|S1], S) :-
    scan:delimited_with_escape(0'', T, S1, S2),
    list:append(T, S3, S),
    substitute_vars(S2, S3).
substitute_vars([0'$, 0'{|S1], S) :-
    scan:not_from_set("}", T, S1, S2),
    getenv(T, V),
    list:append(V, S3, S),
    substitute_vars2(T, S2, S3).
substitute_vars([0'$, C|S1], S) :-
    C =\= 0'{ |
    scan:identifier(T, [C|S1], S2),
    getenv(T, V),
    list:append(V, S3, S),
    substitute_vars(S2, S3).
substitute_vars([C|S1], S) :-
    otherwise |
    S = [C|S2],
    substitute_vars(S1, S2).

substitute_vars2(_, [0'}|S1], S) :- substitute_vars(S1, S).
substitute_vars2(T, S, _) :-
    otherwise |
    abort('missing "}" after variable substitution of "~s": ~s"', [T, S]).
