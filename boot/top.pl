% FLENG compiler - toplevel

:- dynamic([pdef/2, r_atom/2, r_data/1, module_name/1, module_init/1]).
:- dynamic([no_warnings/0, debug_info/0, entry_point/2, uses/1, cmdline_init/1]).
:- dynamic([drop_sequence/3, literal/2, symbol_loc/2, exported/1]).
:- dynamic([pdef_name/2, module_info/3, resolve_cache/2, synthesized_name/2]).
:- dynamic([include_path/1, foreign_wrapper/2]).
:- dynamic([generate_entry_points/0, print_progress/0]).

:- if(current_prolog_flag(dialect, gprolog)).
% GNU
:- initialization(main).

main :-
    argument_list(Args),
    main(Args).
:- endif.

:- if(current_prolog_flag(dialect, swi)).
% SWI
save_to_file(Name) :- 
    qsave_program(Name, [stand_alone(true), goal(start)]), 
    halt(0).

start :-
    set_prolog_flag(on_error, halt),
    current_prolog_flag(argv, Args),
    main(Args).

format_to_atom(Out, Fstr, Args) :-
    format_to_codes(Fstr, Args, S),
    atom_codes(Out, S).

file_exists(F) :- exists_file(F).

g_assign(Var, Val) :- nb_setval(Var, Val).
g_read(Var, Val) :- nb_getval(Var, Val).
:- endif.

:- if(current_prolog_flag(dialect, yap)).
% YAP
:- initialization(main).

:- use_module(library(lists)).
:- use_module(library(charsio)).

start :-
    set_prolog_flag(syntax_errors, error),
    set_prolog_flag(unknown, error),
    unix(argv(Args)),
    catch(main(Args), E, quit(E)).

subtract([], _, []).
subtract([Element|Residue], Set, Difference) :-
	memberchk(Element, Set), !,
	subtract(Residue, Set, Difference).
subtract([Element|Residue], Set, [Element|Difference]) :-
	subtract(Residue, Set, Difference).

numbervars(Term) :- numbervars(Term, 0, _).

format_to_atom(Out, Fstr, Args) :-
    format_to_chars(Fstr, Args, S),
    atom_codes(Out, S).

save_to_file(Name) :- save(Name), halt(0).

quit(E) :-
    print_message(error, E),
    halt(1).

g_assign(Var, Val) :- nb_setval(Var, Val).
g_read(Var, Val) :- nb_getval(Var, Val).
:- endif.
