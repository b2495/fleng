/* C support for EZD */

#include "fleng.h"
#include "fleng-util.h"
#include <math.h>

#define min(x, y)  ((x) < (y) ? (x) : (y))
#define max(x, y)  ((x) > (y) ? (x) : (y))

static double numval(FL_VAL x)
{
    if(ISINT(x)) return INT(x);
    else return FLOATVAL(x);
}

/* https://stackoverflow.com/questions/7050186/find-if-point-lies-on-line-segment */
void ezd_point_on_line_4(FL_TCB *tcb, FL_VAL xp, FL_VAL yp, FL_VAL x1a, FL_VAL more)
{
    LIST(more, y1a, more2);
    LIST(more2, x2a, more3);
    LIST(more3, y2a, more4);
    LIST(more4, eps, result);
    double x = numval(xp);
    double y = numval(yp);
    double x1 = numval(x1a);
    double y1 = numval(y1a);
    double x2 = numval(x2a);
    double y2 = numval(y2a);
    double epsilon = numval(eps);
    double minX = min(x1, x2);
    double maxX = max(x1, x2);
    double minY = min(y1, y2);
    double maxY = max(y1, y2);
    int r;
    if((x - max(x1, x2) > epsilon) || (min(x1, x2) - x > epsilon) ||
       (y - max(y1, y2) > epsilon) || (min(y1, y2) - y > epsilon)) r = 0;
    else if(fabs(x2 - x1) < epsilon)
        r = fabs(x1 - x) < epsilon || fabs(x2 - x) < epsilon;
    else if(fabs(y2 - y1) < epsilon)
        r = fabs(y1 - y) < epsilon || fabs(y2 - y) < epsilon;
    else {
    	double px = x1 + (y - y1) * (x2 - x1) / (y2 - y1);
    	double py = y1 + (x - x1) * (y2 - y1) / (x2 - x1);
       r = fabs(x - px) < epsilon || fabs(y - py) < epsilon;
    }
    fl_assign(tcb, r ? fl_true : fl_false, result);
}

void ezd_point_in_box_4(FL_TCB *tcb, FL_VAL xp, FL_VAL yp, FL_VAL box, FL_VAL r)
{
    TUPLE(box, x1a, n, more);
    LIST(more, x2a, more2);
    LIST(more2, y1a, more3);
    LIST(more3, y2a, ignore);
    double x1 = numval(x1a);
    double y1 = numval(y1a);
    double x2 = numval(x2a);
    double y2 = numval(y2a);
    double x = numval(xp);
    double y = numval(yp);
    int f = x >= x1 && y >= y1 && x < x2 && y < y2;
    fl_assign(tcb, f ? fl_true : fl_false, r);
}
