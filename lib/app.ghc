%= "app" module: Higher-order operations on lists
%
% This module provides operations to invoke user-specifcied goals
% over lists, like fold, mapp, filter and partition.
%
% All goals default to the empty module ('':...) when no module prefix
% in given. Goals are invoked using "apply/2".
%
% The FGHC->FLENG compiler will detect and wrap primitive operations in GOAL
% position, but if GOAL is a variable, then it must refer to a
% user-defined process.
%
% [PCN] For PCN programs, the goals must be user-defined and given either
% in the "``...``" syntax or as strings or tuples of the form
%
%   {"<name>", <argument>, ...}
%
% Calls to specific modules should be encoded as tuples like this:
%
%   {":", "<module>", "<name>"}
%   {":", "<module>", {"<name>", <arguments>, ...}}
%
%-
% app:maplist(GOAL?, LIST?, RESULTLIST^)
% app:maplist(GOAL?, LIST?, RESULTLIST^, TAIL?)
%       Invoke "apply(GOAL?, [ELEMENT?, RESULT^])" for each element of LIST and
%       unify the list of results with RESULTLIST, optionally terminated by
%       TAIL.
%
% app:mapappend(GOAL?, LIST?, RESULT^, TAIL?)
%       Invoke "apply(GOAL?, [ELEMENT?, ARESULT^, ATAIL?])" for each
%       element of LIST, where ARESULT + ATAIL represent a list of results
%       which are combined into a final list. ARESULT always equals the
%       ATAIL of the goal-invocation of the previous element. The final list
%       is terminated by TAIL.
%
% app:foreach(GOAL?, LIST?)
% app:foreach(GOAL?, LIST?, DONE^)
%       Invoke "apply(GOAL?, [ELEMENT?])" for each element of LIST and unifies DONE
%       with the empty list when the last application is invoked.
%       The applications of GOAL are performed as a separate task and
%       each application only takes place after the previous one completed.
%
% app:filter(GOAL?, LIST?, RESULTLIST^)
% app:filter(GOAL?, LIST?, RESULTLIST^, TAIL?)
%       Invoke "apply(GOAL?, [ELEMENT?, RESULT^])" for each element of LIST and
%       unify the list of elements for which RESULT was true (anything but the string "false")
%       with RESULTLIST, optionally terminated with TAIL.
%
% app:partition(GOAL?, LIST?, TRUERESULTS^, FALSERESULTS^)
%       Invoke "apply(GOAL?, [ELEMENT?, RESULT^])" for each element of LIST and
%       unify the list of elements for which RESULT was true (anything but the string "false")
%       with TRUERESULTS and those for which RESULT was "false" with FALSERESULTS.
%
% app:foldl(GOAL?, LIST?, INITIAL?, FINAL^)
%       Invoke "apply(GOAL, [ELEMENT?, PREVIOUS?, RESULT^])" for
%       each element of LIST, where PREVIOUS holds the result of the previous
%       application (INITIAL if this is the first one) and unifies FINAL with the final result.
%
% app:mapreduce(GOAL?, LIST?, INITIAL?, FINAL^)
%       Similar to app:foldl, but invokes GOAL over as many threads as LIST has
%       elements.
%
% app:take(GOAL?, LIST?, RESULT^, TAIL^)
% app:take(GOAL?, LIST?, RESULT^, TAIL^, MORE^)
%       Unify RESULT with the elements of LIST until "apply(ELEMENT, FLAG)"
%       assigns "false" to FLAG. The result list is terminated with TAIL.
%       if MORE is given, it will be unified with the remaining list in case
%       GOAL returns false.
%
% app:drop(GOAL?, LIST?, RESULT^)
%       Unify RESULT with the elements of LIST starting from the location
%       at which "apply(ELEMENT, FLAG)" first assigns "false" to FLAG.
%
% app:any(GOAL?, LIST?, RESULT^)
%       Unifies RESULT with "true" if "apply(GOAL, [ELEMENT?, FLAG^])"
%       assigns a value other than "false" to FLAG for at least one of the
%       elements of LIST.
%
% app:every(GOAL?, LIST?, RESULT^)
%       Unifies RESULT with "true" if "apply(GOAL, [ELEMENT?, FLAG^])"
%       assigns a value other than "false" to FLAG for all elements of LIST.
%
% app:sequence(GOAL?, COUNT?, RESULT^, TAIL?)
% app:sequence(GOAL?, START?, COUNT?, RESULT^, TAIL?)
%       Call GOAL with additional arguments [N, R], where N increases from
%       START (or 1) to COUNT (inclusive) and unify RESULT with the list of
%       results R, terminated by TAIL.
%
% app:times(GOAL?, END?, DONE^)
% app:times(GOAL?, START?, END?, DONE^)
%       Invokes GOAL with integers from START (or 1 if not given) to END
%       (inclusive) and assigns the empty list to DONE when finished.
%
% app:compose(GOALS?, INITIAL?, RESULT^)
%       Invokes each goal in the list GOALS with "apply(GOAL?, VALUE?,
%       NEXT^)", taking the result of the previous application (INITIAL,
%       in the first GOAL and the previous NEXT in all subsequent goals)
%       and unifies RESULT with the final value.
%
% app:index(GOAL?, LIST?, INDEX^)
%       Invokes "apply(GOAL, [ELEMENT?, FLAG^])" and assigns the index
%       (starting from 1) to INDEX where FLAG returns a non-false value.
%       If no element succeeds, assigns the string "false" to INDEX.
%
% app:count(GOAL?, LIST?, RESULT^)
%       Invokes "apply(GOAL, [ELEMENT?, FLAG^])" for every element of
%       LIST and assigns the number of times FLAG was bound to a value
%       other than the string "false" to RESULT.
%
% app:walk(GOAL?, MAP?, INITIAL?, RESULT^)
%       Traverses MAP, which should be a map as created by the "map"
%       library module, by invoking "apply(GOAL, [KEY?, VALUE?, PREVIOUS?, NEXT^])"
%       to every key/value pair of MAP, in sorted and ascending key order.
%       INITIAL represents the first value of PREVIOUS, and RESULT will be unified
%       with the final result returned in NEXT.

-module(app).

maplist(G, L, R) :- maplist(G, L, R, []).

maplist(_, [], R, T) :- R = T.
maplist(C, [X|L], R, T) :-
    papply(C, [X, Y]),
    R = [Y|R2],
    maplist(C, L, R2, T).

foreach(C, L) :- foreach(C, L, _).
foreach(_, [], Ok) :- Ok = [].
foreach(C, [X|L], Ok) :-
    papply(C, [X]) &
    foreach(C, L, Ok).

filter(G, L, R) :- filter(G, L, R, []).

filter(_, [], R, T) :- R = T.
filter(C, [X|L], R, T) :-
    papply(C, [X, B]),
    (B == false ->
        filter(C, L, R, T)
     ;
        R = [X|R2],
        filter(C, L, R2, T)
    ).

partition(_, [], Y, N) :- Y = [], N = [].
partition(C, [X|L], Y, N) :-
    papply(C, [X, B]),
    (B == false ->
        N = [X|N2],
        partition(C, L, Y, N2)
     ;
        Y = [X|Y2],
        partition(C, L, Y2, N)
    ).

foldl(_, [], I, R) :- R = I.
foldl(C, [X|L], I, R) :-
    papply(C, [X, I, R1]),
    foldl(C, L, R1, R).

papply(M:T, Args) :- apply(M:T, Args).
papply(T, Args) :- otherwise | apply('':T, Args).

mapreduce(_, [], _, L, R) :- R = L.
mapreduce(Call, [X|List], L, R) :-
    papply(Call, [X, L, L2]),
    mapreduce(Call, List, L2, R)@fwd.

take(C, L, R, T) :- take(C, L, R, T, _).

take(_, [], R, T, T2) :- R = T, T2 = [].
take(C, [X|L], R, T, T2) :-
    papply(C, [X, F]),
    (F == false ->
        R = T, T2 = [X|L]
     ;
        R = [X|R2],
        take(C, L, R2, T, T2)
    ).

drop(_, [], R) :- R = [].
drop(C, [X|L], R) :-
    papply(C, [X, F]),
    (F == false ->
    	R = [X|L]
     ;
     	drop(C, L, R)
    ).

any(_, [], R) :- R = false.
any(G, [X|L], R) :-
    papply(G, [X, F]),
    (F == false -> any(G, L, R)
     ; R = true
    ).

every(_, [], R) :- R = true.
every(G, [X|L], R) :-
    papply(G, [X, F]),
    (F == false -> R = false
     ; every(G, L, R)
    ).

times(G, N, Done) :- times(G, 1, N, Done).

times(_, N, M, Done) :- N > M | Done = [].
times(G, N, M, Done) :-
	otherwise |
	papply(G, [N]),
	N2 is N + 1,
	times(G, N2, M, Done).

sequence(G, N, R, T) :- sequence(G, 1, N, R, T).

sequence(_, I, N, R, T) :- I > N | R = T.
sequence(G, I, N, R, T) :-
    otherwise |
    papply(G, [I, X]),
    R = [X|R2],
    I2 is I + 1,
    sequence(G, I2, N, R2, T).

compose([], S1, S) :- S = S1.
compose([G|L], S1, S) :-
    papply(G, [S1, S2]),
    compose(L, S2, S).

mapappend(_, [], R, T) :- R = T.
mapappend(G, [X|L], R, T) :-
    papply(G, [X, R, T2]),
    mapappend(G, L, T2, T).

index(G, L, I) :- index(G, 1, L, I).

index(_, _, [], I) :- I = false.
index(G, P, [X|L], I) :-
	papply(G, [X, R]),
	(R == false ->
		P2 is P + 1,
		index(G, P2, L, I)
	;
		I = P
	).

count(G, L, R) :- count(G, L, 0, R).

count(_, [], R1, R) :- R = R1.
count(G, [X|L], R1, R) :-
	papply(G, [X, F]),
	(F =\= false -> R2 is R1 + 1; R2 = R1),
	count(G, L, R2, R).

% map-walker, assumes "t(Height, Key, Val, Left, Right)" representation

walk(_, [], X1, X) :- X = X1.
walk(G, t(_, K, V, L, R), X1, X) :-
	walk2(G, t(0, K, V, L, R), X1, X).
walk(_, X, _, _) :-
	otherwise | error('app:walk: expected map'(X)).

walk2(_, [], X1, X) :- X = X1.
walk2(G, t(_, K, V, L, R), X1, X) :-
	walk2(G, L, X1, X2),
	papply(G, [K, V, X2, X3]),
	walk2(G, R, X3, X).
