% FLENG compiler - x86_64 backend - BTI ("b") flavor

target_architecture(x64b).

sym_prefix('').

b_target(_, Out) :- emit('  endbr64\n', Out).
