/* Forward data to detached port

    A small command-line program to send expressions to
    another running FLENG process via "detached" message-ports.

    usage: fwd PORTFILE [DATAFILE]

    Parses the data in DATAFILE or from stdin which should be in FGHC
    term notation (terminated by ".") and sends it to the PORTFILE,
    naming a detached message port from another FLENG process
    that had called "sys:detach_message_port" earlier and is still
    running.
*/

-initialization(main).
-uses([lex, parse, fmt]).

main :-
    command_line(Args),
    parse_args(Args, FName, DFName),
    open_data_file(DFName, In),
    open_port(E, S),
    lex:lex_file(In, TS, E),
    parse:parse_terms(TS, 1, Forms, E),
    report(S, 0),
    sys:attach(FName, _, MPort),
    forward(Forms, MPort).

parse_args([FN1], FN, DFN) :-FN = FN1, DFN = ('-').
parse_args([FN1, DFN1], FN, DFN) :- FN = FN1, DFN = DFN1.
parse_args(_, _, _) :-
    otherwise |
    fmt:format(2, 'usage: fwd PORTFILE [DATAFILE]\n', [], Ok),
    when(Ok, halt(1)).

open_data_file('-', File) :- File = 0.
open_data_file(FN, File) :- otherwise | open_file(FN, r, File).

report([], 0).
report([], 1) :- halt(1).
report([error(_, Fmt, Args, Code)|S], C1) :-
    fmt:format(2, Fmt, Args, Ok),
    C is C1 \/ Code,
    when(Ok, report(S, C)).

forward([], _).
forward([X|R], MPort) :-
    sys:transmit(MPort, X, Ok),
    when(Ok, forward(R, MPort)).
