% common code for Strand/GHC and KL1 lexical analyzers

lex_file(FNAME, O, E) :-
    string(FNAME) |
    open_file(FNAME, r, FD),
    lex_file(FD, O, E).
lex_file(FD, O, E) :-
    integer(FD) |
    io:read_char_stream(FD, I),
    lex(I, 1, O, E).

lex([], _, O, _) :- O = [].
lex([0'_|R], L, O, E) :- var(0'_, R, L, O, E).
lex([0',|R], L, O, E) :- punct(0',, R, L, O, E).
lex([0';|R], L, O, E) :- punct(0';, R, L, O, E).
lex([0'.|R], L, O, E) :- punct(0'., R, L, O, E).
lex([0'[|R], L, O, E) :- punct(0'[, R, L, O, E).
lex([0']|R], L, O, E) :- punct(0'], R, L, O, E).
lex([0'||R], L, O, E) :- punct(0'|, R, L, O, E).
lex([0'(|R], L, O, E) :- punct(0'(, R, L, O, E).
lex([0')|R], L, O, E) :- punct(0'), R, L, O, E).
lex([0'{|R], L, O, E) :- punct(0'{, R, L, O, E).
lex([0'}|R], L, O, E) :- punct(0'}, R, L, O, E).
lex([10|R], L, O, E) :- L2 is L + 1, lex(R, L2, O, E).
lex([0'\'|R], L, O, E) :- str(R, L, O, E).
lex([0'\"|R], L, O, E) :- charlist(R, L, O, E).
lex([0'#|R], L, O, E) :- special(R, L, O, E).
lex([0'%|R], L, O, E) :- skip(R, L, O, E).
lex([0'/, 0'*|R], L, O, E) :- skip_block(R, L, O, E).
lex([32|R], L, O, E) :- lex(R, L, O, E).
lex([9|R], L, O, E) :- lex(R, L, O, E).
lex([13|R], L, O, E) :- lex(R, L, O, E).
lex([C|R], L, O, E) :- C >= 0'0, C =< 0'9 | num(C, R, L, O, E).
lex([C|R], L, O, E) :-
    otherwise |
    ucs:lower(C, LF),
    ucs:upper(C, UF),
    lex_l(LF, UF, C, R, L, O, E).

lex_l(true, false, C, R, L, O, E) :- ident(C, R, L, O, E).
lex_l(false, true, C, R, L, O, E) :- var(C, R, L, O, E).
lex_l(_, _, C, R, L, O, E) :- otherwise | op([C|R], L, O, E).

skip([], _, O, _) :- O = [].
skip([10|R], L, O, E) :- L2 is L + 1, lex(R, L2, O, E).
skip([_|R], L, O, E) :- otherwise | skip(R, L, O, E).

skip_block([], L, O, E) :-
    send(E, error(L, 'premature end of block comment', [], 1)),
    O = [].
skip_block([10|R], L, O, E) :-
    L2 is L + 1,
    skip_block(R, L2, O, E).
skip_block([0'*, 0'/|R], L, O, E) :- lex(R, L, O, E).
skip_block([_|R], L, O, E) :-
    otherwise | skip_block(R, L, O, E).

ident(C, R, L, O, E) :-
    scan_ident(R, R2, LST),
    list_to_string([C|LST], ID),
    O = [id(L, ID)|O2],
    lex(R2, L, O2, E).

scan_ident([], R2, LST) :- LST = [], R2 = [].
scan_ident([0'_|R], R2, LST) :-
    LST = [0'_|LST2], scan_ident(R, R2, LST2).
scan_ident([C|R], R2, LST) :-
    otherwise |
    ucs:alpha(C, F),
    scan_ident2(F, C, R, R2, LST).

scan_ident2(true, C, R, R2, LST) :-
    LST = [C|LST2], scan_ident(R, R2, LST2).
scan_ident2(false, C, R, R2, LST) :-
    C >= 0'0, C =< 0'9 | LST = [C|LST2], scan_ident(R, R2, LST2).
scan_ident2(_, C, R, R2, LST) :-
    otherwise | R2 = [C|R], LST = [].

num(0'0, [0'x|R], L, O, E) :-
    base_crange(16, CR1, CR2),
    scan_bnum(R, CR1, CR2, R2, Lst),
    list_to_integer(Lst, 16, Num),
    finalize_num(Lst, Num, R2, L, O, E).
num(0'0, [0'b|R], L, O, E) :-
    scan_bnum(R, 0'0-0'1, xxx, R2, Lst),
    list_to_integer(Lst, 2, Num),
    finalize_num(Lst, Num, R2, L, O, E).
num(0'0, [0'o|R], L, O, E) :-
    scan_bnum(R, 0'0-0'7, xxx, R2, Lst),
    list_to_integer(Lst, 8, Num),
    finalize_num(Lst, Num, R2, L, O, E).
num(0'0, [0'\', C|R], L, O, E) :-
    esc([C|R], R2, CO),
    O = [num(L, CO)|O2],
    lex(R2, L, O2, E).
num(C, R, L, O, E) :-
    otherwise |
    scan_num(R, R2, LST, FF),
    num2(FF, R2, L, [C|LST], O, E).

finalize_num(Lst, error, _, L, _, E) :-
	send(E, error(L, 'unrepresentable number "0x~s"', [Lst], 1)).
finalize_num(_, Num, R, L, O, E) :-
	otherwise |
	O = [num(L, Num)|O2],
	lex(R, L, O2, E).

num2(false, [0'\'|R], L, LST, O, E) :-
    list_to_number(LST, BASE),
    base_crange(BASE, CR1, CR2),
    scan_bnum(R, CR1, CR2, R2, LST2),
    list_to_integer(LST2, BASE, NUM),
    O = [num(L, NUM)|O2],
    lex(R2, L, O2, E).
num2(_, R, L, LST, O, E) :-
    otherwise |
    list_to_number(LST, NUM),
    O = [num(L, NUM)|O2],
    lex(R, L, O2, E).

scan_num([0'., C|R], R2, LST, FF) :-
    C >= 0'0, C =< 0'9 |
    LST = [0'., C|LST2],
    FF = true,
    scan_fnum(R, R2, LST2).
scan_num([C|R], R2, LST, FF) :-
    C >= 0'0, C =< 0'9 |
    LST = [C|LST2],
    scan_num(R, R2, LST2, FF).
scan_num(R, R2, LST, FF) :-
    otherwise | R2 = R, LST = [], FF = false.

scan_fnum([C|R], R2, LST) :-
    C >= 0'0, C =< 0'9 |
    LST = [C|LST2],
    scan_fnum(R, R2, LST2).
scan_fnum(R, R2, LST) :-
    otherwise | R2 = R, LST = [].

scan_bnum([C|R], CR1, CR2, R2, LST) :-
    ucs:tolower(C, CLO),
    scan_bnum1([CLO|R], CR1, CR2, R2, LST).

scan_bnum1([C|R], CL-CH, CR2, R2, LST) :-
    C >= CL, C =< CH |
    LST = [C|LST2],
    scan_bnum(R, CL-CH, CR2, R2, LST2).
scan_bnum1([C|R], CR1, CL-CH, R2, LST) :-
    C >= CL, C =< CH |
    LST = [C|LST2],
    scan_bnum(R, CR1, CL-CH, R2, LST2).
scan_bnum1(R, _, _, R2, LST) :-
    otherwise | R2 = R, LST = [].

base_crange(BASE, CR1, CR2) :-
    BASE =< 10 |
    CH1 is 0'0 + BASE,
    CR1 = 0'0-CH1,
    CR2 = xxx.
base_crange(BASE, CR1, CR2) :-
    BASE > 10 |
    CH2 is 0'a + BASE - 11,
    CR1 = 0'0-0'9,
    CR2 = 0'a-CH2.

var(C, R, L, O, E) :-
    scan_ident(R, R2, LST),
    list_to_string([C|LST], VAR),
    O = [var(L, VAR)|O2],
    lex(R2, L, O2, E).

punct(C, R, L, O, E) :-
    O = [punct(L, C)|O2], lex(R, L, O2, E).

op([0'=, 0'., 0'.|TL], L, O, E) :-
    % special case
    O = [op(L, '=..')|O2],
    lex(TL, L, O2, E).
op(TL, L, O, E) :-
    otherwise |
    scan_op(TL, R2, LST), op1(LST, L, R2, O, E).

op1([], L, [C|_], O, E) :-
    send(E, error(L, 'invalid character "~c"', [C], 1)), O = [].
op1(LST, L, R, O, E) :-
    otherwise |
    list_to_string(LST, OP),
    O = [op(L, OP)|O2],
    lex(R, L, O2, E).

scan_op([], R, Lst) :-
    R = [], Lst = [].
scan_op([C|R], R2, LST) :-
    op_char(C, F),
    scan_op2(F, C, R, R2, LST).

scan_op2(true, C, R, R2, LST) :-
    LST = [C|LST2],
    scan_op(R, R2, LST2).
scan_op2(false, C, R, R2, LST) :-
    otherwise | R2 = [C|R], LST = [].

op_char(0'-, R) :- R = true.
op_char(0'^, R) :- R = true.
op_char(0'&, R) :- R = true.
op_char(0'$, R) :- R = true.
op_char(0'/, R) :- R = true.
op_char(0'\\, R) :- R = true.
op_char(0'!, R) :- R = true.
op_char(0'~, R) :- R = true.
op_char(C, R) :- otherwise | op_char2(C, R).

op_char2(C, R) :- C >= 0'*, C =< 0'+ | R = true.
op_char2(C, R) :- C >= 0':, C =< 0'@ | R = true.
op_char2(_, R) :- otherwise | R = false.

str(R, L, O, E) :-
    scan_str(R, 0'\', L, R2, LST, E),
    list_to_string(LST, STR),
    O = [id(L, STR)|O2],
    lex(R2, L, O2, E).

charlist(R, L, O, E) :-
    scan_str(R, 0'\", L, R2, LST, E),
    build_string(L, LST, S),
    O = [S|O2],
    lex(R2, L, O2, E).

special([0'"|R], L, O, E) :-
    scan_str(R, 0'\", L, R2, LST, E),
    length(LST, N),
    array:make(char, N, A),
    utf_encode(LST, Enc),
    array:put(0, Enc, A) &
    O = [buf(L, A)|O2],
    lex(R2, L, O2, E).
special([0'{|R], L, O, E) :-
    O = [punct(L, 0'#), punct(L, 0'{)|O2],
    lex(R, L, O2, E).
special([0'$|R], L, O, E) :-
    array:hex_to_binary(kl1string, A, R, R2),
    O = [buf(L, A)|O2],
    lex(R2, L, O2, E).
special(R, L, O, E) :-
    otherwise |
    array:hex_to_binary(A, R, R2),
    O = [buf(L, A)|O2],
    lex(R2, L, O2, E).

scan_str([], _, L, R, _, E) :-
    R = [],
    send(E, error(L, 'premature end of string or character list', [], 1)).
scan_str([0'\\, 0'x, X1, X2|R], DELIM, L, R2, LST, E) :-
    list_to_integer([X1, X2], 16, N),
    (N =\= error ->
        LST = [N|LST2],
        scan_str(R, DELIM, L, R2, LST2, E)
    ;
        R2 = [],
        send(E, error(L, 'invalid "\\x.." escape sequence', [], 1))
    ).
scan_str([0'\\, 0'u, X1, X2, X3, X4|R], DELIM, L, R2, LST, E) :-
    list_to_integer([X1, X2, X3, X4], 16, N),
    (N =\= error ->
        LST = [N|LST2],
        scan_str(R, DELIM, L, R2, LST2, E)
    ;
        R2 = [],
        send(E, error(L, 'invalid "\\u...." escape sequence', [], 1))
    ).
scan_str([0'\\, C|R], DELIM, L, R2, LST, E) :-
    C =\= 0'x, C =\= 0'u |
    esc(C, CO),
    LST = [CO|LST2],
    scan_str(R, DELIM, L, R2, LST2, E).
scan_str([DELIM|R], DELIM, _, R2, LST, _) :-
    otherwise | R2 = R, LST = [].
scan_str([C|R], DELIM, L, R2, LST, E) :-
    otherwise, C =\= DELIM, C =\= 0'\\ |
    LST = [C|LST2],
    scan_str(R, DELIM, L, R2, LST2, E).

esc([0'\\, EC|R], R2, CO) :-
    esc(EC, CO),
    R2 = R.
esc([C|R], R2, CO) :-
    otherwise | R2 = R, CO = C.

esc(0'n, CO) :- CO = 10.
esc(0'r, CO) :- CO = 13.
esc(0't, CO) :- CO = 9.
esc(0'e, CO) :- CO = 27.
esc(C, CO) :- otherwise | CO = C.
