# FLENG - asm part of aarch64 runtime

    .section .text

    .global fl_check_atom
    // x0=val
fl_check_atom:
    and x8, x0, #3
    cmp x8, #2
    bne l1
    ret
l1:
    mov x1, x0
    mov x0, x19
    mov x2, #1          // FL_NOT_AN_ATOM
    b fl_rt_error

    .global fl_check_cell
    // x0=val
fl_check_cell:
    tst x0, #3
    bne l2
    ret
l2:
    mov x1, x0
    mov x0, x19
    mov x2, #2          // FL_NOT_A_CELL
    b fl_rt_error

    .global fl_check_integer
    // x0=val
fl_check_integer:
    tst x0, #1         // FL_INT_BIT
    beq l3
    ret
l3:
    mov x1, x0
    mov x0, x19
    mov x2, #3          // FL_NOT_AN_INT
    b fl_rt_error

    .global fl_check_list
    // x0=val
fl_check_list:
    tst x0, #3
    bne l4
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x01000000 // FL_LIST_TAG
    cmp w8, w9
    bne l4
    ret
l4:
    mov x1, x0
    mov x0, x19
    mov x2, #4          // FL_NOT_A_LIST
    b fl_rt_error

    .global fl_check_module
    // x0=val
fl_check_module:
    tst x0, #3
    bne l4b
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x0a000000 // FL_MODULE_TAG
    cmp w8, w9
    bne l4b
    ret
l4b:
    mov x1, x0
    mov x0, x19
    mov x2, #23          // FL_NOT_A_MDOULE
    b fl_rt_error

    .global fl_check_tuple
    // x0=val
fl_check_tuple:
    tst x0, #3
    bne l5
    ldr w8, [x0]
    mov w9, #0x80000000  // FL_TUPLE_BIT
    tst w8, w9
    bne l5
    ret
l5:
    mov x1, x0
    mov x0, x19
    mov x2, #5          // FL_NOT_A_TUPLE
    b fl_rt_error

    .global fl_check_var
    // x0=val
fl_check_var:
    tst x0, #3
    bne l6
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x04000000 // FL_VAR_TAG
    cmp w8, w9
    beq l6
    mov w9, #0x07000000 // FL_REF_TAG
    cmp w8, w9
    beq l6
    mov x1, x0
    mov x0, x19
    mov x2, #6          // FL_NOT_A_VAR
    b fl_rt_error
l6:
    ret

    .global fl_check_number
    // x0=val
fl_check_number:
    tst x0, #1
    beq l7
    ret
l7:
    tst x0, #2
    bne l8
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x02000000 // FL_FLOAT_TAG
    cmp w8, w9
    bne l8
    ret
l8:
    mov x1, x0
    mov x0, x19
    mov x2, #11          // FL_NOT_A_NUMBER
    b fl_rt_error

    // x1=val -> d0
float_arg_1:
    tst x1, #1
    beq l9
    asr x1, x1, #1
    scvtf d0, x1
    ret
l9:
    ldr d0, [x1, #8]
    ret

    // x2=val -> d1
float_arg_2:
    tst x2, #1
    beq l10
    asr x2, x2, #1
    scvtf d1, x2
    ret
l10:
    ldr d1, [x2, #8]
    ret

    .global fl_add
    // x1=num, x2=num -> x0
fl_add:
    tst x1, #1
    beq l11
    tst x2, #1
    beq l11
    sub x0, x1, #1
    add x0, x0, x2
    ret
l11:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    fadd d0, d0, d1
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_sub
    // x1=num, x2=num -> x0
fl_sub:
    tst x1, #1
    beq l12
    tst x2, #1
    beq l12
    sub x1, x1, x2
    add x0, x1, #1
    ret
l12:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    fsub d0, d0, d1
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_mul
    // x1=num, x2=num -> x0
fl_mul:
    tst x1, #1
    beq l13
    tst x2, #1
    beq l13
    asr x1, x1, #1
    asr x2, x2, #1
    mul x0, x1, x2
    lsl x0, x0, #1
    orr x0, x0, #1
    ret
l13:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    fmul d0, d0, d1
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_div
    // x1=num, x2=num -> x0
fl_div:
    cmp x2, #1
    bne l14
    mov x0, x19
    mov x2, #12         // FL_DIV_BY_ZERO
    b fl_rt_error
l14:
    tst x1, #1
    beq l15
    tst x2, #1
    beq l15
    asr x1, x1, #1
    asr x2, x2, #1
    sdiv x0, x1, x2
    lsl x0, x0, #1
    orr x0, x0, #1
    ret
l15:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    fdiv d0, d0, d1
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_mod
    // x1=int, x2=int -> x0
fl_mod:
    cmp x2, #1
    bne l14b
    mov x0, x19
    mov x2, #12         // FL_DIV_BY_ZERO
    b fl_rt_error
l14b:
    asr x1, x1, #1
    asr x2, x2, #1
    sdiv x0, x1, x2
    msub x0, x2, x0, x1
    lsl x0, x0, #1
    orr x0, x0, #1
    ret

    .global fl_pow
    // x1=num, x2=num -> x0
fl_pow:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    bl pow
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_sqrt
    // x1=num -> x0
fl_sqrt:
    str x30, [x28, #-8]!
    bl float_arg_1
    fsqrt d0, d0
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_sin
    // x1=num -> x0
fl_sin:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl sin
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_cos
    // x1=num -> x0
fl_cos:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl cos
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_tan
    // x1=num -> x0
fl_tan:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl tan
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_asin
    // x1=num -> x0
fl_asin:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl asin
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_acos
    // x1=num -> x0
fl_acos:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl acos
    ldr x30, [x28], #8
    b fl_mkfloat

   .global fl_atan
    // x1=num -> x0
fl_atan:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl atan
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_loge
    // x1=num -> x0
fl_loge:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl log
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_exp
    // x1=num -> x0
fl_exp:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl exp
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_truncate
    // x1=num -> x0
fl_truncate:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl trunc
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_round
    // x1=num -> x0
fl_round:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl round
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_floor
    // x1=num -> x0
fl_floor:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl floor
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_ceiling
    // x1=num -> x0
fl_ceiling:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl ceil
    ldr x30, [x28], #8
    b fl_mkfloat

    .global fl_shl
    // x1=int, x2=int -> x0
fl_shl:
    lsr x2, x2, #1
    bic x1, x1, #1
    lsl x0, x1, x2
    orr x0, x0, #1
    ret

    .global fl_shr
    // x1=int, x2=int -> x0
fl_shr:
    lsr x2, x2, #1
    asr x0, x1, x2
    orr x0, x0, #1
    ret

    .global fl_int
    // x1=num -> x0
fl_int:
    tst x1, #1
    beq l16
    mov x0, x1
    ret
l16:
    ldr d0, [x1, #8]
    fcvtzs x0, d0
    lsl x0, x0, #1
    orr x0, x0, #1
    ret

    .global fl_float
    // x1=num -> x0
fl_float:
    asr x1, x1, #1
    scvtf d0, x1
    b fl_mkfloat

true:
    adrp x0, fl_true
    add x0, x0, :lo12:fl_true
    ldr x0, [x0]
    ret

false:
    adrp x0, fl_false
    add x0, x0, :lo12:fl_false
    ldr x0, [x0]
    ret

    .global fl_sametype
    // x1=val, x2=val -> x0
fl_sametype:
    and x0, x1, #3
    and x8, x2, #3
    cmp x0, x8
    bne false
    tst x0, #3
    bne true
    ldr w0, [x1]
    ldr w8, [x2]
    mov w9, #0x80000000    // FL_TUPLE_BIT
    tst w0, w9
    beq l19
    tst w8, w9
    bne false
    b true
l19:
    tst w8, w9
    bne false
    mov w9, #0xff000000     // FL_TAG_MASK
    and w0, w0, w9
    and w8, w8, w9
    cmp w0, w8
    bne false
    b true

    .global fl_eq
    // x1=val, x2=val -> x0
    // must preserve P0-P3
fl_eq:
    cmp x1, x2
    beq true
    b false

    .global fl_equal
    // x1=val, x2=val -> x0
    // must preserve P0-P3
fl_equal:
    cmp x1, x2
    beq true
    tst x1, #1
    beq l22
    tst x2, #1
    beq l23
    b false
l22:
    tst x2, #2
    bne false
    ldr w8, [x1]
    mov w9, #0xff000000     // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x02000000     // FL_FLOAT_TAG
    cmp w8, w9
    bne false
l23:
    tst x2, #2
    bne false
    tst x2, #1
    bne l25
    ldr w8, [x2]
    mov w9, #0xff000000     // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x02000000     // FL_FLOAT_TAG
    cmp w8, w9
    bne false
l25:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    ldr x30, [x28], #8
    fcmp d0, d1
    bne false
    b true

    .global fl_pf_equal
    // principal functor equality
    // x1=val, x2=val -> x0
    // must preserve P0-P3
fl_pf_equal:
    cmp x1, x2
    beq true
    tst x1, #3
    bne false
    tst x2, #3
    bne false
    ldr w8, [x1]
    mov w5, #0xff000000		// FL_TAG_MASK
    and w8, w8, w5
    ldr w9, [x2]
    and w9, w9, w5
    cmp w8, w9
    bne false
    mov w5, #0x80000000		// FL_TUPLE_BIT
    tst w8, w5
    beq true
    ldr x8, [x1, #8]
    ldr x9, [x2, #8]
    cmp x8, x9
    bne false
    b true

    .global fl_less
    // x1=val, x2=val -> x0
    // must preserve P0-P3
fl_less:
    tst x1, #1
    beq l26
    tst x2, #1
    beq l26
    cmp x1, x2
    bge false
    b true
l26:
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    ldr x30, [x28], #8
    fcmp d0, d1
    bmi true
    b false

first:
    mov x0, x1
    ret

second:
    mov x0, x2
    ret

    .global fl_max
    // x1=val, x2=val -> x0
    // must preserve P0-P3
fl_max:
    tst x1, #1
    beq l26a
    tst x2, #1
    beq l26a
    cmp x1, x2
    ble second
    b first
l26a:
    stp x1, x2, [x28, #-16]!
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    ldr x30, [x28], #8
    ldp x1, x2, [x28], #16
    fcmp d0, d1
    bmi second
    b first

    .global fl_min
    // x1=val, x2=val -> x0
    // must preserve P0-P3
fl_min:
    tst x1, #1
    beq l26i
    tst x2, #1
    beq l26i
    cmp x1, x2
    ble first
    b second
l26i:
    stp x1, x2, [x28, #-16]!
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    ldr x30, [x28], #8
    ldp x1, x2, [x28], #16
    fcmp d0, d1
    bmi first
    b second

    .global fl_sign
    // x1=val -> x0
fl_sign:
    tst x1, #1
    bne lsign1
    str x30, [x28, #-8]!
    bl float_arg_1
    ldr x30, [x28], #8
    adrp x0, zero
    add x0, x0, :lo12:zero
    ldr d1, [x0]
    fcmp d0, d1
    beq lsign3
    ble lsign2
lsign4:
    mov x0, #3
    ret
lsign2:
    mov x0, #-1
    ret
lsign3:
    mov x0, #1
    ret
lsign1:
    cmp x0, #1
    bmi lsign2
    bne lsign4
    mov x0, x1
    ret
    .data
zero: .double 0
    .text

    .global fl_abs
    // x1=val -> x0
fl_abs:
    tst x1, #1
    bne labs1
    str x30, [x28, #-8]!
    bl float_arg_1
    ldr x30, [x28], #8
    fabs d0, d0
    b fl_mkfloat
labs1:
    cmp x1, #0
    bmi labs2
    mov x0, x1
    ret
labs2:
    neg x0, x1
    add x0, x0, #2
    ret

    .global fl_rnd
    // x1=val -> x0
fl_rnd:
    mov x0, x1
    b fl_random_int

    .global fl_fip
    // x1=val -> x0
fl_fip:
    tst x1, #1
    beq lfip1
    mov x0, x1
    ret
lfip1:
    ldr x0, [x1, #8]
    lsr x0, x0, #52
    and x0, x0, #0x7ff
    sub x0, x0, #0x3ff
    cmp x0, #52
    blt lfip2
    mov x0, x1      // nan
    ret
lfip2:
    cmp x0, #0
    bge lfip4
    mov x0, #1
    ret
lfip4:
    mov x8, #-1
    lsr x8, x8, #12
    lsr x8, x8, x0
    ldr x0, [x1, #8]
    and x8, x0, x8
    cmp x8, #0
    bne lfip3
    mov x0, x1
    ret
lfip3:
    mvn x8, x8
    and x0, x8, x0
    fmov d0, x0
    b fl_mkfloat

    .global fl_ffp
    // x1=val -> x0
fl_ffp:
    tst x1, #1
    beq lffp1
    mov x0, #1
    ret
lffp1:
    ldr x0, [x1, #8]
    lsr x0, x0, #52
    and x0, x0, #0x7ff
    sub x0, x0, #0x3ff
    cmp x0, #52
    blt lffp2
    cmp x0, #0x400
    bne lffp3
    ldr x0, [x1, #8]
    lsl x0, x0, #12
    cmp x0, #0
    beq lffp3
    mov x0, x1      // nan
    ret
lffp3:
    mov x0, #1
    ret
lffp2:
    cmp x0, #0
    bge lffp4
    mov x0, x1
    ret
lffp4:
    mov x8, #-1
    lsr x8, x8, #12
    lsr x8, x8, x0
    ldr x0, [x1, #8]
    and x8, x8, x0
    cmp x8, #0
    beq lffp3
    mvn x8, x8
    and x0, x0, x8
    fmov d1, x0
    ldr d0, [x1, #8]
    fsub d0, d0, d1
    b fl_mkfloat

    .global fl_iless
    // x1=val, x2=val -> x0
fl_iless:
    cmp x1, x2
    blt true
    b false

    .global fl_fless
    // x1=val, x2=val -> x0
fl_fless:
    stp x1, x2, [x28, #-16]!
    str x30, [x28, #-8]!
    bl float_arg_1
    bl float_arg_2
    ldr x30, [x28], #8
    ldp x1, x2, [x28], #16
    fcmp d0, d1
    bmi true
    b false

    .global fl_below
    // x1=val, x2=val -> x0
fl_below:
    sub x28, x28, #40
    str x1, [x28, #8]
    str x2, [x28, #16]
    str x3, [x28, #24]
    str x30, [x28, #32]
    mov x0, x19
    mov x3, x28
    bl fl_ordering
    ldr x30, [x28, #32]
    ldr x3, [x28, #24]
    ldr x2, [x28, #16]
    ldr x1, [x28, #8]
    ldr x8, [x28]
    add x28, x28, #40
    tst x8, #1
    beq lbel1
    adrp x0, fl_nil
    add x0, x0, :lo12:fl_nil
    ldr x0, [x0]
    ret
lbel1:
    tst x0, #2     // -1
    bne true
    b false

    .global fl_below_or_equal
    // x1=val, x2=val -> x0
fl_below_or_equal:
    sub x28, x28, #40
    str x1, [x28, #8]
    str x2, [x28, #16]
    str x3, [x28, #24]
    str x30, [x28, #32]
    mov x0, x19
    mov x3, x28
    bl fl_ordering
    ldr x30, [x28, #32]
    ldr x3, [x28, #24]
    ldr x2, [x28, #16]
    ldr x1, [x28, #8]
    ldr x8, [x28]
    add x28, x28, #40
    tst x8, #1
    beq lbeleq1
    adrp x0, fl_nil
    add x0, x0, :lo12:fl_nil
    ldr x0, [x0]
    ret
lbeleq1:
    cmp x0, #1
    beq false
    b true

    .global fl_above
    // x1=val, x2=val -> x0
fl_above:
    sub x28, x28, #40
    str x1, [x28, #8]
    str x2, [x28, #16]
    str x3, [x28, #24]
    str x30, [x28, #32]
    mov x0, x19
    mov x3, x28
    bl fl_ordering
    ldr x30, [x28, #32]
    ldr x3, [x28, #24]
    ldr x2, [x28, #16]
    ldr x1, [x28, #8]
    ldr x8, [x28]
    add x28, x28, #40
    tst x8, #1
    beq labo1
    adrp x0, fl_nil
    add x0, x0, :lo12:fl_nil
    ldr x0, [x0]
    ret
labo1:
    cmp x0, #1
    bne false
    b true

    .global fl_above_or_equal
    // x1=val, x2=val -> x0
fl_above_or_equal:
    sub x28, x28, #40
    str x1, [x28, #8]
    str x2, [x28, #16]
    str x3, [x28, #24]
    str x30, [x28, #32]
    mov x0, x19
    mov x3, x28
    bl fl_ordering
    ldr x30, [x28, #32]
    ldr x3, [x28, #24]
    ldr x2, [x28, #16]
    ldr x1, [x28, #8]
    ldr x8, [x28]
    add x28, x28, #40
    tst x8, #1
    beq laboeq1
    adrp x0, fl_nil
    add x0, x0, :lo12:fl_nil
    ldr x0, [x0]
    ret
laboeq1:
    cmp x0, #-1
    beq false
    b true

    .global fl_deref
    // x0=val -> x0
    // must preserve P0-P3
fl_deref:
    tst x0, #3
    beq l27
l28:
    ret
l27:
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x04000000 // FL_VAR_TAG
    cmp w8, w9
    bne l28
    ldr x8, [x0, #8]
    cmp x0, x8
    beq l28
    mov x0, x8
    b fl_deref

    .global fl_unbox
    // x0=val -> x0
fl_unbox:
    tst x0, #3
    beq lu27
lu28:
    ret
lu27:
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x04000000 // FL_VAR_TAG
    cmp w8, w9
    bne lu29
    ldr x8, [x0, #8]
    cmp x0, x8
    beq l28
    mov x0, x8
    b fl_unbox
lu29:
    mov x1, x0
    mov x0, x19
    b fl_unbox_cell

    .global fl_box
    // x1=val -> A
fl_box:
    tst x1, #1
    bne box_l1
    ret
box_l1:
    mov x2, x30          // save return address
    adrp x0, fl_nil
    add x0, x0, :lo12:fl_nil
    ldr x0, [x0]
    mov x8, x0
    bl cons
    mov w9, #0x0f000000     // FL_BOX_LONG_TAG
    str w9, [x0]
    asr x1, x1, #1
    str x1, [x0, #8]
    ret x2

    .global fl_unref
    // x8=val
    // must preserve A + P0-P3
fl_unref:
    tst x8, #3
    beq l29
    ret
l29:
    mov w10, #0x00ffffff      // FL_COUNT_MASK
    ldr w11, [x8]
    and w11, w11, w10
    cmp w11, w10
    bne l30
    ret
l30:
    ldr w11, [x8]
    sub w11, w11, #1
    tst w11, w10
    beq l30a
    str w11, [x8]
    ret
l30a:
    sub x28, x28, #40
    stp x1, x2, [x28]
    stp x3, x30, [x28, #16]
    str x0, [x28, #32]
    mov x0, x19
    mov x1, x8
    bl fl_release
    ldr x0, [x28, #32]
    ldp x3, x30, [x28, #16]
    ldp x1, x2, [x28]
    add x28, x28, #40
    ret

setcarry:
    mov x8, #1 << 29
    msr nzcv, x8
    ret

clrcarry:
    mrs x8, nzcv
    and x8, x8, #~(1 << 29)
    msr nzcv, x8
    ret

    .global fl_force
    // x0=val -> x0, CF is 1 when suspending
fl_force:
    tst x0, #3
    bne clrcarry
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x07000000 // FL_REF_TAG
    cmp w8, w9
    beq l33
    mov w9, #0x04000000 // FL_VAR_TAG
    cmp w8, w9
    bne clrcarry
    ldr x8, [x0, #8]
    cmp x0, x8
    beq l33
    mov x0, x8
    b fl_force
l33:
    ldr x8, [x19, #8]   // tcb->sstackp
    str x0, [x8]
    ldr x0, [x20]      // goal->addr
    str x0, [x8, #8]
    add x8, x8, #16
    str x8, [x19, #8]   // tcb->sstackp
    b setcarry

    .global fl_match
    // x1=val1, x2=val2 -> x0
    // assumes args are forced (allows initial quick eq check)
fl_match:
    cmp x1, x2
    beq true
    mov x0, x19
    sub x28, x28, #32
    stp x1, x30, [x28]
    str x2, [x28, #16]
    bl fl_match_rec
    ldr x2, [x28, #16]
    ldp x1, x30, [x28]
    add x28, x28, #32
    ret

    .global fl_islist_t
    // x0=val, set CF if not a list
fl_islist_t:
    tst x0, #3
    bne setcarry
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x01000000 // FL_LIST_TAG
    cmp w8, w9
    beq clrcarry
    b setcarry

    .global fl_istuple_t
    // x0=val, set CF if not a tuple
fl_istuple_t:
    tst x0, #3
    bne setcarry
    ldr w8, [x0]
    mov w9, #0x80000000 // FL_TUPLE_BIT
    tst w8, w9
    bne clrcarry
    b setcarry

    // x1=addr -> Carry (fail)
trynext:
    str x1, [x20]      // goal->addr
    b setcarry

    .global fl_isval
    // x0=val, x2=atom, x1=addr
fl_isval:
    cmp x0, x2
    bne trynext
    b clrcarry

    .global fl_isfloat
    // x0=val, d0=float, x1=addr
fl_isfloat:
    tst x0, #3
    bne trynext
    ldr d1, [x0, #8]
    fcmp d0, d1
    bne trynext
    b clrcarry

    .global fl_islist
    // x0=val, x1=addr -> x0(car)
    // push cdr on stack
fl_islist:
    str x30, [x28, #-8]!
    bl fl_islist_t
    ldr x30, [x28], #8
    bcs trynext
    ldr x8, [x0, #16]
    str x8, [x28, #-8]!
    ldr x0, [x0, #8]
    b clrcarry

   .global fl_isvector
    // x0=val, x1=addr, x2=len -> x0(car)
    // push vector on stack
fl_isvector:
    tst x0, #3
    bne trynext
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x03000000 // FL_VECTOR_TAG
    cmp w8, w9
    bne trynext
    ldr x8, [x0, #16]
    ldr x9, [x8, #8]
    asr x9, x9, #1
    cmp x9, x2
    bne trynext
    str x0, [x28, #-8]!
    b clrcarry

    .global fl_velement
    // x1=index(fixnum), stack=vector -> x0=item
fl_velement:
    str x30, [x28, #-8]!
    ldr x0, [x28, #8]
    ldr x0, [x0, #8]		// get map
    bl fl_deref
    adrp x5, fl_nil
    add x5, x5, :lo12:fl_nil
    ldr x5, [x5]
lie1:
    cmp x0, x5
    bne lie4
    ldr x0, [x28, #8]
    ldr x0, [x0, #16]
    ldr x0, [x0, #16]		// default
    ldr x30, [x28], #8
    ret
lie4:
    ldr x0, [x0, #16]		// get part of "t" tuple holding k, v
    ldr x0, [x0, #16]
    ldr x8, [x0, #8]		// key
    cmp x1, x8
    blt lie2
    bgt lie3
    ldr x0, [x0, #16]
    ldr x0, [x0, #8]
    ldr x30, [x28], #8
    ret
lie2:
    ldr x0, [x0, #16]		// left part
    ldr x0, [x0, #16]
    ldr x0, [x0, #8]
    bl fl_deref
    b lie1
lie3:
    ldr x0, [x0, #16]		// right part
    b lie2

   .global fl_isarrayval
    // x0=val, x1=addr, x2=literal_array -> carry (fail)
fl_isarrayval:
    tst x0, #3
    bne trynext
    ldr w8, [x0]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w8, w8, w9
    mov w9, #0x08000000 // FL_ARRAY_TAG
    cmp w8, w9
    bne trynext
    ldr x8, [x0, #8]		// CAR(A) = [ptr len/type]
    ldr x9, [x2, #8]		// CAR(P1) = [ptr len/type]
    ldr x3, [x8, #16]
    bic x3, x3, #8		// clear FL_ARRAY_IMMUTABLE
    ldr x4, [x9, #16]
    bic x4, x4, #8
    cmp x3, x4
    bne trynext
    lsr x3, x3, #4
    ldr x8, [x8, #8]
    ldr x9, [x9, #8]
liav1:
    cmp x3, #0
    beq clrcarry
    ldrb w4, [x8]
    ldrb w5, [x9]
    cmp w4, w5
    bne trynext
    add x8, x8, #1
    add x9, x9, #1
    sub x3, x3, #1
    b liav1

    .global fl_istuple
    // x0=val, x1=addr, x2=arity -> x0(head)
    // push arglist on stack
fl_istuple:
    tst x0, #3
    bne trynext
    ldr w8, [x0]
    mov w9, #0x80000000 // FL_TUPLE_BIT
    tst w8, w9
    beq trynext
    lsr w8, w8, #24
    and w8, w8, #0x7f
    cmp w8, w2
    bne trynext
    ldr x3, [x0, #16]
    str x3, [x28, #-8]!
    ldr x0, [x0, #8]
    b clrcarry

    // x8=car, x0=cdr -> x0
    // must preserve parameter registers
cons:
    ldr x9, [x19, #6 * 8]   // tcb->freelist
    cmp x9, #0
    bne l34
    sub x28, x28, #48
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    str x8, [x28, #32]
    str x0, [x28, #40]
    mov x0, x19
    bl fl_new_chunk
    ldr x0, [x28, #40]
    ldr x8, [x28, #32]
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #48
    ldr x9, [x19, #6 * 8]   // tcb->freelist
l34:
    str x30, [x28, #-8]!
    ldr x10, [x9, #16]
    str x10, [x19, #6 * 8]  // tcb->freelist
    bl fl_addref
    str x0, [x9, #16]
    mov x0, x8
    bl fl_addref
    str x0, [x9, #8]
    ldr x10, [x19, #11 * 8]  // tcb->used
    add x10, x10, #1
    str x10, [x19, #11 * 8]    // tcb->used
    mov x0, x9
    ldr x30, [x28], #8
    ret

    .global fl_mkfloat
    // d0=float64 -> x0
    // must preserve parameter registers
fl_mkfloat:
    str x30, [x28, #-8]!
    mov x8, #1
    mov x0, #1
    bl cons
    str d0, [x0, #8]
    mov x8, #0x02000000      // FL_FLOAT_TAG
    str w8, [x0]
    ldr x30, [x28], #8
    ret

    .global fl_mklist
    // x0=cdr, <stack>=car -> x0
fl_mklist:
    ldr x8, [x28]
    str x30, [x28]
    bl cons
    mov x8, #0x01000000      // FL_LIST_TAG
    str w8, [x0]
    ldr x30, [x28], #8
    ret

    .global fl_mktuple
    // x0=args, x8=arity, x9=atom -> x0
fl_mktuple:
    stp x8, x30, [x28, #-16]!
    mov x8, x9
    bl cons
    ldp x8, x30, [x28], #16
    // fall through ...

    .global fl_mktuple0
    // x0=args, x8=arity -> x0
fl_mktuple0:
    lsl x8, x8, #24
    mov w9, #0x80000000 // FL_TUPLE_BIT
    orr w8, w8, w9
    str w8, [x0]
    ret

    .global fl_tovector
    # x0=lst -> x0
fl_tovector:
    mov x8, #0x03000000       // FL_VECTOR_TAG
    str w8, [x0]
    ret

    .global fl_mkvar
    // -> x0
fl_mkvar:
    str x30, [x28, #-8]!
    mov x8, #1
    adrp x0, fl_nil
    add x0, x0, :lo12:fl_nil
    ldr x0, [x0]
    bl cons
    str x0, [x0, #8]
    mov x8, #0x04000000   // FL_VAR_TAG
    str w8, [x0]
    ldr x30, [x28], #8
    ret

    // x21=goal
enqueue:
    ldr x0, [x19, #13 * 8]     // tcb->qend
    str x21, [x0]
    add x0, x0, #8
    ldr x3, [x19, #12 * 8]     // tcb->qstart
    cmp x0, x3
    bne l40
    adrp x1, fl_nil
    add x1, x1, :lo12:fl_nil
    ldr x1, [x1]
    mov x2, #7                  // FL_QUEUE_FULL
    mov x0, x19
    b fl_rt_error
l40:
    ldr x8, [x19, #15 * 8]     // tcb->max_goals
    lsl x8, x8, #3
    ldr x9, [x19, #14 * 8]     // tcb->queue
    add x8, x8, x9
    cmp x0, x8
    blo l41
    ldr x0, [x19, #14 * 8]     // tcb->queue
l41:
    str x0, [x19, #13 * 8]     // tcb->qend
    ldr x8, [x19, #10 * 8]     // tcb->active
    add x8, x8, #1
    str x8, [x19, #10 * 8]    // tcb->active
    ret

    .global fl_opengoal
    // -> x21
fl_opengoal:
    ldr x21, [x19, #16 * 8]     // tcb->freegoals
    cmp x21, #0
    bne lg1
    mov x2, #26                  // FL_TOO_MANY_GOALS
    mov x0, x19
    b fl_rt_error
lg1:
    ldr x8, [x21]               // goal->addr
    ldr x0, [x20, #6 * 8]       // goal->task
    str x30, [x28, #-8]!
    bl fl_addref
    ldr x30, [x28], #8
    str x0, [x21, #6 * 8]       // goal->task
    str x8, [x19, #16 * 8]      // tcb->freegoals
    ldr x8, [x19, #21 * 8]    // tcb->goals
    add x8, x8, #1
    str x8, [x19, #21 * 8]    // tcb->goals
    b enqueue

    .global fl_dropgoal
fl_dropgoal:
    ldr x8, [x19]               // tcb->goal
    ldr x9, [x19, #16 * 8]     // tcb->freegoals
    str x9, [x8]               // goal->addr
    str x8, [x19, #16 * 8]      // tcb->freegoals
    ldr x9, [x19, #21 * 8]    // tcb->goals
    sub x9, x9, #1
    str x9, [x19, #21 * 8]    // tcb->goals
    mov x1, #0
    str x1, [x8, #5 * 8]     // goal->info
    adrp x1, fl_nil
    add x1, x1, :lo12:fl_nil
    ldr x1, [x1]
    ldr x0, [x8, #6 * 8]      // goal->task
    str x1, [x8, #6 * 8]      // goal->task
    mov x8, x0
    b fl_unref

    .global fl_addref
    // x0=val
    // may not change any registers
fl_addref:
    tst x0, #3
    beq l42
    ret
l42:
    stp x8, x9, [x28, #-16]!
    mov w9, #0x00ffffff     // FL_COUNT_MASK
    ldr w8, [x0]
    and w8, w8, w9
    cmp w8, w9
    beq l42a
    ldr w8, [x0]
    add w8, w8, #1
    str w8, [x0]
l42a:
    ldp x8, x9, [x28], #16
    ret

    .global fl_puttailarg
    // x0=list
fl_puttailarg:
    ldr x8, [x0, #16]
    tst x8, #3
    beq l43
    ldr x0, [x0, #8]
l43:
    str x30, [x28, #-8]!
    bl fl_addref
    str x0, [x21, #4 * 8]   // goal->args[ 3 ]
    ldr x30, [x28], #8
    ret

    .global fl_reify
    // replace cdr of pair with deref'd element
    // x0=pair
fl_reify:
    str x30, [x28, #-8]!
    bl fl_deref
    ldr x8, [x0, #16]
    stp x0, x8, [x28, #-16]!
    mov x0, x8
    bl fl_deref
    bl fl_addref
    ldp x1, x8, [x28], #16
    bl fl_unref
    str x0, [x1, #16]
    ldr x30, [x28], #8
    ret

    .global fl_resolve
    // x4=cache, x1=arity, x2=module, x3=name
fl_resolve:
    ldr x8, [x4]
    cmp x8, #0
    beq l44
    str x8, [x21]        // goal->addr
    ret
l44:
    mov x0, x19
    mov x5, x21
    b fl_resolve_pdef

    .global fl_replace
    // x0=val, x2=argaddr
fl_replace:
    ldr x8, [x2]
    cmp x0, x8
    bne l45
    ret
l45:
    ldr x8, [x2]
    str x30, [x28, #-8]!
    bl fl_addref
    ldr x30, [x28], #8
    str x0, [x2]
    b fl_unref

    .global fl_tailgoal
    // x2=addr -> x21(N)
fl_tailgoal:
    ldr x3, [x19, #16] // tcb->tailcalls
    ldr x1, [x19, #24] // tcb->timeslice
    cmp x3, x1
    blt l_tg_1
    stp x2, x30, [x28, #-16]!
    bl fl_opengoal
    ldp x2, x30, [x28], #16
    str x2, [x21]      // goal->addr
    ret
l_tg_1:
    mov x21, x20
    str x2, [x21]      // goal->addr
    ret

    .global fl_tail_jump
fl_tail_jump:
    ldr x3, [x19, #16] // tcb->tailcalls
    ldr x1, [x19, #24] // tcb->timeslice
    cmp x3, x1
    blt l46
    mov x0, #0
    str x0, [x19, #16] // tcb->tailcalls
    ret
l46:
    add x3, x3, #1
    str x3, [x19, #16] // tcb->tailcalls
    // fall through ...

    .global fl_invoke_jump
fl_invoke_jump:
    mov x28, x29
    mov x20, x21
    str x21, [x19]      // tcb->goal
    ldr x0, [x21]      // goal->addr
    br x0

    .global fl_test_integer
    // x1=val -> x0 (bool)
fl_test_integer:
    tst x1, #1
    bne true
    b false

    .global fl_test_number
    // x1=val -> x0 (bool)
fl_test_number:
    tst x1, #1
    bne true
    mov w8, #0x02000000      // FL_FLOAT_TAG
    b fl_test_tag

    .global fl_test_atom
    // x1=val -> x0 (bool)
fl_test_atom:
    mov x0, x1
    and x0, x0, #3
    cmp x0, #2
    bne false
    b true

    .global fl_test_tag
    // x1=val, w8=tag -> x0 (bool)
fl_test_tag:
    tst x1, #3
    bne false
    ldr w0, [x1]
    mov w9, #0xff000000 // FL_TAG_MASK
    and w0, w0, w9
    cmp w0, w8
    bne false
    b true

    .global fl_test_var
    // x1=val -> x0 (bool)
fl_test_var:
    tst x1, #3
    bne false
    ldr w0, [x1]
    mov w8, #0xff000000 // FL_TAG_MASK
    and w0, w0, w8
    mov w8, #0x07000000 // FL_REF_TAG
    cmp w0, w8
    beq true
    mov w8, #0x04000000 // FL_VAR_TAG
    cmp w0, w8
    bne false
    ldr x0, [x1, #8]
    cmp x0, x1
    bne false
    b true

    .global fl_test_remote
    // x1=val -> x0 (bool)
fl_test_remote:
    tst x1, #3
    bne false
    ldr w0, [x1]
    mov w8, #0xff000000 // FL_TAG_MASK
    and w0, w0, w8
    mov w8, #0x07000000 // FL_REF_TAG
    cmp w0, w8
    beq true
    mov w8, #0x05000000 // FL_PORT_TAG
    cmp w0, w8
    bne false
    ldr x0, [x1, #8]
    tst x0, #1
    bne true
    b false

    .global fl_test_tuple
    // x1=val -> x0 (bool)
fl_test_tuple:
    tst x1, #3
    bne false
    ldr w0, [x1]
    mov w8, #0x80000000 // FL_TUPLE_BIT
    tst w0, w8
    beq false
    b true

    .global fl_test_struct
    // x1=val, x2=name, x3=arity -> x0 (bool)
fl_test_struct:
    tst x1, #3         // FL_BITS_MASK
    bne false
    ldr w0, [x1]
    lsr w5, w0, #24
    and w5, w5, #0xff
    lsr w3, w3, #1
    orr w3, w3, #0x80	 // FL_TUPLE_BIT
    cmp w5, w3
    bne false
    ldr x0, [x1, #8]
    cmp x0, x2
    bne false
    b true

    .global fl_test_array
    // x1=val -> x0 (bool)
fl_test_array:
    tst x1, #3         // FL_BITS_MASK
    bne false
    ldr w0, [x1]
    mov w8, #0xff000000     // FL_TAG_MASK
    and w0, w0, w8
    mov w8, #0x08000000     // FL_ARRAY_TAG
    cmp w0, w8
    bne false
    b true

    .global fl_test_kl1string
    // x1=val -> x0 (bool)
fl_test_kl1string:
    tst x1, #3         // FL_BITS_MASK
    bne false
    ldr w0, [x1]
    mov w8, #0xff000000     // FL_TAG_MASK
    and w0, w0, w8
    mov w8, #0x08000000     // FL_ARRAY_TAG
    cmp w0, w8
    bne false
    ldr x0, [x1, #8]
    ldr x0, [x0, #16]
    and x0, x0, #0x7
    cmp x0, #6			// FL_KL1_STRING
    bne false
    b true

    .global fl_test_vector
    // x1=val -> x0 (bool)
fl_test_vector:
    tst x1, #3         // FL_BITS_MASK
    bne false
    ldr w0, [x1]
    mov w8, #0xff000000     // FL_TAG_MASK
    and w0, w0, w8
    mov w8, #0x03000000     // FL_VECTOR_TAG
    cmp w0, w8
    bne false
    b true

    .global fl_enter
    // x0=tcb
fl_enter:
    mov x19, x0
    ldr x28, [x19, #5 * 8]  // tcb->stackbase
    ldr x0, [x19, #22*8]    // tcb->sp0
    mov sp, x0
    mov x29, x28
    ldr x20, [x19]      // tcb->goal
    ldr x0, [x20]      // goal->addr
    br x0

    .global fl_atomic
    // x1=val -> x0 (bool)
fl_atomic:
    tst x1, #3         // FL_BITS_MASK
    bne true
    ldr w0, [x1]
    mov w8, #0xff000000     // FL_TAG_MASK
    and w0, w0, w8
    mov w8, #0x02000000     // FL_FLOAT_TAG
    cmp w0, w8
    bne false
    b true

    .global fl_atomic_nonfloat
    // x1=val -> x0 (bool)
fl_atomic_nonfloat:
    tst x1, #3         // FL_BITS_MASK
    bne true
    b false

    .global fl_entry
    // x1=arity
fl_entry:
    adrp x8, fl_nil
    add x8, x8, :lo12:fl_nil
    ldr x8, [x8]
    ldr x0, [x20, #6 * 8]    // goal->task
    cmp x0, x8
    beq l50a
    ldr x0, [x0, #8]
    ldr x0, [x0, #8]
    ldr x0, [x0, #8]
    cmp x0, x8
    bne l50
l50a:
    ldr x0, [x19, #20 * 8]     // tcb->logging
    tst x0, #1
    bne l50
    ret
l50:
    mov x0, x19
    b fl_log_entry

    .global fl_assign0
    // x1=val, x2=val -> x0 (bool)
    // must preserve P0-P3
fl_assign0:
    sub x28, x28, #32
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    mov x0, x19
    bl fl_assign
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #32
    ret

    .global fl_d_assign0
    // x1=val, x2=val -> x0 (bool)
    // must preserve P0-P3
fl_d_assign0:
    sub x28, x28, #32
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    mov x0, x19
    bl fl_d_assign
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #32
    ret

    .global fl_unify
    // x1=val, x2=val -> x0 (bool)
    // must preserve P0-P3
fl_unify:
    sub x28, x28, #32
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    mov x0, x19
    bl fl_unify_rec
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #32
    ret

    .global fl_d_unify
    // x1=val, x2=val -> x0 (bool)
    // must preserve P0-P3
fl_d_unify:
    sub x28, x28, #32
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    mov x0, x19
    bl fl_d_unify_rec
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #32
    ret

    .global fl_unify_safe
    // x1=val, x2=val -> x0 (bool)
    // must preserve P0-P3
fl_unify_safe:
    sub x28, x28, #32
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    mov x0, x19
    bl fl_unify_safe1
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #32
    ret

    .global fl_unify_checked
    // x1=val, x2=val
    // must preserve P0-P3
fl_unify_checked:
    sub x28, x28, #32
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    mov x0, x19
    bl fl_unify_rec_checked
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #32
    ret

    .global fl_d_unify_checked
    // x1=val, x2=val
    // must preserve P0-P3
fl_d_unify_checked:
    sub x28, x28, #32
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    mov x0, x19
    bl fl_d_unify_rec_checked
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #32
    ret

    .global fl_lookup
    // A, X0: table [count, int, label, ..., label]
fl_lookup:
    mov x1, x8
    ldr x9, [x8], #8
llok1:
    ldr x5, [x8]
    cmp x0, x5
    beq llok2
    add x8, x8, #16
    subs x9, x9, #1
    bne llok1
    sub x8, x8, #8
llok2:
    ldr x8, [x8, #8]
    add x8, x1, x8
    br x8

    .global fl_goto
    // A, <SP>: table [min, max*, flabel, label1, ...]
    // max: mark bit is 0
fl_goto:
    mov x1, x8
    ldr x9, [x8]
    sub x0, x0, x9
    ldr x9, [x8, #8]
    cmp x0, x9
    bhi lgoto1
    add x0, x0, #6
    lsl x0, x0, #2
    add x0, x0, x1
    ldr x0, [x0]
    add x1, x0, x1
    br x1
lgoto1:
    ldr x0, [x8, #16]
    add x1, x0, x1
    br x1

    .global fl_prepare_c_arg
    // A: Arg
fl_prepare_c_arg:
    tst x0, #2      // FL_ATOM_BIT
    beq lprep1
    add x0, x0, #1
    ret
lprep1:
    ldr w8, [x0]
    and w8, w8, #0xff000000 // FL_TAG_MASK
    mov w9, #0x08000000 // FL_ARRAY_TAG
    cmp w8, w9
    bne lprep2
    ldr x0, [x0, #8]
    ldr x0, [x0, #8]
    ret
lprep2:
    add x0, x0, #8
    ret

    .global fl_new_task
    // G, A = status(var)
fl_new_task:
    mov x2, x30         // save return addr in P1
    mov x1, x0          // save status in P0
    adrp x8, fl_nil
    add x8, x8, :lo12:fl_nil
    ldr x8, [x8]
    ldr x0, [x20, #6*8]   // goal->task
    cmp x0, x8
    bne new_task_l1
    // cons new env/parent cell [[]|[]]]
    bl cons
    mov x9, #0x01000000   // FL_LIST_TAG
    str w9, [x0]
    mov x8, x0
    b new_task_l2
new_task_l1:
    ldr x0, [x0, #8]
    ldr x8, [x0, #8]            // env
new_task_l2:
    ldr x0, [x20, #6*8]            // goal->task (parent)
    bl cons
    mov x8, x0
    mov x0, x1
    bl cons                       // task: [env/parent|st]
    mov x9, #0x06000000        // FL_TASK_TAG
    orr x9, x9, #1
    str w9, [x0]
    ldr x8, [x20, #6*8]            // goal->task (old)
    bl fl_unref
    str x0, [x20, #6*8]            // goal->task
    ret x2

    .global fl_box_long_ref
    // A = box -> A
fl_box_long_ref:
    ldr w8, [x0]
    and w8, w8, #0xff000000 // FL_TAG_MASK
    mov w9, #0x08000000 // FL_ARRAY_TAG
    cmp w8, w9
    bne lblr1
    ldr x0, [x0, #8]
    ldr x0, [x0, #8]
lblr1:
    ldr x0, [x0, #8]
fix:
    lsl x0, x0, #1
    orr x0, x0, #1
    ret

    .global fl_box_int_ref
    // A = box -> A
fl_box_int_ref:
    ldr w8, [x0]
    and w8, w8, #0xff000000 // FL_TAG_MASK
    mov w9, #0x08000000 // FL_ARRAY_TAG
    cmp w8, w9
    bne lbir1
    ldr x0, [x0, #8]
    ldr x0, [x0, #8]
lbir1:
    mov x8, #0
    ldr w8, [x0, #8]
    mov x0, x8
    b fix

    .global fl_box_char_ref
    // A = box -> A
fl_box_char_ref:
    ldr w8, [x0]
    and w8, w8, #0xff000000 // FL_TAG_MASK
    mov w9, #0x08000000 // FL_ARRAY_TAG
    cmp w8, w9
    bne lbcr1
    ldr x0, [x0, #8]
    ldr x0, [x0, #8]
lbcr1:
    ldrb w0, [x0, #8]
    b fix

    .global fl_box_short_ref
    // A = box -> A
fl_box_short_ref:
    ldr w8, [x0]
    and w8, w8, #0xff000000 // FL_TAG_MASK
    mov w9, #0x08000000 // FL_ARRAY_TAG
    cmp w8, w9
    bne lbsr1
    ldr x0, [x0, #8]
    ldr x0, [x0, #8]
lbsr1:
    ldrsh x0, [x0, #8]
    b fix

    .global fl_box_double_ref
    // T, A = box -> A
fl_box_double_ref:
    ldr w8, [x0]
    and w8, w8, #0xff000000 // FL_TAG_MASK
    mov w9, #0x08000000 // FL_ARRAY_TAG
    cmp w8, w9
    bne lbdr1
    ldr x0, [x0, #8]
    ldr x0, [x0, #8]
lbdr1:
    ldr d0, [x0, #8]
    b fl_mkfloat

    .global fl_factset
    // x1=arity, x2=tree
    // must preserve P0-P3
fl_factset:
    sub x28, x28, #32
    str x1, [x28]
    str x2, [x28, #8]
    str x3, [x28, #16]
    str x30, [x28, #24]
    mov x0, x19
    bl fl_match_facts
    ldr x30, [x28, #24]
    ldr x3, [x28, #16]
    ldr x2, [x28, #8]
    ldr x1, [x28]
    add x28, x28, #32
    tst x0, #1
    beq setcarry
    b clrcarry

    .global fl_candidate
    // T -> set carry if pre-selecting fair choice candidate
fl_candidate:
    ldr x0, [x19, #24*8]		// tcb->candidates
    ldr x8, [x19, #25*8]		// tcb->selected
    tst x8, #1
    bne lcand1
    // case 1: the clause is a possible candidate (selected == 0)
    add x0, x0, #1			// ++tcb->candidates
    str x0, [x19, #24*8]
    b setcarry			// skip
lcand1:
    cmp x0, #0
    bne lcand2
    // case 2: the clause is the selected candidate (selected == 1, candidates == 0)
    str x0, [x19, #25*8]		// tcb->selected = 0
    b clrcarry				// execute candidates
lcand2:
    // case 3: the clause not the selected candidate (selected == 1, candidates > 0)
    ldr x9, [x19, #24*8]		// --tcb->candidates
    sub x9, x9, #1
    str x9, [x19, #24*8]
    b setcarry

    .global fl_select
    // T, X0=cell address
    // clobbers P0, P1
fl_select:
    ldr x2, [x19, #24*8]			// tcb->candidates
    cmp x2, #0
    bne lsel1
    ret						// no candidates, normal suspend
lsel1:
    mov x9, #1
    str x9, [x19, #25*8]			// tcb->selected = 1
    ldr x9, [x8]
    cmp x9, #0
    bne lsel2
    add x9, x9, #1
lsel2:
    lsr x5, x9, #8
    add x0, x9, x5
    udiv x1, x0, x2
    msub x0, x1, x2, x0
    ldr x10, =1664525			// LCG
    mul x9, x9, x10
    ldr x10, =1013904223
    add x9, x9, x10
    str x9, [x8]					// seed = (1664525 * seed) + 1013904223
    str x0, [x19, #24*8]			// tcb->candidates = tcb->candidates % <cell>
    ldr x0, [x20]				// goal->addr
    br x0						// reenter goal
