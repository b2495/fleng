%= "$rt" module: low-level primitives
%
% The definitions in this module are available directly, without using
% any module prefix and provide some higher-order operations for
% invoking dynamically computed goals ("call", "trace", "run") and
% for FLENG's built-in "compute" operation.
%
%+ $rt:call
% call(GOAL?)
% call(GOAL?, STATUS^)
% call(GOAL?, ENVIRONMENT?, STATUS^)
%     Invokes the process specified by GOAL which must be a tuple
%     or string. If GOAL is not a variable, then it may refer to a
%     primitive operation (a suitable user-defined wrapper clause is
%     synthesized by the compiler), but if it is a variable, then it
%     must refer to a user defined process.
%     GOAL may refer to a module-qualified term and must be exported
%     from the module in which it is defined.
%
%     "call/2" and "call/3" execute GOAL in a new group of processes
%     called a "task". Every process created in GOAL belongs to the
%     same task and once all processes in that group have terminated
%     (including child tasks), STATUS is unified with the empty list.
%     IF ENVIRONMENT is given, it is provided as a parameter associated
%     with the task group and can be accessed by the "environment/1"
%     primitive, otherwise the new task inherits the same environment
%     as the current task (if any).
%
%     Tasks may span multiple threads, a task is considered completed
%     when all processes of the task and it's child tasks in all threads
%     have terminated.
%
%+ $rt:call_detached
% call_detached(GOAL?, STATUS^)
%     Invokes GOAL in a new task, similar to "call/2", but does not
%     Link the new task to the current one. Executuion of said new
%     task will be independent and allow the current task to complete
%     without waiting for the new task.
%
%+ $rt:trace
% trace(GOAL?)
% trace(GOAL?, STATUS^)
%     Similar to "call/2", but enabled logging for the newly created
%     task to show entered predicates along with their arguments.
%     Tracing is inherited, so child tasks will be logged as well,
%     even in other threads. Note that tracing only produces logging
%     output for code that was compiled with the "-d" option.
%_
%     [PCN] Note that in PCN GOAL must be a callable entity, so
%     you should enclose the goal in ``...``.
%
%+ $rt:compute
% compute(OP?, ARG?, RESULT^)
% compute(OP?, ARG1?, ARG2?, RESULT^)
%     Performs a unary or binary numeric computation. OP must be a
%     string and may be one of the following:
%
%                 Unary      Binary   Result
%         +         -          x      Sum
%         -         -          x      Difference
%         *         -          x      Product
%         /         -          x      Quotient
%         mod       -          x      Remainder
%         and       -          x      Bitwise AND
%         or        -          x      Bitwise OR
%         xor       -          x      Bitwise XOR
%         shl       -          x      Shift bits left
%         shr       -          x      Arithmetic shift bits right
%         =         -          x      Equality comparison
%         >         -          x      Is ARG1 numerically greater than ARG2
%         <         -          x      Is ARG1 numerically less than ARG2
%         min       -          x      Return lesser of the two arguments
%         max       -          x      Return greater of the two arguments
%         sametype  -          x      Have ARG1 an ARG2 the same type
%         **        -          x      Exponentiation
%         sqrt      x          -      Square root
%         sin       x          -      Sine (radians)
%         cos       x          -      Cosine (radians)
%         tan       x          -      Tangent
%         asin      x          -      Arc sine
%         acos      x          -      Arc cosine
%         atan      x          -      Arc tangent
%         exp       x          -      Exponential value
%         abs       x          -      Absolute value
%         sign      x          -      Sign (returns integer)
%         rnd       x          -      Returns random integer
%         real_integer_part
%                   x          -      Extract integer part of float
%         real_fractional_part
%                   x          -      Extract fractional part of float
%         integer   x          -      Convert float to integer
%         real      x          -      Convert integer to float
%         truncate  x          -      Truncate float
%         floor     x          -      Round float to next lower integer
%         round     x          -      Round float
%         ceiling   x          -      Round float to next higher integer
%
%     +, -, * and / produce real results if one of the arguments is
%     a real number. "abs" returns a result of the same type as the argument.
%     "**" always produces a real result. "and", "or", "xor", "rnd",
%     "shl" and "shr" require integer arguments. With the exception of
%     "=", "sametype" the arguments must be bound to numbers.
%
%     Note that arguments to "compute" are not forced if unbound.
%     Note that integer over- and underflow is not detected and will
%     result in significant bits being silently truncated. "sametype"
%     will not force unbound arguments (the type of the argument is
%     considered "var" in that case).
%
%     See "is/2" for a more convenient way to perform arithmetic
%     computations.
%
%+ $rt:current_module
% current_module(MODULE^)
%     Assigns the module containing this expression to MODULE.
%
%+ $rt:run
% run(MODULE?, TERM?)
%     Invokes TERM similar to "call/1" in the module represented by MODULE,
%     which may be a module object or a name referring to a linked
%     module.

-module('$rt').

% dynamic call

-entry_point(rt_run/2, fl_run).
rt_run(M, Term) :-
    foreign_call(fl_get_module_ptr(M, Mod)),
    call1(Term, Mod).

-entry_point(rt_call/2, fl_call).
rt_call(M:Term, _) :-
    foreign_call(fl_find_module(M, Mod)),
    call1(Term, Mod).
rt_call(Term, Mod) :- call1(Term, Mod).

call1(!Term, Mod) :-
    '$atomic'(Term, Atm),
    call1(Atm, Term, Mod).

call1(true, Name, Mod) :-
    foreign_call(fl_find_pdef(Mod, Name, 0, PDef)),
    '$invoke'(PDef, []).
call1(false, Term, Mod) :-
    foreign_call(fl_uncons(Term, Name, Args)),
    foreign_call(fl_arity(Term, Arity)),
    call2(Mod, Name, Arity, Args).

call2(Mod, !Name, Arity, Args) :-
    foreign_call(fl_find_pdef(Mod, Name, Arity, PDef)),
    invoke(Args, PDef, Args).

invoke([], Pdef, Args) :- '$invoke'(Pdef, Args).
invoke([_|!T], Pdef, Args) :-
    '$reify',
    invoke(T, Pdef, Args).


% dynamic call (cached)

-entry_point(rtccall/2, fl_ccall).
rtccall(M:Term, Cache) :-
    foreign_call(fl_find_module(M, Mod)),
    ccall1(Term, Mod, Cache).

ccall1(!Term, Mod, Cache) :-
    '$atomic'(Term, Atm),
    ccall1(Atm, Term, Mod, Cache).

ccall1(true, Name, Mod, Cache) :-
    foreign_call(fl_find_pdef(Mod, Name, 0, PDef)),
    foreign_call(fl_set_cache(Cache, PDef)),
    '$invoke'(PDef, []).  % assumes PDef is bound (primitive goal ordering)
ccall1(false, Term, Mod, Cache) :-
    foreign_call(fl_uncons(Term, Name, Args)),
    foreign_call(fl_arity(Term, Arity)),
    ccall2(Mod, Name, Arity, Args, Cache).

ccall2(Mod, !Name, Arity, Args, Cache) :-
    foreign_call(fl_find_pdef(Mod, Name, Arity, PDef)),
    foreign_call(fl_set_cache(Cache, PDef)),
    invoke(Args, PDef, Args).


% call with newly created task

-entry_point(rt_call_task_with_env/4, fl_call_task_with_env).
rt_call_task_with_env(Goal, Mod, Env, Status) :-
    foreign_call(fl_new_task(Env, Status, Ok)),
    rttcall2(Ok, Goal, Mod).


% call with newly created task (inheriting environment)

-entry_point(rt_call_task/3, fl_call_task).
rt_call_task(Goal, Mod, Status) :-
   '$new_task'(Status),
    rt_call(Goal, Mod).

-entry_point(rt_trace_task/3, fl_trace_task).
rt_trace_task(Goal, Mod, Status) :-
    foreign_call(fl_new_traced_task(Status, Ok)),
    rttcall2(Ok, Goal, Mod).

-entry_point(rt_call_task_detached/3, fl_call_task_detached).
rt_call_task_detached(Goal, Mod, Status) :-
    foreign_call(fl_new_task_detached(Status, Ok)),
    rttcall2(Ok, Goal, Mod).

rttcall2([], Goal, Mod) :- rt_call(Goal, Mod).


% dynamic call with new task (remote)

-entry_point(rt_call_with_remote_task/3, fl_call_with_remote_task).
rt_call_with_remote_task(Task, T, M) :-
    foreign_call(fl_set_task(Task)),
    rt_call(T, M).


% remote call in other thread

-entry_point(rt_call_thread/3, fl_call_thread).
rt_call_thread(M:TERM, ID, _) :-
    foreign_call(fl_find_module(M, Mod)),
    rcall1(Mod, TERM, ID).
rt_call_thread(TERM, ID, MOD) :-
    rcall1(MOD, TERM, ID).

rcall1(Mod, Term, Id) :-
    resolve_peer(Id, Peer),
    rcall2(Peer, Term, Mod).

rcall2(!Peer, Term, Mod) :-
    foreign_call(fl_call_event(Peer, Term, Mod)).


% "jump"
-entry_point(jump/2, fl_jump).
jump(L, Args) :- invoke(Args, L, Args).


% peer resolution

resolve_peer(!ID, PEER) :-
    '$integer'(ID, Int),
    resolve_peer(ID, Int, PEER).

resolve_peer(ID, true, PEER) :-
    foreign_call(fl_threads(N)),
    compute(+, N, 1, N2),
    compute(>, ID, 0, F1),
    compute(<, ID, N2, F2),
    resolve_peer2(F1, F2, ID, PEER).
resolve_peer(node(!N), false, PEER) :-
    compute(+, N, 1, N2),
    resolve_peer(N2, true, PEER).
resolve_peer(all, false, PEER) :- '$assign'(-7, PEER).  % FL_ALL
resolve_peer(fwd, false, PEER) :- '$assign'(-1, PEER).  % FL_FWD
resolve_peer(bwd, false, PEER) :- '$assign'(-2, PEER).  % FL_BWD
resolve_peer(north, false, PEER) :- '$assign'(-3, PEER).  % FL_NORTH
resolve_peer(east, false, PEER) :- '$assign'(-4, PEER).  % FL_EAST
resolve_peer(south, false, PEER) :- '$assign'(-5, PEER).  % FL_SOUTH
resolve_peer(west, false, PEER) :- '$assign'(-6, PEER).  % FL_WEST
resolve_peer(random, false, PEER) :-
	foreign_call(fl_threads(N)),
	compute(rnd, N, T),
	compute(+, T, 1, T2),
	'$assign'(T2, PEER).
resolve_peer(unloaded, false, PEER) :- foreign_call(fl_unloaded(PEER)).
resolve_peer(ID, false, _) :-
	% force this clause to end, or indexing (case) will execute this before
	% the factset above (would be ambiguous)
	'$suspend', '$error'('invalid peer'(ID)).

resolve_peer2(true, true, ID, PEER) :-
    '$assign'(ID, PEER).
resolve_peer2(false, _, ID, _) :-
    '$error'('invalid peer'(ID)).
resolve_peer2(_, false, ID, _) :-
    '$error'('invalid peer'(ID)).

% computations

-entry_point(rtcompute/4, fl_compute).
rtcompute(+, X, Y, R) :- compute(+, X, Y, R).
rtcompute(-, X, Y, R) :- compute(-, X, Y, R).
rtcompute(*, X, Y, R) :- compute(*, X, Y, R).
rtcompute(/, X, Y, R) :- compute(/, X, Y, R).
rtcompute(mod, X, Y, R) :- compute(mod, X, Y, R).
rtcompute(and, X, Y, R) :- compute(and, X, Y, R).
rtcompute(or, X, Y, R) :- compute(or, X, Y, R).
rtcompute(xor, X, Y, R) :- compute(xor, X, Y, R).
rtcompute(shl, X, Y, R) :- compute(shl, X, Y, R).
rtcompute(shr, X, Y, R) :- compute(shr, X, Y, R).
rtcompute(=, X, Y, R) :- compute(=, X, Y, R).
rtcompute(<, X, Y, R) :- compute(<, X, Y, R).
rtcompute(>, X, Y, R) :- compute(<, Y, X, R).
rtcompute(max, X, Y, R) :- compute(max, X, Y, R).
rtcompute(min, X, Y, R) :- compute(min, X, Y, R).
rtcompute(sametype, X, Y, R) :- compute(sametype, X, Y, R).
rtcompute(**, X, Y, R) :- compute(**, X, Y, R).
rtcompute(Op, _, _, _) :- '$suspend', '$error'('invalid computation'(Op)).

-entry_point(rtcompute/3, fl_compute2).
rtcompute(sqrt, X, R) :- compute(sqrt, X, R).
rtcompute(integer, X, R) :- compute(integer, X, R).
rtcompute(real, X, R) :- compute(real, X, R).
rtcompute(sin, X, R) :- compute(sin, X, R).
rtcompute(cos, X, R) :- compute(cos, X, R).
rtcompute(asin, X, R) :- compute(asin, X, R).
rtcompute(acos, X, R) :- compute(acos, X, R).
rtcompute(tan, X, R) :- compute(tan, X, R).
rtcompute(atan, X, R) :- compute(atan, X, R).
rtcompute(log, X, R) :- compute(log, X, R).
rtcompute(exp, X, R) :- compute(exp, X, R).
rtcompute(sign, X, R) :- compute(sign, X, R).
rtcompute(rnd, X, R) :- compute(rnd, X, R).
rtcompute(real_fractional_part, X, R) :- compute(real_fractional_part, X, R).
rtcompute(real_integer_part, X, R) :- compute(real_integer_part, X, R).
rtcompute(abs, X, R) :- compute(abs, X, R).
rtcompute(floor, X, R) :- compute(floor, X, R).
rtcompute(truncate, X, R) :- compute(truncate, X, R).
rtcompute(round, X, R) :- compute(round, X, R).
rtcompute(ceiling, X, R) :- compute(ceiling, X, R).
rtcompute(Op, _, _) :- '$suspend', '$error'('invalid computation'(Op)).


% instantiate current module

-entry_point(rt_current_module/2, fl_current_module).
rt_current_module(!M, R) :- foreign_call(fl_mkmodule(M, R)).
