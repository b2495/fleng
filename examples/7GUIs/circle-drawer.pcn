/* Circle drawer
   https://eugenkiss.github.io/7guis/tasks/
*/

#define RADIUS  50
#define MAXRADIUS	300

main() {||
    ezd:init([], ezd, ok),
    open_port(events, ps),
    //follow(ps),
    bb:std_events(events, ezd, ezd2),
    ezd2 = [{"when", "*", "resize", ``fwd_event(events)``},
            {"when", "*", "mouse", ``fwd_event(events)``}|ezd3],
    if(data(ok)) resize(ps, 0, [], [], false, events, ezd3)
}

follow(s) {?
	s ?= [x|s2] -> { writeln(x), follow(s2) }
}

buttons(ev, undo, redo, :ezd) {||
    if(undo == []) umode = "disabled" else umode = "normal",
    if(redo == []) rmode = "disabled" else rmode = "normal",
    sdl:window_size(w, _),
    ux = w / 2 - 180, rx = w / 2 + 100,
    bb:button("undo", {ux, 50}, {80, 30}, "Undo", [], umode, ``fwd_event(ev, "undo")``, :ezd),
    bb:button("redo", {rx, 50}, {80, 30}, "Redo", [], rmode, ``fwd_event(ev, "redo")``, :ezd)
}

resize(s, n, undo, redo, mark, ev, ezd) {||
    sdl:window_size(w, h),
    ezd = [{"drawing", "ezd"},
      	     {"object", "canvas", [{"rectangle", 0, 100, w, h}]},
            {"when", "canvas", {"buttondown", 1}, ``fwd_event(ev)``}|ezd2],
    buttons(ev, undo, redo, ezd2, ezd3),
    loop(s, n, undo, redo, mark, ev, ezd3)
}

fwd_event(ev, event) {send(ev, event)}

find_circle(list, mx, my, mark) {?
    list ?= [{i, x, y, r}|list2] -> {||
        d = distance(x, y, mx, my),
        if(d <= r) mark = {i, x, y, r}
        else find_circle(list2, mx, my, mark)
    },
    default -> mark = false
}

function distance(x1, y1, x2, y2) {
    xd = x2 - x1,
    yd = y2 - y1,
    return(sqrt(xd * xd + yd * yd))
}

loop(s, n, undo, redo, mark, ev, ezd) {?
    s ?= [{"resize", _, _}|s2] -> resize(s2, n, undo, redo, mark, ev, ezd),
    s ?= [{"mouse", mx, my}|s2] -> {||
        find_circle(undo, mx, my, mark2),
        {?
            mark != mark2, mark2 ?= {i, x, y, r} -> {||
            	  unmark(mark, ev, ezd, ezd2),
                ezd2 = [{"object", i, [{"filled_polygon", x, y, 20, r, 0, "gray"}]}|ezd3],
                loop(s2, n, undo, redo, mark2, ev, ezd3)
            },
            default -> loop(s2, n, undo, redo, mark, ev, ezd)
        }
    },
    s ?= [{"buttondown", 1, x, y}|s2] -> {||
    	 unmark(mark, ev, ezd, ezd1),
        polygon(n, x, y, RADIUS, true, ev, ezd1, ezd2),
        undo2 = [{n, x, y, RADIUS}|undo],
        buttons(ev, undo2, [], ezd2, ezd3),
        loop(s2, n + 1, undo2, [], false, ev, ezd3)
    },
    s ?= [{"change", poly}|s2] -> {||
        question(poly, ev, ezd, ezd2),
        loop(s2, n, undo, redo, poly, ev, ezd2)
    },
    s ?= [{"adjust", "slider", val}|s2] -> {||
        adjusted(mark, mark2, val, ev, ezd, ezd2),
        loop(s2, n, undo, redo, mark2, ev, ezd2)
    },
    s ?= [{"confirmed", poly}|s2] -> {||
        adjust(poly, ev, ezd, ezd2),
        loop(s2, n, undo, redo, poly, ev, ezd2)
    },
    s ?= ["done"|s2] -> {||
        close_dialog(ev, ezd, ezd2),
        loop(s2, n, [mark|undo], redo, mark, ev, ezd2)
    },
    s ?= ["unconfirmed"|s2] -> {||
    	 close_dialog(ev, ezd, ezd2),
    	 loop(s2, n, undo, redo, mark, ev, ezd2)
    },
    s ?= ["undo"|s2], undo ?= [{i, x, y, r}|undo2] -> {||
    	 unmark(mark, ev, ezd, ezd1),
        ezd1 = [{"object", i, []}|ezd2],
        redo2 = [{i, x, y, r}|redo],
        restore(undo2, i, ev, ezd2, ezd3),
        buttons(ev, undo2, redo2, ezd3, ezd4),
        loop(s2, n, undo2, redo2, false, ev, ezd4)
    },
    s ?= ["redo"|s2], redo ?= [{i, x, y, r}|redo2] -> {||
    	 unmark(mark, ev, ezd, ezd1),
        polygon(i, x, y, r, false, ev, ezd1, ezd2),
        undo2 = [{i, x, y, r}|undo],
        buttons(ev, undo2, redo2, ezd2, ezd3),
        loop(s2, n, undo2, redo2, false, ev, ezd3)
    },
    s ?= [_|s2] -> loop(s2, n, undo, redo, mark, ev, ezd)
}

unmark(mark, ev, :ezd) {?
    mark ?= {i, x, y, r} -> polygon(i, x, y, r, false, ev, :ezd)
}

close_dialog(ev, :ezd) {||
	ezd ++ {"delete_view", "d"} ++ {"drawing", "ezd"}
		++ {"when", "*", "mouse", ``fwd_event(ev)``}
    		++ {"when", "canvas", {"buttondown", 1}, ``fwd_event(ev)``}
}

restore(list, i, ev, :ezd) {?
    list ?= [{j, x, y, r}|_], i == j ->
        polygon(i, x, y, r, false, ev, :ezd),
    default ->
        if(list ?= [_|l2]) restore(l2, i, ev, :ezd)
}

polygon(i, x, y, r, selected, ev, :ezd) {
    if(selected) ezd ++ {"object", i, [{"filled_polygon", x, y, 20, r, 0, "gray"}]}
    else ezd ++ {"object", i, [{"polygon", x, y, 20, r, 0, "black"}]};
    ezd ++ {"when", i, {"buttondown", 3}, ``change({i, x, y, r}, ev)``}
}

change(poly, ev, _) {send(ev, {"change", poly})}

question(poly, ev, :ezd) {||
    sdl:window_size(w, h),
    dx = w / 4,
    dy = h / 4,
    dw = w / 2,
    ezd ++ {"when", "*", "mouse", []}
    	++ {"when", "canvas", {"buttondown", 1}, []},
    bb:dialog("d", {dx, dy}, {dw, dw}, :ezd),
    tx = dw / 2,
    ty = dw / 3,
    ezd ++ {"object", "title", [{"text", tx, ty, "Adjust diameter?"}]},
    yx = dw / 4,
    nx = (dw / 4) * 3,
    by = (dw / 3) * 2,
    bb:button("yes", {yx, by}, {40, 20}, "Yes", ``answer({"confirmed", poly}, ev)``, :ezd),
    bb:button("no", {nx, by}, {40, 20}, "No", ``answer("unconfirmed", ev)``, :ezd)
}

answer(a, ev) {send(ev, a)}

adjust(poly, ev, :ezd) {||
    let poly ?= {_, _, _, r};
    sdl:window_size(w, h),
    dx = w / 4,
    dy = h / 2,
    dw = w / 2,
    dh = h / 4,
    ezd ++ {"delete_view", "d"} ++ {"drawing", "ezd"},
    bb:dialog("d", {dx, dy}, {dw, dh}, :ezd),
    tx = dw / 2,
    ty = dh / 3,
    ezd ++ {"object", "title", [{"text", tx, ty, "Adjusting diameter"}]},
    by = (dh / 3) * 2,
    bb:button("done", {tx, by}, {40, 20}, "Done", ``answer("done", ev)``, :ezd),
    slider(r, ev, :ezd)
}

slider(val, ev, :ezd) {||
    sdl:window_size(w, h),
    dw = w / 2,
    dh = h / 4,
    sx = dw / 4,
    sy = dh / 2,
    sw = dw / 3 * 2,
    bb:slider("slider", {sx, sy}, {sw, 20}, real(val) / MAXRADIUS, ev, :ezd)
}

adjusted(poly1, poly, val, ev, :ezd) {||
    let poly1 ?= {i, x, y, _};
    r2 = val * MAXRADIUS,
    ezd ++ {"drawing", "ezd"},
    polygon(i, x, y, r2, true, ev, :ezd),
    poly = {i, x, y, r2},
    ezd ++ {"drawing", "d"},
    slider(r2, ev, :ezd)
}
