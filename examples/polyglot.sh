#ifdef ___XXX___
# sh(1) part

# An X11 Hello-world example as a "polyglot", written in 3 languages
# Compile and run by entering:
#
#   sh polyglot.sh
#
# (Click window to exit).

set -e
$(fleng -cc) -I/usr/X11R6/include -c -x c $(fleng -cflags) $0 -o polyglot1.o
fleng $0 polyglot1.o -o polyglot -lX11 -L/usr/X11R6/lib
rm -f polyglot1.o
exec ./polyglot

#elif defined(PCN)
/* PCN part */

main() 
int fd;
{
    init(fd);
    wait_for_events(fd)
}

wait_for_events(fd) 
int clicked;
{
    handle_events("hello, world!", clicked),
    if(clicked == 1) halt()
    else {||
        listen(fd, event),
        if(data(event)) wait_for_events(fd)
    }
}

#else

/* C part */

/* adapted from: 
   https://gist.github.com/whosaysni/5733660 
*/
#include <stdio.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

static Display *display;
static Window window;
static GC gc;

void init(int *fd)
{
  XSizeHints hint;  
  int screen;
  unsigned long myforeground, mybackground;
  display = XOpenDisplay("");
  screen = DefaultScreen(display);
  myforeground = BlackPixel(display, screen);
  mybackground = WhitePixel(display, screen);
  hint.x = 200;
  hint.y = 300;
  hint.width = 350;
  hint.height = 250;
  hint.flags = PPosition|PSize;
  window = XCreateSimpleWindow(display, DefaultRootWindow(display),
                               hint.x, hint.y,
                               hint.width, hint.height,
                               5, myforeground, mybackground);
  gc = XCreateGC(display, window, 0, 0);
  XSetBackground(display, gc, mybackground);
  XSetForeground(display, gc, myforeground);
  XSelectInput(display, window, ButtonPressMask|KeyPressMask|ExposureMask);
  XMapRaised(display, window);
  *fd = XConnectionNumber(display);
}
 
void handle_events(char *msg, long *clicked)
{
    XEvent event;
    *clicked = 0;
    while(XPending(display)) {
        XNextEvent(display, &event);
        switch(event.type) { 
        case Expose:
          if(event.xexpose.count==0)
            XDrawImageString(event.xexpose.display,
                             event.xexpose.window,
                             gc, 
                             50, 50, 
                             msg, strlen(msg));
          break;
        case ButtonPress:
          *clicked = 1;
        }
    }
}

#endif
