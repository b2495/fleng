# FLENG - asm part of x86_64 runtime

    .text

    # rdx=error-code, rsi=argument
error:
    movq %rbx,%rdi
    andq $-16,%rsp
    call _fl_rt_error

    .global _fl_check_atom
    # rax=val
    .balign 16
_fl_check_atom:
    movq %rax,%rdx
    andq $3,%rdx
    cmpq $2,%rdx
    jne fl_check_atom_l1
    ret
fl_check_atom_l1:
    movq %rax,%rsi
    movq $1,%rdx        # FL_NOT_AN_ATOM
    jmp error

    .global _fl_check_cell
    # rax=val
    .balign 16
_fl_check_cell:
    testq $3,%rax
    jnz fl_check_cell_l1
    ret
fl_check_cell_l1:
    movq %rax,%rsi
    movq $2,%rdx        # FL_NOT_A_CELL
    jmp error

    .global _fl_check_integer
    # rax=val
    .balign 16
_fl_check_integer:
    testq $1,%rax       # FL_INT_BIT
    jz fl_check_integer_l1
    ret
fl_check_integer_l1:
    movq %rax,%rsi
    movq $3,%rdx        # FL_NOT_AN_INT
    jmp error

    .global _fl_check_list
    # rax=val
    .balign 16
_fl_check_list:
    testq $3,%rax
    jnz fl_check_list_l1
    movq (%rax),%rdx
    movq $0xff000000,%rcx # FL_TAG_MASK
    andq %rcx,%rdx
    cmpq $0x01000000,%rdx # FL_LIST_TAG
    jne fl_check_list_l1
    ret
fl_check_list_l1:
    movq %rax,%rsi
    movq $4,%rdx        # FL_NOT_A_LIST
    jmp error

    .global _fl_check_tuple
    # rax=val
    .balign 16
_fl_check_tuple:
    testq $3,%rax
    jnz fl_check_tuple_l1
    movl (%rax), %r9d
    testl $0x80000000, %r9d
    jnz fl_check_tuple_l1
    ret
fl_check_tuple_l1:
    movq %rax,%rsi
    movq $5,%rdx        # FL_NOT_A_TUPLE
    jmp error

    .global _fl_check_var
    # rax=val
    .balign 16
_fl_check_var:
    testq $3,%rax
    jnz fl_check_var_l1
    movl (%rax),%edx
    andl $0xff000000,%edx # FL_TAG_MASK
    cmpl $0x04000000,%edx # FL_VAR_TAG
    je fl_check_var_l1
    cmpl $0x07000000,%edx # FL_REF_TAG
    je fl_check_var_l1
    movq %rax,%rsi
    movq $6,%rdx        # FL_NOT_A_VAR
    jmp error
fl_check_var_l1:
    ret

    .global _fl_check_module
    # rax=val
    .balign 16
_fl_check_module:
    testq $3,%rax
    jnz fl_check_module_l1
    movl (%rax),%edx
    movq $0xff000000,%rcx # FL_TAG_MASK
    andq %rcx,%rdx
    cmpq $0x0a000000,%rdx # FL_MODULE_TAG
    jne fl_check_module_l1
    ret
fl_check_module_l1:
    movq %rax,%rsi
    movq $23,%rdx        # FL_NOT_A_MODULE
    jmp error

    .global _fl_check_number
    # rax=val
    .balign 16
_fl_check_number:
    testq $1,%rax
    jz fl_check_number_l1
    ret
fl_check_number_l1:
    testq $2,%rax
    jnz fl_check_number_l2
    movl (%rax),%edx
    andl $0xff000000,%edx # FL_TAG_MASK
    cmpl $0x02000000,%edx # FL_FLOAT_TAG
    jne fl_check_number_l2
    ret
fl_check_number_l2:
    movq %rax,%rsi
    movq $11,%rdx       # FL_NOT_A_NUMBER
    jmp error

    # rsi=val -> xmm0
    .balign 16
float_arg_1:
    testq $1,%rsi
    jz float_arg_1_l1
    sarq %rsi
    cvtsi2sd %rsi, %xmm0
    ret
float_arg_1_l1:
    movsd 8(%rsi), %xmm0
    ret

    # rdx=val -> xmm1
    .balign 16
float_arg_2:
    testq $1,%rdx
    jz float_arg_2_l1
    sarq %rdx
    cvtsi2sd %rdx, %xmm1
    ret
float_arg_2_l1:
    movsd 8(%rdx), %xmm1
    ret

    # xmm0=val -> rax (float)
    .balign 16
float_result:
    movq $1,%rax
    movq %rax, %r10
    call cons
    movsd %xmm0, 8(%rax)
    movl $0x02000000,(%rax)         # FL_FLOAT_TAG
    ret

    .global _fl_add
    # rsi=num, rdx=num -> rax
    .balign 16
_fl_add:
    testq $1,%rsi
    jz fl_add_l1
    testq $1,%rdx
    jz fl_add_l1
    leaq -1(%rsi),%rax
    addq %rdx,%rax
    ret
fl_add_l1:
    call float_arg_1
    call float_arg_2
    addsd %xmm1, %xmm0
    jmp float_result

    .global _fl_sub
    # rsi=num, rdx=num -> rax
    .balign 16
_fl_sub:
    testq $1,%rsi
    jz fl_sub_l1
    testq $1,%rdx
    jz fl_sub_l1
    subq %rdx,%rsi
    leaq 1(%rsi),%rax
    ret
fl_sub_l1:
    call float_arg_1
    call float_arg_2
    subsd %xmm1, %xmm0
    jmp float_result

    .global _fl_mul
    # rsi=num, rdx=num -> rax
    .balign 16
_fl_mul:
    testq $1,%rsi
    jz fl_mul_l1
    testq $1,%rdx
    jz fl_mul_l1
    sarq %rsi
    sarq %rdx
    imulq %rdx,%rsi
    movq %rsi,%rax
    shlq %rax
    orq $1,%rax
    ret
fl_mul_l1:
    call float_arg_1
    call float_arg_2
    mulsd %xmm1, %xmm0
    jmp float_result

    .global _fl_div
    # rsi=num, rdx=num -> rax
    .balign 16
_fl_div:
    cmpq $1,%rdx
    jne fl_div_l0
    movq $12,%rdx       # FL_DIV_BY_ZERO
    jmp error
fl_div_l0:
    testq $1,%rsi
    jz fl_div_l1
    testq $1,%rdx
    jz fl_div_l1
    sarq %rsi
    sarq %rdx
    movq %rsi,%rax
    movq %rdx,%rcx
    cqto
    idivq %rcx
    shlq %rax
    orq $1,%rax
    ret
fl_div_l1:
    call float_arg_1
    call float_arg_2
    divsd %xmm1, %xmm0
    jmp float_result

    .global _fl_mod
    # rsi=num, rdx=num -> rax
    .balign 16
_fl_mod:
    cmpq $1,%rdx
    jne fl_mod_l0
    movq $12,%rdx       # FL_DIV_BY_ZERO
    jmp error
fl_mod_l0:
    sarq %rsi
    sarq %rdx
    movq %rsi,%rax
    movq %rdx,%rcx
    cqto
    idivq %rcx
    shlq %rdx
    orq $1,%rdx
    movq %rdx,%rax
    ret

    .global _fl_sqrt
    # rsi=num -> rax
    .balign 16
_fl_sqrt:
    call float_arg_1
    movsd %xmm0, -8(%rsp)
    sqrtsd %xmm0, %xmm0
    jmp float_result

    .global _fl_pow
    # rsi=num, rdx=num -> rax
    .balign 16
_fl_pow:
    call float_arg_1
    call float_arg_2
    pushq %rax  # dummy
    call _pow
    addq $8,%rsp
    jmp float_result

    .global _fl_sin
    # rsi=num -> rax
    .balign 16
_fl_sin:
    call float_arg_1
    pushq %rax  # dummy
    call _sin
    addq $8,%rsp
    jmp float_result

    .global _fl_cos
    # rsi=num -> rax
    .balign 16
_fl_cos:
    call float_arg_1
    pushq %rax  # dummy
    call _cos
    addq $8,%rsp
    jmp float_result

    .global _fl_tan
    # rsi=num -> rax
    .balign 16
_fl_tan:
    call float_arg_1
    pushq %rax  # dummy
    call _tan
    addq $8,%rsp
    jmp float_result

    .global _fl_asin
    # rsi=num -> rax
    .balign 16
_fl_asin:
    call float_arg_1
    pushq %rax  # dummy
    call _asin
    addq $8,%rsp
    jmp float_result

    .global _fl_acos
    # rsi=num -> rax
    .balign 16
_fl_acos:
    call float_arg_1
    pushq %rax  # dummy
    call _acos
    addq $8,%rsp
    jmp float_result

    .global _fl_atan
    # rsi=num -> rax
    .balign 16
_fl_atan:
    call float_arg_1
    pushq %rax  # dummy
    call _atan
    addq $8,%rsp
    jmp float_result

    .global _fl_loge
    # rsi=num -> rax
    .balign 16
_fl_loge:
    call float_arg_1
    pushq %rax  # dummy
    call _log
    addq $8,%rsp
    jmp float_result

    .global _fl_exp
    # rsi=num -> rax
    .balign 16
_fl_exp:
    call float_arg_1
    pushq %rax  # dummy
    call _exp
    addq $8,%rsp
    jmp float_result

    .global _fl_truncate
    # rsi=num -> rax
    .balign 16
_fl_truncate:
    call float_arg_1
    pushq %rax  # dummy
    call _trunc
    addq $8,%rsp
    jmp float_result

    .global _fl_floor
    # rsi=num -> rax
    .balign 16
_fl_floor:
    call float_arg_1
    pushq %rax  # dummy
    call _floor
    addq $8,%rsp
    jmp float_result

    .global _fl_round
    # rsi=num -> rax
_fl_round:
    call float_arg_1
    pushq %rax  # dummy
    call _round
    addq $8,%rsp
    jmp float_result

    .global _fl_ceiling
    # rsi=num -> rax
_fl_ceiling:
    call float_arg_1
    pushq %rax  # dummy
    call _ceil
    addq $8,%rsp
    jmp float_result

    .global _fl_shl
    # rsi=int, rdx=int -> rax(int)
    .balign 16
_fl_shl:
    movq %rsi,%rax
    movq %rdx,%rcx
    shr %rcx
    andq $~1, %rax
    shl %cl,%rax
    orq $1,%rax
    ret

    .global _fl_shr
    # rsi=int, rdx=int -> rax(int)
    .balign 16
_fl_shr:
    movq %rsi,%rax
    movq %rdx,%rcx
    shr %rcx
    sar %cl,%rax
    orq $1,%rax
    ret

    .global _fl_int
    # rsi=num -> rax
    .balign 16
_fl_int:
    testq $1,%rsi
    jz fl_int_l1
    movq %rsi,%rax
    ret
fl_int_l1:
    fldl 8(%rsi)
    fisttpq -8(%rsp)
    movq -8(%rsp),%rax
    shlq %rax
    orq $1,%rax
    ret

    .global _fl_float
    # rsi=num -> rax
    .balign 16
_fl_float:
    call float_arg_1
    call float_result
    ret

    .balign 16
true:
    movq _fl_true@GOTPCREL(%rip),%rax
    movq (%rax), %rax
    ret

    .balign 16
false:
    movq _fl_false@GOTPCREL(%rip),%rax
    movq (%rax), %rax
    ret

    .global _fl_sametype
    # rsi=val, rdx=val -> rax
    .balign 16
_fl_sametype:
    movq %rsi,%rax
    andq $3,%rax
    movq %rdx, %r9
    andq $3, %r9
    cmpq %r9, %rax
    jne false
    testq $3,%rax
    jnz true
    movl (%rsi),%eax
    movl (%rdx), %r9d
    testl $0x80000000,%eax  # FL_TUPLE_BIT
    jz fl_sametype_l4
    testl $0x80000000, %r9d
    jnz false
    jmp true
fl_sametype_l4:
    testl $0x80000000, %r9d
    jnz false
    andl $0xff000000,%eax   # FL_TAG_MASK
    andl $0xff000000, %r9d
    cmpl %r9d, %eax
    jne false
    jmp true

    .global _fl_eq
    # rsi=val, rdx=val -> rax
    # must preserve P0-P3
    .balign 16
_fl_eq:
    cmpq %rdx,%rsi
    je true
    jmp false

    .global _fl_equal
    # rsi=val, rdx=val -> rax
    # must preserve P0-P3
    .balign 16
_fl_equal:
    cmpq %rdx,%rsi
    je true
    testq $1,%rsi
    jz fl_equal_l2
    testq $1,%rdx
    jz fl_equal_l3
    jmp false
fl_equal_l2:
    testq $2,%rsi
    jnz false
    movl (%rsi), %r9d
    andl $0xff000000, %r9d
    cmpl $0x02000000, %r9d
    jne false
fl_equal_l3:
    testq $2,%rdx
    jnz false
    testq $1,%rdx
    jnz fl_equal_l5
    movl (%rdx), %r9d
    andl $0xff000000, %r9d
    cmpl $0x02000000, %r9d
    jne false
fl_equal_l5:
    call float_arg_1
    call float_arg_2
    ucomisd %xmm1, %xmm0
    jne false
    jmp true

    .global _fl_pf_equal
    # principal functor equality
    # rsi=val, rdx=val -> rax
    # must preserve P0-P3
    .balign 16
_fl_pf_equal:
    cmpq %rsi, %rdx
    je true
    testq $3, %rsi
    jnz false
    testq $3, %rdx
    jnz false
    movl (%rsi), %r9d
    andl $0xff000000, %r9d
    movl (%rdx), %r10d
    andl $0xff000000, %r10d
    cmpl %r9d, %r10d
    jne false
    testl $0x80000000, %r9d
    jz true
    movq 8(%rsi), %r9
    cmpq 8(%rdx), %r9
    jne false
    jmp true

    .global _fl_less
    # rsi=val, rdx=val -> rax
    # must preserve P0-P3
    .balign 16
_fl_less:
    testq $1,%rsi
    jz fl_less_l1
    testq $1,%rdx
    jz fl_less_l1
    movq _fl_true@GOTPCREL(%rip),%rax
    movq (%rax), %rax
    cmpq %rdx,%rsi
    movq _fl_false@GOTPCREL(%rip), %r10
    cmovgeq (%r10), %rax
    ret
fl_less_l1:
    call float_arg_1
    call float_arg_2
    movq _fl_true@GOTPCREL(%rip),%rax
    movq (%rax), %rax
    ucomisd %xmm1, %xmm0
    movq _fl_false@GOTPCREL(%rip), %r10
    cmovaeq (%r10), %rax
    ret

    .global _fl_iless
    # rsi=val, rdx=val -> rax
    # must preserve P0-P3
    .balign 16
_fl_iless:
    movq _fl_true@GOTPCREL(%rip),%rax
    movq (%rax), %rax
    cmpq %rdx,%rsi
    movq _fl_false@GOTPCREL(%rip), %r10
    cmovgeq (%r10), %rax
    ret

    .global _fl_fless
    # rsi=val, rdx=val -> rax
    # must preserve P0-P3
    .balign 16
_fl_fless:
    call float_arg_1
    call float_arg_2
    movq _fl_true@GOTPCREL(%rip),%rax
    movq (%rax), %rax
    ucomisd %xmm1, %xmm0
    movq _fl_false@GOTPCREL(%rip), %r10
    cmovaeq (%r10), %rax
    ret

    .global _fl_below
    # rsi=val, rdx=val -> rax
    .balign 16
_fl_below:
    movq %rbx, %rdi
    pushq %rcx
    subq $8, %rsp
    movq %rsp, %rcx
    pushq %rsi
    pushq %rdx
    call _fl_ordering
    popq %rdx
    popq %rsi
    popq %r9
    popq %rcx
    testq %r9, %r9
    je lbel1
    movq _fl_nil@GOTPCREL(%rip), %rax
    movq (%rax), %rax
    ret
lbel1:
    testq $2,%rax   # -1
    jnz true
    jmp false

    .global _fl_below_or_equal
    # rsi=val, rdx=val -> rax
    .balign 16
_fl_below_or_equal:
    movq %rbx, %rdi
    pushq %rcx
    subq $8, %rsp
    movq %rsp, %rcx
    pushq %rsi
    pushq %rdx
    call _fl_ordering
    popq %rdx
    popq %rsi
    popq %r9
    popq %rcx
    testq %r9, %r9
    je lbeleq1
    movq _fl_nil@GOTPCREL(%rip), %rax
    movq (%rax), %rax
    ret
lbeleq1:
    cmpq $1,%rax
    je false
    jmp true

    .global _fl_above
    # rsi=val, rdx=val -> rax
    .balign 16
_fl_above:
    pushq %rcx
    movq %rbx, %rdi
    subq $8, %rsp
    movq %rsp, %rcx
    pushq %rsi
    pushq %rdx
    call _fl_ordering
    popq %rdx
    popq %rsi
    popq %r9
    popq %rcx
    testq %r9, %r9
    je labo1
    movq _fl_nil@GOTPCREL(%rip), %rax
    movq (%rax), %rax
    ret
labo1:
    cmpq $1,%rax
    jne false
    jmp true

    .global _fl_above_or_equal
    # rsi=val, rdx=val -> rax
    .balign 16
_fl_above_or_equal:
    pushq %rcx
    movq %rbx, %rdi
    subq $8, %rsp
    movq %rsp, %rcx
    pushq %rsi
    pushq %rdx
    call _fl_ordering
    popq %rdx
    popq %rsi
    popq %r9
    popq %rcx
    testq %r9, %r9
    je laboeq1
    movq _fl_nil@GOTPCREL(%rip), %rax
    movq (%rax), %rax
    ret
laboeq1:
    testq $2,%rax   # negative?
    jnz false
    jmp true

    .global _fl_max
    # rsi=val, rdx=val -> rax
    .balign 16
_fl_max:
    # must preserve P0-P3
    testq $1,%rsi
    jz fl_max_l1
    testq $1,%rdx
    jz fl_max_l1
    movq %rsi,%rax
    cmpq %rdx,%rsi
    cmovleq %rdx, %rax
    ret
fl_max_l1:
    pushq %rsi
    pushq %rdx
    call float_arg_1
    call float_arg_2
    popq %rdx
    popq %rax
    ucomisd %xmm1, %xmm0
    cmovbeq %rdx, %rax
    ret

    .global _fl_min
    # rsi=val, rdx=val -> rax
    .balign 16
_fl_min:
    # must preserve P0-P3
    testq $1,%rsi
    jz fl_min_l1
    testq $1,%rdx
    jz fl_min_l1
    movq %rsi,%rax
    cmpq %rdx,%rsi
    cmovaeq %rdx, %rax
    ret
fl_min_l1:
    pushq %rsi
    pushq %rdx
    call float_arg_1
    call float_arg_2
    popq %rdx
    popq %rax
    ucomisd %xmm1, %xmm0
    cmovaeq %rdx, %rax
    ret

    .global _fl_sign
    # rsi=val -> rax
_fl_sign:
    testq $1, %rsi
    jnz fl_sign_1
    call float_arg_1
    movsd zero(%rip), %xmm1
    ucomisd %xmm1, %xmm0
    jne fl_sign_5
    movq $1, %rax
    ret
fl_sign_5:
    jae fl_sign_4
    jmp fl_sign_3
fl_sign_2:
    cmpq $0, %rsi
    jl fl_sign_3
fl_sign_4:
    movq $3, %rax
    ret
fl_sign_3:
    movq $-1, %rax
    ret
fl_sign_1:
    cmpq $1, %rsi
    jl fl_sign_3
    jne fl_sign_4
    movq %rsi, %rax
    ret
zero: .double 0

    .global _fl_abs
    # rsi=val -> rax
_fl_abs:
    testq $1, %rsi
    jne labs1
    fldl 8(%rsi)
    call _fl_mkfloat
    fabs
    fstpl 8(%rax)
    ret
labs1:
    mov %rsi, %rax
    cmpq $0, %rax
    jl labs2
    ret
labs2:
    negq %rax
    add $2, %rax
    ret

    .global _fl_rnd
    # rsi=val -> rax
_fl_rnd:
    movq %rsi, %rdi
    jmp _fl_random_int

    .global _fl_fip
    # rsi=val -> rax
_fl_fip:
    testq $1, %rsi
    jz lfip1
    movq %rsi, %rax
    ret
lfip1:
    movq 8(%rsi), %rax
    shrq $52, %rax
    andq $0x7ff, %rax
    subq $0x3ff, %rax
    cmpq $52, %rax
    jl lfip2
    mov %rsi, %rax      # nan
    ret
lfip2:
    cmpq $0, %rax
    jge lfip4
    movq $1, %rax
    ret
lfip4:
    pushq %rcx
    movq %rax, %rcx
    movq $-1, %r9
    shrq $12, %r9
    shrq %cl, %r9
    popq %rcx
    movq 8(%rsi), %rax
    andq %rax, %r9
    cmpq $0, %r9
    jnz lfip3
    movq %rsi, %rax
    ret
lfip3:
    notq %r9
    andq %r9, %rax
    pushq %rax
    movsd (%rsp), %xmm0
    addq $8, %rsp
    jmp float_result

    .global _fl_ffp
    # rsi=val -> rax
_fl_ffp:
    testq $1, %rsi
    jz lffp1
    movq $1, %rax
    ret
lffp1:
    movq 8(%rsi), %rax
    shrq $52, %rax
    andq $0x7ff, %rax
    subq $0x3ff, %rax
    cmpq $52, %rax
    jl lffp2
    cmpq $0x400, %rax
    jne lffp3
    movq 8(%rsi), %rax
    shlq $12, %rax
    jz lffp3
    mov %rsi, %rax      # nan
    ret
lffp3:
    movq $1, %rax
    ret
lffp2:
    cmpq $0, %rax
    jge lffp4
    movq %rsi, %rax
    ret
lffp4:
    pushq %rcx
    movq %rax, %rcx
    movq $-1, %r9
    shrq $12, %r9
    shrq %cl, %r9
    popq %rcx
    movq 8(%rsi), %rax
    andq %rax, %r9
    cmpq $0, %r9
    jz lffp3
    notq %r9
    andq %r9, %rax
    pushq %rax
    movsd (%rsp), %xmm1
    addq $8, %rsp
    movsd 8(%rsi), %xmm0
    subsd %xmm1, %xmm0
    jmp float_result

    .global _fl_deref
    # rax=val -> rax
    # must preserve P0-P3
    .balign 16
_fl_deref:
    testq $3,%rax
    jz fl_deref_l1
fl_deref_l2:
    ret
fl_deref_l1:
    movl (%rax), %r9d
    andl $0xff000000, %r9d
    cmpl $0x04000000, %r9d
    jne fl_deref_l2
    movq 8(%rax), %r9
    cmpq %r9, %rax
    je fl_deref_l2
    movq %r9, %rax
    jmp _fl_deref

    .global _fl_unbox
    # A=val -> A
    .balign 16
_fl_unbox:
    testq $3,%rax
    jz unbox_l1
unbox_l2:
    ret
unbox_l1:
    movl (%rax), %r9d
    andl $0xff000000, %r9d
    cmpl $0x04000000, %r9d
    jne unbox_l3
    movq 8(%rax), %r9
    cmpq %r9, %rax
    je unbox_l2
    movq %r9, %rax
    jmp _fl_unbox
unbox_l3:
    movq %rax, %rsi
    movq %rbx, %rdi
    jmp _fl_unbox_cell

    .global _fl_box
    # rsi=val -> A
    .balign 16
_fl_box:
    testq $1,%rsi
    jnz box_l1
    ret
box_l1:
    movq _fl_nil@GOTPCREL(%rip),%rax
    movq (%rax), %rax
    movq %rax, %r10
    call cons
    movl $0x0f000000,(%rax)         # FL_BOX_LONG_TAG
    sarq $1, %rsi
    movq %rsi, 8(%rax)
    ret

    .global _fl_unref
    # r9=val, rbx=tcb
    # must preserve A + P0-P3
    .balign 16
_fl_unref:
    testq $3, %r9
    jz fl_unref_l1
    ret
fl_unref_l1:
    movl (%r9), %r10d
    andl $0xffffff, %r10d
    cmpl $0xffffff, %r10d
    jne fl_unref_l3
    ret
fl_unref_l3:
    decl %r10d
    jz fl_unref_l2
    decl (%r9)
    ret
fl_unref_l2:
    pushq %rax
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    movq %r9, %rsi
    call _fl_release
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    popq %rax
    ret

    .balign 16
setcarry:
    stc
    ret

    .balign 16
clrcarry:
    clc
    ret

    .global _fl_force
    # rax=val -> rax, CF is 1 when suspending
    # rbx=tcb
    .balign 16
_fl_force:
    testq $3,%rax
    jnz clrcarry
    movl (%rax),%edx
    andl $0xff000000,%edx # FL_TAG_MASK
    cmpl $0x07000000,%edx # FL_REF_TAG
    je fl_force_l3
    cmpl $0x04000000,%edx # FL_VAR_TAG
    jne clrcarry
    movq 8(%rax),%rdx
    cmpq %rdx,%rax
    je fl_force_l3
    movq %rdx,%rax
    jmp _fl_force
fl_force_l3:
    movq 8(%rbx), %r9       # tcb->sstackp
    movq %rax, (%r9)
    movq (%r12), %rax       # goal->addr
    movq %rax, 8(%r9)
    addq $16, %r9
    movq %r9, 8(%rbx)       # tcb->sstackp
    jmp setcarry

    .global _fl_match
    # rsi=val1, rdx=val2 -> rax
    # assumes args are forced (allows initial quick eq check)
_fl_match:
    cmpq %rsi, %rdx
    je true
    movq %rbx, %rdi
    pushq %rsi
    pushq %rdx
    call _fl_match_rec
    popq %rdx
    popq %rsi
    ret

    .global _fl_islist_t
    # rax=val, set CF if not a list
    .balign 16
_fl_islist_t:
    testq $3,%rax
    jnz setcarry
    movl (%rax), %r9d
    andl $0xff000000, %r9d
    cmpl $0x01000000, %r9d
    jne setcarry
    jmp clrcarry

    .global _fl_istuple_t
    # rax=val, set CF if not a tuple
    .balign 16
_fl_istuple_t:
    testq $3,%rax
    jnz setcarry
    movl (%rax),%edx
    testl $0x80000000,%edx # FL_TUPLE_BIT
    jz setcarry
    jmp clrcarry

    # rsi=addr, r12=G -> Carry (fail)
    .balign 16
trynext:
    movq %rsi,(%r12)       # goal->addr
    jmp setcarry

    .global _fl_isval
    # rdx=val, rsi=addr -> Carry (fail)
    .balign 16
_fl_isval:
    cmpq %rdx,%rax
    jne trynext
    jmp clrcarry

    .global _fl_isfloat
    # rax=val, rdx=float, rsi=addr -> Carry (fail)
    .balign 16
_fl_isfloat:
    testq $3,%rax
    jz fl_isfloat_l1
    jmp trynext
fl_isfloat_l1:
    cmpq %rdx,8(%rax)
    jne trynext
    jmp clrcarry

    .global _fl_islist
    # rax=val, rsi=addr -> rax(car), Carry (fail)
    # push cdr on stack
    .balign 16
_fl_islist:
    call _fl_islist_t
    jc trynext
    movq 16(%rax),%rdx
    xchgq %rdx,(%rsp)
    movq 8(%rax),%rax
    clc
    jmp *%rdx

    .global _fl_isvector
    # rax=val, rsi=addr, rdx=len -> rax(M), Carry (fail)
    # push vector on stack
    .balign 16
_fl_isvector:
    testq $3,%rax
    jnz trynext
    movl (%rax), %r9d
    andl $0xff000000, %r9d
    cmpl $0x03000000, %r9d
    jne trynext
    movq 16(%rax),%r10
    movq 8(%r10), %r9
    sarq $1, %r9
    cmpq %r9, %rdx
    jne trynext
    xchgq %rax,(%rsp)
    clc
    jmp *%rax

    .global _fl_velement
    # rsi=index(fixnum), stack=vector -> rax=item
    .balign 16
_fl_velement:
    movq 8(%rsp), %rax
    movq 8(%rax), %rax		# get map
    call _fl_deref
    movq _fl_nil@GOTPCREL(%rip),%r11
    movq (%r11), %r11
lie1:
    cmpq %rax, %r11
    jne lie4
    movq 8(%rsp), %rax
    movq 16(%rax), %rax
    movq 16(%rax), %rax	# default
    ret
lie4:
    movq 16(%rax), %rax	# get part of "t" tuple holding k, v
    movq 16(%rax), %rax
    movq 8(%rax), %r10		# key
    cmpq %r10, %rsi
    jb lie2
    ja lie3
    movq 16(%rax), %rax
    movq 8(%rax), %rax
    ret
lie2:
    movq 16(%rax), %rax		# left part
    movq 16(%rax), %rax
    movq 8(%rax), %rax
    call _fl_deref
    jmp lie1
lie3:
    movq 16(%rax), %rax		# right part
    jmp lie2

    .global _fl_isarrayval
    # rax=val, rsi=addr, rdx=literal_array -> Carry (fail)
    .balign 16
_fl_isarrayval:
    testq $3,%rax
    jnz trynext
    movl (%rax), %r9d
    andl $0xff000000, %r9d
    cmpl $0x08000000, %r9d		# FL_ARRAY_TAG
    jne trynext
    movq 8(%rax), %r10		# CAR(A) = [ptr len/type]
    movq 8(%rdx), %r8		# CAR(P1) = [ptr len/type]
    movq 16(%r10), %rcx
    andq $~8, %rcx			# clear FL_ARRAY_IMMUTABLE bit
    movq 16(%r8), %r9
    andq $~8, %r9
    cmpq %r9, %rcx
    jne trynext
    sarq $4, %rcx			# length
    pushq %rsi
    pushq %rdi
    movq 8(%r10), %rsi
    movq 8(%r8), %rdi
    cld
liav1:
    repe cmpsb
    popq %rdi
    popq %rsi
    jne trynext
    jmp clrcarry

    .global _fl_istuple
    # rax=val, rsi=addr, rdx=arity -> rax(head), Carry (fail)
    # push arglist on stack
    .balign 16
_fl_istuple:
    testq $3,%rax
    jnz trynext
    movl (%rax), %r8d
    testl $0x80000000, %r8d
    jz trynext
    shrl $24, %r8d
    andl $0x7f, %r8d
    cmpl %edx, %r8d
    jne trynext
    movq 16(%rax),%rdx
    xchgq %rdx, (%rsp)
    movq 8(%rax), %rax
    clc
    jmp *%rdx

    # rbx=tcb, r10=car, rax=cdr -> rax
    # must preserve parameter registers
    .balign 16
cons:
    pushq %rax
    movq 6 * 8(%rbx), %r9   # tcb->freelist
    testq %r9, %r9
    jnz cons_l1
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    pushq %r9
    pushq %r10
    pushq %r12
    movq %rbx,%rdi
    xorq %r12, %r12
    testq $0x0f, %rsp
    jz cons_l2
    movq $8, %r12
    subq %r12, %rsp
cons_l2:
    call _fl_new_chunk
    addq %r12, %rsp
    popq %r12
    popq %r10
    popq %r9
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    movq 6 * 8(%rbx), %r9   # tcb->freelist
cons_l1:
    movq 16(%r9), %rax
    movq %rax,6*8(%rbx)     # tcb->freelist
    popq %rax
    call _fl_addref
    movq %rax, 16(%r9)
    movq %r10, %rax
    call _fl_addref
    movq %rax, 8(%r9)
    incq 11*8(%rbx)            # tcb->used
    movq %r9, %rax
    ret

    .global _fl_mkfloat
    # rdx=float64 -> rax
    .balign 16
_fl_mkfloat:
    pushq %rdx
    movq $1,%rax
    movq $1,%r10
    call cons
    popq %r9
    movl $0x02000000,(%rax)         # FL_FLOAT_TAG
    movq %r9, 8(%rax)
    ret

    .global _fl_mklist
    # rax=cdr, [rsp+8]=car -> rax
    .balign 16
_fl_mklist:
    movq 8(%rsp), %r10
    call cons
    movq $0x01000000, %r9
    movq %r9, (%rax)
    popq %r10
    addq $8, %rsp
    pushq %r10
    ret                         # avoid indirect branch

    .global _fl_mktuple
    # rax=args, r10=atom, r9=arity -> rax
    .balign 16
_fl_mktuple:
    pushq %r9
    call cons
    popq %r9
    # fall through...

    .global _fl_mktuple0
    # rax=list, r9=arity -> rax
    .balign 16
_fl_mktuple0:
    shl $24, %r9
    orl $0x80000000, %r9d
    movq %r9, (%rax)
    ret

    .global _fl_tovector
    # rax=lst -> rax
    .balign 16
_fl_tovector:
    movl $0x03000000,(%rax)       # FL_VECTOR_TAG
    ret

    .global _fl_mkvar
    # -> rax
    .balign 16
_fl_mkvar:
    movq $1, %r10
    movq _fl_nil@GOTPCREL(%rip),%rax
    movq (%rax), %rax
    call cons
    movq %rax,8(%rax)
    movl $0x04000000,(%rax)       # FL_VAR_TAG
    ret

    # r13=goal, rbx=tcb
    .balign 16
enqueue:
    movq 13*8(%rbx),%rax        # tcb->qend
    movq %r13, (%rax)
    addq $8,%rax
    movq 12*8(%rbx),%rcx        # tcb->qstart
    cmpq %rcx,%rax
    jne enqueue_l1
    movq $7,%rdx                # FL_QUEUE_FULL
    jmp error
enqueue_l1:
    movq 15*8(%rbx),%rcx        # tcb->max_goals
    shlq $3,%rcx
    addq 14*8(%rbx),%rcx        # tcb->queue
    cmpq %rcx,%rax
    jb enqueue_l2
    movq 14*8(%rbx),%rax        # tcb->queue
enqueue_l2:
    movq %rax,13*8(%rbx)        # tcb->qend
    incq 10*8(%rbx)             # tcb->active
    ret

    .global _fl_opengoal
    # -> r13
    .balign 16
_fl_opengoal:
    movq 16 * 8(%rbx), %r13     # tcb->freegoals
    testq %r13, %r13
    jne fl_opengoal_l1
    movq $26,%rdx               # FL_TOO_MANY_GOALS
    jmp error
fl_opengoal_l1:
    movq (%r13), %r9            # goal->addr
    movq 6 * 8(%r12), %rax      # goal->task
    call _fl_addref
    movq %rax, 6 * 8(%r13)      # goal->task
    movq %r9, 16 * 8(%rbx)      # tcb->freegoals
    incq 21*8(%rbx)            # tcb->goals
    jmp enqueue

    .global _fl_dropgoal
    # rbx=tcb
    .balign 16
_fl_dropgoal:
    movq (%rbx), %r9            # tcb->goal
    movq 16 * 8(%rbx), %r10     # tcb->freegoals
    movq %r10, (%r9)
    movq %r9, 16 * 8(%rbx)      # tcb->freegoals
    decq 21*8(%rbx)            # tcb->goals
    xorq %r10, %r10
    movq %r10, 5 * 8(%r9)       # goal->info
    movq _fl_nil@GOTPCREL(%rip), %r10
    movq (%r10), %r10
    xchgq 6 * 8(%r9), %r10      # goal->task
    movq %r10, %r9
    jmp _fl_unref

    .global _fl_addref
    # rax=val
    # must preserve registers
    .balign 16
_fl_addref:
    testq $3,%rax
    jz fl_addref_l1
    ret
fl_addref_l1:
    pushq %rax
    movl (%rax),%eax
    andl $0xffffff,%eax    # FL_COUNT_MASK
    cmpl $0xffffff,%eax    # FL_COUNT_MASK
    je fl_addref_l2
    popq %rax
    incl (%rax)
    ret
fl_addref_l2:
    popq %rax
    ret

    .global _fl_puttailarg
    # r13=N, rax=list
    .balign 16
_fl_puttailarg:
    movq 16(%rax),%rdx
    testq $3,%rdx
    jz fl_puttailarg_l1
    movq 8(%rax),%rax
fl_puttailarg_l1:
    call _fl_addref
    movq %rax, 4 * 8(%r13)
    ret

    .global _fl_reify
    # replace cdr of pair with deref'd element
    # rax=pair
_fl_reify:
    call _fl_deref
    push %rax
    movq 16(%rax), %rax
    push %rax
    call _fl_deref
    call _fl_addref
    popq %r9
    call _fl_unref
    popq %r9
    movq %rax, 16(%r9)
    ret

    .global _fl_resolve
    # r8=cache, rsi=arity, rdx=module, rcx=name
    # r13=N
    .balign 16
_fl_resolve:
    movq (%r8), %r9
    testq %r9, %r9
    je fl_resolve_l1
    movq %r9, (%r13)
    ret
fl_resolve_l1:
    movq %rbx,%rdi
    movq %r13, %r9
    jmp _fl_resolve_pdef

    .global _fl_replace
    # rax=val, rdx=argaddr
    .balign 16
_fl_replace:
    cmpq (%rdx),%rax
    jne fl_replace_l1
    ret
fl_replace_l1:
    movq (%rdx), %r9
    call _fl_addref
    movq %rax,(%rdx)
    jmp _fl_unref

    .global _fl_tailgoal
    # rdx=addr, rbx=tcb -> r13(N)
    .balign 16
_fl_tailgoal:
    movq 16(%rbx),%rcx  # tcb->tailcalls
    movq 24(%rbx),%rsi  # tcb->timeslice
    cmpq %rsi,%rcx
    jl fl_tg_1
    pushq %rdx
    call _fl_opengoal
    popq %rdx
    movq %rdx,(%r13)       # goal->addr
    ret
fl_tg_1:
    movq %r12, %r13
    movq %rdx, (%r13)     # goal->addr
    ret

    .global _fl_tail_jump
    # r13=r12=N, rbx=tcb
    .balign 16
_fl_tail_jump:
    movq 16(%rbx),%rcx  # tcb->tailcalls
    movq 24(%rbx),%rsi  # tcb->timeslice
    cmpq %rsi,%rcx
    jl fl_tail_jump_l1
    xorq %rax,%rax
    movq %rax,16(%rbx)  # tcb->tailcalls
    ret
fl_tail_jump_l1:
    incq %rcx
    movq %rcx,16(%rbx)  # tcb->tailcalls
    # fall through ...

    .global _fl_invoke_jump
    # r13=r12=N, rbx=tcb
_fl_invoke_jump:
    movq %rbp,%rsp
    movq %r13, %r12
    movq %r13, (%rbx)       # tcb->goal
    movq (%r13), %rax       # goal->addr
    jmpq *%rax

    .global _fl_test_integer
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_integer:
    testq $1,%rsi
    jnz true
    jmp false

    .global _fl_test_number
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_number:
    testq $1,%rsi
    jnz true
    movl $0x02000000, %r9d
    jmp _fl_test_tag

    .global _fl_test_atom
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_atom:
    movq %rsi,%rax
    andq $3,%rax
    cmpq $2,%rax
    jne false
    jmp true

    .global _fl_test_tag
    # rsi=val, r9d=tag -> rax (bool)
    .balign 16
_fl_test_tag:
    testq $3,%rsi
    jnz false
    movl (%rsi),%eax
    andl $0xff000000,%eax # FL_TAG_MASK
    cmpl %r9d, %eax
    jne false
    jmp true

    .global _fl_test_var
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_var:
    testq $3,%rsi
    jnz false
    movl (%rsi),%eax
    andl $0xff000000,%eax # FL_TAG_MASK
    cmpl $0x07000000,%eax # FL_REF_TAG
    je true
    cmpl $0x04000000,%eax # FL_VAR_TAG
    jne false
    movq 8(%rsi),%rax
    cmpq %rsi,%rax
    jne false
    jmp true

    .global _fl_test_remote
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_remote:
    testq $3,%rsi
    jnz false
    movl (%rsi),%eax
    andl $0xff000000,%eax # FL_TAG_MASK
    cmpl $0x07000000,%eax # FL_REF_TAG
    je true
    cmpl $0x05000000,%eax # FL_PORT_TAG
    jne false
    movq 8(%rsi),%rax
    testq $1, %rax
    jne true
    jmp false

    .global _fl_test_tuple
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_tuple:
    testq $3,%rsi
    jnz false
    movl (%rsi),%eax
    testl $0x80000000,%eax # FL_TUPLE_BIT
    jz false
    jmp true

    .global _fl_test_struct
    # rsi=val, rdx=name, rcx=arity -> rax (bool)
_fl_test_struct:
    testq $3, %rsi         # FL_BITS_MASK
    jnz false
    movl (%rsi), %eax
    shrl $24, %eax
    andl $0xff, %eax
    shrl %ecx
    orl $0x80, %ecx	 # FL_TUPLE_BIT
    cmpl %eax, %ecx
    jne false
    movq 8(%rsi), %rax
    cmpq %rax, %rdx
    jne false
    jmp true

    .global _fl_test_array
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_array:
    testq $3,%rsi       # FL_BITS_MASK
    jnz false
    movl (%rsi),%eax
    andl $0xff000000,%eax   # FL_TAG_MASK
    cmpl $0x08000000,%eax   # FL_ARRAY_TAG
    je true
    jmp false

    .global _fl_test_kl1string
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_kl1string:
    testq $3,%rsi       # FL_BITS_MASK
    jnz false
    movl (%rsi),%eax
    andl $0xff000000,%eax   # FL_TAG_MASK
    cmpl $0x08000000,%eax   # FL_ARRAY_TAG
    jne false
    movq 8(%rsi), %rax
    movq 16(%rax), %rax
    andq $0x7, %rax
    cmp $6, %rax			# FL_KL1_STRING
    jne false
    jmp true

    .global _fl_test_vector
    # rsi=val -> rax (bool)
    .balign 16
_fl_test_vector:
    testq $3,%rsi       # FL_BITS_MASK
    jnz false
    movl (%rsi),%eax
    andl $0xff000000,%eax   # FL_TAG_MASK
    cmpl $0x03000000,%eax   # FL_VECTOR_TAG
    je true
    jmp false

    .global _fl_enter
    # rdi=tcb
    .balign 16
_fl_enter:
    movq 5*8(%rdi),%rsp     # tcb->stackbase
    movq %rsp,%rbp
    movq (%rdi), %r12       # tcb->goal
    movq (%r12), %rax       # goal->addr
    movq %rdi,%rbx
    jmpq *%rax

    .global _fl_atomic
    # rsi=val -> rax (bool)
    .balign 16
_fl_atomic:
    testq $3,%rsi       # FL_BITS_MASK
    jnz true
    movl (%rsi),%eax
    andl $0xff000000,%eax   # FL_TAG_MASK
    cmpl $0x02000000,%eax   # FL_FLOAT_TAG
    je true
    jmp false

    .global _fl_atomic_nonfloat
    # rsi=val -> rax (bool)
    .balign 16
_fl_atomic_nonfloat:
    testq $3,%rsi       # FL_BITS_MASK
    jnz true
    jmp false

    .global _fl_entry
    # rbx=tcb, rsi=arity
    .balign 16
_fl_entry:
    movq _fl_nil@GOTPCREL(%rip), %r10
    movq (%r10), %r10
    movq 6*8(%r12), %rax    # goal->task
    cmpq %rax, %r10
    je fl_entry_l0
    movq 8(%rax), %rax     # get trace flag (car(car(car(task))))
    movq 8(%rax), %rax
    movq 8(%rax), %rax
    cmpq %rax, %r10
    jne fl_entry_l1
fl_entry_l0:
    movl 20*8(%rbx),%eax        # tcb->logging
    testl $1,%eax
    jnz fl_entry_l1
    ret
fl_entry_l1:
    movq %rbx,%rdi
    jmp _fl_log_entry

    .global _fl_assign0
    # rsi=val, rdx=var
    # must preserve P0-P3
    .balign 16
_fl_assign0:
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    subq $8,%rsp            # (un)align
    call _fl_assign
    addq $8,%rsp
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    ret

    .global _fl_d_assign0
    # rsi=val, rdx=var
    # must preserve P0-P3
    .balign 16
_fl_d_assign0:
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    subq $8,%rsp            # (un)align
    call _fl_d_assign
    addq $8,%rsp
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    ret

    .global _fl_unify
    # rsi=val, rdx=val -> rax (bool)
    # must preserve P0-P3
    .balign 16
_fl_unify:
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    subq $8,%rsp            # (un)align
    call _fl_unify_rec
    addq $8,%rsp
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    ret

    .global _fl_d_unify
    # rsi=val, rdx=val -> rax (bool)
    # must preserve P0-P3
    .balign 16
_fl_d_unify:
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    subq $8,%rsp            # (un)align
    call _fl_d_unify_rec
    addq $8,%rsp
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    ret

    .global _fl_unify_safe
    # rsi=val, rdx=val -> rax (bool)
    # must preserve P0-P3
    .balign 16
_fl_unify_safe:
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    subq $8,%rsp            # (un)align
    call _fl_unify_safe1
    addq $8,%rsp
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    ret

    .global _fl_unify_checked
    # rsi=val, rdx=val
    # must preserve P0-P3
    .balign 16
_fl_unify_checked:
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    subq $8,%rsp            # (un)align
    call _fl_unify_rec_checked
    addq $8,%rsp
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    ret

    .global _fl_d_unify_checked
    # rsi=val, rdx=val
    # must preserve P0-P3
    .balign 16
_fl_d_unify_checked:
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    subq $8,%rsp            # (un)align
    call _fl_d_unify_rec_checked
    addq $8,%rsp
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    ret

    .global _fl_lookup
    # A, X0: table [count, int1, label1, ..., label]
    .balign 16
_fl_lookup:
    addq $8, %rsp
    movq %r9, %rsi
    movq (%r9), %r10
    addq $8, %r9
llok1:
    cmpq (%r9), %rax
    je llok2
    addq $16, %r9
    decq %r10
    jne llok1
    subq $8, %r9
llok2:
    addq 8(%r9), %rsi
    jmp *%rsi

    .global _fl_goto
    # A, X0: table [min, max*, flabel, label1, ...]
    # max: mark bit is 0
    .balign 16
_fl_goto:
    addq $8, %rsp
    movq %r9, %rsi
    subq (%r9), %rax
    cmpq 8(%r9), %rax
    ja lgoto1
    addq $6, %rax
    shlq $2, %rax
    addq %rsi, %rax
    movq (%rax), %rax
    addq %rax, %rsi
    jmp *%rsi
lgoto1:
    movq 16(%r9), %rax
    addq %rax, %rsi
    jmp *%rsi

    .global _fl_prepare_c_arg
    # A: Arg
    .balign 16
_fl_prepare_c_arg:
    testq $2, %rax      # FL_ATOM_BIT
    jz lprep1
    incq %rax
    ret
lprep1:
    movl (%rax),%r9d
    andl $0xff000000,%r9d # FL_TAG_MASK
    cmpl $0x08000000,%r9d # FL_ARRAY_TAG
    jne lprep2
    movq 8(%rax), %rax
    movq 8(%rax), %rax
    ret
lprep2:
    addq $8, %rax
    ret

    .global _fl_new_task
    # G, rbx=tcb, A = status(var)
    .balign 16
_fl_new_task:
    pushq %rax
    movq _fl_nil@GOTPCREL(%rip), %r10
    movq (%r10), %r10
    movq 6*8(%r12), %rax    # goal->task
    cmpq %rax, %r10
    jne new_task_l1
    # cons new env/parent cell [[]|[]]]
    call cons
    movl $0x01000000,(%rax)         # FL_LIST_TAG
    movq %rax, %r10
    jmp new_task_l2
new_task_l1:
    movq 8(%rax), %rax
    movq 8(%rax), %r10              # env
new_task_l2:
    movq 6*8(%r12), %rax            # goal->task (parent)
    call cons
    movq %rax, %r10
    popq %rax
    call cons                       # task: [env/parent|st]
    movl $0x06000001,(%rax)         # FL_TASK_TAG|1
    movq 6*8(%r12), %r9            # goal->task (old)
    call _fl_unref
    movq %rax, 6*8(%r12)            # goal->task
    ret

    .global _fl_box_long_ref
    # A = box -> A
    .balign 16
_fl_box_long_ref:
    movl (%rax), %r9d
    andl $0xff000000,%r9d # FL_TAG_MASK
    cmpl $0x08000000,%r9d # FL_ARRAY_TAG
    jne lblr1
    movq 8(%rax), %rax
    movq 8(%rax), %rax
lblr1:
    movq 8(%rax), %rax
    cdqe
fix:
    shlq %rax
    orq $1, %rax
    ret

    .global _fl_box_int_ref
    # A = box -> A
    .balign 16
_fl_box_int_ref:
    movl (%rax), %r9d
    andl $0xff000000,%r9d # FL_TAG_MASK
    cmpl $0x08000000,%r9d # FL_ARRAY_TAG
    jne lbir1
    movq 8(%rax), %rax
    movq 8(%rax), %rax
lbir1:
    movl 8(%rax), %eax
    cdqe
    jmp fix

    .global _fl_box_short_ref
    # A = box -> A
    .balign 16
_fl_box_short_ref:
    movl (%rax), %r9d
    andl $0xff000000,%r9d # FL_TAG_MASK
    cmpl $0x08000000,%r9d # FL_ARRAY_TAG
    jne lbsr1
    movq 8(%rax), %rax
    movq 8(%rax), %rax
lbsr1:
    movw 8(%rax), %ax
    cwde
    cdqe
    jmp fix

    .global _fl_box_char_ref
    # A = box -> A
    .balign 16
_fl_box_char_ref:
    movl (%rax), %r9d
    andl $0xff000000,%r9d # FL_TAG_MASK
    cmpl $0x08000000,%r9d # FL_ARRAY_TAG
    jne lbcr1
    movq 8(%rax), %rax
    movq 8(%rax), %rax
lbcr1:
    movsbq 8(%rax), %rax
    jmp fix

    .global _fl_box_double_ref
    # T, A = box -> A
    .balign 16
_fl_box_double_ref:
    movl (%rax), %r9d
    andl $0xff000000,%r9d # FL_TAG_MASK
    cmpl $0x08000000,%r9d # FL_ARRAY_TAG
    jne lbdr1
    movq 8(%rax), %rax
    movq 8(%rax), %rax
lbdr1:
    movq 8(%rax), %rdx
    jmp _fl_mkfloat

    .global _fl_factset
    # rsi=arity, rdx=tree
    # must preserve P0-P3
    .balign 16
_fl_factset:
    pushq %rsi
    pushq %rdx
    pushq %rcx
    pushq %r8
    movq %rbx,%rdi
    subq $8,%rsp            # (un)align
    call _fl_match_facts
    addq $8,%rsp
    popq %r8
    popq %rcx
    popq %rdx
    popq %rsi
    testl $1, %eax
    jz setcarry
    jmp clrcarry

    .global _fl_candidate
    # T -> set carry if pre-selecting fair choice candidate
    .balign 16
_fl_candidate:
    movq 24*8(%rbx), %rax	# tcb->candidates
    movq 25*8(%rbx), %r9	# tcb->selected
    testq $1, %r9
    jnz lcand1
    # case 1: the clause is a possible candidate (selected == 0)
    incq 24*8(%rbx)		# ++tcb->candidates
    jmp setcarry			# skip
lcand1:
    cmpq $0, %rax
    jne lcand2
    # case 2: the clause is the selected candidate (selected == 1, candidates == 0)
    movq %rax, 25*8(%rbx)	# tcb->selected = 0
    jmp clrcarry			# execute candidates
lcand2:
    # case 3: the clause not the selected candidate (selected == 1, candidates > 0)
    decq 24*8(%rbx)		# --tcb->candidates
    jmp setcarry

    .global _fl_select
    # T, X0=cell address
    # clobbers P0, P1
    .balign 16
_fl_select:
    movq 24*8(%rbx), %rsi		# tcb->candidates
    cmpq $0, %rsi
    jne lsel1
    ret						# no candidates, normal suspend
lsel1:
    movq $1, %r10
    movq %r10, 25*8(%rbx)		# tcb->selected = 1
    movq (%r9), %r10
    cmpq $0, %r10
    jne lsel2
    incq %r10					# adjust initial seed to 1
lsel2:
    movq %rsi, %rax
    movq %r10, %r11
    shrq $8, %r11
    addq %r11, %rax
    xorq %rdx, %rdx
    divl %esi
    movq $1664525, %rax		# LCG
    imulq %rax, %r10
    addq $1013904223, %r10
    movq %r10, (%r9)
    movq %rdx, 24*8(%rbx)		# tcb->candidates = tcb->candidates % <cell>
    addq $8, %rsp
    movq (%r12), %rax			# goal->addr
    jmp *%rax					# reenter goal
