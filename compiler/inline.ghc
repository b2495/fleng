% FLENG compiler - inlining
%
% inline candidates are procedures wiht a single clause that only take non-forced
% variables as arguments and have a single body term that is either a user-defined
% local definition or a primitive (as in "lfa:primitive") or one of the following forms:
%	'$asm'/*, true/0, foreign_call/1, compute/3, compute/4, ('=')/2, unify/3,
%	call/1, call/2, call/3, '$call'/2, '$call_with_remote_task'/3, '$rcall'/2, '$rcall'/4,
%	current_module/1

-module(inline).

collect_inlining_candidates([], _, _, IC1, IC) :- IC = IC1.
collect_inlining_candidates([pdef(N/A, _, _, [t((H :- B), _, _)])|Gs], Init, PD, IC1, IC) :-
    N =\= Init |
    term:functor_parts(H, _, _, Args),
    term:functor_parts(B, BN, BA, _),
    inlineable_operation(BN/BA, PD, F),
    (F == true  ->
        var_args_only(Args, Vars, F2),
        collect_inlining_candidates2(F2, Vars, N/A, B, Gs, Init, PD, IC1, IC)
    ;
        collect_inlining_candidates(Gs, Init, PD, IC1, IC)
    ).
collect_inlining_candidates([_|Gs], Init, PD, IC1, IC) :-
    otherwise |
    collect_inlining_candidates(Gs, Init, PD, IC1, IC).

collect_inlining_candidates2(false, _, _, _, Gs, Init, PD, IC1, IC) :-
    collect_inlining_candidates(Gs, Init, PD, IC1, IC).
collect_inlining_candidates2(true, Vars, NA, B, Gs, Init, PD, IC1, IC) :-
    map:insert(NA, {Vars, B}, IC1, IC2),
    collect_inlining_candidates(Gs, Init, PD, IC2, IC).

inlineable_operation(NA, PD, F) :-
    map:lookup(NA, PD, I),
    (I == []  ->
        lfa:primitive(NA, _, _, T),
        inlineable_operation2(T, NA, F)
    ;
    	 F = true
    ).

inlineable_operation2([], '$asm'/_, F) :- F = true.
inlineable_operation2([], N/A, F) :-
    N =\= '$asm' |
    list:member(N/A, [true/0, foreign_call/1, compute/3, compute/4,
        ('=')/2, unify/3, call/1, call/2, call/3, '$call'/2,
        '$call_with_remote_task'/3, '$rcall'/2, '$rcall'/4,
        current_module/1], F).
inlineable_operation2(_, _, F) :- otherwise | F = true.

% only accept non-forced vars as args
var_args_only([], Vars, F) :- Vars = [], F = true.
var_args_only(['$VAR'(_, I, _)|Args], Vars, F) :-
    integer(I) |
    Vars = [I|Tail],
    var_args_only(Args, Tail, F).
var_args_only(_, _, F) :- otherwise | F = false.

reduce_inline_calls(X, X2, IC, F, P, Ss1, Ss) :-
    term:extract_environment(X, Vars),
    inline_calls(X, X1, Vars, IC, Ss1, _, Ss2),
    (X =\= X1  ->
        debug(F, P, X, X1),
        reduce_inline_calls(X1, X2, IC, F, P, Ss2, Ss)
    ;
        X2 = X1, Ss = Ss2
    ).

debug(true, P, Old, New) :-
    t:comment_prefix(CP),
    send(P, note('~a inline: ~q\n~a inline:  -> ~q', [CP, Old, CP, New])).
debug(false, _, _, _).

inline_calls((X, Y), R, Env1, IC, Ss1, Env, Ss) :-
    inline_calls(X, X2, Env1, IC, Ss1, E2, Ss2),
    inline_calls(Y, Y2, E2, IC, Ss2, Env, Ss),
    R = (X2, Y2).
inline_calls(X1, X, Env1, IC, Ss1, Env, Ss) :-
    otherwise |
    term:functor_parts(X1, N, A, Args),
    map:lookup(N/A, IC, Info),
    (Info == []  ->
        Env = Env1,
        Ss = Ss1,
        X = X1
    ;
        Info = {Vars, Body},
        map_argument_variables(Vars, Args, VarMap),
        substitute_variables(Body, VarMap, Env1, [], X2, _, Env2, Ss2),
        list:append(Ss1, Ss2, Ss3),
        inline_calls(X2, X, Env2, IC, Ss3, Env, Ss)
    ).

map_argument_variables([], _, M) :- M = [].
map_argument_variables([V|Vars], [A|Args], M) :-
    M = [V-A|VarMap],
    map_argument_variables(Vars, Args, VarMap).

substitute_variables(X, M1, E1, Ss1, R, M, E, Ss) :-
    atomic(X) | E = E1, R = X, Ss = Ss1, M = M1.
substitute_variables('$VAR'(_, V, _), Map, E1, Ss1, Arg, M, E, Ss) :-
    integer(V) |
    substitute_variables_var(Map, V, E1, Ss1, Arg, M, E, Ss).
substitute_variables([X|Y], Map, E1, Ss1, Arg, M, E, Ss) :-
    substitute_variables(X, Map, E1, Ss1, X2, M1, E2, Ss2),
    substitute_variables(Y, M1, E2, Ss2, Y2, M, E, Ss),
    Arg = [X2|Y2].
substitute_variables(V, Map, E1, Ss1, Arg, M, E, Ss) :-
    vector(V) |
    vector:elements(V, L),
    substitute_variables(L, Map, E1, Ss1, L2, M, E, Ss),
    vector:new(Arg, L2).
substitute_variables(X, Map, E1, Ss1, X2, M, E, Ss) :-
    otherwise |
    tuple_to_list(X, L),
    substitute_variables(L, Map, E1, Ss1, L2, M, E, Ss),
    list_to_tuple(L2, X2).

substitute_variables_var([], V, E1, Ss1, Arg, M, E, Ss) :-
    term:fresh_variable(E1, V2, E),
    Ss = [V2|Ss1],
    Arg = '$VAR'(0, V2, 0),
    M = [V-Arg].
substitute_variables_var([V-Arg1|Map], V, E1, Ss1, Arg, M, E, Ss) :-
    drop_singleton(Arg1, Ss1, Ss),
    Arg = Arg1, E = E1, M = [V-Arg1|Map].
substitute_variables_var([VA|M1], V, E1, Ss1, Arg, M, E, Ss) :-
    otherwise |
    M = [VA|M2],
    substitute_variables_var(M1, V, E1, Ss1, Arg, M2, E, Ss).

drop_singleton('$VAR'(_, V, _), Ss1, Ss) :-
    integer(V) |
    list:delete(V, Ss1, Ss).
drop_singleton(_, Ss1, Ss) :- otherwise | Ss = Ss1.
